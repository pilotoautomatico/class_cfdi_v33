<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

use Exception;
use DOMDocument;

class Nomina {
	var $StringXML;
	var $xml_base;
	var $version;
	var $tipoNomina;
	var $fechaPago;
	var $fechaInicialPago;
	var $fechaFinalPago;
	var $numDiasPagados;
	var $totalPercepciones;
	var $totalDeducciones;
	var $totalOtrosPagos;

	var $Emisor;
	var $Receptor;
	var $Percepciones = array();
	var $Deducciones = array();
	var $OtrosPagos = array();
	var $Incapacidades = array();
	var $HorasExtra = array();
	var $AccionesOTitulos;
	var $JubilacionPensionRetiro;
	var $SeparacionIndemnizacion;
	var $logger;

	function __construct($StringXML) {
		$this->StringXML = $StringXML;
		$this->logger = new Logger(); //clase para escribir logs
	}
	
	function validar(){
		if($this->StringXML){
			$xml = new DOMDocument('1.0', 'UTF-8');
			libxml_use_internal_errors(true);
			if (!$xml->loadXML($this->StringXML)) {
				$errors = libxml_get_errors();
				$this->logger->write("Validar Nomina(): fallo al cargar el xml de la nomina " . print_r($errors, true));
				throw new Exception("Validar Nomina(): fallo al cargar el xml de la nomina " . print_r($errors, true));
			}
		}else{
			$this->logger->write("Validar Nomina(): No se encontro Nomina para validar ");
			throw new Exception("Validar Nomina(): No se encontro Nomina para validar ");
		}
	}

	function toXML() {
		$this->xml_base = new DOMDocument('1.0', 'UTF-8');
		libxml_use_internal_errors(true);
		if (!$this->xml_base->loadXML($this->StringXML)) {
			$errors = libxml_get_errors();
			$this->logger->write("Nomina(): fallo al cargar el xml de la nomina " . print_r($errors, true));
			throw new Exception("Nomina(): fallo al cargar el xml de la nomina " . print_r($errors, true));
		}
		/*
		$this->xml_base = new DOMDocument('1.0', 'UTF-8');
		//$nomina = $this->xml_base->createElement('cfdi:Nomina');
		//$this->xml_base->appendChild($nomina);
		$nominaXML = $this->xml_base->importNode($xml->documentElement, true);
		$this->xml_base->appendChild($nominaXML);
		*/
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		//$xml = $this->xml_base->getElementsByTagName("nomina12:Nomina")->item(0);
		$xml = $this->xml_base->documentElement;
		return $xml;
	}

	function loadXML()
	{
		if(!$this->StringXML){
			throw new Exception("loadXML Nomina(): No se encontro Nomina para Leer.");
		}
				
		$xml = simplexml_load_string($this->StringXML);		
		libxml_use_internal_errors(true);
		if(!$xml){
			$errors = libxml_get_errors();
			$this->logger->write("validar Nomina(): fallo al cargar el xml de la nomina " . print_r($errors, true));
			throw new Exception("loadXML Nomina(): No se pudo Leer Nomina.");			
		}		
		$ns = $xml->getNamespaces(true);
		
		foreach ($xml->xpath('//nomina12:Nomina') as $nominas){			        	            	
			$nomina_attr = $nominas->attributes();
			
			$this->version = (string)$nomina_attr['Version'];
			$this->tipoNomina = (string)$nomina_attr['TipoNomina'];
			$this->fechaPago = (string)$nomina_attr['FechaPago'];
			$this->fechaInicialPago = (string)$nomina_attr['FechaInicialPago'];
			$this->fechaFinalPago = (string)$nomina_attr['FechaFinalPago'];
			$this->numDiasPagados = (string)$nomina_attr['NumDiasPagados'];
			if(isset($nomina_attr['TotalPercepciones']))
				$this->totalPercepciones = (string)$nomina_attr['TotalPercepciones'];
			if(isset($nomina_attr['TotalDeducciones']))
				$this->totalDeducciones = (string)$nomina_attr['TotalDeducciones'];
			if(isset($nomina_attr['TotalOtrosPagos']))
				$this->totalOtrosPagos = (string)$nomina_attr['TotalOtrosPagos'];
			
			$nomina = $nominas->children($ns["nomina12"]);
				
			//EMISOR
			if(isset($nomina->Emisor)){
				//$this->logger->write("Nomina_emisor(): " . print_r($nomina->Emisor, true));
				$this->Emisor = new NominaEmisor();
				$emisor = $nomina->Emisor->attributes();
	
				if(isset($emisor->Curp))
					$this->Emisor->curp = (string)$emisor->Curp;
				if(isset($emisor->RegistroPatronal))
					$this->Emisor->registroPatronal = (string)$emisor->RegistroPatronal;
				if(isset($emisor->RfcPatronOrigen))
					$this->Emisor->rfcPatronOrigen = (string)$emisor->RfcPatronOrigen;
	
				if(isset($nomina->Emisor->EntidadSNCF)){
					$entidadSNCF = $nomina->Emisor->EntidadSNCF->attributes();
					if(empty($entidadSNCF->OrigenRecurso))
						throw new Exception('Nomina. Origen de recurso se encuentra Vacío.');
					$this->Emisor->entidadSNCF = new EntidadSNCF($entidadSNCF->OrigenRecurso);
				}
			}

			//RECEPTOR
			$receptor = $nomina->Receptor->attributes();
			/*if(empty($receptor->Curp))
				throw new Exception('Nomina. Curp se encuentra vacío.');
			if(empty($receptor->TipoContrato))
				throw new Exception('Nomina. Tipo de contrato se encuentra vacío.');
			if(empty($receptor->TipoRegimen))
				throw new Exception('Nomina. Tipo de regimen se encuentra vacío.');
			if(empty($receptor->NumEmpleado))
				throw new Exception('Nomina. Número de empleado se encuentra vacío.');
			if(empty($receptor->PeriodicidadPago))
				throw new Exception('Nomina. Periodicidad de pago se encuentra vacío.');
			if(empty($receptor->ClaveEntFed))
				throw new Exception('Nomina. Clave de entidad federativa se encuentra vacía.');*/

			$this->Receptor = new NominaReceptor(
				(string)$receptor->Curp,
				(string)$receptor->TipoContrato,
				(string)$receptor->TipoRegimen,
				(string)$receptor->NumEmpleado,
				(string)$receptor->PeriodicidadPago,
				(string)$receptor->ClaveEntFed
			);

			if(isset($receptor->NumSeguridadSocial))
				$this->Receptor->numSeguridadSocial = (string)$receptor->NumSeguridadSocial;
			if(isset($receptor->FechaInicioRelLaboral))
				$this->Receptor->fechaInicioRelLaboral = (string)$receptor->FechaInicioRelLaboral;
			if(isset($receptor->Antigüedad))
				$this->Receptor->antigüedad = (string)$receptor->Antigüedad;
			if(isset($receptor->Sindicalizado))
				$this->Receptor->sindicalizado = (string)$receptor->Sindicalizado;
			if(isset($receptor->TipoJornada))
				$this->Receptor->tipoJornada = (string)$receptor->TipoJornada;
			if(isset($receptor->Departamento))
				$this->Receptor->departamento = (string)$receptor->Departamento;
			if(isset($receptor->Puesto))
				$this->Receptor->puesto = (string)$receptor->Puesto;
			if(isset($receptor->RiesgoPuesto))
				$this->Receptor->riesgoPuesto = (string)$receptor->RiesgoPuesto;
			if(isset($receptor->Banco))
				$this->Receptor->banco = (string)$receptor->Banco;
			if(isset($receptor->CuentaBancaria))
				$this->Receptor->cuentaBancaria = (string)$receptor->CuentaBancaria;
			if(isset($receptor->SalarioBaseCotApor))
				$this->Receptor->salarioBaseCotApor = (string)$receptor->SalarioBaseCotApor;
			if(isset($receptor->SalarioDiarioIntegrado))
				$this->Receptor->salarioDiarioIntegrado = (string)$receptor->SalarioDiarioIntegrado;
			if(isset($receptor->SubContratacion)){
				$subContratacion = $receptor->SubContratacion->attributes();
				if(empty($subContratacion->RfcLabora))
					throw new Exception('Nomina. Rfc se encuentra vacío.');
				if(empty($subContratacion->PorcentajeTiempo))
					throw new Exception('Nomina. Porcentaje de tiempo se encuentra vacío.');

				$this->Receptor->SubContratacion = new SubContratacion(
					(string)$subContratacion->RfcLabora,
					(string)$subContratacion->PorcentajeTiempo
				);
			}

			//PERCEPCIONES
			if(isset($nomina->Percepciones) && !empty($nomina->Percepciones)){
				$percepciones = $nomina->Percepciones->attributes();
				/*if(empty($percepciones->TotalGravado))
					throw new Exception('Nomina. Total gravado se encuentra vacío.');
				if(empty($percepciones->TotalExento))
					throw new Exception('Nomina. Total exento se encuentra vacío.');*/

				$this->Percepciones = new Percepciones(
					(string)$percepciones->TotalGravado,
					(string)$percepciones->TotalExento
				);
				if(isset($percepciones->TotalSueldos))
					$this->Percepciones->totalSueldos = (string)$percepciones->TotalSueldos;
				if(isset($percepciones->TotalSeparacionIndemnizacion))
					$this->Percepciones->totalSeparacionIndemnizacion = (string)$percepciones->TotalSeparacionIndemnizacion;
				if(isset($percepciones->TotalJubilacionPensionRetiro))
					$this->Percepciones->totalJubilacionPensionRetiro = (string)$percepciones->TotalJubilacionPensionRetiro;

				foreach($nomina->Percepciones->children($ns['nomina12']) as $percepcion){
					$percepcion_attr = $percepcion->attributes();

					if($percepcion->getName() == 'Percepcion'){
						$perc = new Percepcion(
							(string)$percepcion_attr->TipoPercepcion,
							(string)$percepcion_attr->Clave,
							(string)$percepcion_attr->Concepto,
							(string)$percepcion_attr->ImporteGravado,
							(string)$percepcion_attr->ImporteExento
						);
						if(isset($percepcion->AccionesOTitulos))
							$perc->accionesOTitulos = (string)$percepcion->AccionesOTitulos;

						if(((string)$percepcion_attr->Clave) == '019'){
							foreach($percepcion->children($ns['nomina12']) as $extra){
								$extra_attr = $extra->attributes();
								$this->HorasExtra[] = new HorasExtra(
									(string)$extra_attr->Dias,
									(string)$extra_attr->TipoHoras,
									(string)$extra_attr->HorasExtra,
									(string)$extra_attr->ImportePagado
								);
							}
						}
						$this->Percepciones->percepciones[] = $perc;
					}elseif($percepcion->getName() == 'JubilacionPensionRetiro'){
						$this->JubilacionPensionRetiro = new JubilacionPensionRetiro(
							(string)$percepcion_attr->IngresoNoAcumulable,
							(string)$percepcion_attr->IngresoAcumulable
						);

						if(isset($percepcion_attr->TotalParcialidad))
							$this->JubilacionPensionRetiro->totalParcialidad = (string)$percepcion_attr->TotalParcialidad;
						if(isset($percepcion_attr->TotalUnaExhibicion))
							$this->JubilacionPensionRetiro->totalUnaExhibicion = (string)$percepcion_attr->TotalUnaExhibicion;
						if(isset($percepcion_attr->MontoDiario))
							$this->JubilacionPensionRetiro->montoDiario = (string)$percepcion_attr->MontoDiario;

					}elseif($percepcion->getName() == 'SeparacionIndemnizacion'){
						$this->SeparacionIndemnizacion = new SeparacionIndemnizacion(
							(string)$percepcion_attr->TotalPagado,
							(string)$percepcion_attr->NumAñosServicio,
							(string)$percepcion_attr->UltimoSueldoMensOrd,
							(string)$percepcion_attr->IngresoAcumulable,
							(string)$percepcion_attr->IngresoNoAcumulable
						);
					}
				}
			}

			//DEDUCCIONES
			if(isset($nomina->Deducciones) && !empty($nomina->Deducciones)){
				$deducciones = $nomina->Deducciones->attributes();

				$this->Deducciones = new Deducciones();
				if(isset($deducciones->TotalOtrasDeducciones))
					$this->Deducciones->totalOtrasDeducciones = (string)$deducciones->TotalOtrasDeducciones;
				if(isset($deducciones->TotalImpuestosRetenidos))
					$this->Deducciones->totalImpuestosRetenidos = (string)$deducciones->TotalImpuestosRetenidos;

				foreach($nomina->Deducciones->children($ns['nomina12']) as $deduccion){
					$deduccion = $deduccion->attributes();

					$this->Deducciones->deducciones[] = new Deduccion(
						(string)$deduccion->TipoDeduccion,
						(string)$deduccion->Clave,
						(string)$deduccion->Concepto,
						(string)$deduccion->Importe
					);
				}
			}

			//OTROS PAGOS
			if(isset($nomina->OtrosPagos) && !empty($nomina->OtrosPagos)){
				foreach($nomina->OtrosPagos->children($ns['nomina12']) as $pago){
					$pago_attr = $pago->attributes();

					$otro_pago = new OtroPago(
						(string)$pago_attr->TipoOtroPago,
						(string)$pago_attr->Clave,
						(string)$pago_attr->Concepto,
						(string)$pago_attr->Importe
					);

					if(((string)$pago_attr->Clave) == '002'){
						$subsidio = $pago->children($ns['nomina12']);
						$subsidio_attr = $subsidio->attributes();
						$otro_pago->subsidioAlEmpleo = new SubsidioAlEmpleo(
							(string)$subsidio_attr->SubsidioCausado
						);
					}
					if(((string)$pago_attr->Clave) == '004'){
						$sal_fav = $pago->children($ns['nomina12']);
						$sal_fav_attr = $sal_fav->attributes();
						$otro_pago->compensacionSaldosAFavor = new CompensacionSaldosAFavor(
							(string)$sal_fav_attr->SaldoAFavor,
							(string)$sal_fav_attr->Año,
							(string)$sal_fav_attr->RemanenteSalFav
						);
					}
					$this->OtrosPagos[] = $otro_pago;
				}
			}

			//INCAPACIDADES
			if(isset($nomina->Incapacidades) && !empty($nomina->Incapacidades)){
				foreach($nomina->Incapacidades->children($ns['nomina12']) as $incapacidad){
					$inc_attr = $incapacidad->attributes();

					$inc_obj = new Incapacidad(
						(string)$inc_attr->DiasIncapacidad,
						(string)$inc_attr->TipoIncapacidad
					);
					if(isset($inc_attr->ImporteMonetario))
						$inc_obj->importeMonetario = (string)$inc_attr->ImporteMonetario;

					$this->Incapacidades[] = $inc_obj;
				}
			}	
		}		
	}
}

class NominaEmisor{
	var $curp;
	var $registroPatronal;
	var $rfcPatronOrigen;
	var $entidadSNCF;
}

class EntidadSNCF {
	var $origenRecurso;
	var $montoRecursoPropio;

	public function __construct($origenRecurso) {
		$this->origenRecurso = $origenRecurso;
	}
}

class NominaReceptor {
	var $curp;
	var $numSeguridadSocial;
	var $fechaInicioRelLaboral;
	var $antigüedad;
	var $tipoContrato;
	var $sindicalizado;
	var $tipoJornada;
	var $tipoRegimen;
	var $numEmpleado;
	var $departamento;
	var $puesto;
	var $riesgoPuesto;
	var $periodicidadPago;
	var $banco;
	var $cuentaBancaria;
	var $salarioBaseCotApor;
	var $salarioDiarioIntegrado;
	var $claveEntFed;
	var $subContratacion;

	public function __construct($curp, $tipoContrato, $tipoRegimen, $numEmpleado, $periodicidadPago, $claveEntFed) {
		$this->curp = $curp;
		$this->tipoContrato = $tipoContrato;
		$this->tipoRegimen = $tipoRegimen;
		$this->numEmpleado = $numEmpleado;
		$this->periodicidadPago = $periodicidadPago;
		$this->claveEntFed = $claveEntFed;
	}
}

class SubContratacion {
	var $rfcLabora;
	var $porcentajeTiempo;

	public function __construct($rfcLabora, $porcentajeTiempo) {
		$this->rfcLabora = $rfcLabora;
		$this->porcentajeTiempo = $porcentajeTiempo;
	}
}

class Percepciones {
	var $totalSueldos;
	var $totalSeparacionIndemnizacion;
	var $totalJubilacionPensionRetiro;
	var $totalGravado;
	var $totalExento;

	var $percepciones = array();

	public function __construct($totalGravado, $totalExento) {
		$this->totalGravado = $totalGravado;
		$this->totalExento = $totalExento;
	}
}

class Percepcion {
	var $tipoPercepcion;
	var $clave;
	var $concepto;
	var $importeGravado;
	var $importeExento;

	public function __construct($tipoPercepcion, $clave, $concepto, $importeGravado, $importeExento) {
		$this->tipoPercepcion = $tipoPercepcion;
		$this->clave = $clave;
		$this->concepto = $concepto;
		$this->importeGravado = $importeGravado;
		$this->importeExento = $importeExento;
	}
}

class AccionesOTitulos {
	var $valorMercado;
	var $precioAlOtorgarse;

	public function __construct($valorMercado, $precioAlOtorgarse) {
		$this->valorMercado = $valorMercado;
		$this->precioAlOtorgarse = $precioAlOtorgarse;
	}
}

class HorasExtra {
	var $dias;
	var $tipoHoras;
	var $horasExtra;
	var $importePagado;

	public function __construct($dias, $tipoHoras, $horasExtra, $importePagado) {
		$this->dias = $dias;
		$this->tipoHoras = $tipoHoras;
		$this->horasExtra = $horasExtra;
		$this->importePagado = $importePagado;
	}
}

class JubilacionPensionRetiro {
	var $totalUnaExhibicion;
	var $totalParcialidad;
	var $montoDiario;
	var $ingresoAcumulable;
	var $ingresoNoAcumulable;

	public function __construct($ingresoNoAcumulable, $ingresoAcumulable) {
		$this->ingresoNoAcumulable = $ingresoNoAcumulable;
		$this->ingresoAcumulable = $ingresoAcumulable;
	}
}

class SeparacionIndemnizacion {
	var $totalPagado;
	var $numAñosServicio;
	var $ultimoSueldoMensOrd;
	var $ingresoAcumulable;
	var $ingresoNoAcumulable;

	public function __construct($totalPagado, $numAñosServicio, $ultimoSueldoMensOrd, $ingresoAcumulable, $ingresoNoAcumulable) {
		$this->totalPagado = $totalPagado;
		$this->numAñosServicio = $numAñosServicio;
		$this->ultimoSueldoMensOrd = $ultimoSueldoMensOrd;
		$this->ingresoAcumulable = $ingresoAcumulable;
		$this->ingresoNoAcumulable = $ingresoNoAcumulable;
	}
}

class Deducciones {
	var $totalOtrasDeducciones;
	var $totalImpuestosRetenidos;
	var $deducciones = array();
}

class Deduccion {
	var $tipoDeduccion;
	var $clave;
	var $concepto;
	var $importe;

	public function __construct($tipoDeduccion, $clave, $concepto, $importe) {
		$this->tipoDeduccion = $tipoDeduccion;
		$this->clave = $clave;
		$this->concepto = $concepto;
		$this->importe = $importe;
	}
}

class OtroPago {
	var $tipoOtroPago;
	var $clave;
	var $concepto;
	var $importe;
	var $subsidioAlEmpleo;
	var $compensacionSaldosAFavor;

	public function __construct($tipoOtroPago, $clave, $concepto, $importe) {
		$this->tipoOtroPago = $tipoOtroPago;
		$this->clave = $clave;
		$this->concepto = $concepto;
		$this->importe = $importe;
	}
}

class SubsidioAlEmpleo {
	var $subsidioCausado;

	public function __construct($subsidioCausado) {
		$this->subsidioCausado = $subsidioCausado;
	}
}

class CompensacionSaldosAFavor {
	var $saldoAFavor;
	var $año;
	var $remanenteSalFav;

	public function __construct($saldoAFavor, $año, $remanenteSalFav) {
		$this->saldoAFavor = $saldoAFavor;
		$this->año = $año;
		$this->remanenteSalFav = $remanenteSalFav;
	}
}

class Incapacidad {
	var $diasIncapacidad;
	var $tipoIncapacidad;
	var $importeMonetario;

	public function __construct($diasIncapacidad, $tipoIncapacidad) {
		$this->diasIncapacidad = $diasIncapacidad;
		$this->tipoIncapacidad = $tipoIncapacidad;
	}
}
?>