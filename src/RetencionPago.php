<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;
Use cfdi\Data\Arrays;

use Exception;
use DOMDocument;

class RetencionPago {
	var $Impuesto;
	var $Importe;
	var $xml_base;
	var $Decimales;
	var $logger;

	function __construct($Impuesto, $TasaOCuota, $Importe, $Decimales) {
		$this->Impuesto = $Impuesto;
		$this->Importe = $Importe;
		$this->TasaOCuota = $TasaOCuota;
		$this->Decimales = $Decimales;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'Impuesto',
			'TasaOCuota',
			"Importe"
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Retencion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Retencion Campo Requerido: ' . $field);
			}
		}
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$retencion = $this->xml_base->createElement("pago10:Retencion");
		$this->xml_base->appendChild($retencion);

		# datos de tralado
		$retencion->SetAttribute('Impuesto', $this->Impuesto);
		$retencion->SetAttribute('Importe', $this->addZeros($this->Importe));
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("pago10:Retencion")->item(0);
		return $xml;
	}

	function addZeros($cantidad = null) {
		return sprintf('%0.' . $this->Decimales . 'f', $cantidad);
	}

	function validateDecimals() {
		$decimalesTotal = strlen(substr(strrchr($this->Importe, "."), 1));
		if ($decimalesTotal > $this->Decimales) {
			throw new Exception("El importe de " . $this->Importe .
			" en la retencion del pago no coincide con el valor de los decimales especificado por la moneda ,valor de decimales: " . $this->Decimales);
		}
	}

	// valido la tazaOcuota aunque no la vaya a poner en el xml por que de ahi valido el importe del impuesto en cuestion
	function validateTax() {
		$valorTasa = null;
		$arrayCatalog = new Arrays();
		$valorTasa = array_search((float) $this->TasaOCuota, $arrayCatalog->arrayTasa[$this->Impuesto][$this->TipoFactor]);

		if (!is_int($valorTasa)) {
			throw new Exception('El valor del campo TasaOCuota : ' . $this->TasaOCuota . ' del traslado no contiene un valor del catalogo de c_TasaOCuota especificado por el SAT.<br>'
			. 'Impuestos 001,002,003 valor introducido :' . $this->Impuesto . '<br>'
			. 'Factores Tasa,Cuota,Exento valor introducido :' . $this->TipoFactor . '<br>');
		}
		if ($this->Impuesto == '002' && $this->TipoFactor == "Taza") {
			if ((float) $this->TasaOCuota <= 0.0000 || (float) $this->TasaOCuota > 0.160000) {
				throw new Exception('El valor de' . $this->TipoFactor . ': ' . $this->TasaOCuota . ' en la retencion No esta dentro del rango permitido 0.000000 a 0.160000 verfique sus datos');
			}
		}

		if ($this->TipoFactor == "Cuota" && $this->Impuesto == "003") {
			if ((float) $this->TasaOCuota <= 0.0000 || (float) $this->TasaOCuota > 43.770000) {
				throw new Exception('El valor de la ' . $this->TipoFactor . ': ' . $this->TasaOCuota . ' en la retencion No esta dentro del rango permitido 0.000000 a 43.770000 verfique sus datos');
			}
		}
		// checar aqui a ver que ondazz
	}
}
?>