<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;
Use cfdi\Data\Arrays;

use Exception;
use DOMDocument;

// esta clase iva a ser ulitlizada para generar los impuestos globales del CFDI , ya que acutalmente los calcula en automatico,
// el nodo de impuestos va cuando se da un anticipo.

class TrasladoPagoV2 {
	var $BaseP;
	var $ImpuestoP;
	var $TipoFactorP;
	var $TasaOCuotaP;
	var $ImporteP; // el importe se calculara
	var $xml_base;
	var $Decimales;
	var $logger;

	function __construct($BaseP, $ImpuestoP, $TipoFactorP, $TasaOCuotaP = null, $ImporteP = null, $Decimales=6) {
		$this->BaseP= $BaseP;
		$this->ImpuestoP = $ImpuestoP;
		$this->TipoFactorP = $TipoFactorP;
		$this->TasaOCuotaP = $TasaOCuotaP;
		$this->ImporteP = $ImporteP;
		$this->Decimales = $Decimales;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'BaseP',
			'ImpuestoP',
			'TipoFactorP'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("TrasladoPagoV2 validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('TrasladoPagoV2 Campo Requerido: ' . $field);
			}
		}
		if($this->TasaOCuotaP<0.000000){
			$this->logger->write('TrasladoPagoV2 TasaOCuotaP ' . $this->TasaOCuotaP . ' debe tener un valor minimo de 0.000000');
			throw new Exception('TrasladoPagoV2 TasaOCuotaP ' . $this->TasaOCuotaP . ' debe tener un valor minimo de 0.000000');
		}

		if(!empty($this->ImporteP)){
			if($this->ImporteP<0){
				$this->logger->write('TrasladoDR ImporteP ' . $this->ImporteP . ' debe tener un valor positivo');
				throw new Exception('TrasladoDR ImporteP ' . $this->ImporteP . ' debe tener un valor positivo');
			}
		}
		// $this->validateDecimals();
		// $this->validateTax();
	}

	// pendiente para ver como va estar el rollo
	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$traslado = $this->xml_base->createElement("pago20:TrasladoP");
		$this->xml_base->appendChild($traslado);

		# datos de tralado
		$traslado->SetAttribute('BaseP', $this->addZeros($this->BaseP));
		$traslado->SetAttribute('ImpuestoP', $this->ImpuestoP);
		$traslado->SetAttribute('TipoFactorP', $this->TipoFactorP);
		if($this->TasaOCuotaP !== null)
			$traslado->SetAttribute('TasaOCuotaP', $this->addZeros($this->TasaOCuotaP));
		if($this->ImporteP !== null)
			$traslado->SetAttribute('ImporteP', $this->addZeros($this->ImporteP));
		return $traslado;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("pago20:TrasladoP")->item(0);
		return $xml;
	}

	function addZeros($cantidad = null) {
		return sprintf('%0.' . $this->Decimales . 'f', $cantidad);
	}

	function validateDecimals() {
		$decimalesTotal = strlen(substr(strrchr($this->ImporteP, "."), 1));
		if ($decimalesTotal > $this->Decimales) {
			throw new Exception("El importe de " . $this->ImporteP .
			" en el traslado del pago no coincide con el valor de los decimales especificado por la moneda ,valor de decimales: " . $this->Decimales);
		}
	}

	function validateTax() {
		$valorTasa = null;
		$arrayCatalog = new Arrays();

		if ($this->TipoFactorP == "Cuota" && $this->ImpuestoP == "003") {
			if ((float) $this->TasaOCuotaP <= 0.0000 || (float) $this->TasaOCuotaP > 43.770000) {
				throw new Exception('El valor de la ' . $this->TipoFactorP . 'que corresponde al impuesto 003 (ISR) : ' . $this->TasaOCuotaP . ' en la retencion No esta dentro del rango permitido 0.000000 a 43.770000 verfique sus datos');
			}
		}

		if ($this->TipoFactorP != 'Exento') { //solo valida cuando no sea exento
			$valorTasa = array_search((float) $this->TasaOCuotaP, $arrayCatalog->arrayTasa[$this->ImpuestoP][$this->TipoFactorP]);
			if (!is_int($valorTasa)) {
				throw new Exception('El valor del campo TasaOCuotaP : ' . $this->TasaOCuotaP . ' del traslado no contiene un valor del catalogo de c_TasaOCuota especificado por el SAT.<br>'
				. 'Impuestos 001,002,003 valor introducido :' . $this->ImpuestoP . '<br>'
				. 'Factores Tasa,Cuota,Exento valor introducido :' . $this->TipoFactorP . '<br>');
			}
		}

		if ($this->ImpuestoP == '001') {
			throw new Exception('El impuesto 001 que corresponde al ISR no debe declararse en un traslado');
		}
	}
}
?>