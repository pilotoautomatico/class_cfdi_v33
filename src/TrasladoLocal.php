<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

use Exception;
use DOMDocument;

class TrasladoLocal {
	//normales
	var $ImpLocTrasladado;
	var $TasadeTraslado;
	var $Importe;
	var $Decimales;
	var $logger;

	function __construct($ImpLocTrasladado, $TasadeTraslado, $Importe, $Decimales = 2) {
		$this->ImpLocTrasladado = $ImpLocTrasladado;
		$this->TasadeTraslado = $TasadeTraslado;
		$this->Importe = $Importe;
		$this->Decimales = $Decimales;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		# valida campos requeridos de impuestos locales
		$required = array(
			'ImpLocTrasladado',
			'TasadeTraslado',
			'Importe'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("TrasladoLocal validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('TrasladoLocal Campo Requerido: ' . $field);
			}
		}

		# valida decimales permitidos		
		$decimales = array(
			'TasadeTraslado',
			'Importe'
		);
		/*foreach ($decimales as $field) {
			$this->validateDecimals($field);	
		}	*/
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$traslado = $this->xml_base->createElement("implocal:TrasladosLocales");
		$this->xml_base->appendChild($traslado);

		# datos de traslado
		$traslado->SetAttribute('ImpLocTrasladado', $this->ImpLocTrasladado);
		$traslado->SetAttribute('TasadeTraslado', $this->addZeros($this->TasadeTraslado, 2));
		$traslado->SetAttribute('Importe', $this->addZeros($this->Importe, 2)); //fijo solo permitido 2
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("implocal:TrasladosLocales")->item(0);
		return $xml;
	}

	function validateDecimals($field) {
		$decimales = strlen(substr(strrchr($this->$field, "."), 1));
		if ($decimales > $this->Decimales) {
			throw new Exception("El valor de " . $this->$field . " en TrasladoLocal excede los decimales permitidos en impuestos locales: " . $this->Decimales);
		}
	}
	
	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return  sprintf('%0.'.$dec.'f',$cantidad);
	}
}
?>