<?php
    require '../../vendor/autoload.php';
    Use cfdi\ComprobanteV4;
    try{
        $objDateTime = new DateTime("now", new DateTimeZone('America/Mexico_City'));
        $key = getcwd() . "/keys/PAU1207301E5.key.pem";
	    $cer = getcwd() . "/keys/PAU1207301E5.cer.pem";
        $key = getcwd() . "/keys/00001000000505422144.key.pem";
	    $cer = getcwd() . "/keys/00001000000505422144.cer.pem";
        $cfdi= new ComprobanteV4();
        $cfdi->addKeys($cer, $key);
        /* '00001000000401148681' */
        $cfdi->addGenerales('00001000000505422144', 0, 'XXX', 0, 'P', null, null, '64000', null, 'A', '11', "", null, null,'4.0', "", $objDateTime->format('Y-m-d\TH:i:s'),null,"01");
        $cfdi->addEmisor('PAU1207301E5', 'PILOTO AUTOMATICO', '601');
        $cfdi->addReceptor('PAU1207301E5', 'P01', 'PILOTO AUTOMATICO',"64000",null,null,'601');
        $cfdi->addConcepto("84111506","Pago",1,0,null,'ACT',null,null,'01');
        $pagos=$cfdi->addPagos('2.0');
        $pago=$pagos->addPago($objDateTime->format('Y-m-d\TH:i:s'),'01','MXN',200,'1','12');
        $pago->addDoctoRelacionado("a9543552-167e-4b0c-94e5-b86024dc10f4", "MXN",'1',200,'FAC',"0000032",1,200,200,0,'01','MXN');
        $pagos->addTotales(null,null,null,null,null,null,null,null,null,null,200);

	    $cfdi->addSellos();

        $cfdi->validar();

        $cfdi->validateXSD();
        $cfdi->toXML();
	    $cfdi->toSaveXML();

	    error_log(date("Y-m-d H:i:s") . " : CREATE CFDI GLOBAL: ".print_r($cfdi, true)." \n", 3, "debug.log");
        
        error_log(date("Y-m-d H:i:s") . " : LOAD CFDI GLOBAL: ".print_r($cfdi, true)." \n", 3, "debug.log");

	    print_r($cfdi);


        // $cfdi->addKeys($cer, $key);
    }catch (Exception $e) {
        echo 'Error al generar el CFDI: ', $e->getMessage(), "\n";
        error_log(date("Y-m-d H:i:s") . " : Error al generar el CFDI: " . print_r($e->getMessage(), true) . "\n", 3, "debug.log");
    }
?>