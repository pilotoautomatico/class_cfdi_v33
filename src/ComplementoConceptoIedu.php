<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

Use Exception;
use DOMDocument;

class ComplementoConceptoIedu {
	//normales
	var $version;
	var $nodo_name = 'instEducativas';
	var $nombreAlumno;
	var $CURP;
	var $nivelEducativo;
	var $autRVOE;
	var $rfcPago=null;
	var $logger;
	
	function __construct($nombreAlumno, $CURP, $nivelEducativo, $autRVOE, $rfcPago=null) {
		$this->version = '1.0';
		$this->nombreAlumno = $nombreAlumno;
		$this->CURP = $CURP;
		$this->nivelEducativo = $nivelEducativo;
		$this->autRVOE = $autRVOE;
		$this->rfcPago = $rfcPago;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		# valida campos requeridos de concepto
		$required = array(
			'version',
			'nombreAlumno',
			'CURP',
			'nivelEducativo',
			'autRVOE'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("ComplementoConceptoIedu validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('ComplementoConceptoIedu Campo Requerido: ' . $field);
			}
		}
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$iedu = $this->xml_base->createElement("iedu:instEducativas");
		$this->xml_base->appendChild($iedu);

		# datos de complementoConceptoIedu
		$iedu->SetAttribute('version', $this->version);
		$iedu->SetAttribute('nombreAlumno', $this->nombreAlumno);
		$iedu->SetAttribute('CURP', $this->CURP);
		$iedu->SetAttribute('nivelEducativo', $this->nivelEducativo);
		$iedu->SetAttribute('autRVOE', $this->autRVOE);
		if ($this->rfcPago)
			$iedu->SetAttribute('rfcPago', $this->rfcPago);
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("iedu:instEducativas")->item(0);
		return $xml;
	}
}
?>