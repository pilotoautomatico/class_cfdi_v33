<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv40 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;
Use cfdi\Data\Arrays;

use Exception;
use DOMDocument;

class RetencionDR {
    var $BaseDR;
	var $ImpuestoDR;
    var $TipoFactorDR;
	var $ImporteDR;
    var $TasaOCuotaDR;
	var $xml_base;
	var $Decimales;
	var $logger;

	function __construct($BaseDR, $ImpuestoDR, $TipoFactorDR, $TasaOCuotaDR, $ImporteDR, $Decimales=6) {
		$this->BaseDR = round($BaseDR,6);
        $this->ImpuestoDR = $ImpuestoDR;
        $this->TipoFactorDR = $TipoFactorDR;
		$this->ImporteDR = $ImporteDR;
		$this->TasaOCuotaDR = round($TasaOCuotaDR,6);;
		$this->Decimales = $Decimales;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
            'BaseDR',
			'ImpuestoDR',
            'TipoFactorDR',
			'TasaOCuotaDR',
			"ImporteDR"
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("RetencionDR validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('RetencionDR Campo Requerido: ' . $field);
			}
		}
        if($this->BaseDR < 0.000001){ //valor minimo
            $this->logger->write('RetencionDR BaseDR ' . $this->BaseDR . ' debe tener un valor minimo de 0.000001');
            throw new Exception('RetencionDR BaseDR ' . $this->BaseDR . ' debe tener un valor minimo de 0.000001');
        }

        if($this->TasaOCuotaDR < 0.000000){ //valor minimo
            $this->logger->write('RetencionDR TasaOCuotaDR ' . $this->TasaOCuotaDR . ' debe tener un valor minimo de 0.000000');
            throw new Exception('RetencionDR TasaOCuotaDR ' . $this->TasaOCuotaDR . ' debe tener un valor minimo de 0.000000');
        }

        if($this->ImporteDR<0){
            $this->logger->write('RetencionDR ImporteDR ' . $this->ImporteDR . ' debe ser un valor positivo');
            throw new Exception('RetencionDR ImporteDR ' . $this->ImporteDR . ' debe ser un valor positivo');
        }
        
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$retencion = $this->xml_base->createElement("pago20:RetencionDR");
		$this->xml_base->appendChild($retencion);

		# datos de tralado
		$retencion->SetAttribute('BaseDR', $this->addZeros($this->BaseDR));
        $retencion->SetAttribute('ImpuestoDR', $this->ImpuestoDR);
        $retencion->SetAttribute('TipoFactorDR', $this->TipoFactorDR);
		$retencion->SetAttribute('TasaOCuotaDR', $this->addZeros($this->TasaOCuotaDR));
        $retencion->SetAttribute('ImporteDR', $this->addZeros($this->ImporteDR));
	
        return $retencion;
    }

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("pago20:RetencionDR")->item(0);
		return $xml;
	}

	function addZeros($cantidad = null) {
		return sprintf('%0.' . $this->Decimales . 'f', $cantidad);
	}

	function validateDecimals() {
		$decimalesTotal = strlen(substr(strrchr($this->ImporteDR, "."), 1));
		if ($decimalesTotal > $this->Decimales) {
			throw new Exception("El importe de " . $this->ImporteDR .
			" en la retencion del pago no coincide con el valor de los decimales especificado por la moneda ,valor de decimales: " . $this->Decimales);
		}
	}

	// valido la tazaOcuota aunque no la vaya a poner en el xml por que de ahi valido el importe del impuestoDR en cuestion
	function validateTax() {
		$valorTasa = null;
		$arrayCatalog = new Arrays();
		$valorTasa = array_search((float) $this->TasaOCuotaDR, $arrayCatalog->arrayTasa[$this->ImpuestoDR][$this->TipoFactor]);

		if (!is_int($valorTasa)) {
			throw new Exception('El valor del campo TasaOCuotaDR : ' . $this->TasaOCuotaDR . ' del traslado no contiene un valor del catalogo de c_TasaOCuotaDR especificado por el SAT.<br>'
			. 'ImpuestoDRs 001,002,003 valor introducido :' . $this->ImpuestoDR . '<br>'
			. 'Factores Tasa,Cuota,Exento valor introducido :' . $this->TipoFactor . '<br>');
		}
		if ($this->ImpuestoDR == '002' && $this->TipoFactor == "Taza") {
			if ((float) $this->TasaOCuotaDR <= 0.0000 || (float) $this->TasaOCuotaDR > 0.160000) {
				throw new Exception('El valor de' . $this->TipoFactor . ': ' . $this->TasaOCuotaDR . ' en la retencion No esta dentro del rango permitido 0.000000 a 0.160000 verfique sus datos');
			}
		}

		if ($this->TipoFactor == "Cuota" && $this->ImpuestoDR == "003") {
			if ((float) $this->TasaOCuotaDR <= 0.0000 || (float) $this->TasaOCuotaDR > 43.770000) {
				throw new Exception('El valor de la ' . $this->TipoFactor . ': ' . $this->TasaOCuotaDR . ' en la retencion No esta dentro del rango permitido 0.000000 a 43.770000 verfique sus datos');
			}
		}
		// checar aqui a ver que ondazz
	}
}
?>