<?php
    require '../../vendor/autoload.php';
    Use cfdi\ReadComprobante;
    try{
        $read= new ReadComprobante();
        //STRING XML
        // $xml=<<<XML
        //     <cfdi:Comprobante xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd" Version="3.3" Fecha="2021-12-08T05:10:06" Sello="WmVhG7zMfUYYU/8GFSpwxn8WjunPURTYu5TySXeUV3eGfZ4yuUQBlwS3PQon9B6F9iH5snsPx0eDB++SlwUPTi2b7bo9fUf3yf8PemgNRzDO4mHCNqW56v35b5tltcW5RrKeVm6AcCCsdPzKyjpOs3v4t5lZD5JWtBvp840q9GtLomC98+j9W8wA0TryRKVPhM/JMKHvWF/95cEAgl+HKeAT0ibCDwdHmzcf3n8t53FxKfR/J2/L83KopbUGE5nJ4VdtVXbPWzdUbjL3EsAr4QLBoADWGyVEKQJD8zdgexn90T05xAvAPO67bD2PP+Xdw4sCbnuE6VmXlTA6syk0Lg==" NoCertificado="00001000000505422144" Certificado="MIIF+TCCA+GgAwIBAgIUMDAwMDEwMDAwMDA1MDU0MjIxNDQwDQYJKoZIhvcNAQELBQAwggGEMSAwHgYDVQQDDBdBVVRPUklEQUQgQ0VSVElGSUNBRE9SQTEuMCwGA1UECgwlU0VSVklDSU8gREUgQURNSU5JU1RSQUNJT04gVFJJQlVUQVJJQTEaMBgGA1UECwwRU0FULUlFUyBBdXRob3JpdHkxKjAoBgkqhkiG9w0BCQEWG2NvbnRhY3RvLnRlY25pY29Ac2F0LmdvYi5teDEmMCQGA1UECQwdQVYuIEhJREFMR08gNzcsIENPTC4gR1VFUlJFUk8xDjAMBgNVBBEMBTA2MzAwMQswCQYDVQQGEwJNWDEZMBcGA1UECAwQQ0lVREFEIERFIE1FWElDTzETMBEGA1UEBwwKQ1VBVUhURU1PQzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMVwwWgYJKoZIhvcNAQkCE01yZXNwb25zYWJsZTogQURNSU5JU1RSQUNJT04gQ0VOVFJBTCBERSBTRVJWSUNJT1MgVFJJQlVUQVJJT1MgQUwgQ09OVFJJQlVZRU5URTAeFw0yMDEwMTkxNTE3NTlaFw0yNDEwMTkxNTE3NTlaMIHHMSMwIQYDVQQDExpQSUxPVE8gQVVUT01BVElDTyBTQSBERSBDVjEjMCEGA1UEKRMaUElMT1RPIEFVVE9NQVRJQ08gU0EgREUgQ1YxIzAhBgNVBAoTGlBJTE9UTyBBVVRPTUFUSUNPIFNBIERFIENWMSUwIwYDVQQtExxQQVUxMjA3MzAxRTUgLyBGVUdINzAxMTMwR0E2MR4wHAYDVQQFExUgLyBGVUdINzAxMTMwSE5MTkpNMDExDzANBgNVBAsTBk1hdHJpejCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKIkKEmtg1XN1l1rPSuoBXxiLYc20sYOwCsVfBfaaVFjwdE64RtfxRl4sHmTMFwS+pQDLVOOr9pV7ILl6U3CIHjLoi8kunQn8+f7MTsCLlJYcPIidnQj7BN2hCTtI13mrCVOt9O1erKiEvqjpJa7hOLD5b79RYmgSPWvIbqmGrhYGXQj6MSugJYjPXuZmRts7+5yGREZGfHqMfDmklCJEJUhHn+hJ6/ZPvK2MnPSyIXVAV8Y2O8peNW41TM7L70LV69Lx17ZrDpTPiExh0hYr1vVsmTGRTiiTnhcok+FuHWQuDTWVoe1ApuAMZ/sOryV+pwGeuXkHGG1A7XKNgL5UFUCAwEAAaMdMBswDAYDVR0TAQH/BAIwADALBgNVHQ8EBAMCBsAwDQYJKoZIhvcNAQELBQADggIBALXRTxaewotNNh5zcJsGOrYyentYy+CMtyI0N0ztVKj+DSW1K92DA6VBeAO8PenGpETY6JzkFPpHKKTC+OrnCiYOVv9nujIuwTljoSXOYF+G+g5ubrJ0OFogNvnETArzIKnOEG9WvhQ3rZVFAL5ZhPQmLevZqK9sqT4D9d2OvjVgdBIO5LVqbdw/RsmkCSywidM+nwqYL9rd/P2rNz3l5zmxbM0uN/Bak2cUf1QJMcRCdBPnMirO84FpRoeTb+rUP0IOWG1m+/HsNhq1aHF5JqK1azKN3iQK5HlgXeaf2uYbFZL+1q2mDbSjs771qHAkK9tF0cpdmDVUn2vKp0gCq02SLIbN4qXt9t7RjkG1CUdzGu4rER2bann11pInjWijfQsc5nkFuZ336s2l8tmPB6Gqg3rQRVHoQkSqkHeBTcin1Lk9qbC/ZdZmgKctweMhbYjirHFmWyfKDSjcQlymeR+byBBtAq1FRG2a+OFbUI55+0leyB2wl0GAskk74em7lgZ9duhijCmUJpdh6Y898l3u2xAX/qhEmzs0m2EOYdlOZD1T2XIyWfbUYr7RMXfFjyLlG0U+4MJA8HpMYLSqgkMo1qrlta5MMPgZXqHE2HObFK5lyXVdwfz3xEi/GXGGiaZZKX+Ut5DyOtQGO2J2FVgZ8aA8j62LaRu+U0px3pPM" SubTotal="1500.00" Moneda="MXN" Total="1740.00" TipoDeComprobante="I" LugarExpedicion="64000" Serie="B" Folio="6073" FormaPago="06" CondicionesDePago="En 10 Días" TipoCambio="1" MetodoPago="PUE">
        //         <cfdi:Emisor Rfc="PAU1207301E5" Nombre="PILOTO AUTOMATICO SA DE CV" RegimenFiscal="601"/>
        //         <cfdi:Receptor Rfc="SOBS780823RQ7" Nombre="SUSANA SORIANO BASTIDA" UsoCFDI="G03"/>
        //         <cfdi:Conceptos>
        //             <cfdi:Concepto ClaveProdServ="81112500" NoIdentificacion="ERP" Cantidad="1.00" ClaveUnidad="E48" Unidad="srv" Descripcion="Licencia SaaSMexico ERP mensualidad Diciembre 5 usuarios&#9; " ValorUnitario="1500.000000" Importe="1500.00">
        //                 <cfdi:Impuestos>
        //                     <cfdi:Traslados>
        //                         <cfdi:Traslado Base="1500" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="240.000000"/>
        //                     </cfdi:Traslados>
        //                 </cfdi:Impuestos>
        //             </cfdi:Concepto>
        //         </cfdi:Conceptos>
        //         <cfdi:Impuestos TotalImpuestosTrasladados="240.00">
        //             <cfdi:Traslados>
        //                 <cfdi:Traslado Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="240.00"/>
        //             </cfdi:Traslados>
        //         </cfdi:Impuestos>
        //         <cfdi:Complemento>
        //             <tfd:TimbreFiscalDigital xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd" Version="1.1" UUID="efc68f62-67c9-408a-bd11-59b0904b1c92" FechaTimbrado="2021-12-08T05:11:53" RfcProvCertif="LSO1306189R5" SelloCFD="WmVhG7zMfUYYU/8GFSpwxn8WjunPURTYu5TySXeUV3eGfZ4yuUQBlwS3PQon9B6F9iH5snsPx0eDB++SlwUPTi2b7bo9fUf3yf8PemgNRzDO4mHCNqW56v35b5tltcW5RrKeVm6AcCCsdPzKyjpOs3v4t5lZD5JWtBvp840q9GtLomC98+j9W8wA0TryRKVPhM/JMKHvWF/95cEAgl+HKeAT0ibCDwdHmzcf3n8t53FxKfR/J2/L83KopbUGE5nJ4VdtVXbPWzdUbjL3EsAr4QLBoADWGyVEKQJD8zdgexn90T05xAvAPO67bD2PP+Xdw4sCbnuE6VmXlTA6syk0Lg==" NoCertificadoSAT="00001000000509846663" SelloSAT="cANdiY6oXHzbYe1Ga9R5i7RuIFPvJczSafsedLGrQwqROYuQP2RjWcyoRLuCnU5hZNRusY+09a+SuQvhMcO23Ljgx6PKG5kUPtlHXLyqNMYNieUOJ7aIRWl+eI5AaUHt5P9f0sSBRhwbbeD9+ADrSrTPSw5pX9k6AyFOO0QV6x6eoHif5KHW1VNhdZTdPeolo5uLnpfS9lqxwM9MGZlE5MuypF1G44hcmI76gbkQg5dxkio1m+Briy+aOP0yDfgMAaV6IQ4VcxHi8/bspq5ULt7cfwfecn7feLvv5/J6AObWQjZHISPhOI/n9Z2MSfgz9H71FnV5DA2Jdd/HdLfvDw=="/>
        //         </cfdi:Complemento>
        //     </cfdi:Comprobante>
        // XML;

        //ARCHIVO XML
        $xml='CfdiV40.xml';
        $cfdi=$read->loadXML($xml);
        error_log(date("Y-m-d H:i:s") . " : LOAD COMPROBANTE: ".print_r($cfdi,true)." \n", 3, "debug.log");
	    print_r('is_object -> '. is_object($cfdi['comprobante']) . "\n");
        print_r($cfdi['comprobante']);
        print_r($cfdi['version']);

    }catch (Exception $e) {
        echo 'Error al generar el CFDI: ', $e->getMessage(), "\n";
        error_log(date("Y-m-d H:i:s") . " : Error COMPROBANTE : " . print_r($e->getMessage(), true) . "\n", 3, "debug.log");
    }
   
?>