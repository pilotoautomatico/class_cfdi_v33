<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

Use Exception;
use DOMDocument;

class ComplementoConceptoTerceros {
	//normales
	var $version;
	var $nodo_name = 'PorCuentadeTerceros';
	var $rfc;
	var $nombre=null;
	var $InformacionFiscalTercero; //no implementada
	var $InformacionAduanera;
	var $Parte = array();
	var $CuentaPredial; //no implementada
	var $Impuestos;
	var $xml_base;
	var $logger;
	
	function __construct($rfc, $nombre=null) {
		$this->version = '1.1';
		$this->rfc = $rfc;
		$this->nombre = $nombre;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		# valida campos requeridos
		$required = array(
			'version',
			'rfc'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("ComplementoConceptoTerceros validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('ComplementoConceptoTerceros Campo Requerido: ' . $field);
			}
		}
		# validar InformacionFiscalTercero
		if($this->InformacionFiscalTercero)
			$this->InformacionFiscalTercero->validar();

		# validar InformacionAduanera
		if($this->InformacionAduanera)
			$this->InformacionAduanera->validar();

		# validar Parte
		if($this->Parte && !empty($this->Parte)){
			foreach($this->Parte as $parte){
				$parte->validar();
			}
		}

		# validar CuentaPredial

		# validar Impuestos
		if(!$this->Impuestos)
			throw new Exception('ComplementoConceptoTerceros Impuestos No definidos.');
		$this->Impuestos->validar();
	}

	function toXML() {
		# antes de crear xml validar datos
		$this->validar();

		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$terceros = $this->xml_base->createElement("terceros:PorCuentadeTerceros");
		$this->xml_base->appendChild($terceros);

		# datos de complementoConceptoTerceros
		$terceros->SetAttribute('version', $this->version);
		$terceros->SetAttribute('rfc', $this->rfc);
		if ($this->nombre)
			$terceros->SetAttribute('nombre', $this->nombre);

		# nodo InformacionFiscalTercero
		if($this->InformacionFiscalTercero){
			$this->InformacionFiscalTercero->toXML();
			$ift_xml = $this->xml_base->importNode($this->InformacionFiscalTercero->importXML(), true);
			$terceros->appendChild($ift_xml);
		}
		

		# nodo InformacionAduanera
		if($this->InformacionAduanera){
			$this->InformacionAduanera->toXML();
			$ia_xml = $this->xml_base->importNode($this->InformacionAduanera->importXML(), true);
			$terceros->appendChild($ia_xml);
		}

		# nodo Parte
		if($this->Parte && !empty($this->Parte)){
			foreach($this->Parte as $parte){
				$parte->toXML();
				$parte_xml = $this->xml_base->importNode($parte->importXML(), true);
				$terceros->appendChild($parte_xml);
			}
		}

		# nodo CuentaPredial

		# nodo Impuestos
		$this->Impuestos->toXML();
		$impt_xml = $this->xml_base->importNode($this->Impuestos->importXML(), true);
		$terceros->appendChild($impt_xml);
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("terceros:PorCuentadeTerceros")->item(0);
		return $xml;
	}

	function addInformacionAduanera($numero, $fecha, $aduana=null)
	{
		$ia = new InformacionAduanera($numero, $fecha, $aduana);
		$this->InformacionAduanera = $ia;
		return $ia;
	}

	function addInformacionFiscalTercero($calle, $noInterior, $noExterior, $colonia, $localidad, $referencia, 
		$municipio, $estado, $pais, $codigoPostal)
	{
		$ift = new InformacionFiscalTercero($calle, $noInterior, $noExterior, $colonia, $localidad, $referencia, 
		$municipio, $estado, $pais, $codigoPostal);
		$this->InformacionAduanera = $ift;
		return $ift;
	}

	function addParte($cantidad, $descripcion, $unidad=null, $noIdentificacion=null, $valorUnitario=null, $importe=null)
	{
		$parte = new Parte($cantidad, $descripcion, $unidad, $noIdentificacion, $valorUnitario, $importe);
		$this->Parte[] = $parte;
		return $parte;
	}

	function addImpuestos(){
		$impto = new Impuestos();
		$this->Impuestos = $impto;
		return $impto;
	}
}

class InformacionAduanera {
	var $numero;
	var $fecha;
	var $aduana;
	var $xml_base;
	var $logger;
	
	function __construct($numero, $fecha, $aduana=null) {
		$this->numero = $numero;
		$this->fecha = $fecha;
		$this->aduana = $aduana;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		# valida campos requeridos
		$required = array(
			'numero',
			'fecha'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("ComplementoConceptoTerceros->InformacionAduanera validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('ComplementoConceptoTerceros->InformacionAduanera Campo Requerido: ' . $field);
			}
		}
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$nodo = $this->xml_base->createElement("terceros:InformacionAduanera");
		$this->xml_base->appendChild($nodo);

		# datos de InformacionAduanera
		$nodo->SetAttribute('numero', $this->numero);
		$nodo->SetAttribute('fecha', $this->fecha);
		if ($this->aduana)
			$nodo->SetAttribute('aduana', $this->aduana);
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("terceros:InformacionAduanera")->item(0);
		return $xml;
	}

}

class InformacionFiscalTercero {
	var $calle;
	var $noInterior;
	var $noExterior;
	var $colonia;
	var $localidad;
	var $referencia;
	var $municipio;
	var $estado;
	var $pais;
	var $codigoPostal;
	
	function __construct($calle, $noInterior, $noExterior, $colonia, $localidad, $referencia, 
						$municipio, $estado, $pais, $codigoPostal) {
		$this->calle = $calle;
		$this->noInterior = $noInterior;
		$this->noExterior = $noExterior;
		$this->colonia = $colonia;
		$this->localidad = $localidad;
		$this->referencia = $referencia;
		$this->municipio = $municipio;
		$this->estado = $estado;
		$this->pais = $pais;
		$this->codigoPostal = $codigoPostal;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		# valida campos requeridos
		$required = array(
			'calle',
			'municipio',
			'estado',
			'pais',
			'codigoPostal'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("ComplementoConceptoTerceros->InformacionFiscalTercero validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('ComplementoConceptoTerceros->InformacionFiscalTercero Campo Requerido: ' . $field);
			}
		}
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$nodo = $this->xml_base->createElement("terceros:InformacionFiscalTercero");
		$this->xml_base->appendChild($nodo);

		# datos de InformacionFiscalTercero
		$nodo->SetAttribute('codigoPostal', $this->codigoPostal);
		$nodo->SetAttribute('pais', $this->pais);
		$nodo->SetAttribute('estado', $this->estado);
		$nodo->SetAttribute('municipio', $this->municipio);
		if ($this->referencia)
			$nodo->SetAttribute('referencia', $this->referencia);
		if ($this->localidad)
			$nodo->SetAttribute('localidad', $this->localidad);
		if ($this->colonia)
			$nodo->SetAttribute('colonia', $this->colonia);
		if ($this->noInterior)
			$nodo->SetAttribute('noInterior', $this->noInterior);
		if ($this->noExterior)
			$nodo->SetAttribute('noExterior', $this->noExterior);
		$nodo->SetAttribute('calle', $this->calle);
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("terceros:InformacionFiscalTercero")->item(0);
		return $xml;
	}

}

class Parte {
	var $cantidad;
	var $descripcion;
	var $unidad;
	var $noIdentificacion;
	var $valorUnitario;
	var $importe;
	var $InformacionAduanera = array();
	var $xml_base;
	var $logger;
	
	function __construct($cantidad, $descripcion, $unidad=null, $noIdentificacion=null, $valorUnitario=null, $importe=null) {
		$this->cantidad = $cantidad;
		$this->descripcion = $descripcion;
		$this->unidad = $unidad;
		$this->noIdentificacion = $noIdentificacion;
		$this->valorUnitario = $valorUnitario;
		$this->importe = $importe;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		# valida campos requeridos
		$required = array(
			'cantidad',
			'descripcion'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("ComplementoConceptoTerceros->Parte validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('ComplementoConceptoTerceros->Parte Campo Requerido: ' . $field);
			}
		}

		# validar InformacionAduanera
		if($this->InformacionAduanera && !empty($this->InformacionAduanera)){
			foreach ($this->InformacionAduanera as $key => $ia) {
				$ia->validar();
			}
			//$this->InformacionAduanera->validar();
		}
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$nodo = $this->xml_base->createElement("terceros:Parte");
		$this->xml_base->appendChild($nodo);

		# datos de Parte
		$nodo->SetAttribute('cantidad', $this->cantidad);
		$nodo->SetAttribute('descripcion', $this->descripcion);
		if ($this->unidad)
			$nodo->SetAttribute('unidad', $this->unidad);
		if ($this->noIdentificacion)
			$nodo->SetAttribute('noIdentificacion', $this->noIdentificacion);
		if ($this->valorUnitario)
			$nodo->SetAttribute('valorUnitario', $this->valorUnitario);
		if ($this->importe)
			$nodo->SetAttribute('importe', $this->importe);

		# nodo InformacionAduanera
		if($this->InformacionAduanera && !empty($this->InformacionAduanera)){
			foreach ($this->InformacionAduanera as $key => $ia) {
				$ia->toXML();
				$ia_xml = $this->xml_base->importNode($ia->importXML(), true);
				$nodo->appendChild($ia_xml);
			}
		}
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("terceros:Parte")->item(0);
		return $xml;
	}

	function addInformacionAduanera($numero, $fecha, $aduana=null)
	{
		$ia = new InformacionAduanera($numero, $fecha, $aduana);
		$this->InformacionAduanera[] = $ia;
		return $ia;
	}
}

class Impuestos {
	var $Retenciones = array();
	var $Traslados = array();
	var $xml_base;
	var $logger;
	
	function __construct() {
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		#valida retenciones
		foreach ($this->Retenciones as $retencion) {
			$retencion->validar();
		}
		#valida traslados
		foreach ($this->Traslados as $traslado) {
			$traslado->validar();
		}
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$impuestos = $this->xml_base->createElement("terceros:Impuestos");
		$this->xml_base->appendChild($impuestos);

		# retenciones
		if (!empty($this->Retenciones)) {
			$retenciones = $this->xml_base->createElement("terceros:Retenciones");
			$impuestos->appendChild($retenciones);
			foreach ($this->Retenciones as $key => $retencion) {
				$retencion->toXML();
				$retencion_xml = $this->xml_base->importNode($retencion->importXML(), true);
				$retenciones->appendChild($retencion_xml);
			}
		}

		# traslados
		if (!empty($this->Traslados)) {
			$traslados = $this->xml_base->createElement("terceros:Traslados");
			$impuestos->appendChild($traslados);
			foreach ($this->Traslados as $key => $traslado) {
				$traslado->toXML();
				$traslado_xml = $this->xml_base->importNode($traslado->importXML(), true);
				$traslados->appendChild($traslado_xml);
			}
		}
	
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("terceros:Impuestos")->item(0);
		return $xml;
	}

	function addRetencion($impuesto, $importe) {
		$retencion = new Retencion(
				$impuesto, $importe
		);
		$this->Retenciones[] = $retencion;
		return $retencion;
	}

	function addTraslado($impuesto, $tasa, $importe) {
		$traslado = new Traslado(
				$impuesto, $tasa, $importe
		);
		$this->Traslados[] = $traslado;
		return $traslado;
	}
}

class Retencion {
	var $impuesto;
	var $importe;
	var $xml_base;
	var $logger;
	
	function __construct($impuesto, $importe) {
		$this->impuesto = $impuesto;
		$this->importe = $importe;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		# valida campos requeridos
		$required = array(
			'impuesto',
			'importe'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("ComplementoConceptoTerceros->Impuestos->Retencion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('ComplementoConceptoTerceros->Impuestos->Retencion Campo Requerido: ' . $field);
			}
		}
		
		# impuesto: valores permitidos (ISR, IVA)
		$valores = array('ISR', 'IVA');
		if(!in_array($this->impuesto, $valores))
			throw new Exception('ComplementoConceptoTerceros->Impuestos->Retencion->Impuesto Valor no Permitido: ' . $this->impuesto);

		if(!is_numeric($this->importe))
			throw new Exception('ComplementoConceptoTerceros->Impuestos->Retencion->Importe Valor no Permitido: ' . $this->importe);
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$retencion = $this->xml_base->createElement("terceros:Retencion");
		$this->xml_base->appendChild($retencion);

		$retencion->SetAttribute('impuesto', $this->impuesto);
		$retencion->SetAttribute('importe', $this->importe);
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("terceros:Retencion")->item(0);
		return $xml;
	}
}

class Traslado {
	var $impuesto;
	var $tasa;
	var $importe;
	var $xml_base;
	var $logger;
	
	function __construct($impuesto, $tasa, $importe) {
		$this->impuesto = $impuesto;
		$this->tasa = $tasa;
		$this->importe = $importe;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		# valida campos requeridos
		$required = array(
			'impuesto',
			'tasa',
			'importe'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("ComplementoConceptoTerceros->Impuestos->Traslado validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('ComplementoConceptoTerceros->Impuestos->Traslado Campo Requerido: ' . $field);
			}
		}
		
		# impuesto: valores permitidos (IEPS, IVA)
		$valores = array('IEPS', 'IVA');
		if(!in_array($this->impuesto, $valores))
			throw new Exception('ComplementoConceptoTerceros->Impuestos->Traslado Valor no Permitido: ' . $this->impuesto);
			
		if(!is_numeric($this->tasa))
			throw new Exception('ComplementoConceptoTerceros->Impuestos->Traslado->Tasa Valor no Permitido: ' . $this->tasa);
			
		if(!is_numeric($this->importe))
			throw new Exception('ComplementoConceptoTerceros->Impuestos->Traslado->Importe Valor no Permitido: ' . $this->importe);
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$traslado = $this->xml_base->createElement("terceros:Traslado");
		$this->xml_base->appendChild($traslado);

		$traslado->SetAttribute('impuesto', $this->impuesto);
		$traslado->SetAttribute('tasa', $this->tasa);
		$traslado->SetAttribute('importe', $this->importe);
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("terceros:Traslado")->item(0);
		return $xml;
	}
}
?>