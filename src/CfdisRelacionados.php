<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

Use Exception;
use DOMDocument;

class CfdisRelacionados {
	//normales
	var $TipoRelacion;
	//objetos
	var $xml_base;
	var $CfdiRelacionado = array();
	var $logger;

	function __construct($TipoRelacion) {
		$this->TipoRelacion = $TipoRelacion;
		$this->CfdiRelacionado = array();
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		# valida campos requeridos de concepto
		$required = array(
			'TipoRelacion'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("CfdiRelacionado validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('CfdiRelacionado Campo Requerido: ' . $field);
			}
		}
		
		#validar lista de uuid
		if (empty($this->CfdiRelacionado)){
			$this->logger->write("CfdiRelacionado validar(): no se encontraron UUID relacionados");
			throw new Exception('CfdiRelacionado validar(): no se encontraron UUID relacionados');
		}
		foreach($this->CfdiRelacionado as $key => $uuid){
			if(empty($uuid) || strlen($uuid) != 36){
				$this->logger->write( "CfdiRelacionado validar(): no se encontro UUID valido");
				throw new Exception('CfdiRelacionado validar(): no se encontro UUID valido');
			}
		}
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$CfdiRelacionados = $this->xml_base->createElement("cfdi:CfdiRelacionados");
		$this->xml_base->appendChild($CfdiRelacionados);

		# datos de concepto
		$CfdiRelacionados->SetAttribute('TipoRelacion', $this->TipoRelacion);

		# lista de cfdis
		if (!empty($this->CfdiRelacionado)) {
			foreach($this->CfdiRelacionado as $key => $uuid){
				$cfdi = $this->xml_base->createElement("cfdi:CfdiRelacionado");
				$CfdiRelacionados->appendChild($cfdi);
				$cfdi->SetAttribute('UUID', $uuid);
			}
		}
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("cfdi:CfdiRelacionados")->item(0);
		return $xml;
	}

	function addCfdiUUID($UUID) {
		$this->CfdiRelacionado[] = $UUID;
		//return $traslado;
	}
}
?>