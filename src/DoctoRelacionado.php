<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;
Use cfdi\Data\Arrays;

use Exception;
use DOMDocument;

class DoctoRelacionado {
	public $IdDocumento;
	public $Serie;
	public $Folio;
	public $MonedaDR;
	public $TipoCambioDR;
	public $MetodoDePagoDR;
	public $NumParcialidad;
	public $ImpSaldoAnt;
	public $ImpPagado;
	public $ImpSaldoInsoluto;
	public $Monto; // valor del nodo de pago, sirve para calcular y validar el attributo ImpSaldoInsoluto
	public $Decimales;
	public $moneda;
	public $xml_base;
	public $MonedaP;
	var $logger;

	function __construct($IdDocumento, $MetodoDePagoDR, $MonedaDR, $Monto, $Serie = null, $Folio = null, $TipoCambioDR = null, $NumParcialidad = null, $ImpSaldoAnt = null, $ImpPagado = null, $ImpSaldoInsoluto = null, $MonedaP = null) {
		$this->logger = new Logger(); //clase para escribir logs
		$arrayCatalog = new Arrays();
		$this->IdDocumento = trim($IdDocumento);
		$this->Serie = trim($Serie);
		$this->Folio = trim($Folio);
		$this->MonedaDR = $MonedaDR;
		$this->MonedaP = $MonedaP;
		$this->TipoCambioDR = $TipoCambioDR;
		$this->MetodoDePagoDR = $MetodoDePagoDR;
		$this->NumParcialidad = $NumParcialidad;
		$this->Monto = $Monto;
		$this->moneda = $arrayCatalog->arrayMoneda[$this->MonedaDR];
		$this->Decimales = $this->moneda['decimales'];
		$this->ImpSaldoAnt = round($ImpSaldoAnt,$this->Decimales);
		$this->ImpPagado = round($ImpPagado,$this->Decimales);
		$this->ImpSaldoInsoluto = round($ImpSaldoInsoluto,$this->Decimales);
		if($this->ImpSaldoInsoluto == -0){
			$this->ImpSaldoInsoluto = abs($this->ImpSaldoInsoluto);
		}

		if (!array_key_exists($this->MonedaDR, $arrayCatalog->arrayMoneda)) {
			$this->logger->write("Construct Pago() La moneda declarada " . $this->MonedaDR . " no se encuentra dentro del catalogo del SAT");
			throw new Exception('La moneda declarada ' . $this->MonedaDR . '  no se encuentra declarada en el catalogo de monedas.');
		}
	}

	function validar() {
	 //  print_r($this);
		// primero validar los elementos requeridos
		$required = array("IdDocumento", "MetodoDePagoDR", "MonedaDR");
	
		if ($this->MetodoDePagoDR == "PPD") {
			$required = array("IdDocumento", "MetodoDePagoDR", "MonedaDR", "NumParcialidad", "ImpSaldoAnt", "ImpSaldoInsoluto");
		}

		// ver aqui que onda con este campo ya que puede ser opcinal cuando solo existe un documento
		// relacionado pero cuando hay 2 o mas debe se obligatorio
		if (!empty($this->TipoCambioDR)) {
			array_push($required, "ImpPagado");
		}

		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("DoctoRelacionadoV2 validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('DoctoRelacionadoV2 Campo Requerido: ' . $field);
			}
		}
		// validar los decimales
		if (!empty($this->ImpSaldoAnt)) {
			$this->validateDecimals($this->ImpSaldoAnt, "ImpSaldoAnt");
		}
		if (!empty($this->ImpPagado)) {
			$this->validateDecimals($this->ImpPagado, "ImpPagado");
		}
		if (!empty($this->ImpSaldoInsoluto)) {
			$this->validateDecimals($this->ImpSaldoInsoluto, "ImpSaldoInsoluto");
		}

		// if (empty($this->ImpSaldoAnt) && $this->ImpSaldoAnt <= 0) {
		// 	$this->logger->write("validar DoctoRelacionado() el valor de ImpSaldoAnt " . $this->ImpSaldoAnt . " debe ser mayor a cero.");
		// 	throw new Exception("validar DoctoRelacionado() el valor de ImpSaldoAnt " . $this->ImpSaldoAnt . " debe ser mayor a cero.");
		// }

		if ($this->MonedaDR == "XXX") {
			$this->logger->write(" : DoctoRelacionado validar(): MonedaDR no valido :" . $this->MonedaDR);	
			throw new Exception('DoctoRelacionado Campo MonedaDR debe ser distinto de : ' . $this->MonedaDR);
		}

		
		if($this->ImpSaldoAnt>0){
			if (!empty($this->ImpPagado) && $this->ImpPagado>0) {
				$impsaldoinsoluto = $this->ImpSaldoAnt - $this->ImpPagado;
			} else {
				$impsaldoinsoluto = $this->ImpSaldoAnt - $this->Monto;
			}
			$impsaldoinsoluto = round($impsaldoinsoluto,6);
			
			$bccomp = bccomp((float)$impsaldoinsoluto, (float)$this->ImpSaldoInsoluto); //0 si los dos operandos son iguales, 1 si el left_operand es mayor que el right_operand, de lo contrario -1.
			if($bccomp != 0){
				$this->logger->write("DoctoRelacionado validar(): El valor del ImpSaldoInsoluto declarado  debe ser mayor o igual a cero y calcularse con la suma de los campos ImpSaldoAnt menos el ImpPagado o el Monto. Valor Recibido: ".$this->ImpSaldoInsoluto." Valor esperado: " . $impsaldoinsoluto);
				throw new Exception("  : DoctoRelacionado validar(): El valor del ImpSaldoInsoluto declarado  debe ser mayor o igual a cero y calcularse con la suma de los campos ImpSaldoAnt menos el ImpPagado o el Monto. Valor Recibido: ".$this->ImpSaldoInsoluto." Valor esperado: " . $impsaldoinsoluto);
			}
		}else{
			if ($this->MetodoDePagoDR == "PPD") {
				$this->logger->write("ComplementoDePago validar(): El campo ImpSaldoAnt debe ser mayor a 0 el valor ingresado :" . $this->ImpSaldoAnt);
				throw new Exception('El campo ImpSaldoAnt debe ser mayor a 0 el valor ingresado : ' . $this->ImpSaldoAnt);
			}
		}

		if ($this->MonedaDR == "MXN" AND (!empty($this->TipoCambioDR && $this->MonedaP == "MXN"))) {
			$this->logger->write("ComplementoDePago validar(): Cuando el campo monedaDR sea MXN, el campo TipoCambioDR no se debe registrar, valor registrado :" . $this->TipoCambioDR);
			throw new Exception('Cuando el campo monedaDR sea MXN, el campo TipoCambioDR no se debe registrar, valor registrado : ' . $this->TipoCambioDR);
		}
//        if ($this->MonedaDR == "MXN" and $this->TipoCambioDR != null) {
//            $this->logger->write("DoctoRelacionado validar(): El campo TipoCambioDR no se debe registrar cuando la moneda es MXN");
//            throw new Exception('" : DoctoRelacionado validar(): El campo TipoCambioDR no se debe registrar cuando la moneda es MXN ');
//        }
	}

	function toXML() {
		$this->xml_base = new DOMDocument('1.0', 'UTF-8');
		$documento = $this->xml_base->createElement("pago10:DoctoRelacionado");
		$this->xml_base->appendChild($documento);
		$documento->setAttribute("IdDocumento", $this->IdDocumento);
		if ($this->Serie)
			$documento->setAttribute("Serie", $this->Serie);
		if ($this->Folio)
			$documento->setAttribute("Folio", $this->Folio);
		$documento->setAttribute("MonedaDR", $this->MonedaDR);
		if ($this->TipoCambioDR)
			$documento->setAttribute("TipoCambioDR", $this->addZeros($this->TipoCambioDR, 6));
		$documento->setAttribute("MetodoDePagoDR", $this->MetodoDePagoDR);
		if ($this->NumParcialidad)
			$documento->setAttribute("NumParcialidad", $this->NumParcialidad);
		if ($this->ImpSaldoAnt)
			$documento->setAttribute("ImpSaldoAnt", $this->addZeros($this->ImpSaldoAnt));
		if ($this->ImpPagado)
			$documento->setAttribute("ImpPagado", $this->addZeros($this->ImpPagado));
		//if ($this->ImpSaldoInsoluto!=null)
		$documento->setAttribute("ImpSaldoInsoluto", $this->addZeros($this->ImpSaldoInsoluto));
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("pago10:DoctoRelacionado")->item(0);
		return $xml;
	}

	function validateDecimals($valor, $nombre = null) {
		$decimalesTotal = strlen(substr(strrchr($valor, "."), 1));
		if ($decimalesTotal > $this->Decimales) {
			throw new Exception("El valor de " . $valor .
			" en el attributo " . $nombre . " no coincide con el valor de los decimales especificado por la moneda ,valor de decimales: " . $this->Decimales);
		}
	}

	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return sprintf('%0.' . $dec . 'f', $cantidad);
	}
}
?>