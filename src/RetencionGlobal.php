<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

use Exception;
use DOMDocument;

class RetencionGlobal{
	var $Impuesto;
	var $Importe;
	var $xml_base;
	var $Decimales;
	
	function __construct($Impuesto, $Importe, $Decimales = 2) {
		$this->Decimales = $Decimales;
		$this->Impuesto = $Impuesto;
		$this->Importe = $Importe;
	}
	
	function validar() {
		$required = array(
			'Impuesto',
			'Importe'
		);

		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Retencion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Retencion Campo Requerido: ' . $field);
			}
		}

		if($this->Importe < 0){
			throw new Exception("Error en el importe, contiene valor negativo ");
		}
	}
	
	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$retencion = $this->xml_base->createElement("cfdi:Retencion");
		$this->xml_base->appendChild($retencion);

		# datos de tralado
		$retencion->SetAttribute('Impuesto', $this->Impuesto);
		$retencion->SetAttribute('Importe', $this->addZeros($this->Importe));
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("cfdi:Retencion")->item(0);
		return $xml;
	}
	
	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return  sprintf('%0.'.$dec.'f',$cantidad);
	}
}
?>