<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

use Exception;
use DOMDocument;

class Complemento {
	var $StringXML;
	var $xml_base;
	var $logger;

	function __construct($StringXML) {
		$this->StringXML = $StringXML;
		$this->logger = new Logger(); //clase para escribir logs
	}
	
	function validar(){
		if($this->StringXML){
			$xml = new DOMDocument('1.0', 'UTF-8');
			libxml_use_internal_errors(true);
			if (!$xml->loadXML($this->StringXML)) {
				$errors = libxml_get_errors();
				$this->logger->write("complemento(): fallo al validar el xml del complemento " . print_r($errors, true));
				throw new Exception("validar complemento(): fallo al validar el xml del complemento " . print_r($errors, true));
			}
		}else{
			throw new Exception("validar complemento(): No se encontro complemento para validar ");
		}
	}

	function toXML() {
		$this->xml_base = new DOMDocument('1.0', 'UTF-8');
		libxml_use_internal_errors(true);
		if (!$this->xml_base->loadXML($this->StringXML)) {
			$errors = libxml_get_errors();
			$this->logger->write("complemento(): fallo al cargar el xml del complemento " . print_r($errors, true));
			throw new Exception("complemento(): fallo al cargar el xml del complemento " . print_r($errors, true));
		}
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		//$xml = $this->xml_base->getElementsByTagName("cce11:ComercioExterior")->item(0);
		$xml = $this->xml_base->documentElement;
		return $xml;
	}
}
?>