<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

use Exception;
use DOMDocument;

Use cfdi\Logger;
Use cfdi\Data\Arrays;

class Totales {
    var $TotalRetencionesIVA;
    var $TotalRetencionesISR;
    var $TotalRetencionesIEPS;
	var $TotalTrasladosBaseIVA16;
    var $TotalTrasladosImpuestoIVA16;
    var $TotalTrasladosBaseIVA8;
    var $TotalTrasladosImpuestoIVA8;
    var $TotalTrasladosBaseIVA0;
    var $TotalTrasladosImpuestoIVA0;
    var $TotalTrasladosBaseIVAExento;
    var $MontoTotalPagos;
	var $xml;
	var $logger;
    public $moneda;
    public $Decimales;

	public function __construct($TotalRetencionesIVA=null, $TotalRetencionesISR=null, $TotalRetencionesIEPS=null, $TotalTrasladosBaseIVA16=null, 
    $TotalTrasladosImpuestoIVA16=null, $TotalTrasladosBaseIVA8=null, $TotalTrasladosImpuestoIVA8=null, $TotalTrasladosBaseIVA0=null, 
    $TotalTrasladosImpuestoIVA0=null, $TotalTrasladosBaseIVAExento=null, $MontoTotalPagos) {
        $arrayCatalog = new Arrays();
        $this->TotalRetencionesIVA=$TotalRetencionesIVA;
        $this->TotalRetencionesISR=$TotalRetencionesISR;
        $this->TotalRetencionesIEPS=$TotalRetencionesIEPS;
        $this->TotalTrasladosBaseIVA16=$TotalTrasladosBaseIVA16;
        $this->TotalTrasladosImpuestoIVA16=$TotalTrasladosImpuestoIVA16;
        $this->TotalTrasladosBaseIVA8=$TotalTrasladosBaseIVA8;
        $this->TotalTrasladosImpuestoIVA8=$TotalTrasladosImpuestoIVA8;
        $this->TotalTrasladosBaseIVA0=$TotalTrasladosBaseIVA0;
        $this->TotalTrasladosImpuestoIVA0=$TotalTrasladosImpuestoIVA0;
        $this->TotalTrasladosBaseIVAExento=$TotalTrasladosBaseIVAExento;
        $this->MontoTotalPagos=$MontoTotalPagos;
        $this->moneda = $arrayCatalog->arrayMoneda['MXN'];
		$this->Decimales = $this->moneda['decimales'];
		$this->logger = new Logger(); //clase para escribir logs
	}

	public function validar() {
		$required = array(
			'MontoTotalPagos',
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Totales Pagos validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Totales Pagos Campo Requerido: ' . $field);
			}
		}
        if(!empty($this->TotalRetencionesIVA)){
            if($this->TotalRetencionesIVA < 0){ //valor minimo
                $this->logger->write('Totales Pagos TotalRetencionesIVA ' . $this->TotalRetencionesIVA . ' debe ser un valor positivo');
                throw new Exception('Totales Pagos TotalRetencionesIVA ' . $this->TotalRetencionesIVA . ' debe ser un valor positivo');
            }
        }

        if(!empty($this->TotalRetencionesISR)){
            if($this->TotalRetencionesISR < 0){ //valor minimo
                $this->logger->write('Totales Pagos TotalRetencionesISR ' . $this->TotalRetencionesISR . ' debe ser un valor positivo');
                throw new Exception('Totales Pagos TotalRetencionesISR ' . $this->TotalRetencionesISR . ' debe ser un valor positivo');
            }
        }

        if(!empty($this->TotalRetencionesIEPS)){
            if($this->TotalRetencionesIEPS < 0){ //valor minimo
                $this->logger->write('Totales Pagos TotalRetencionesIEPS ' . $this->TotalRetencionesIEPS . ' debe ser un valor positivo');
                throw new Exception('Totales Pagos TotalRetencionesIEPS ' . $this->TotalRetencionesIEPS . ' debe ser un valor positivo');
            }
        }

        if(!empty($this->TotalTrasladosBaseIVA16)){
            if($this->TotalTrasladosBaseIVA16 < 0){ //valor minimo
                $this->logger->write('Totales Pagos TotalTrasladosBaseIVA16 ' . $this->TotalTrasladosBaseIVA16 . ' debe ser un valor positivo');
                throw new Exception('Totales Pagos TotalTrasladosBaseIVA16 ' . $this->TotalTrasladosBaseIVA16 . ' debe ser un valor positivo');
            }
        }

        if(!empty($this->TotalTrasladosImpuestoIVA16)){
            if($this->TotalTrasladosImpuestoIVA16 < 0){ //valor minimo
                $this->logger->write('Totales Pagos TotalTrasladosImpuestoIVA16 ' . $this->TotalTrasladosImpuestoIVA16 . ' debe ser un valor positivo');
                throw new Exception('Totales Pagos TotalTrasladosImpuestoIVA16 ' . $this->TotalTrasladosImpuestoIVA16 . ' debe ser un valor positivo');
            }
        }

        if(!empty($this->TotalTrasladosBaseIVA8)){
            if($this->TotalTrasladosBaseIVA8 < 0){ //valor minimo
                $this->logger->write('Totales Pagos TotalTrasladosBaseIVA8 ' . $this->TotalTrasladosBaseIVA8 . ' debe ser un valor positivo');
                throw new Exception('Totales Pagos TotalTrasladosBaseIVA8 ' . $this->TotalTrasladosBaseIVA8 . ' debe ser un valor positivo');
            }
        }

        if(!empty($this->TotalTrasladosImpuestoIVA8)){
            if($this->TotalTrasladosImpuestoIVA8 < 0){ //valor minimo
                $this->logger->write('Totales Pagos TotalTrasladosImpuestoIVA8 ' . $this->TotalTrasladosImpuestoIVA8 . ' debe ser un valor positivo');
                throw new Exception('Totales Pagos TotalTrasladosImpuestoIVA8 ' . $this->TotalTrasladosImpuestoIVA8 . ' debe ser un valor positivo');
            }
        }

        if(!empty($this->TotalTrasladosBaseIVA0)){
            if($this->TotalTrasladosBaseIVA0 < 0){ //valor minimo
                $this->logger->write('Totales Pagos TotalTrasladosBaseIVA0 ' . $this->TotalTrasladosBaseIVA0 . ' debe ser un valor positivo');
                throw new Exception('Totales Pagos TotalTrasladosBaseIVA0 ' . $this->TotalTrasladosBaseIVA0 . ' debe ser un valor positivo');
            }
        }
        
        if(!empty($this->TotalTrasladosImpuestoIVA0)){
            if($this->TotalTrasladosImpuestoIVA0 < 0){ //valor minimo
                $this->logger->write('Totales Pagos TotalTrasladosImpuestoIVA0 ' . $this->TotalTrasladosImpuestoIVA0 . ' debe ser un valor positivo');
                throw new Exception('Totales Pagos TotalTrasladosImpuestoIVA0 ' . $this->TotalTrasladosImpuestoIVA0 . ' debe ser un valor positivo');
            }
        }

        if(!empty($this->TotalTrasladosBaseIVAExento)){
            if($this->TotalTrasladosBaseIVAExento < 0){ //valor minimo
                $this->logger->write('Totales Pagos TotalTrasladosBaseIVAExento ' . $this->TotalTrasladosBaseIVAExento . ' debe ser un valor positivo');
                throw new Exception('Totales Pagos TotalTrasladosBaseIVAExento ' . $this->TotalTrasladosBaseIVAExento . ' debe ser un valor positivo');
            }
        }
	}

	public function toXML() {
		$this->xml = new DOMdocument("1.0", "UTF-8");
		$domTotales = $this->xml->createElement('pago20:Totales');
		$this->xml->appendChild($domTotales);
        if($this->TotalRetencionesIVA !== null){
            $domTotales->setAttribute('TotalRetencionesIVA', (round($this->TotalRetencionesIVA, $this->Decimales)));
        }

        if(!empty($this->TotalRetencionesISR)){
            $domTotales->setAttribute('TotalRetencionesISR', (round($this->TotalRetencionesISR,$this->Decimales)));
        }

        if(!empty($this->TotalRetencionesIEPS)){
            $domTotales->setAttribute('TotalRetencionesIEPS', (round($this->TotalRetencionesIEPS,$this->Decimales)));
        }

        if(!empty($this->TotalTrasladosBaseIVA16)){
            $domTotales->setAttribute('TotalTrasladosBaseIVA16', (round($this->TotalTrasladosBaseIVA16,$this->Decimales)));
        }

        if(!empty($this->TotalTrasladosImpuestoIVA16)){
            $domTotales->setAttribute('TotalTrasladosImpuestoIVA16', (round($this->TotalTrasladosImpuestoIVA16,$this->Decimales)));
        }

        if(!empty($this->TotalTrasladosBaseIVA8)){
            $domTotales->setAttribute('TotalTrasladosBaseIVA8', (round($this->TotalTrasladosBaseIVA8,$this->Decimales)));
        }

        if(!empty($this->TotalTrasladosImpuestoIVA8)){
            $domTotales->setAttribute('TotalTrasladosImpuestoIVA8', (round($this->TotalTrasladosImpuestoIVA8,$this->Decimales)));
        }

        if(!empty($this->TotalTrasladosBaseIVA0)){
            $domTotales->setAttribute('TotalTrasladosBaseIVA0', (round($this->TotalTrasladosBaseIVA0,$this->Decimales)));
        }

        if($this->TotalTrasladosImpuestoIVA0 !== null){
            $domTotales->setAttribute('TotalTrasladosImpuestoIVA0', (round($this->TotalTrasladosImpuestoIVA0,$this->Decimales)));
        }

        if(!empty($this->TotalTrasladosBaseIVAExento)){
            $domTotales->setAttribute('TotalTrasladosBaseIVAExento', (round($this->TotalTrasladosBaseIVAExento,$this->Decimales)));
        }

		$domTotales->setAttribute('MontoTotalPagos', (round($this->MontoTotalPagos,$this->Decimales)));
		
        
        return $domTotales;
	}

	function toStringXML() {
		return $this->xml->saveXML();
	}

	function importXML() {
		$xml = $this->xml->getElementsByTagName("pago20:Totales")->item(0);
		return $xml;
	}
}
?>