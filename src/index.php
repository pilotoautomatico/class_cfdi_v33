<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

require '../vendor/autoload.php';

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
//include_once("Comprobante.php");

Use cfdi\Comprobante;

try {
	//se crea objeto comprobante
	//agregarle los paths de las keys
	//$cer = getcwd() . "/keys/2.cer";
	//$key = getcwd() . "/keys/1.key";
	//$rfc = "PAU1207301E5";
	//$pass = "caracoles44";
	//
	//$keys = new Csd($cer, $key, $rfc, $pass, $path = getcwd() . "/keys/");
	//
	//try {
	//	$keys->convertCerToPem();
	//	$keys->convertKeyToPem();
	//	echo 'se convirtieron a pem';
	//} catch (Exception $e) {
	//	var_dump($e->getMessage());
	//	return;
	//}

	$key = getcwd() . "/tests/keys/00001000000505422144.key.pem";
	$cer = getcwd() . "/tests/keys/00001000000505422144.cer.pem";

	# CREACION DE XML -------------------------------------------------------------------------------------------------
	$cfdi = new Comprobante();

	// añadir este metodo en la creacion de los xml , se encarga de guardar en el objeto las rutas de los archivos .pem
	// necesarios para la creacion de la cadena original y el sello encriptado en sha1
	$cfdi->addKeys($cer, $key);

	$cfdi->addGenerales('00001000000505422144', 3000, 'MXN', 3400, 'I', '01', 1, '64000', 'PUE', '12');

	$cfdi->addEmisor('PAU1207301E5', 'PILOTO AUTOMATICO SA DE CV', '601');
	$cfdi->addReceptor('RPE841207E99', 'G01', 'RESTAURANTE PERISUR SA DE CV');

	//se agrega concepto al comprobante
	$concepto = $cfdi->addConcepto('01010101', 'descripcion 1', 1, 1000, 'TONELADA', 'F52', '0001');
	$concepto->addTraslado(1000, '002', 'Tasa', '0.160000', 160);
	$concepto->addTraslado(1000, '003', 'Tasa', '0.070000', 70);
	$concepto->addRetencion(1000, '002', 'Tasa', '0.150000', 150);

	//se agrega concepto al comprobante
	$concepto = $cfdi->addConcepto('01010101', 'descripcion 2', 1, 1000, 'TONELADA', 'F52', '0001');
	$concepto->addTraslado(1000, '002', 'Tasa', '0.160000', 160);
	$concepto->addRetencion(1000, '002', 'Tasa', '0.100000', 100);

	//se agrega concepto al comprobante
	$concepto = $cfdi->addConcepto('01010101', 'descripcion 3', 1, 1000, 'TONELADA', 'F52', '0001');
	$concepto->addTraslado(1000, '003', 'Tasa', '0.300000', 300);
	$concepto->addRetencion(1000, '001', 'Tasa', '0.040000', 40);

	$cfdi->addImpuestosGlobales();
	
	$cfdi->addSellos();
	//$cfdi->validateSellos();

	$cfdi->validar();

	$cfdi->validateXSD();

	/* TIMBRADO -------------------------
	$xml = $cfdi->toStringXML();
	error_log(date("Y-m-d H:i:s") . " : XML: " . print_r($xml, true) . "\n", 3, "debug.log");
	// PRUEBA DE TIMBRADO DESDE LA API
	// $stringxml = file_get_contents($xml);
	// curl que se encarga de enviar el xml a la API

	$data = array("rfc"=>"PAU1207301E5","xml" => $xml, "token" =>"1234");

	$ch = curl_init("http://localhost:3000/V33/timbrar");

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
	$response = curl_exec($ch);
	curl_close($ch);
	if (curl_error($ch)) {
		echo 'error :' . curl_error($ch);
		return false;
	} else {
			var_dump($response);
		// $response;
	}

	// este metodo realiza la verficacion de que se haya timbrado correectamente

	$result_array = get_object_vars(json_decode($response));
	if (!$result_array['data']->tfd) {
		error_log(date("Y-m-d H:i:s") . " : Error al timbrar el CFDI: " . $response);
		throw new Exception("Error al timbrar " . $response);
	}

	$cfdi->addTimbreFiscal($result_array['data']->tfd);
	
	$cfdi->addAddenda('<ad:MiAddenda xmlns:ad="http://www.addenda.com">
							<ad:Cabecera UUID="@@FS@@uuid@@FS@@" SerieFolio="@@CDF@@SerieFolio@@CDF@@">
								<ad:Conceptos>
									<ad:Concepto RazonSocialReceptor="@@CDF@@RazonSocialReceptor@@CDF@@" FechaTimbrado="@@CDF@@FechaTimbrado@@CDF@@" />
								</ad:Conceptos>
							</ad:Cabecera>
						</ad:MiAddenda>');
	$cfdi->validar();
	$cfdi->validateXSD();
	*/

	$cfdi->toXML();
	$cfdi->toSaveXML();
	print_r($cfdi->getCadenaOriginal());
	error_log(date("Y-m-d H:i:s") . " : CREATE CFDI: ".print_r($cfdi, true)." \n", 3, "debug.log");

	# LECTURA DE XML --------------------------------------------------------------------------------------------------
	/*$cfdi = new Comprobante();
	$file_xml='XMLV33.xml';
	$cfdi->fromLoadXML($file_xml);
	$cfdi->validar();
	$cfdi->toXML();
	$cfdi->validateXSD();
	$cfdi->toSaveXML();*/

	error_log(date("Y-m-d H:i:s") . " : LOAD CFDI: ".print_r($cfdi, true)." \n", 3, "debug.log");

	print_r($cfdi);
} catch (Exception $e) {
	echo 'Error al generar el CFDI: ', $e->getMessage(), "\n";
	error_log(date("Y-m-d H:i:s") . " : Error al generar el CFDI: " . print_r($e->getMessage(), true) . "\n", 3, "debug.log");
}
?>