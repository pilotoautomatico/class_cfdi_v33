<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

use DateTime;
use DateTimeZone;
use Exception;

class Csd {
	protected $cer;
	protected $cerPem;
	protected $key;
	protected $keyPem;
	protected $keyEnc;
	protected $rfc;
	protected $pass;
	protected $path;
	protected $validCer = false;
	protected $validKey = false;

	public function __construct($cer = null, $key = null, $rfc = null, $pass = null, $path = null, $cerPem = null, $keyPem = null) {
		$this->cer = $cer;
		$this->cer = str_replace('\\', '/', $this->cer);
		$this->key = $key;
		$this->key = str_replace('\\', '/', $this->key);
		$this->rfc = $rfc;
		$this->pass = $pass;
		$this->path = is_null($path) ? getcwd()."/csds/" : $path;
		$this->path = str_replace('\\', '/', $this->path);
		$this->cerPem = $cerPem ? $cerPem : "{$this->path}{$this->rfc}.cer.pem";
		$this->cerPem = str_replace('\\', '/', $this->cerPem);
		$this->keyPem = $keyPem ? $keyPem : "{$this->path}{$this->rfc}.key.pem";
		$this->keyPem = str_replace('\\', '/', $this->keyPem);
	}

	public function convertCerToPem() {
		exec("openssl x509 -inform DER -outform PEM -in {$this->cer} -pubkey -out {$this->cerPem}", $out, $errors);
		if ($errors) {
			$this->throwError('Error al convertir el archivo: .cer, verifica la ruta del archivo [convertCerToPem]: '."openssl x509 -inform DER -outform PEM -in {$this->cer} -pubkey -out {$this->cerPem}");
		}
		chmod($this->cerPem, 0777);

		$this->validCer = true;
	}

	public function convertKeyToPem() {
		exec("openssl pkcs8 -inform DER -in {$this->key} -passin pass:'{$this->pass}' -out {$this->keyPem}", $out, $errors);
		if ($errors) {
			$this->throwError("Error al convertir el archivo: .key, verificar la contraseña. [convertKeyToPem]: openssl pkcs8 -inform DER -in {$this->key} -passin pass:{$this->pass} -out {$this->keyPem}");
		}
		chmod($this->keyPem, 0777);

		$this->validKey = true;
	}

	public function encryptPemInToDer($pass) {
		if (!$this->validKey) {
			$this->throwError("No haz convertido el archivo .key a .pem");
		}
		$this->keyEnc = "{$this->path}{$this->rfc}.enc.key";

		exec("openssl rsa -in {$this->keyPem} -des3 -out {$this->keyEnc} -passout pass:{$pass}", $out, $errors);

		if ($errors) {
			$this->throwError("Error al encryptar el archivo key.");
		}
	}

	/**
	 * Metodo que valida las fechas de ultilizacion de las keys
	 * valida las keys en .pem por lo que se debe de ultilizar despues
	 * de haber convertido las llaves
	 * @return array con las fechas
	 * @throws Exception
	 */
	public function verifyValidityPeriod() {
		if (!$this->validCer) {
			$this->throwError("No haz convertido el archivo .cer a .pem");
		}

		exec("openssl x509 -noout -in {$this->cerPem} -dates", $out, $errors);

		if ($errors) {
			$this->throwError(" Ha ocurrido un error al intentar validar el csd. [verifyValidityPeriod]: openssl x509 -noout -in {$this->cerPem} -dates");
		}
		$fecha_inicial = explode("=", $out[0]);
		$fecha_final = explode("=", $out[1]);

		$now = new DateTime("now", new DateTimeZone('America/Mexico_City'));
		$fecha_inicial = new DateTime(end($fecha_inicial), new DateTimeZone('America/Mexico_City'));
		$fecha_final = new DateTime(end($fecha_final), new DateTimeZone('America/Mexico_City'));

		if ($now < $fecha_inicial || $now > $fecha_final) {
			$this->throwError("El csd no es valido. No se encuentra con fecha vigente.");
		}

		return compact('fecha_inicial', 'fecha_final');
	}

	public function verifyValidCsd() {
		if (!$this->validCer) {
			$this->throwError("No haz convertido el archivo .cer a .pem");
		}

		$valid = false;

		// aad
		$csd_data = openssl_x509_parse(file_get_contents($this->cerPem));
		$csd_rfc = $csd_data['subject']['x500UniqueIdentifier'];
		if(strpos(strtoupper($csd_rfc),strtoupper($this->rfc),0) !== FALSE) $valid = true;
		// fin aad

		/*
		exec("openssl x509 -in {$this->cerPem} -subject -noout", $out, $errors);
		if ($errors) {
			$this->throwError("Ha ocurrido un error al intentar validar el csd. [verifyValidCsd]: openssl x509 -in {$this->cerPem} -subject -noout");
		}

		$vars = preg_split("/\s?\/\s?/", $out[0]);

		foreach($vars as $var){
			$value = explode('=', $var);
			if($value[0] == 'x500UniqueIdentifier') {
				 if(strtoupper($value[1])!=strtoupper($this->rfc)){
					 $this->throwError("El RFC especificado($this->rfc) no corresponde al RFC del SELLO($value[1]). [verifyValidCsd]");
				 }else{
					 $valid = true;
				 }
			}
		}
		*/

		if(!$valid){
			$this->throwError("El RFC del SELLO no pudo validarse. [verifyValidCsd]");
		}
	}

	/**
	 * Aqui para ultilizar estos metodos de validacion se deben de haber
	 * convertido las keys a .pem para validar su posterior ulilizacion
	 * ademas de que los comandos requieren que las llaves esten en dicho formato para acceder a sus propiedades.
	 * @return string Numero del certificado del la key
	 * @throws Exception
	 */
	public function getNoCertificado() {
		if (!$this->validCer) {
			$this->throwError("No haz convertido el archivo .cer a .pem");
		}

		exec("openssl x509 -in {$this->cerPem} -noout -serial", $out, $errors);

		if ($errors) {
			$msg = openssl_error_string();
			throw new Exception("Ha ocurrido un error al intentar validar el csd. [getNoCertificado]: openssl x509 -in {$this->cerPem} -noout -serial : " . $msg);
		}

		$vars = explode("=", $out[0]);
		$certificado = end($vars);
		$no_certificado = '';

		for ($i = 0; $i < strlen($certificado); $i++) {
			if ($i % 2 != 0) {
				$no_certificado .= substr($certificado, $i, 1);
			}
		}

		return $no_certificado;
	}

	protected function throwError($error) {
		throw new Exception(
		sprintf("%s", $error)
		);
	}
}
?>