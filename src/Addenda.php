<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

use Exception;
use DOMDocument;

class Addenda {
	var $StringXML;
	var $xml_base;
	var $logger;

	function __construct($StringXML) {
		$this->StringXML = $StringXML;
		$this->logger = new Logger(); //clase para escribir logs
	}
	
	function validar(){
		if($this->StringXML){
			$xml = new DOMDocument('1.0', 'UTF-8');
			libxml_use_internal_errors(true);
			if (!$xml->loadXML($this->StringXML)) {
				$errors = libxml_get_errors();
				$this->logger->write("addenda(): fallo al cargar el xml de la addenda " . print_r($errors, true));
				throw new Exception("validar addenda(): fallo al cargar el xml de la addenda " . print_r($errors, true));
			}
		}else{
			throw new Exception("validar addenda(): No se encontro addenda para validar ");
		}
	}

	function toXML() {
		$xml = new DOMDocument('1.0', 'UTF-8');
		libxml_use_internal_errors(true);
		if (!$xml->loadXML($this->StringXML)) {
			$errors = libxml_get_errors();
			$this->logger->write("addenda(): fallo al cargar el xml de la addenda " . print_r($errors, true));
			throw new Exception("addenda(): fallo al cargar el xml de la addenda " . print_r($errors, true));
		}

		/*$this->xml_base = new DOMDocument('1.0', 'UTF-8');
		$addenda = $this->xml_base->createElement('cfdi:Addenda');
		$this->xml_base->appendChild($addenda);
		$addendaData = $this->xml_base->importNode($xml->documentElement, true);
		$addenda->appendChild($addendaData);  */
		$this->xml_base = $xml;
		//$this->logger->write("addenda(): " . print_r($this->toStringXML(), true));

	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		//$xml = $this->xml_base->getElementsByTagName("cfdi:Addenda")->item(0);
		$xml = $this->xml_base->documentElement;
		return $xml;
	}
}
?>