<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

use Exception;
use DOMDocument;

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte

class CartaPorte 
{
	// Obligatorios
	public $Version = "1.0";
	var $TranspInternac;
	var $Ubicaciones = [];
	var $Mercancias;

	// opcionales
	var $EntradaSalidaMerc;
	var $ViaEntradaSalida;
	var $TotalDistRec;
	var $FiguraTransporte;

	var $xml_base;
	var $logger;

	function __construct($TranspInternac, $EntradaSalidaMerc = null, $ViaEntradaSalida = null, $TotalDistRec = null)
    {
		$this->xml_base = null;
        $this->TranspInternac = $TranspInternac;
        $this->EntradaSalidaMerc = $EntradaSalidaMerc;
        $this->ViaEntradaSalida = $ViaEntradaSalida;
        $this->TotalDistRec = $TotalDistRec;
		$this->Ubicaciones = [];
		$this->Mercancias = null;
		$this->FiguraTransporte = null;

		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['Version', 'TranspInternac'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte Campo Requerido: ' . $field);
			}
		}

        if(count($this->Ubicaciones) == 0)
        {
            $this->logger->write("Complemento.CartaPorte.Ubicaciones validar(): Debe existir un elemento de Ubicaciones.");
            throw new Exception('Complemento.CartaPorte.Ubicaciones: Debe existir un elemento de Ubicaciones.');
        }

		foreach($this->Ubicaciones as $Ubicacion)
        {
			$Ubicacion->validar();
		}

        if(empty($this->Mercancias))
        {
            $this->logger->write("Complemento.CartaPorte.Mercancias validar(): Debe existir elemento de Mercancias.");
            throw new Exception('Complemento.CartaPorte.Mercancias: Debe existir elemento de Mercancias.');
        }

		if($this->Mercancias)
			$this->Mercancias->validar();

		if($this->FiguraTransporte)
			$this->FiguraTransporte->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoCartaPorte = $this->xml_base->createElement("cartaporte:CartaPorte");

		$nodoCartaPorte->setAttribute('Version',  $this->Version);
		$nodoCartaPorte->setAttribute('TranspInternac',  $this->TranspInternac);
        if($this->EntradaSalidaMerc)
		    $nodoCartaPorte->setAttribute('EntradaSalidaMerc',  $this->EntradaSalidaMerc);
        if($this->ViaEntradaSalida)
		    $nodoCartaPorte->setAttribute('ViaEntradaSalida',  $this->ViaEntradaSalida);
        if($this->TotalDistRec)
		    $nodoCartaPorte->setAttribute('TotalDistRec',  $this->TotalDistRec);

        if(!empty($this->Ubicaciones))
        {
		    $nodoUbicaciones = $this->xml_base->createElement("cartaporte:Ubicaciones");
		    $nodoCartaPorte->appendChild($nodoUbicaciones);

            foreach ($this->Ubicaciones as $key => $Ubicacion) 
            {
                $Ubicacion->toXML();
                $nodoUbicacion = $this->xml_base->importNode($Ubicacion->importXML(), true);
                $nodoUbicaciones->appendChild($nodoUbicacion);
            }
        }

        if(!empty($this->Mercancias))
        {
            $this->Mercancias->toXML();
            $nodoMercancias = $this->xml_base->importNode($this->Mercancias->importXML(), true);
		    $nodoCartaPorte->appendChild($nodoMercancias);
        }

        if(!empty($this->FiguraTransporte))
        {
            $this->FiguraTransporte->toXML();
            $nodoFiguraTransporte = $this->xml_base->importNode($this->FiguraTransporte->importXML(), true);
            $nodoCartaPorte->appendChild($nodoFiguraTransporte);
        }

		$this->xml_base->appendChild($nodoCartaPorte);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:CartaPorte")->item(0);
		return $xml;
	}

	function addUbicacion($TipoEstacion = null, $DistanciaRecorrida = null) 
    {
		$Ubicacion = new Ubicacion(
            $TipoEstacion, 
            $DistanciaRecorrida
        );
		
		// $Ubicacion->validar();
		$this->Ubicaciones[] = $Ubicacion;
		return $Ubicacion;
	}

	function addMercancias($NumTotalMercancias, $PesoBrutoTotal = null, $UnidadPeso = null, $PesoNetoTotal = null, $CargoPorTasacion = null) 
    {
		$Mercancias = new Mercancias(
            $NumTotalMercancias, 
            $PesoBrutoTotal, 
            $UnidadPeso, 
            $PesoNetoTotal, 
            $CargoPorTasacion
        );
		
		// $Mercancias->validar();
		$this->Mercancias = $Mercancias;
		return $Mercancias;
	}

	function addFiguraTransporte($CveTransporte) 
    {
		$FiguraTransporte = new FiguraTransporte(
            $CveTransporte
        );
		
		// $FiguraTransporte->validar();
		$this->FiguraTransporte = $FiguraTransporte;
		return $FiguraTransporte;
	}

}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Ubicaciones.Ubicacion

class Ubicacion 
{
	// opcionales
	var $TipoEstacion;
	var $DistanciaRecorrida;
    var $Origen;
    var $Destino;
    var $Domicilio;

	var $xml_base;
	var $logger;

	function __construct($TipoEstacion = null, $DistanciaRecorrida = null)
    {
		$this->xml_base = null;
        $this->TipoEstacion = $TipoEstacion;
        $this->DistanciaRecorrida = $DistanciaRecorrida;
        $this->Origen = null;
        $this->Destino = null;
        $this->Domicilio = null;

		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        if($this->Origen)
            $this->Origen->validar();

        if($this->Destino)
            $this->Destino->validar();

        if($this->Domicilio)
            $this->Domicilio->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoUbicacion = $this->xml_base->createElement("cartaporte:Ubicacion");

        if($this->TipoEstacion)
		    $nodoUbicacion->setAttribute('TipoEstacion',  $this->TipoEstacion);
        if($this->DistanciaRecorrida)
		    $nodoUbicacion->setAttribute('DistanciaRecorrida',  $this->DistanciaRecorrida);

        if($this->Origen)
        {
            $this->Origen->toXML();
            $nodoOrigen = $this->xml_base->importNode($this->Origen->importXML(), true);
		    $nodoUbicacion->appendChild($nodoOrigen);
        }

        if($this->Destino)
        {
            $this->Destino->toXML();
            $nodoDestino = $this->xml_base->importNode($this->Destino->importXML(), true);
		    $nodoUbicacion->appendChild($nodoDestino);
        }

        if($this->Domicilio)
        {
            $this->Domicilio->toXML();
            $nodoDomicilio = $this->xml_base->importNode($this->Domicilio->importXML(), true);
		    $nodoUbicacion->appendChild($nodoDomicilio);
        }

		$this->xml_base->appendChild($nodoUbicacion);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Ubicacion")->item(0);
		return $xml;
	}

    function addOrigen($FechaHoraSalida, $IDOrigen = null, $RFCRemitente = null, $NombreRemitente = null, $NumRegIdTrib = null, $ResidenciaFiscal = null, 
                        $NumEstacion = null, $NombreEstacion = null, $NavegacionTrafico = null)
    {
        $Origen = new Origen(
            $FechaHoraSalida,
            $IDOrigen, 
            $RFCRemitente, 
            $NombreRemitente, 
            $NumRegIdTrib, 
            $ResidenciaFiscal, 
            $NumEstacion, 
            $NombreEstacion, 
            $NavegacionTrafico
        );
        // $Origen->validar();
        $this->Origen = $Origen;
        return $Origen;
    }

    function addDestino($FechaHoraProgLlegada, $IDDestino = null, $RFCDestinatario = null, $NombreDestinatario = null, $NumRegIdTrib = null, $ResidenciaFiscal = null, 
                        $NumEstacion = null, $NombreEstacion = null, $NavegacionTrafico = null)
    {
        $Destino = new Destino(
            $FechaHoraProgLlegada,
            $IDDestino, 
            $RFCDestinatario, 
            $NombreDestinatario, 
            $NumRegIdTrib, 
            $ResidenciaFiscal, 
            $NumEstacion, 
            $NombreEstacion, 
            $NavegacionTrafico
        );
        // $Destino->validar();
        $this->Destino = $Destino;
        return $Destino;
    }

    function addDomicilio($Calle, $Estado, $Pais, $CodigoPostal, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
        $Domicilio = new Domicilio(
            $Calle, 
            $Estado, 
            $Pais, 
            $CodigoPostal, 
            $NumeroExterior, 
            $NumeroInterior, 
            $Colonia, 
            $Localidad, 
            $Referencia, 
            $Municipio
        );
        // $Domicilio->validar();
        $this->Domicilio = $Domicilio;
        return $Domicilio;
    }

}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Ubicaciones.Ubicacion.Origen

class Origen 
{
    // obligatorios
    var $FechaHoraSalida;

	// opcionales
	var $IDOrigen;
	var $RFCRemitente;
    var $NombreRemitente;
    var $NumRegIdTrib;
    var $ResidenciaFiscal;
    var $NumEstacion;
    var $NombreEstacion;
    var $NavegacionTrafico;

	var $xml_base;
	var $logger;

	function __construct($FechaHoraSalida, $IDOrigen = null, $RFCRemitente = null, $NombreRemitente = null, $NumRegIdTrib = null, $ResidenciaFiscal = null, 
                        $NumEstacion = null, $NombreEstacion = null, $NavegacionTrafico = null)
    {
		$this->xml_base = null;
        $this->FechaHoraSalida = $FechaHoraSalida;
        $this->IDOrigen = $IDOrigen;
        $this->RFCRemitente = $RFCRemitente;
        $this->NombreRemitente = $NombreRemitente;
        $this->NumRegIdTrib = $NumRegIdTrib;
        $this->ResidenciaFiscal = $ResidenciaFiscal;
        $this->NumEstacion = $NumEstacion;
        $this->NombreEstacion = $NombreEstacion;
        $this->NavegacionTrafico = $NavegacionTrafico;

		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['FechaHoraSalida'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Ubicaciones.Ubicacion.Origen validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Ubicaciones.Ubicacion.Origen Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoOrigen = $this->xml_base->createElement("cartaporte:Origen");
        $nodoOrigen->setAttribute('FechaHoraSalida',  $this->FechaHoraSalida);

        if($this->IDOrigen)
		    $nodoOrigen->setAttribute('IDOrigen',  $this->IDOrigen);
        if($this->RFCRemitente)
		    $nodoOrigen->setAttribute('RFCRemitente',  $this->RFCRemitente);
        if($this->NombreRemitente)
		    $nodoOrigen->setAttribute('NombreRemitente',  $this->NombreRemitente);
        if($this->NumRegIdTrib)
		    $nodoOrigen->setAttribute('NumRegIdTrib',  $this->NumRegIdTrib);
        if($this->ResidenciaFiscal)
		    $nodoOrigen->setAttribute('ResidenciaFiscal',  $this->ResidenciaFiscal);
        if($this->NumEstacion)
		    $nodoOrigen->setAttribute('NumEstacion',  $this->NumEstacion);
        if($this->NombreEstacion)
		    $nodoOrigen->setAttribute('NombreEstacion',  $this->NombreEstacion);
        if($this->NavegacionTrafico)
		    $nodoOrigen->setAttribute('NavegacionTrafico',  $this->NavegacionTrafico);

		$this->xml_base->appendChild($nodoOrigen);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Origen")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Ubicaciones.Ubicacion.Destino

class Destino 
{
	// opcionales
	var $IDDestino;
	var $RFCDestinatario;
    var $NombreDestinatario;
    var $NumRegIdTrib;
    var $ResidenciaFiscal;
    var $NumEstacion;
    var $NombreEstacion;
    var $NavegacionTrafico;
    var $FechaHoraProgLlegada;

	var $xml_base;
	var $logger;

	function __construct($FechaHoraProgLlegada, $IDDestino = null, $RFCDestinatario = null, $NombreDestinatario = null, $NumRegIdTrib = null, $ResidenciaFiscal = null, 
                        $NumEstacion = null, $NombreEstacion = null, $NavegacionTrafico = null)
    {
		$this->xml_base = null;
        $this->FechaHoraProgLlegada = $FechaHoraProgLlegada;
        $this->IDDestino = $IDDestino;
        $this->RFCDestinatario = $RFCDestinatario;
        $this->NombreDestinatario = $NombreDestinatario;
        $this->NumRegIdTrib = $NumRegIdTrib;
        $this->ResidenciaFiscal = $ResidenciaFiscal;
        $this->NumEstacion = $NumEstacion;
        $this->NombreEstacion = $NombreEstacion;
        $this->NavegacionTrafico = $NavegacionTrafico;

		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['FechaHoraProgLlegada'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Ubicaciones.Ubicacion.Destino validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Ubicaciones.Ubicacion.Destino Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoDestino = $this->xml_base->createElement("cartaporte:Destino");
        $nodoDestino->setAttribute('FechaHoraProgLlegada',  $this->FechaHoraProgLlegada);
        if($this->IDDestino)
		    $nodoDestino->setAttribute('IDDestino',  $this->IDDestino);
        if($this->RFCDestinatario)
		    $nodoDestino->setAttribute('RFCDestinatario',  $this->RFCDestinatario);
        if($this->NombreDestinatario)
		    $nodoDestino->setAttribute('NombreDestinatario',  $this->NombreDestinatario);
        if($this->NumRegIdTrib)
		    $nodoDestino->setAttribute('NumRegIdTrib',  $this->NumRegIdTrib);
        if($this->ResidenciaFiscal)
		    $nodoDestino->setAttribute('ResidenciaFiscal',  $this->ResidenciaFiscal);
        if($this->NumEstacion)
		    $nodoDestino->setAttribute('NumEstacion',  $this->NumEstacion);
        if($this->NombreEstacion)
		    $nodoDestino->setAttribute('NombreEstacion',  $this->NombreEstacion);
        if($this->NavegacionTrafico)
		    $nodoDestino->setAttribute('NavegacionTrafico',  $this->NavegacionTrafico);

		$this->xml_base->appendChild($nodoDestino);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Destino")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Ubicaciones.Ubicacion.Domicilio
// clase que crea el nodo de CartaPorte.FiguraTransporte.Operador.Domicilio
// clase que crea el nodo de CartaPorte.FiguraTransporte.Propietario.Domicilio
// clase que crea el nodo de CartaPorte.FiguraTransporte.Arrendatario.Domicilio
// clase que crea el nodo de CartaPorte.FiguraTransporte.Notificado.Domicilio

class Domicilio 
{
	// Obligatorios
	var $Calle;
	var $Estado;
	var $Pais;
	var $CodigoPostal;

	// opcionales
	var $NumeroExterior;
	var $NumeroInterior;
    var $Colonia;
    var $Localidad;
    var $Referencia;
    var $Municipio;

	var $xml_base;
	var $logger;

	function __construct($Calle, $Estado, $Pais, $CodigoPostal, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
		$this->xml_base = null;
        $this->Calle = $Calle;
        $this->Estado = $Estado;
        $this->Pais = $Pais;
        $this->CodigoPostal = $CodigoPostal;
        $this->NumeroExterior = $NumeroExterior;
        $this->NumeroInterior = $NumeroInterior;
        $this->Colonia = $Colonia;
        $this->Localidad = $Localidad;
        $this->Referencia = $Referencia;
        $this->Municipio = $Municipio;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['Calle', 'Estado', 'Pais', 'CodigoPostal'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Domicilio validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Domicilio Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoDomicilio = $this->xml_base->createElement("cartaporte:Domicilio");
        $nodoDomicilio->setAttribute('Calle',  $this->Calle);
        $nodoDomicilio->setAttribute('Estado',  $this->Estado);
        $nodoDomicilio->setAttribute('Pais',  $this->Pais);
        $nodoDomicilio->setAttribute('CodigoPostal',  $this->CodigoPostal);
        if($this->NumeroExterior)
		    $nodoDomicilio->setAttribute('NumeroExterior',  $this->NumeroExterior);
        if($this->NumeroInterior)
		    $nodoDomicilio->setAttribute('NumeroInterior',  $this->NumeroInterior);
        if($this->Colonia)
		    $nodoDomicilio->setAttribute('Colonia',  $this->Colonia);
        if($this->Localidad)
		    $nodoDomicilio->setAttribute('Localidad',  $this->Localidad);
        if($this->Referencia)
		    $nodoDomicilio->setAttribute('Referencia',  $this->Referencia);
        if($this->Municipio)
		    $nodoDomicilio->setAttribute('Municipio',  $this->Municipio);

		$this->xml_base->appendChild($nodoDomicilio);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Domicilio")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias

class Mercancias 
{
	// Obligatorios
	var $NumTotalMercancias;

	// opcionales
	var $PesoBrutoTotal;
	var $UnidadPeso;
    var $PesoNetoTotal;
    var $CargoPorTasacion;

    var $Mercancia = [];
    var $AutotransporteFederal;
    var $TransporteMaritimo;
    var $TransporteAereo;
    var $TransporteFerroviario;

	var $xml_base;
	var $logger;

	function __construct($NumTotalMercancias, $PesoBrutoTotal = null, $UnidadPeso = null, $PesoNetoTotal = null, $CargoPorTasacion = null)
    {
		$this->xml_base = null;
        $this->NumTotalMercancias = $NumTotalMercancias;
        $this->PesoBrutoTotal = $PesoBrutoTotal;
        $this->UnidadPeso = $UnidadPeso;
        $this->PesoNetoTotal = $PesoNetoTotal;
        $this->CargoPorTasacion = $CargoPorTasacion;

        $this->Mercancia = [];
        $this->AutotransporteFederal = null;
        $this->TransporteMaritimo = null;
        $this->TransporteAereo = null;
        $this->TransporteFerroviario = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['NumTotalMercancias'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias Campo Requerido: ' . $field);
			}
		}

        if(count($this->Mercancia) == 0)
        {
            $this->logger->write("Complemento.CartaPorte.Mercancias validar(): Debe existir un elemento de Mercancia.");
            throw new Exception('Complemento.CartaPorte.Mercancias: Debe existir un elemento de Mercancia.');
        }

		foreach($this->Mercancia as $Mercancia)
        {
			$Mercancia->validar();
		}

		if($this->AutotransporteFederal)
			$this->AutotransporteFederal->validar();

		if($this->TransporteMaritimo)
			$this->TransporteMaritimo->validar();

		if($this->TransporteAereo)
			$this->TransporteAereo->validar();

		if($this->TransporteFerroviario)
			$this->TransporteFerroviario->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoMercancias = $this->xml_base->createElement("cartaporte:Mercancias");
        $nodoMercancias->setAttribute('NumTotalMercancias',  $this->NumTotalMercancias);
        if($this->PesoBrutoTotal)
		    $nodoMercancias->setAttribute('PesoBrutoTotal',  $this->PesoBrutoTotal);
        if($this->UnidadPeso)
		    $nodoMercancias->setAttribute('UnidadPeso',  $this->UnidadPeso);
        if($this->PesoNetoTotal)
		    $nodoMercancias->setAttribute('PesoNetoTotal',  $this->PesoNetoTotal);
        if($this->CargoPorTasacion)
		    $nodoMercancias->setAttribute('CargoPorTasacion',  $this->CargoPorTasacion);

        if(!empty($this->Mercancia))
        {
            foreach ($this->Mercancia as $key => $Mercancia) 
            {
                $Mercancia->toXML();
                $nodoMercancia = $this->xml_base->importNode($Mercancia->importXML(), true);
                $nodoMercancias->appendChild($nodoMercancia);
            }
        }

        if(!empty($this->AutotransporteFederal))
        {
            $this->AutotransporteFederal->toXML();
            $nodoAutotransporteFederal = $this->xml_base->importNode($this->AutotransporteFederal->importXML(), true);
		    $nodoMercancias->appendChild($nodoAutotransporteFederal);
        }

        if(!empty($this->TransporteMaritimo))
        {
            $this->TransporteMaritimo->toXML();
            $nodoTransporteMaritimo = $this->xml_base->importNode($this->TransporteMaritimo->importXML(), true);
		    $nodoMercancias->appendChild($nodoTransporteMaritimo);
        }

        if(!empty($this->TransporteAereo))
        {
            $this->TransporteAereo->toXML();
            $nodoTransporteAereo = $this->xml_base->importNode($this->TransporteAereo->importXML(), true);
		    $nodoMercancias->appendChild($nodoTransporteAereo);
        }

        if(!empty($this->TransporteFerroviario))
        {
            $this->TransporteFerroviario->toXML();
            $nodoTransporteFerroviario = $this->xml_base->importNode($this->TransporteFerroviario->importXML(), true);
		    $nodoMercancias->appendChild($nodoTransporteFerroviario);
        }

		$this->xml_base->appendChild($nodoMercancias);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Mercancias")->item(0);
		return $xml;
	}

    function addMercancia($PesoEnKg, $BienesTransp = null, $ClaveSTCC = null, $Descripcion = null, $Cantidad = null, $ClaveUnidad = null, $Unidad = null, $Dimensiones = null, $MaterialPeligroso = null, 
                $CveMaterialPeligroso = null, $Embalaje = null, $DescripEmbalaje = null, $ValorMercancia = null, $Moneda = null, $FraccionArancelaria = null, $UUIDComercioExt = null)
    {
		$Mercancia = new Mercancia(
            $PesoEnKg, 
            $BienesTransp, 
            $ClaveSTCC, 
            $Descripcion, 
            $Cantidad, 
            $ClaveUnidad,
            $Unidad, 
            $Dimensiones, 
            $MaterialPeligroso, 
            $CveMaterialPeligroso, 
            $Embalaje, 
            $DescripEmbalaje, 
            $ValorMercancia, 
            $Moneda, 
            $FraccionArancelaria, 
            $UUIDComercioExt
        );
		
		// $Mercancia->validar();
		$this->Mercancia[] = $Mercancia;
		return $Mercancia;
    }

    function addAutotransporteFederal($PermSCT, $NumPermisoSCT, $NombreAseg, $NumPolizaSeguro)
    {
		$AutotransporteFederal = new AutotransporteFederal(
            $PermSCT, 
            $NumPermisoSCT, 
            $NombreAseg, 
            $NumPolizaSeguro
        );
		
		// $AutotransporteFederal->validar();
		$this->AutotransporteFederal = $AutotransporteFederal;
		return $AutotransporteFederal;
    }

    function addTransporteMaritimo($TipoEmbarcacion, $Matricula, $NumeroOMI, $NacionalidadEmbarc, $UnidadesDeArqBruto, $TipoCarga, $NumCertITC, $NombreAgenteNaviero, $NumAutorizacionNaviero, 
                $PermSCT = null, $NumPermisoSCT = null, $NombreAseg = null, $NumPolizaSeguro = null, $AnioEmbarcacion = null, $NombreEmbarc = null, $Eslora = null, 
                $Manga = null, $Calado = null, $LineaNaviera = null, $NumViaje = null, $NumConocEmbarc = null)
    {
		$TransporteMaritimo = new TransporteMaritimo(
            $TipoEmbarcacion, 
            $Matricula, 
            $NumeroOMI, 
            $NacionalidadEmbarc, 
            $UnidadesDeArqBruto, 
            $TipoCarga, 
            $NumCertITC, 
            $NombreAgenteNaviero, 
            $NumAutorizacionNaviero, 
            $PermSCT, 
            $NumPermisoSCT, 
            $NombreAseg, 
            $NumPolizaSeguro, 
            $AnioEmbarcacion, 
            $NombreEmbarc, 
            $Eslora, 
            $Manga, 
            $Calado, 
            $LineaNaviera, 
            $NumViaje, 
            $NumConocEmbarc
        );
		
		// $TransporteMaritimo->validar();
		$this->TransporteMaritimo = $TransporteMaritimo;
		return $TransporteMaritimo;
    }

    function addTransporteAereo($PermSCT, $NumPermisoSCT, $MatriculaAeronave, $NumeroGuia, $CodigoTransportista, $NombreAseg = null, $NumPolizaSeguro = null, $LugarContrato = null,
                        $RFCTransportista = null, $NumRegIdTribTranspor = null, $ResidenciaFiscalTranspor = null, $NombreTransportista = null, $RFCEmbarcador = null, 
                        $NumRegIdTribEmbarc = null, $ResidenciaFiscalEmbarc = null, $NombreEmbarcador = null)
    {
		$TransporteAereo = new TransporteAereo(
            $PermSCT, 
            $NumPermisoSCT, 
            $MatriculaAeronave, 
            $NumeroGuia, 
            $CodigoTransportista, 
            $NombreAseg,
            $NumPolizaSeguro, 
            $LugarContrato,
            $RFCTransportista, 
            $NumRegIdTribTranspor, 
            $ResidenciaFiscalTranspor, 
            $NombreTransportista, 
            $RFCEmbarcador, 
            $NumRegIdTribEmbarc, 
            $ResidenciaFiscalEmbarc, 
            $NombreEmbarcador
        );
		
		// $TransporteAereo->validar();
		$this->TransporteAereo = $TransporteAereo;
		return $TransporteAereo;
    }

    function addTransporteFerroviario($TipoDeServicio, $NombreAseg = null, $NumPolizaSeguro = null, $Concesionario = null)
    {
        $TransporteFerroviario = new TransporteFerroviario(
            $TipoDeServicio, 
            $NombreAseg, 
            $NumPolizaSeguro, 
            $Concesionario
        );

        // $TransporteFerroviario->validar();
        $this->TransporteFerroviario = $TransporteFerroviario; 
        return $TransporteFerroviario;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.Mercancia

class Mercancia
{
	// Obligatorios
	var $PesoEnKg;

	// opcionales
	var $BienesTransp;
	var $ClaveSTCC;
    var $Descripcion;
    var $Cantidad;
    var $ClaveUnidad;
    var $Unidad;
    var $Dimensiones;
    var $MaterialPeligroso;
    var $CveMaterialPeligroso;
    var $Embalaje;
    var $DescripEmbalaje;
    var $ValorMercancia;
    var $Moneda;
    var $FraccionArancelaria;
    var $UUIDComercioExt;

    var $CantidadTransporta = [];
    var $DetalleMercancia;

	var $xml_base;
	var $logger;

	function __construct($PesoEnKg, $BienesTransp = null, $ClaveSTCC = null, $Descripcion = null, $Cantidad = null, $ClaveUnidad = null, $Unidad = null, $Dimensiones = null, $MaterialPeligroso = null, 
                $CveMaterialPeligroso = null, $Embalaje = null, $DescripEmbalaje = null, $ValorMercancia = null, $Moneda = null, $FraccionArancelaria = null, $UUIDComercioExt = null)
    {
		$this->xml_base = null;
        $this->PesoEnKg = $PesoEnKg;
        $this->BienesTransp = $BienesTransp;
        $this->ClaveSTCC = $ClaveSTCC;
        $this->Descripcion = $Descripcion;
        $this->Cantidad = $Cantidad;
        $this->ClaveUnidad = $ClaveUnidad;
        $this->Unidad = $Unidad;
        $this->Dimensiones = $Dimensiones;
        $this->MaterialPeligroso = $MaterialPeligroso;
        $this->CveMaterialPeligroso = $CveMaterialPeligroso;
        $this->Embalaje = $Embalaje;
        $this->DescripEmbalaje = $DescripEmbalaje;
        $this->ValorMercancia = $ValorMercancia;
        $this->Moneda = $Moneda;
        $this->FraccionArancelaria = $FraccionArancelaria;
        $this->UUIDComercioExt = $UUIDComercioExt;

        $this->CantidadTransporta = [];
        $this->DetalleMercancia = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['PesoEnKg'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.Mercancia validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.Mercancia Campo Requerido: ' . $field);
			}
		}

		foreach($this->CantidadTransporta as $CantidadTransporta)
        {
			$CantidadTransporta->validar();
		}

		if($this->DetalleMercancia)
			$this->DetalleMercancia->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoMercancia = $this->xml_base->createElement("cartaporte:Mercancia");

        $nodoMercancia->setAttribute('PesoEnKg',  $this->PesoEnKg);
        if($this->BienesTransp)
		    $nodoMercancia->setAttribute('BienesTransp',  $this->BienesTransp);
        if($this->ClaveSTCC)
		    $nodoMercancia->setAttribute('ClaveSTCC',  $this->ClaveSTCC);
        if($this->Descripcion)
		    $nodoMercancia->setAttribute('Descripcion',  $this->Descripcion);
        if($this->Cantidad)
		    $nodoMercancia->setAttribute('Cantidad',  $this->Cantidad);
        if($this->ClaveUnidad)
		    $nodoMercancia->setAttribute('ClaveUnidad',  $this->ClaveUnidad);
        if($this->Unidad)
		    $nodoMercancia->setAttribute('Unidad',  $this->Unidad);
        if($this->Dimensiones)
		    $nodoMercancia->setAttribute('Dimensiones',  $this->Dimensiones);
        if($this->MaterialPeligroso)
		    $nodoMercancia->setAttribute('MaterialPeligroso',  $this->MaterialPeligroso);
        if($this->CveMaterialPeligroso)
		    $nodoMercancia->setAttribute('CveMaterialPeligroso',  $this->CveMaterialPeligroso);
        if($this->Embalaje)
		    $nodoMercancia->setAttribute('Embalaje',  $this->Embalaje);
        if($this->DescripEmbalaje)
		    $nodoMercancia->setAttribute('DescripEmbalaje',  $this->DescripEmbalaje);
        if($this->ValorMercancia)
		    $nodoMercancia->setAttribute('ValorMercancia',  $this->ValorMercancia);
        if($this->Moneda)
		    $nodoMercancia->setAttribute('Moneda',  $this->Moneda);
        if($this->FraccionArancelaria)
		    $nodoMercancia->setAttribute('FraccionArancelaria',  $this->FraccionArancelaria);
        if($this->UUIDComercioExt)
		    $nodoMercancia->setAttribute('UUIDComercioExt',  $this->UUIDComercioExt);

        if(!empty($this->CantidadTransporta))
        {
            foreach ($this->CantidadTransporta as $key => $CantidadTransporta) 
            {
                $CantidadTransporta->toXML();
                $nodoCantidadTransporta = $this->xml_base->importNode($CantidadTransporta->importXML(), true);
                $nodoMercancia->appendChild($nodoCantidadTransporta);
            }
        }

        if(!empty($this->DetalleMercancia))
        {
            $this->DetalleMercancia->toXML();
            $nodoDetalleMercancia = $this->xml_base->importNode($this->DetalleMercancia->importXML(), true);
		    $nodoMercancia->appendChild($nodoDetalleMercancia);
        }

		$this->xml_base->appendChild($nodoMercancia);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Mercancia")->item(0);
		return $xml;
	}

    function addCantidadTransporta($Cantidad, $IDOrigen, $IDDestino, $CvesTransporte = null)
    {
		$CantidadTransporta = new CantidadTransporta(
            $Cantidad, 
            $IDOrigen, 
            $IDDestino, 
            $CvesTransporte
        );
		
		// $CantidadTransporta->validar();
		$this->CantidadTransporta[] = $CantidadTransporta;
		return $CantidadTransporta;

    }

    function addDetalleMercancia($UnidadPeso, $PesoBruto, $PesoNeto, $PesoTara, $NumPiezas = null)
    {
		$DetalleMercancia = new DetalleMercancia(
            $UnidadPeso, 
            $PesoBruto, 
            $PesoNeto, 
            $PesoTara,
            $NumPiezas
        );
		
		// $DetalleMercancia->validar();
		$this->DetalleMercancia = $DetalleMercancia;
		return $DetalleMercancia;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.Mercancia.CantidadTransporta

class CantidadTransporta
{
	// Obligatorios
	var $Cantidad;
	var $IDOrigen;
	var $IDDestino;

	// opcionales
	var $CvesTransporte;

	var $xml_base;
	var $logger;

	function __construct($Cantidad, $IDOrigen, $IDDestino, $CvesTransporte = null)
    {
		$this->xml_base = null;
        $this->Cantidad = $Cantidad;
        $this->IDOrigen = $IDOrigen;
        $this->IDDestino = $IDDestino;
        $this->CvesTransporte = $CvesTransporte;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['Cantidad', 'IDOrigen', 'IDDestino'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.Mercancia.CantidadTransporta validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.Mercancia.CantidadTransporta Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoCantidadTransporta = $this->xml_base->createElement("cartaporte:CantidadTransporta");

        $nodoCantidadTransporta->setAttribute('Cantidad',  $this->Cantidad);
        $nodoCantidadTransporta->setAttribute('IDOrigen',  $this->IDOrigen);
        $nodoCantidadTransporta->setAttribute('IDDestino',  $this->IDDestino);
        if($this->CvesTransporte)
		    $nodoCantidadTransporta->setAttribute('CvesTransporte',  $this->CvesTransporte);

		$this->xml_base->appendChild($nodoCantidadTransporta);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:CantidadTransporta")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.Mercancia.DetalleMercancia

class DetalleMercancia
{
	// Obligatorios
	var $UnidadPeso;
	var $PesoBruto;
	var $PesoNeto;
	var $PesoTara;

	// opcionales
	var $NumPiezas;

	var $xml_base;
	var $logger;

	function __construct($UnidadPeso, $PesoBruto, $PesoNeto, $PesoTara, $NumPiezas = null)
    {
		$this->xml_base = null;
        $this->UnidadPeso = $UnidadPeso;
        $this->PesoBruto = $PesoBruto;
        $this->PesoNeto = $PesoNeto;
        $this->PesoTara = $PesoTara;
        $this->NumPiezas = $NumPiezas;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['UnidadPeso', 'PesoBruto', 'PesoNeto', 'PesoTara'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.Mercancia.DetalleMercancia validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.Mercancia.DetalleMercancia Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoDetalleMercancia = $this->xml_base->createElement("cartaporte:DetalleMercancia");

        $nodoDetalleMercancia->setAttribute('UnidadPeso',  $this->UnidadPeso);
        $nodoDetalleMercancia->setAttribute('PesoBruto',  $this->PesoBruto);
        $nodoDetalleMercancia->setAttribute('PesoNeto',  $this->PesoNeto);
        $nodoDetalleMercancia->setAttribute('PesoTara',  $this->PesoTara);
        if($this->NumPiezas)
		    $nodoDetalleMercancia->setAttribute('NumPiezas',  $this->NumPiezas);

		$this->xml_base->appendChild($nodoDetalleMercancia);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:DetalleMercancia")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.AutotransporteFederal

class AutotransporteFederal
{
	// Obligatorios
	var $PermSCT;
	var $NumPermisoSCT;
	var $NombreAseg;
	var $NumPolizaSeguro;

	var $IdentificacionVehicular;
    var $Remolques = [];

	var $xml_base;
	var $logger;

	function __construct($PermSCT, $NumPermisoSCT, $NombreAseg, $NumPolizaSeguro)
    {
		$this->xml_base = null;
        $this->PermSCT = $PermSCT;
        $this->NumPermisoSCT = $NumPermisoSCT;
        $this->NombreAseg = $NombreAseg;
        $this->NumPolizaSeguro = $NumPolizaSeguro;

        $this->Remolques = [];
        $this->IdentificacionVehicular = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['PermSCT', 'NumPermisoSCT', 'NombreAseg', 'NumPolizaSeguro'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.AutotransporteFederal validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.AutotransporteFederal Campo Requerido: ' . $field);
			}
		}

        if(count($this->Remolques) > 2)
        {
            $this->logger->write("Complemento.CartaPorte.Mercancias.AutotransporteFederal.Remolques. Debe existir un maximo de 2 elementos Remolque.");
            throw new Exception('Complemento.CartaPorte.Mercancias.AutotransporteFederal.Remolques Debe existir un maximo de 2 elementos Remolque.');
        }

		foreach($this->Remolques as $Remolques)
        {
			$Remolques->validar();
		}

        if(empty($this->IdentificacionVehicular))
        {
            $this->logger->write("Complemento.CartaPorte.Mercancias.AutotransporteFederal. Debe existir un elemento IdentificacionVehicular.");
            throw new Exception('Complemento.CartaPorte.Mercancias.AutotransporteFederal. Debe existir un elemento IdentificacionVehicular.');
        }

		if($this->IdentificacionVehicular)
			$this->IdentificacionVehicular->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoAutotransporteFederal = $this->xml_base->createElement("cartaporte:AutotransporteFederal");

        $nodoAutotransporteFederal->setAttribute('PermSCT',  $this->PermSCT);
        $nodoAutotransporteFederal->setAttribute('NumPermisoSCT',  $this->NumPermisoSCT);
        $nodoAutotransporteFederal->setAttribute('NombreAseg',  $this->NombreAseg);
        $nodoAutotransporteFederal->setAttribute('NumPolizaSeguro',  $this->NumPolizaSeguro);

        if(!empty($this->IdentificacionVehicular))
        {
            $this->IdentificacionVehicular->toXML();
            $nodoIdentificacionVehicular = $this->xml_base->importNode($this->IdentificacionVehicular->importXML(), true);
		    $nodoAutotransporteFederal->appendChild($nodoIdentificacionVehicular);
        }

        if(!empty($this->Remolques))
        {
		    $nodoRemolques = $this->xml_base->createElement("cartaporte:Remolques");

            foreach ($this->Remolques as $key => $Remolque) 
            {
                $Remolque->toXML();
                $nodoRemolque = $this->xml_base->importNode($Remolque->importXML(), true);
                $nodoRemolques->appendChild($nodoRemolque);
            }
            
		    $nodoAutotransporteFederal->appendChild($nodoRemolques);
        }

		$this->xml_base->appendChild($nodoAutotransporteFederal);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:AutotransporteFederal")->item(0);
		return $xml;
	}

    function addRemolque($SubTipoRem, $Placa)
    {
		$Remolque = new Remolque(
            $SubTipoRem, 
            $Placa
        );
		
		// $Remolque->validar();
		$this->Remolques[] = $Remolque;
		return $Remolque;
    }

    function addIdentificacionVehicular($ConfigVehicular, $PlacaVM, $AnioModeloVM)
    {
		$IdentificacionVehicular = new IdentificacionVehicular(
            $ConfigVehicular, 
            $PlacaVM, 
            $AnioModeloVM
        );
		
		// $IdentificacionVehicular->validar();
		$this->IdentificacionVehicular = $IdentificacionVehicular;
		return $IdentificacionVehicular;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.AutotransporteFederal.IdentificacionVehicular

class IdentificacionVehicular
{
	// Obligatorios
	var $ConfigVehicular;
	var $PlacaVM;
	var $AnioModeloVM;

	var $xml_base;
	var $logger;

	function __construct($ConfigVehicular, $PlacaVM, $AnioModeloVM)
    {
		$this->xml_base = null;
        $this->ConfigVehicular = $ConfigVehicular;
        $this->PlacaVM = $PlacaVM;
        $this->AnioModeloVM = $AnioModeloVM;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['ConfigVehicular', 'PlacaVM', 'AnioModeloVM'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.AutotransporteFederal.IdentificacionVehicular validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.AutotransporteFederal.IdentificacionVehicular Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoIdentificacionVehicular = $this->xml_base->createElement("cartaporte:IdentificacionVehicular");

        $nodoIdentificacionVehicular->setAttribute('ConfigVehicular',  $this->ConfigVehicular);
        $nodoIdentificacionVehicular->setAttribute('PlacaVM',  $this->PlacaVM);
        $nodoIdentificacionVehicular->setAttribute('AnioModeloVM',  $this->AnioModeloVM);

		$this->xml_base->appendChild($nodoIdentificacionVehicular);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:IdentificacionVehicular")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.AutotransporteFederal.Remolques.Remolque

class Remolque
{
	// Obligatorios
	var $SubTipoRem;
	var $Placa;

	var $xml_base;
	var $logger;

	function __construct($SubTipoRem, $Placa)
    {
		$this->xml_base = null;
        $this->SubTipoRem = $SubTipoRem;
        $this->Placa = $Placa;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['SubTipoRem', 'Placa'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.AutotransporteFederal.Remolques.Remolque validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.AutotransporteFederal.Remolques.Remolque Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoRemolque = $this->xml_base->createElement("cartaporte:Remolque");

        $nodoRemolque->setAttribute('SubTipoRem',  $this->SubTipoRem);
        $nodoRemolque->setAttribute('Placa',  $this->Placa);

		$this->xml_base->appendChild($nodoRemolque);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Remolque")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.TransporteMaritimo

class TransporteMaritimo
{
	// Obligatorios
	var $TipoEmbarcacion;
	var $Matricula;
	var $NumeroOMI;
	var $NacionalidadEmbarc;
	var $UnidadesDeArqBruto;
	var $TipoCarga;
	var $NumCertITC;
	var $NombreAgenteNaviero;
	var $NumAutorizacionNaviero;

	// opcionales
	var $PermSCT;
	var $NumPermisoSCT;
    var $NombreAseg;
    var $NumPolizaSeguro;
    var $AnioEmbarcacion;
    var $NombreEmbarc;
    var $Eslora;
    var $Manga;
    var $Calado;
    var $LineaNaviera;
    var $NumViaje;
    var $NumConocEmbarc;

    var $Contenedor = [];

	var $xml_base;
	var $logger;

	function __construct($TipoEmbarcacion, $Matricula, $NumeroOMI, $NacionalidadEmbarc, $UnidadesDeArqBruto, $TipoCarga, $NumCertITC, $NombreAgenteNaviero, $NumAutorizacionNaviero, 
                $PermSCT = null, $NumPermisoSCT = null, $NombreAseg = null, $NumPolizaSeguro = null, $AnioEmbarcacion = null, $NombreEmbarc = null, $Eslora = null, 
                $Manga = null, $Calado = null, $LineaNaviera = null, $NumViaje = null, $NumConocEmbarc = null)
    {
		$this->xml_base = null;
        $this->TipoEmbarcacion = $TipoEmbarcacion;
        $this->Matricula = $Matricula;
        $this->NumeroOMI = $NumeroOMI;
        $this->NacionalidadEmbarc = $NacionalidadEmbarc;
        $this->UnidadesDeArqBruto = $UnidadesDeArqBruto;
        $this->TipoCarga = $TipoCarga;
        $this->NumCertITC = $NumCertITC;
        $this->NombreAgenteNaviero = $NombreAgenteNaviero;
        $this->NumAutorizacionNaviero = $NumAutorizacionNaviero;
        $this->PermSCT = $PermSCT;
        $this->NumPermisoSCT = $NumPermisoSCT;
        $this->NombreAseg = $NombreAseg;
        $this->NumPolizaSeguro = $NumPolizaSeguro;
        $this->AnioEmbarcacion = $AnioEmbarcacion;
        $this->NombreEmbarc = $NombreEmbarc;
        $this->Eslora = $Eslora;
        $this->Manga = $Manga;
        $this->Calado = $Calado;
        $this->LineaNaviera = $LineaNaviera;        
        $this->NumViaje = $NumViaje;
        $this->NumConocEmbarc = $NumConocEmbarc;

        $this->Contenedor = [];

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoEmbarcacion', 'Matricula', 'NumeroOMI', 'NacionalidadEmbarc', 'UnidadesDeArqBruto', 'TipoCarga', 'NumCertITC', 'NombreAgenteNaviero', 'NumAutorizacionNaviero'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.TransporteMaritimo validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.TransporteMaritimo Campo Requerido: ' . $field);
			}
		}

        if(count($this->Contenedor) == 0)
        {
            $this->logger->write("Complemento.CartaPorte.Mercancias.TransporteMaritimo. Debe existir un elemento Contenedor.");
            throw new Exception('Complemento.CartaPorte.Mercancias.TransporteMaritimo. Debe existir un elemento Contenedor.');
        }

		foreach($this->Contenedor as $Contenedor)
        {
			$Contenedor->validar();
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoTransporteMaritimo = $this->xml_base->createElement("cartaporte:TransporteMaritimo");

        $nodoTransporteMaritimo->setAttribute('TipoEmbarcacion',  $this->TipoEmbarcacion);
        $nodoTransporteMaritimo->setAttribute('Matricula',  $this->Matricula);
        $nodoTransporteMaritimo->setAttribute('NumeroOMI',  $this->NumeroOMI);
        $nodoTransporteMaritimo->setAttribute('NacionalidadEmbarc',  $this->NacionalidadEmbarc);
        $nodoTransporteMaritimo->setAttribute('UnidadesDeArqBruto',  $this->UnidadesDeArqBruto);
        $nodoTransporteMaritimo->setAttribute('TipoCarga',  $this->TipoCarga);
        $nodoTransporteMaritimo->setAttribute('NumCertITC',  $this->NumCertITC);
        $nodoTransporteMaritimo->setAttribute('NombreAgenteNaviero',  $this->NombreAgenteNaviero);
        $nodoTransporteMaritimo->setAttribute('NumAutorizacionNaviero',  $this->NumAutorizacionNaviero);
        if($this->PermSCT)
		    $nodoTransporteMaritimo->setAttribute('PermSCT',  $this->PermSCT);
        if($this->NumPermisoSCT)
		    $nodoTransporteMaritimo->setAttribute('NumPermisoSCT',  $this->NumPermisoSCT);
        if($this->NombreAseg)
		    $nodoTransporteMaritimo->setAttribute('NombreAseg',  $this->NombreAseg);
        if($this->NumPolizaSeguro)
		    $nodoTransporteMaritimo->setAttribute('NumPolizaSeguro',  $this->NumPolizaSeguro);
        if($this->AnioEmbarcacion)
		    $nodoTransporteMaritimo->setAttribute('AnioEmbarcacion',  $this->AnioEmbarcacion);
        if($this->NombreEmbarc)
		    $nodoTransporteMaritimo->setAttribute('NombreEmbarc',  $this->NombreEmbarc);
        if($this->Eslora)
		    $nodoTransporteMaritimo->setAttribute('Eslora',  $this->Eslora);
        if($this->Manga)
		    $nodoTransporteMaritimo->setAttribute('Manga',  $this->Manga);
        if($this->Calado)
		    $nodoTransporteMaritimo->setAttribute('Calado',  $this->Calado);
        if($this->NumViaje)
		    $nodoTransporteMaritimo->setAttribute('NumViaje',  $this->NumViaje);
        if($this->ValorMercancia)
		    $nodoTransporteMaritimo->setAttribute('ValorMercancia',  $this->ValorMercancia);
        if($this->NumConocEmbarc)
		    $nodoTransporteMaritimo->setAttribute('NumConocEmbarc',  $this->NumConocEmbarc);

        if(!empty($this->Contenedor))
        {
            foreach ($this->Contenedor as $key => $Contenedor) 
            {
                $Contenedor->toXML();
                $nodoContenedor = $this->xml_base->importNode($Contenedor->importXML(), true);
                $nodoTransporteMaritimo->appendChild($nodoContenedor);
            }
        }

		$this->xml_base->appendChild($nodoTransporteMaritimo);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:TransporteMaritimo")->item(0);
		return $xml;
	}

    function addContenedor($MatriculaContenedor, $TipoContenedor, $NumPrecinto = null)
    {
		$Contenedor = new ContenedorTransporteMaritimo(
            $MatriculaContenedor, 
            $TipoContenedor, 
            $NumPrecinto
        );
		
		// $Contenedor->validar();
		$this->Contenedor[] = $Contenedor;
		return $Contenedor;

    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.TransporteMaritimo.Contenedor

class ContenedorTransporteMaritimo
{
	// Obligatorios
	var $MatriculaContenedor;
	var $TipoContenedor;

    // opcionales
    var $NumPrecinto;

	var $xml_base;
	var $logger;

	function __construct($MatriculaContenedor, $TipoContenedor, $NumPrecinto = null)
    {
		$this->xml_base = null;
        $this->MatriculaContenedor = $MatriculaContenedor;
        $this->TipoContenedor = $TipoContenedor;
        $this->NumPrecinto = $NumPrecinto;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['MatriculaContenedor', 'TipoContenedor'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.TransporteMaritimo.Contenedor validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.TransporteMaritimo.Contenedor Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoContenedor = $this->xml_base->createElement("cartaporte:Contenedor");

        $nodoContenedor->setAttribute('MatriculaContenedor',  $this->MatriculaContenedor);
        $nodoContenedor->setAttribute('TipoContenedor',  $this->TipoContenedor);
        if($this->NumPrecinto)
		    $nodoContenedor->setAttribute('NumPrecinto',  $this->NumPrecinto);

		$this->xml_base->appendChild($nodoContenedor);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Contenedor")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.TransporteAereo

class TransporteAereo
{
	// Obligatorios
	var $PermSCT;
	var $NumPermisoSCT;
	var $MatriculaAeronave;
    var $NumeroGuia;
    var $CodigoTransportista;


    // opcionales
    var $NombreAseg;
    var $NumPolizaSeguro;
    var $LugarContrato;
    var $RFCTransportista;
    var $NumRegIdTribTranspor;
    var $ResidenciaFiscalTranspor;
    var $NombreTransportista;
    var $RFCEmbarcador;
    var $NumRegIdTribEmbarc;
    var $ResidenciaFiscalEmbarc;
    var $NombreEmbarcador;

	var $xml_base;
	var $logger;

	function __construct($PermSCT, $NumPermisoSCT, $MatriculaAeronave, $NumeroGuia, $CodigoTransportista, $NombreAseg = null, $NumPolizaSeguro = null, $LugarContrato = null,
                        $RFCTransportista = null, $NumRegIdTribTranspor = null, $ResidenciaFiscalTranspor = null, $NombreTransportista = null, $RFCEmbarcador = null, 
                        $NumRegIdTribEmbarc = null, $ResidenciaFiscalEmbarc = null, $NombreEmbarcador = null)
    {
		$this->xml_base = null;
        $this->PermSCT = $PermSCT;
        $this->NumPermisoSCT = $NumPermisoSCT;
        $this->MatriculaAeronave = $MatriculaAeronave;
        $this->NumeroGuia = $NumeroGuia;
        $this->CodigoTransportista = $CodigoTransportista;
        $this->NombreAseg = $NombreAseg;
        $this->NumPolizaSeguro = $NumPolizaSeguro;
        $this->LugarContrato = $LugarContrato;
        $this->RFCTransportista = $RFCTransportista;
        $this->NumRegIdTribTranspor = $NumRegIdTribTranspor;
        $this->ResidenciaFiscalTranspor = $ResidenciaFiscalTranspor;
        $this->NombreTransportista = $NombreTransportista;
        $this->RFCEmbarcador = $RFCEmbarcador;
        $this->NumRegIdTribEmbarc = $NumRegIdTribEmbarc;
        $this->ResidenciaFiscalEmbarc = $ResidenciaFiscalEmbarc;
        $this->NombreEmbarcador = $NombreEmbarcador;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['PermSCT', 'NumPermisoSCT', 'MatriculaAeronave', 'NumeroGuia', 'CodigoTransportista'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.TransporteAereo validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.TransporteAereo Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoTransporteAereo = $this->xml_base->createElement("cartaporte:TransporteAereo");

        $nodoTransporteAereo->setAttribute('PermSCT',  $this->PermSCT);
        $nodoTransporteAereo->setAttribute('NumPermisoSCT',  $this->NumPermisoSCT);
        $nodoTransporteAereo->setAttribute('MatriculaAeronave',  $this->MatriculaAeronave);
        $nodoTransporteAereo->setAttribute('NumeroGuia',  $this->NumeroGuia);
        $nodoTransporteAereo->setAttribute('CodigoTransportista',  $this->CodigoTransportista);
        if($this->NombreAseg)
            $nodoTransporteAereo->setAttribute('NombreAseg',  $this->NombreAseg);
        if($this->NumPolizaSeguro)
            $nodoTransporteAereo->setAttribute('NumPolizaSeguro',  $this->NumPolizaSeguro);
        if($this->LugarContrato)
            $nodoTransporteAereo->setAttribute('LugarContrato',  $this->LugarContrato);
        if($this->RFCTransportista)
            $nodoTransporteAereo->setAttribute('RFCTransportista',  $this->RFCTransportista);
        if($this->NumRegIdTribTranspor)
            $nodoTransporteAereo->setAttribute('NumRegIdTribTranspor',  $this->NumRegIdTribTranspor);
        if($this->ResidenciaFiscalTranspor)
            $nodoTransporteAereo->setAttribute('ResidenciaFiscalTranspor',  $this->ResidenciaFiscalTranspor);
        if($this->NombreTransportista)
            $nodoTransporteAereo->setAttribute('NombreTransportista',  $this->NombreTransportista);
        if($this->RFCEmbarcador)
            $nodoTransporteAereo->setAttribute('RFCEmbarcador',  $this->RFCEmbarcador);
        if($this->NumRegIdTribEmbarc)
            $nodoTransporteAereo->setAttribute('NumRegIdTribEmbarc',  $this->NumRegIdTribEmbarc);
        if($this->ResidenciaFiscalEmbarc)
            $nodoTransporteAereo->setAttribute('ResidenciaFiscalEmbarc',  $this->ResidenciaFiscalEmbarc);
        if($this->NombreEmbarcador)
            $nodoTransporteAereo->setAttribute('NombreEmbarcador',  $this->NombreEmbarcador);

		$this->xml_base->appendChild($nodoTransporteAereo);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:TransporteAereo")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.TransporteFerroviario

class TransporteFerroviario
{
	// Obligatorios
	var $TipoDeServicio;

    // opcionales
    var $NombreAseg;
    var $NumPolizaSeguro;
    var $Concesionario;

    var $DerechosDePaso = [];
    var $Carro = [];

	var $xml_base;
	var $logger;

	function __construct($TipoDeServicio, $NombreAseg = null, $NumPolizaSeguro = null, $Concesionario = null)
    {
		$this->xml_base = null;
        $this->TipoDeServicio = $TipoDeServicio;
        $this->NombreAseg = $NombreAseg;
        $this->NumPolizaSeguro = $NumPolizaSeguro;
        $this->Concesionario = $Concesionario;

        $this->DerechosDePaso = [];
        $this->Carro = [];

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoDeServicio'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.TransporteFerroviario validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.TransporteFerroviario Campo Requerido: ' . $field);
			}
		}

        if(count($this->Carro) == 0)
        {
            $this->logger->write("Complemento.CartaPorte.Mercancias.TransporteFerroviario validar(): Debe Contener por lo menos un elemento TransporteFerroviario.Carro");
            throw new Exception('Complemento.CartaPorte.Mercancias.TransporteFerroviario. Debe Contener por lo menos un elemento TransporteFerroviario.Carro: ' . $field);
        }

		foreach($this->DerechosDePaso as $DerechosDePaso)
        {
			$DerechosDePaso->validar();
		}

        foreach($this->Carro as $Carro)
        {
			$Carro->validar();
        }
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoTransporteFerroviario = $this->xml_base->createElement("cartaporte:TransporteFerroviario");

        $nodoTransporteFerroviario->setAttribute('TipoDeServicio',  $this->TipoDeServicio);
        if($this->NombreAseg)
            $nodoTransporteFerroviario->setAttribute('NombreAseg',  $this->NombreAseg);
        if($this->NumPolizaSeguro)
            $nodoTransporteFerroviario->setAttribute('NumPolizaSeguro',  $this->NumPolizaSeguro);
        if($this->Concesionario)
            $nodoTransporteFerroviario->setAttribute('Concesionario',  $this->Concesionario);

        if(!empty($this->DerechosDePaso))
        {
            foreach ($this->DerechosDePaso as $key => $DerechosDePaso) 
            {
                $DerechosDePaso->toXML();
                $nodoDerechosDePaso = $this->xml_base->importNode($DerechosDePaso->importXML(), true);
                $nodoTransporteFerroviario->appendChild($nodoDerechosDePaso);
            }
        }

        if(!empty($this->Carro))
        {
            foreach ($this->Carro as $key => $Carro) 
            {
                $Carro->toXML();
                $nodoCarro = $this->xml_base->importNode($Carro->importXML(), true);
                $nodoTransporteFerroviario->appendChild($nodoCarro);
            }
        }

		$this->xml_base->appendChild($nodoTransporteFerroviario);
    }

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:TransporteFerroviario")->item(0);
		return $xml;
	}

    function addDerechosDePaso($TipoDerechoDePaso, $KilometrajePagado)
    {
        $DerechosDePaso = new DerechosDePaso(
            $TipoDerechoDePaso, 
            $KilometrajePagado
        );

        // $DerechosDePaso->validar();
        $this->DerechosDePaso[] = $DerechosDePaso; 
        return $DerechosDePaso;
    }

    function addCarro($TipoCarro, $MatriculaCarro, $GuiaCarro, $ToneladasNetasCarro)
    {
        $Carro = new Carro(
            $TipoCarro, 
            $MatriculaCarro, 
            $GuiaCarro, 
            $ToneladasNetasCarro
        );

        // $Carro->validar();
        $this->Carro[] = $Carro; 
        return $Carro;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.TransporteFerroviario.DerechosDePaso

class DerechosDePaso
{
	// Obligatorios
	var $TipoDerechoDePaso;
	var $KilometrajePagado;

	var $xml_base;
	var $logger;

	function __construct($TipoDerechoDePaso, $KilometrajePagado)
    {
		$this->xml_base = null;
        $this->TipoDerechoDePaso = $TipoDerechoDePaso;
        $this->KilometrajePagado = $KilometrajePagado;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoDerechoDePaso', 'KilometrajePagado'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.TransporteFerroviario.DerechosDePaso validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.TransporteFerroviario.DerechosDePaso Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoDerechosDePaso = $this->xml_base->createElement("cartaporte:DerechosDePaso");

        $nodoDerechosDePaso->setAttribute('TipoDerechoDePaso',  $this->TipoDerechoDePaso);
        $nodoDerechosDePaso->setAttribute('KilometrajePagado',  $this->KilometrajePagado);

		$this->xml_base->appendChild($nodoDerechosDePaso);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:DerechosDePaso")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.TransporteFerroviario.Carro

class Carro
{
	// Obligatorios
	var $TipoCarro;
	var $MatriculaCarro;
	var $GuiaCarro;
	var $ToneladasNetasCarro;

    var $Contenedor = [];

	var $xml_base;
	var $logger;

	function __construct($TipoCarro, $MatriculaCarro, $GuiaCarro, $ToneladasNetasCarro)
    {
		$this->xml_base = null;
        $this->TipoCarro = $TipoCarro;
        $this->MatriculaCarro = $MatriculaCarro;
        $this->GuiaCarro = $GuiaCarro;
        $this->ToneladasNetasCarro = $ToneladasNetasCarro;

        $this->Contenedor = [];

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoCarro', 'MatriculaCarro', 'GuiaCarro', 'ToneladasNetasCarro'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.TransporteFerroviario.Carro validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.TransporteFerroviario.Carro Campo Requerido: ' . $field);
			}
		}

		foreach($this->Contenedor as $Contenedor)
        {
			$Contenedor->validar();
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoCarro = $this->xml_base->createElement("cartaporte:Carro");

        $nodoCarro->setAttribute('TipoCarro',  $this->TipoCarro);
        $nodoCarro->setAttribute('MatriculaCarro',  $this->MatriculaCarro);
        $nodoCarro->setAttribute('GuiaCarro',  $this->GuiaCarro);
        $nodoCarro->setAttribute('ToneladasNetasCarro',  $this->ToneladasNetasCarro);

        if(!empty($this->Contenedor))
        {
            foreach ($this->Contenedor as $key => $Contenedor) 
            {
                $Contenedor->toXML();
                $nodoContenedor = $this->xml_base->importNode($Contenedor->importXML(), true);
                $nodoCarro->appendChild($nodoContenedor);
            }
        }

		$this->xml_base->appendChild($nodoCarro);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Carro")->item(0);
		return $xml;
	}

    function addContenedor($TipoContenedor, $PesoContenedorVacio, $PesoNetoMercancia)
    {
        $Contenedor = new ContenedorCarro(
            $TipoContenedor, 
            $PesoContenedorVacio, 
            $PesoNetoMercancia
        );

        // $Contenedor->validar();
        $this->Contenedor[] = $Contenedor; 
        return $Contenedor;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.Mercancias.TransporteFerroviario.Carro.Contenedor

class ContenedorCarro
{
	// Obligatorios
	var $TipoContenedor;
	var $PesoContenedorVacio;
    var $PesoNetoMercancia;

	var $xml_base;
	var $logger;

	function __construct($TipoContenedor, $PesoContenedorVacio, $PesoNetoMercancia)
    {
		$this->xml_base = null;
        $this->TipoContenedor = $TipoContenedor;
        $this->PesoContenedorVacio = $PesoContenedorVacio;
        $this->PesoNetoMercancia = $PesoNetoMercancia;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoContenedor', 'PesoContenedorVacio', 'PesoNetoMercancia'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.Mercancias.TransporteFerroviario.Carro.Contenedor validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.Mercancias.TransporteFerroviario.Carro.Contenedor Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoContenedor = $this->xml_base->createElement("cartaporte:Contenedor");

        $nodoContenedor->setAttribute('TipoContenedor',  $this->TipoContenedor);
        $nodoContenedor->setAttribute('PesoContenedorVacio',  $this->PesoContenedorVacio);
        $nodoContenedor->setAttribute('PesoNetoMercancia',  $this->PesoNetoMercancia);

		$this->xml_base->appendChild($nodoContenedor);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Contenedor")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.FiguraTransporte

class FiguraTransporte
{
	// Obligatorios
	var $CveTransporte;

    var $Operadores = [];
    var $Propietario = [];
    var $Arrendatario = [];
    var $Notificado = [];

	var $xml_base;
	var $logger;

	function __construct($CveTransporte)
    {
		$this->xml_base = null;
        $this->CveTransporte = $CveTransporte;

        $this->Operadores = [];
        $this->Propietario = [];
        $this->Arrendatario = [];
        $this->Notificado = [];

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['CveTransporte'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte.FiguraTransporte validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte.FiguraTransporte Campo Requerido: ' . $field);
			}
		}

		foreach($this->Operadores as $Operadores)
        {
			$Operadores->validar();
		}

		foreach($this->Propietario as $Propietario)
        {
			$Propietario->validar();
		}

		foreach($this->Arrendatario as $Arrendatario)
        {
			$Arrendatario->validar();
		}

		foreach($this->Notificado as $Notificado)
        {
			$Notificado->validar();
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoFiguraTransporte = $this->xml_base->createElement("cartaporte:FiguraTransporte");

        $nodoFiguraTransporte->setAttribute('CveTransporte',  $this->CveTransporte);

        if(!empty($this->Operadores))
        {
		    $nodoOperadores = $this->xml_base->createElement("cartaporte:Operadores");
		    $nodoFiguraTransporte->appendChild($nodoOperadores);

            foreach ($this->Operadores as $key => $Operador) 
            {
                $Operador->toXML();
                $nodoOperador = $this->xml_base->importNode($Operador->importXML(), true);
                $nodoOperadores->appendChild($nodoOperador);
            }
        }

        if(!empty($this->Propietario))
        {
            foreach ($this->Propietario as $key => $Propietario) 
            {
                $Propietario->toXML();
                $nodoPropietario = $this->xml_base->importNode($Propietario->importXML(), true);
                $nodoFiguraTransporte->appendChild($nodoPropietario);
            }
        }

        if(!empty($this->Arrendatario))
        {
            foreach ($this->Arrendatario as $key => $Arrendatario) 
            {
                $Arrendatario->toXML();
                $nodoArrendatario = $this->xml_base->importNode($Arrendatario->importXML(), true);
                $nodoFiguraTransporte->appendChild($nodoArrendatario);
            }
        }

        if(!empty($this->Notificado))
        {
            foreach ($this->Notificado as $key => $Notificado) 
            {
                $Notificado->toXML();
                $nodoNotificado = $this->xml_base->importNode($Notificado->importXML(), true);
                $nodoFiguraTransporte->appendChild($nodoNotificado);
            }
        }

		$this->xml_base->appendChild($nodoFiguraTransporte);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:FiguraTransporte")->item(0);
		return $xml;
	}

    function addOperador($RFCOperador = null, $NumLicencia = null, $NombreOperador = null, $NumRegIdTribOperador = null, $ResidenciaFiscalOperador = null)
    {
        $Operador = new Operador(
            $RFCOperador, 
            $NumLicencia, 
            $NombreOperador, 
            $NumRegIdTribOperador, 
            $ResidenciaFiscalOperador
        );

        // $Operador->validar();
        $this->Operadores[] = $Operador; 
        return $Operador;
    }

    function addPropietario($RFCPropietario = null, $NombrePropietario = null, $NumRegIdTribPropietario = null, $ResidenciaFiscalPropietario = null)
    {
        $Propietario = new Propietario(
            $RFCPropietario, 
            $NombrePropietario, 
            $NumRegIdTribPropietario, 
            $ResidenciaFiscalPropietario
        );

        // $Propietario->validar();
        $this->Propietario[] = $Propietario; 
        return $Propietario;
    }

    function addArrendatario($RFCArrendatario = null, $NombreArrendatario = null, $NumRegIdTribArrendatario = null, $ResidenciaFiscalArrendatario = null)
    {
        $Arrendatario = new Arrendatario(
            $RFCArrendatario, 
            $NombreArrendatario, 
            $NumRegIdTribArrendatario, 
            $ResidenciaFiscalArrendatario
        );

        // $Arrendatario->validar();
        $this->Arrendatario[] = $Arrendatario; 
        return $Arrendatario;
    }

    function addNotificado($RFCNotificado = null, $NombreNotificado = null, $NumRegIdTribNotificado = null, $ResidenciaFiscalNotificado = null)
    {
        $Notificado = new Notificado(
            $RFCNotificado, 
            $NombreNotificado, 
            $NumRegIdTribNotificado, 
            $ResidenciaFiscalNotificado
        );

        // $Notificado->validar();
        $this->Notificado[] = $Notificado; 
        return $Notificado;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.FiguraTransporte.Operador

class Operador
{
    // opcionales
    var $RFCOperador;
    var $NumLicencia;
    var $NombreOperador;
    var $NumRegIdTribOperador;
    var $ResidenciaFiscalOperador;

    var $Domicilio = null;

	var $xml_base;
	var $logger;

	function __construct($RFCOperador = null, $NumLicencia = null, $NombreOperador = null, $NumRegIdTribOperador = null, $ResidenciaFiscalOperador = null)
    {
		$this->xml_base = null;
        $this->RFCOperador = $RFCOperador;
        $this->NumLicencia = $NumLicencia;
        $this->NombreOperador = $NombreOperador;
        $this->NumRegIdTribOperador = $NumRegIdTribOperador;
        $this->ResidenciaFiscalOperador = $ResidenciaFiscalOperador;

        $this->Domicilio = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        if($this->Domicilio)
            $this->Domicilio->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoOperador = $this->xml_base->createElement("cartaporte:Operador");

        if($this->RFCOperador)
            $nodoOperador->setAttribute('RFCOperador',  $this->RFCOperador);
        if($this->NumLicencia)
            $nodoOperador->setAttribute('NumLicencia',  $this->NumLicencia);
        if($this->NombreOperador)
            $nodoOperador->setAttribute('NombreOperador',  $this->NombreOperador);
        if($this->NumRegIdTribOperador)
            $nodoOperador->setAttribute('NumRegIdTribOperador',  $this->NumRegIdTribOperador);
        if($this->ResidenciaFiscalOperador)
            $nodoOperador->setAttribute('ResidenciaFiscalOperador',  $this->ResidenciaFiscalOperador);

        if(!empty($this->Domicilio))
        {
            $this->Domicilio->toXML();
            $nodoDomicilio = $this->xml_base->importNode($this->Domicilio->importXML(), true);
            $nodoOperador->appendChild($nodoDomicilio);
        }

		$this->xml_base->appendChild($nodoOperador);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Operador")->item(0);
		return $xml;
	}

    function addDomicilio($Calle, $Estado, $Pais, $CodigoPostal, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
        $Domicilio = new Domicilio(
            $Calle, 
            $Estado, 
            $Pais, 
            $CodigoPostal, 
            $NumeroExterior, 
            $NumeroInterior, 
            $Colonia, 
            $Localidad, 
            $Referencia, 
            $Municipio
        );
        // $Domicilio->validar();
        $this->Domicilio = $Domicilio;
        return $Domicilio;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.FiguraTransporte.Propietario

class Propietario
{
    // opcionales
    var $RFCPropietario;
    var $NombrePropietario;
    var $NumRegIdTribPropietario;
    var $ResidenciaFiscalPropietario;

    var $Domicilio = null;

	var $xml_base;
	var $logger;

	function __construct($RFCPropietario = null, $NombrePropietario = null, $NumRegIdTribPropietario = null, $ResidenciaFiscalPropietario = null)
    {
		$this->xml_base = null;
        $this->RFCPropietario = $RFCPropietario;
        $this->NombrePropietario = $NombrePropietario;
        $this->NumRegIdTribPropietario = $NumRegIdTribPropietario;
        $this->ResidenciaFiscalPropietario = $ResidenciaFiscalPropietario;

        $this->Domicilio = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        if($this->Domicilio)
            $this->Domicilio->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoPropietario = $this->xml_base->createElement("cartaporte:Propietario");

        if($this->RFCPropietario)
            $nodoPropietario->setAttribute('RFCPropietario',  $this->RFCPropietario);
        if($this->NombrePropietario)
            $nodoPropietario->setAttribute('NombrePropietario',  $this->NombrePropietario);
        if($this->NumRegIdTribPropietario)
            $nodoPropietario->setAttribute('NumRegIdTribPropietario',  $this->NumRegIdTribPropietario);
        if($this->ResidenciaFiscalPropietario)
            $nodoPropietario->setAttribute('ResidenciaFiscalPropietario',  $this->ResidenciaFiscalPropietario);

        if(!empty($this->Domicilio))
        {
            $this->Domicilio->toXML();
            $nodoDomicilio = $this->xml_base->importNode($this->Domicilio->importXML(), true);
            $nodoPropietario->appendChild($nodoDomicilio);
        }

		$this->xml_base->appendChild($nodoPropietario);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Propietario")->item(0);
		return $xml;
	}

    function addDomicilio($Calle, $Estado, $Pais, $CodigoPostal, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
        $Domicilio = new Domicilio(
            $Calle, 
            $Estado, 
            $Pais, 
            $CodigoPostal, 
            $NumeroExterior, 
            $NumeroInterior, 
            $Colonia, 
            $Localidad, 
            $Referencia, 
            $Municipio
        );
        // $Domicilio->validar();
        $this->Domicilio = $Domicilio;
        return $Domicilio;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.FiguraTransporte.Arrendatario

class Arrendatario
{
    // opcionales
    var $RFCArrendatario;
    var $NombreArrendatario;
    var $NumRegIdTribArrendatario;
    var $ResidenciaFiscalArrendatario;

    var $Domicilio = null;

	var $xml_base;
	var $logger;

	function __construct($RFCArrendatario = null, $NombreArrendatario = null, $NumRegIdTribArrendatario = null, $ResidenciaFiscalArrendatario = null)
    {
		$this->xml_base = null;
        $this->RFCArrendatario = $RFCArrendatario;
        $this->NombreArrendatario = $NombreArrendatario;
        $this->NumRegIdTribArrendatario = $NumRegIdTribArrendatario;
        $this->ResidenciaFiscalArrendatario = $ResidenciaFiscalArrendatario;

        $this->Domicilio = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        if($this->Domicilio)
            $this->Domicilio->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoArrendatario = $this->xml_base->createElement("cartaporte:Arrendatario");

        if($this->RFCArrendatario)
            $nodoArrendatario->setAttribute('RFCArrendatario',  $this->RFCArrendatario);
        if($this->NombreArrendatario)
            $nodoArrendatario->setAttribute('NombreArrendatario',  $this->NombreArrendatario);
        if($this->NumRegIdTribArrendatario)
            $nodoArrendatario->setAttribute('NumRegIdTribArrendatario',  $this->NumRegIdTribArrendatario);
        if($this->ResidenciaFiscalArrendatario)
            $nodoArrendatario->setAttribute('ResidenciaFiscalArrendatario',  $this->ResidenciaFiscalArrendatario);

        if(!empty($this->Domicilio))
        {
            $this->Domicilio->toXML();
            $nodoDomicilio = $this->xml_base->importNode($this->Domicilio->importXML(), true);
            $nodoArrendatario->appendChild($nodoDomicilio);
        }

		$this->xml_base->appendChild($nodoArrendatario);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Arrendatario")->item(0);
		return $xml;
	}

    function addDomicilio($Calle, $Estado, $Pais, $CodigoPostal, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
        $Domicilio = new Domicilio(
            $Calle, 
            $Estado, 
            $Pais, 
            $CodigoPostal, 
            $NumeroExterior, 
            $NumeroInterior, 
            $Colonia, 
            $Localidad, 
            $Referencia, 
            $Municipio
        );
        // $Domicilio->validar();
        $this->Domicilio = $Domicilio;
        return $Domicilio;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte.FiguraTransporte.Notificado

class Notificado
{
    // opcionales
    var $RFCNotificado;
    var $NombreNotificado;
    var $NumRegIdTribNotificado;
    var $ResidenciaFiscalNotificado;

    var $Domicilio = null;

	var $xml_base;
	var $logger;

	function __construct($RFCNotificado = null, $NombreNotificado = null, $NumRegIdTribNotificado = null, $ResidenciaFiscalNotificado = null)
    {
		$this->xml_base = null;
        $this->RFCNotificado = $RFCNotificado;
        $this->NombreNotificado = $NombreNotificado;
        $this->NumRegIdTribNotificado = $NumRegIdTribNotificado;
        $this->ResidenciaFiscalNotificado = $ResidenciaFiscalNotificado;

        $this->Domicilio = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        if($this->Domicilio)
            $this->Domicilio->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoNotificado = $this->xml_base->createElement("cartaporte:Notificado");

        if($this->RFCNotificado)
            $nodoNotificado->setAttribute('RFCNotificado',  $this->RFCNotificado);
        if($this->NombreNotificado)
            $nodoNotificado->setAttribute('NombreNotificado',  $this->NombreNotificado);
        if($this->NumRegIdTribNotificado)
            $nodoNotificado->setAttribute('NumRegIdTribNotificado',  $this->NumRegIdTribNotificado);
        if($this->ResidenciaFiscalNotificado)
            $nodoNotificado->setAttribute('ResidenciaFiscalNotificado',  $this->ResidenciaFiscalNotificado);

        if(!empty($this->Domicilio))
        {
            $this->Domicilio->toXML();
            $nodoDomicilio = $this->xml_base->importNode($this->Domicilio->importXML(), true);
            $nodoNotificado->appendChild($nodoDomicilio);
        }

		$this->xml_base->appendChild($nodoNotificado);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte:Notificado")->item(0);
		return $xml;
	}

    function addDomicilio($Calle, $Estado, $Pais, $CodigoPostal, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
        $Domicilio = new Domicilio(
            $Calle, 
            $Estado, 
            $Pais, 
            $CodigoPostal, 
            $NumeroExterior, 
            $NumeroInterior, 
            $Colonia, 
            $Localidad, 
            $Referencia, 
            $Municipio
        );
        // $Domicilio->validar();
        $this->Domicilio = $Domicilio;
        return $Domicilio;
    }
}