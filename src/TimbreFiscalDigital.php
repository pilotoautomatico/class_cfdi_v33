<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

use Exception;
use DOMDocument;

class TimbreFiscalDigital {
	var $SchemaLocation = "http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd";
	var $Tfd = "http://www.sat.gob.mx/TimbreFiscalDigital";
	var $Xsi = "http://www.w3.org/2001/XMLSchema-instance";
	var $Version;
	var $UUID;
	var $FechaTimbrado;
	var $RfcProvCertif;
	var $SelloCFD;
	var $NoCertificadoSAT;
	var $SelloSAT;

	var $xml_base;
	var $logger;

	/**
	 * Clase que se encarga de recibir el string del XML para posteriormente añadirlo al xml base del CFDI
	 * recibe el string del xml del la respuesta del pac, lo instancia en un objeto dom document, que posteriormente
	 * enviara al toXML del comprobante para añadirlo una vez timbrado el XML
	 * @params string $timbreXML
	 *
	 * @return DOMdocument object
	 */
	function __construct($Version, $UUID, $FechaTimbrado, $RfcProvCertif, $SelloCFD, $NoCertificadoSAT, $SelloSAT) {
		$this -> Version = $Version;
		$this -> UUID = $UUID;
		$this -> FechaTimbrado = $FechaTimbrado;
		$this -> RfcProvCertif = $RfcProvCertif;
		$this -> SelloCFD = $SelloCFD;
		$this -> NoCertificadoSAT = $NoCertificadoSAT;
		$this -> SelloSAT = $SelloSAT;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array('Version', 'UUID', 'FechaTimbrado', 'RfcProvCertif', "SelloCFD", "NoCertificadoSAT", "SelloSAT");
		foreach ($required as $field) {
			if (!isset($this -> $field) || $this -> $field === '') {
				$this->logger->write("TimbreFiscalDigital validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('TimbreFiscalDigital Campo Requerido: ' . $field);
			}
		}
	}

	//Aqui crea 2 elementos uno que va a ser el base y el otro simplemente parsea el string a un DOMDocument para luego asi importar el nodo
	function toXML() {
		$this -> xml_base = new DOMDocument();
		$timbre = $this -> xml_base -> createElement('tfd:TimbreFiscalDigital');
		$this -> xml_base -> appendChild($timbre);
		$timbre -> setAttribute('xmlns:tfd', $this -> Tfd);
		$timbre -> setAttribute('xmlns:xsi', $this -> Xsi);
		$timbre -> setAttribute('xsi:schemaLocation', $this -> SchemaLocation);
		$timbre -> setAttribute('Version', $this -> Version);
		$timbre -> setAttribute('UUID', $this -> UUID);
		$timbre -> setAttribute('FechaTimbrado', $this -> FechaTimbrado);
		$timbre -> setAttribute('RfcProvCertif', $this -> RfcProvCertif);
		$timbre -> setAttribute('SelloCFD', $this -> SelloCFD);
		$timbre -> setAttribute('NoCertificadoSAT', $this -> NoCertificadoSAT);
		$timbre -> setAttribute('SelloSAT', $this -> SelloSAT);
	}

	function toStringXML() {
		return $this -> xml_base -> saveXML();
	}

	function importXML() {
		$xml = $this -> xml_base -> getElementsByTagName("tfd:TimbreFiscalDigital") -> item(0);
		return $xml;
	}
}
?>