<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\EmisorV4;
Use cfdi\ReceptorV4;
Use cfdi\ConceptoV4;
Use cfdi\CfdisRelacionados;
Use cfdi\TrasladoConcepto;
Use cfdi\RetencionConcepto;
Use cfdi\TrasladoGlobal;
Use cfdi\TrasladoGlobalV4;
Use cfdi\RetencionGlobal;
Use cfdi\TimbreFiscalDigital;
Use cfdi\Addenda;
Use cfdi\ImpuestosLocales;
Use cfdi\RetencionLocal;
Use cfdi\TrasladoLocal;
Use cfdi\Nomina;
Use cfdi\NominaData;
Use cfdi\CartaPorte;
Use cfdi\CartaPorte20;
Use cfdi\CartaPorte30;
Use cfdi\CartaPorte31;
Use cfdi\ComplementoConceptoIedu;
Use cfdi\ComplementoConceptoTerceros;
Use cfdi\Complemento;
Use cfdi\Csd;
Use cfdi\InformacionGlobal;
Use cfdi\DOMValidatorV4;
Use cfdi\Data\Arrays;
Use cfdi\Logger;

Use Exception;
Use DOMDocument;
Use XSLTProcessor;
use DateTime;
use DateTimeZone;

class ComprobanteV4 {
	//normales
	public $Version;
	public $Serie;
	public $Folio;
	public $Fecha;
	public $Sello;
	public $NoCertificado;
	public $Certificado;
	public $SubTotal;
	public $Moneda;
	public $Total;
	public $TipoDeComprobante;
	public $FormaPago;
	public $MetodoPago;
	public $CondicionesDePago;
	public $Descuento;
	public $TipoCambio;
	public $Confirmacion;
	public $LugarExpedicion;
	public $Exportacion;
	
	//objetos
	var $xml_base;
	var $Emisor;
	var $Receptor;
	var $Conceptos = array();
	var $Traslados = array();
	var $Retenciones = array();
	var $CfdisRelacionados = array();
	var $TimbreFiscalDigital;
	var $Addenda;
	var $Nomina;
	var $CartaPorte;
	var $CartaPorte20;
	var $CartaPorte30;
	var $CartaPorte31;
	var $Complemento;
	var $ImpuestosLocales;
	var $Pagos;
	var $InformacionGlobal;

	var $cer;
	var $key;
	var $PorcentajeVariacionMoneda;
	var $Decimales;
	var $TotalTraslados = 0;
	var $TotalRetenciones = 0;
	var $TotalConceptos = 0;
	var $TotalDescuento = 0;
	var $PagosVersion;
	
	var $logger;

	public function __construct(){
		$this->logger = new Logger(); //clase para escribir logs
		$this->logger->write('Entra newComprobante V4 [__construct()]');
		$this->Conceptos = array();
		$this->Traslados = array();
		$this->Retenciones = array();
		$this->TotalTraslados = 0;
		$this->TotalRetenciones = 0;
		$this->TotalConceptos = 0;
		$this->TotalDescuento = 0;
	}
	
	/*
	 * Agrega Valores general del Comprobante
	 * */
	public function addGenerales($NoCertificado, $SubTotal, $Moneda, $Total, $TipoDeComprobante, $FormaDePago = null, $TipoCambio = null,
								$LugarExpedicion, $MetodoPago = null, $Serie = null, $Folio = null, $Certificado = null, $CondicionesDePago = null,
								$Descuento = null, $Version = null, $Sello = null, $Fecha = null, $Confirmacion = null, $Exportacion) {
		$this->Version = trim($Version) ? trim($Version) : '4.0';
		$this->Serie = trim($Serie);
		$this->Folio = trim($Folio);
		$this->Fecha = $Fecha ? $Fecha : date("Y-m-d\TH:i:s");
		$this->NoCertificado = trim($NoCertificado);
		$this->Certificado = $Certificado;
		$this->Sello = trim($Sello);
		$this->SubTotal = round($SubTotal, 6);
		$this->Moneda = $Moneda;
		$this->Total = $Total;
		$this->TipoDeComprobante = $TipoDeComprobante;
		$this->FormaPago = $FormaDePago;
		$this->CondicionesDePago = trim($CondicionesDePago);
		$this->Descuento = $Descuento;
		$this->TipoCambio = $TipoCambio;
		$this->LugarExpedicion = $LugarExpedicion;
		$this->MetodoPago = $MetodoPago;
		$this->Confirmacion = trim($Confirmacion);
		$this->Exportacion=$Exportacion;

		//obtener los decimales de la moneda
		$arrayCatalog = new Arrays();
		if(!array_key_exists($this->Moneda, $arrayCatalog->arrayMoneda)) {
			$this->logger->write("Construct comprobante() La moneda declarada " . $this->Moneda . " no se encuentra dentro del catalogo del SAT");
			throw new Exception('La moneda declarada ' . $this->Moneda . '  no se encuentra declarada en el catalogo de monedas.');
		}
		$this->Decimales = $arrayCatalog->arrayMoneda[$this->Moneda]['decimales']; // toma la cantidad de decimales desde el array de moneda
		$this->PorcentajeVariacionMoneda = $arrayCatalog->arrayMoneda[$this->Moneda]['porcentaje_variacion']; // porcentaje de variacion de la moneda
	}

	/*
	 * Validaciones generales del Comprobante
	 * */
	function validar($diff_limite = 0.001) {    	
		//$diff_limite = 0.001;

		# valida campos requeridos de comprobantes
		if($this->TipoDeComprobante == "P"){ //requeridos para comprobante Pagos
			$required = array(
				'Version',
				'Fecha',
				'Sello',
				'NoCertificado',
				'Certificado',
				'Moneda',
				'TipoDeComprobante',
				'LugarExpedicion',
				'Exportacion'
			);
		}else{	//requeridos para comprobantes diferentes de pagos
			$required = array(
				'Version',
				'Fecha',
				'Sello',
				'NoCertificado',
				'Certificado',
				'SubTotal',
				'Moneda',
				'Total',
				'TipoDeComprobante',
				'LugarExpedicion',
				'Exportacion'
			);			
		}	
		foreach ($required as $field) {
			$this->logger->write("Comprobante validar(): $field :" . print_r($this->$field, true));
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Comprobante validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Comprobante Campo Requerido: ' . $field);
			}
		}

		if(!empty($this->Serie)){
			if(strlen($this->Serie) < 1 || strlen($this->Serie) >25 ){
				$this->logger->write('Comprobante validar Serie: Debe contener entre 1 a 25 carácter(es) .');
				throw new Exception('El valor de Comprobante Serie debe ser entre 1 a 25 carácter(es): len='.strlen($this->Serie));
			}
		}

		if(!empty($this->Folio)){
			if(strlen($this->Folio) < 1 || strlen($this->Folio) >40 ){
				$this->logger->write('Comprobante validar Folio: Debe contener entre 1 a 25 carácter(es) .');
				throw new Exception('El valor de Comprobante Folio debe ser entre 1 a 25 carácter(es): len='.strlen($this->Folio));
			}
		}

		if(!empty($this->CondicionesDePago)){
			if(strlen($this->CondicionesDePago) < 1 || strlen($this->CondicionesDePago) >1000 ){
				$this->logger->write('Comprobante validar Condiciones de Pago: Debe contener entre 1 a 1000 carácter(es) .');
				throw new Exception('El valor de Comprobante Condiciones de Pago debe ser entre 1 a 1000 carácter(es): len='.strlen($this->CondicionesDePago));
			}
		}

		//if ($this->TipoDeComprobante != "P" && $this->TipoDeComprobante != "N") {
			# valida moneda y tipo de cambio
			if (($this->Moneda != 'MXN' && $this->Moneda != 'XXX') && ($this->TipoCambio == null)) {
				$this->logger->write("Comprobante validar(): debe colocar un tipo de cambio correspondiente a la moneda extranjera" . $this->TipoCambio);
				throw new Exception('Se debe establecer un tipo de cambio correspondiente a la moneda extranjera, tipo de cambio declarado: ' . $this->TipoCambio);
			}
			if($this->Moneda == 'MXN' && $this->TipoCambio != 1 && $this->TipoCambio != null){
				$this->logger->write("Comprobante validar(): El tipo de cambio correspondiente a la moneda nacional no es correcto: " . $this->TipoCambio);
				throw new Exception('El tipo de cambio correspondiente a la moneda nacional no es correcto: ' . $this->TipoCambio);
			}
		//}
		
		/*=======
		// si el tipo de comprobante es distitno al de P de pagos realizarar estas validaciones

		if ($this->TipoDeComprobante != "P" and $this->TipoDeComprobante != "T" and $this->TipoDeComprobante != "N") {
			# valida moneda y tipo de cambio
			if ($this->Moneda != 'MXN' && ( $this->TipoCambio == 1 || $this->TipoCambio == '')) {
				$this->logger->write("Comprobante validar(): debe colocar un tipo de cambio correspondiente a la moneda extranjera" . $this->TipoCambio);
				throw new Exception('Se debe establecer un tipo de cambio correspondiente a la moneda extranjera, tipo de cambio declarado: ' . $this->TipoCambio);
			}
			if ($this->Moneda == 'MXN' && $this->TipoCambio != 1) {
				$this->logger->write("Comprobante validar(): El tipo de cambio correspondiente a la moneda nacional no es correcto: " . $this->TipoCambio);
				throw new Exception('El tipo de cambio correspondiente a la moneda nacional no es correcto: ' . $this->TipoCambio);
			}

			# valida subtotal del comprobante
			if ($this->SubTotal <> $this->TotalConceptos) {
				$this->logger->write("Comprobante validar(): El valor del subtotal :" . $this->SubTotal . "no coincide con la suma de los valores de los conceptos " . $this->TotalConceptos);	
				throw new Exception("El valor del subtotal declarado :" . $this->SubTotal . " debe ser igual a la suma de los importes de los conceptos " . $this->TotalConceptos);
			}

			// Validacion del descuento global contra el total de descuento de los conceptos
			if($this->Descuento > 0) {
				if ($this->TotalDescuento <> $this->Descuento) {
					$this->logger->write("Comprobante validar(): El valor del Descuento del comprobante:" . $this->Descuento . " no coincide con la suma de los valores de los conceptos " . $this->TotalDescuento);
					throw new Exception("El valor del Descuento del comprobante: " . $this->Descuento . " no coincide con la suma de los valores de los descuentos en los conceptos " . $this->TotalDescuento);
				}
			}

			// converti a float debido a que algunos valores eran strings y al realizar la comparacion aunque el resultado de comparacion
			// era igual el tipo no lo era y lanzaba la excepcion.
			// AQUI VALIDA EL TOTAL DEL COMPROBANTE


			(float) $impuestos = $this->TotalTraslados - $this->TotalRetenciones;
			(float) $totalComprobante = ($this->TotalConceptos + $impuestos) - $this->TotalDescuento;
			//echo $this->TotalConceptos.'<br>';
			//echo $impuestos.'<br>';
			//echo $this->TotalDescuento;

			$totalComprobante = round($totalComprobante, $this->Decimales);

			if ($totalComprobante <> (float) $this->Total) {
				$this->logger->write("Comprobante validar(): El valor del total declarado " . $this->Total . " no coincide con el valor del subtotal + impuestos - descuentos " . $totalComprobante);
				throw new Exception("El Total Calculado del Comprobante no corresponde con el Total Declarado. Valor Declarado : " . $this->Total
				. " , Valor Esperado : " . $totalComprobante);
			}
		}
		>>>>>>> pagos*/

		// validaciones del tipo de nomina
		if ($this->TipoDeComprobante == "N") {
			// if ($this->MetodoPago != "PUE") {
			// 	$this->logger->write("Comprobante validar(): el valor del metodo de pago en el complemento de Nomina debe ser PUE, dato registrado: " . $this->MetodoPago);
			// 	throw new Exception(' El valor del metodo de pago en el complemento de Nomina debe ser PUE, valor registrado : ' . $this->MetodoPago);
			// }

			if ($this->Moneda <> "MXN") {
				$this->logger->write("Comprobante validar(): el valor de la moneda en el complemento de Nomina debe ser MXN, dato registrado: " . $this->Moneda);
				throw new Exception(' El valor de la moneda en el complemento de Nomina debe ser MXN, valor registrado : ' . $this->Moneda);
			}
			// if ($this->TipoCambio) {
			// 	$this->logger->write("Comprobante validar(): No debe existir el tipo de cambio en el complemento de Nomina , dato registrado: " . $this->TipoCambio);
			// 	throw new Exception(' Comprobante validar(): No debe existir el tipo de cambio en el complemento de Nomina, dato registrado:  ' . $this->TipoCambio);
			// }

			// if ($this->FormaPago <> "99") {
			// 	$this->logger->write("Comprobante validar(): El valor del atributo FormaPago en el complemento de nomina debe ser 99, valor registrado" . $this->FormaPago);
			// 	throw new Exception('El valor del atributo FormaPago en el complemento de nomina debe ser 99 ' . $this->FormaPago);
			// }

			// if ($this->CondicionesDePago) {
			// 	$this->logger->write("Comprobante validar(): No debe de existir el atributo CondicionesDePago en el complemento de nomina, valor registrado: " . $this->CondicionesDePago);
			// 	throw new Exception('No debe de existir el atributo CondicionesDePago en el complemento de nomina valor registrado : ' . $this->CondicionesDePago);
			// }

			// ------------------------------------------  PENDIENTES -----------------------------------------------------------------------------
			// El valor del atributo subTotal, debe registrar la suma de los atributos Nomina12:TotalPercepciones más Nomina12:TotalOtrosPagos.
			// El atributo descuento, debe registrar el valor del campo Nomina12:TotalDeducciones.
			// El atributo total, Debe ser igual a la suma de Nomina12:TotalPercepciones más Nomina12:TotalOtrosPagos menos Nomina12:TotalDeducciones.
			// confirmacion

			if (!empty($this->Receptor->ResidenciaFiscal)) { // traslado
				$this->logger->write("Comprobante validar(): No debe de existir el atributo ResidenciaFiscal  en el complemento de nomina, valor registrado: " . $this->Receptor->ResidenciaFiscal);
				throw new Exception('No debe de existir el atributo ResidenciaFiscal  en el complemento de nomina, valor registrado:  : ' . $this->Receptor->ResidenciaFiscal);
			}
			if (!empty($this->Traslados) || !empty($this->Retenciones)) { // traslado
				$this->logger->write("Comprobante validar(): No debe de existir el nodo de impuestos en el complemento de nomina");
				throw new Exception("No debe de existir el nodo de impuestos en el complemento de nomina");
			}

			if (!empty($this->Receptor->NumRegIdTrib)) { // traslado
				$this->logger->write("Comprobante validar(): No debe de existir el atributo NumRegIdTrib  en el complemento de nomina, valor registrado: " . $this->Receptor->NumRegIdTrib);
				throw new Exception('No debe de existir el atributo NumRegIdTrib  en el complemento de nomina, valor registrado:  : ' . $this->Receptor->NumRegIdTrib);
			}
			// if ($this->Receptor->UsoCFDI != "P01") {
			// 	$this->logger->write("Comprobante validar():El valor de usoCFDI del nodo del receptor en el complemento de Nomina debe ser P01, valor registrado " . $this->Receptor->UsoCFDI);
			// 	throw new Exception('El valor de usoCFDI del nodo del receptor en el complemento de Nomina debe ser P01 valor registrado : ' . $this->Receptor->UsoCFDI);
			// }
			// crear el del CFDI Relacionados
		}

		// Valida que en el complemento de pago solo exista un concepto
		// En traslado no van los impuestos ----------------------  VALIDAR ------------------------------
		// investigar el concepto del traslado pero el valor unitario va en cero
		if ($this->TipoDeComprobante == "P") {
			if ($this->Moneda <> "XXX") {
				$this->logger->write("Comprobante validar(): el valor de la moneda debe ser XXX en el complemento de pago, dato registrado: " . $this->Moneda);
				throw new Exception('El valor de la moneda debe ser XXX en el complemento de pago, valor registrado : ' . $this->Moneda);
			}
			if ($this->TipoCambio) {
				$this->logger->write("Comprobante validar(): No debe de existir el atributo TipoCambio en el complemento de pago, valor registrado " . $this->TipoCambio);
				throw new Exception('No debe de existir el atributo TipoCambio en el complemento de pago valor registrado : ' . $this->TipoCambio);
			}
			if ((string) $this->Receptor->UsoCFDI != "P01" && (string) $this->Receptor->UsoCFDI != "CP01") {
				$this->logger->write("Comprobante validar():El valor de usoCFDI del nodo del receptor en el complemento de pago debe ser P01, valor registrado " . $this->Receptor->UsoCFDI);
				throw new Exception('El valor de usoCFDI del nodo del receptor en el complemento de pago debe ser P01 valor registrado : ' . $this->Receptor->UsoCFDI);
			}
			if (count($this->Conceptos) > 1) {
				$this->logger->write("Comprobante validar():Solo debe declarar un concepto en el complemento de pago");
				throw new Exception('Solo debe declarar un concepto en el complemento de pago: ');
			}
			// foreach ($this->Pagos as $pago) {
			// 	$pago->validar();
			// }
		}

		if ($this->TipoDeComprobante == "P" || $this->TipoDeComprobante == "T") {
			if ($this->Total != 0) { // traslado
				$this->logger->write("Comprobante validar(): el valor del total debe ser cero si el tipo de comprobante es [T,P]  valor registrado" . $this->Total);
				throw new Exception('El valor del total debe ser cero si el tipo de comprobante es [T,P]  valor registrado : ' . $this->Total);
			}
			if ($this->FormaPago) { // traslado
				$this->logger->write("Comprobante validar(): No debe de existir el atributo FormaPago si el tipo de comprobante es [T,P] , valor registrado" . $this->FormaPago);
				throw new Exception('No debe de existir el atributo FormaPago si el tipo de comprobante es [T,P]  valor registrado ' . $this->FormaPago);
			}
			if ($this->SubTotal != 0) { // traslado
				$this->logger->write("Comprobante validar(): el valor del subtotal debe ser cero si el tipo de comprobante es [T,P] , valor registrado" . $this->SubTotal);
				throw new Exception('El valor del subtotal debe ser cero si el tipo de comprobante es [T,P]  valor registrado : ' . $this->SubTotal);
			}
			if ($this->Descuento && (float)$this->Descuento!=0) { // traslado
				$this->logger->write("Comprobante validar(): No debe de existir el atributo Descuento si el tipo de comprobante es [T,P]  , valor registrado : " . $this->Descuento);
				throw new Exception('No debe de existir el atributo Descuento si el tipo de comprobante es [T,P]  valor registrado : ' . $this->Descuento);
			}

			if ($this->MetodoPago) {
				$this->logger->write("Comprobante validar(): No debe de existir el atributo MetodoPago si el tipo de comprobante es [T,P] ,valor registrado " . $this->MetodoPago);
				throw new Exception('No debe de existir el atributo MetodoPago si el tipo de comprobante es [T,P]  valor registrado : ' . $this->MetodoPago);
			}
		}

		if ($this->TipoDeComprobante == "P" || $this->TipoDeComprobante == "T" || $this->TipoDeComprobante == "N") {
			// if ($this->CondicionesDePago) { // traslado
			// 	$this->logger->write("Comprobante validar(): No debe de existir el atributo CondicionesDePago si el tipo de comprobante es [T,P] , valor registrado: " . $this->CondicionesDePago);
			// 	throw new Exception('No debe de existir el atributo CondicionesDePago si el tipo de comprobante es [T,P]  valor registrado : ' . $this->CondicionesDePago);
			// }
			// valida que no tenga impuestos
			if (!empty($this->Traslados) || ! empty($this->Retenciones)) {
				$this->logger->write("Comprobante validar():Cuando el TipoDeComprobante sea T o P no deben existir impuestos en el comprobante");
				throw new Exception('Cuando el TipoDeComprobante sea T o P no deben existir impuestos en el comprobante ');
			}
		}
		
		#valida cfdisRelacionados
		if ($this->CfdisRelacionados){
			foreach($this->CfdisRelacionados as $cfdi_relacionado)
				$cfdi_relacionado->validar();
		}
		
		#valida emisor
		$this->Emisor->validar();

		#valida receptor
		$this->Receptor->validar();

		#valida conceptos
		// valida que al menos exista un concepto
		if ($this->Conceptos == null || empty($this->Conceptos) || count($this->Conceptos) == 0) {
			$this->logger->write("Comprobante validar(): No se encontraron conceptos");
			throw new Exception('No se encontraron Conceptos en el Comprobante: ');
		}
		// foreach para validar los conceptos y dentro de cada concepto se valida sus impuestos de traslado y retencion
		// como tambien los valores maximos y minimos.
		//if ($this->TipoDeComprobante != "P") {
			foreach ($this->Conceptos as $key => $concepto) {
				$concepto->validar();
			}
		//}
		
		#valida maximos y minimos de comprobante
		
		#valida datos de catalogos de comprobante
		
		#valida cfdi_relacionados
		
		#valida impuestos locales
		if ($this->ImpuestosLocales){
			$this->ImpuestosLocales->validar();
		}
		
		# valida subtotal del comprobante
		if (abs(floatval(round($this->SubTotal, $this->Decimales)) - floatval(round($this->TotalConceptos, $this->Decimales))) > $diff_limite) {
			$this->logger->write("Comprobante validar(): El valor del subtotal :" . $this->SubTotal . "no coincide con la suma de los valores de los conceptos " . $this->TotalConceptos);
			throw new Exception("El valor del subtotal declarado: " . $this->SubTotal . " debe ser igual a la suma de los importes de los conceptos " . $this->TotalConceptos);
		}
		
		#valida total , etc
		// AQUI VALIDA EL TOTAL DEL COMPROBANTE
		/*(float)$impuestos = $this->TotalTraslados - $this->TotalRetenciones;
		(float)$totalComprobante =  ($this->TotalConceptos + $impuestos) - $this->TotalDescuento;
		//si existen impuestos locales tomarlos en cuenta para la validacion de totales
		if ($this->ImpuestosLocales){
			$totalComprobante += (float)$this->ImpuestosLocales->TotaldeTraslados;
			$totalComprobante -= (float)$this->ImpuestosLocales->TotaldeRetenciones;
		}
		$totalComprobante = round($totalComprobante,$this->Decimales);		
		if(abs(floatval($totalComprobante) - floatval($this->Total)) > $diff_limite){
			$this->logger->write("Comprobante validar(): El valor del total declarado " . $this->Total . " no coincide con el valor del subtotal + impuestos - descuentos " . $totalComprobante);
			throw new Exception("El Total Calculado del Comprobante no corresponde con el Total Declarado. Valor Declarado : " . $this->Total . " - Valor Calculado : " . $totalComprobante);
		}
		*/
		#valida informacion global
		if($this->InformacionGlobal){
			$this->InformacionGlobal->validar();
		}

		#valida timbre fiscal
		if ($this->TimbreFiscalDigital) {
			$this->TimbreFiscalDigital->validar();
		}

		#valida addenda
		if ($this->Addenda) {
			$this->Addenda->validar();
		}

		#valida nomina
		if ($this->Nomina) {
			$this->Nomina->validar();
		}

		#valida carta porte
		if ($this->CartaPorte) {
			$this->CartaPorte->validar();
		}

		#valida carta porte 2.0
		if ($this->CartaPorte20) {
			$this->CartaPorte20->validar();
		}

		#valida carta porte 3.0
		if ($this->CartaPorte30) {
			$this->CartaPorte30->validar();
		}

		if ($this->CartaPorte31) {
			$this->CartaPorte31->validar();
		}

		// // -------------------------------VER QUE VALIDE CONTRA EL XSD DEL TIPO DE COMPLEMENTO DE PAGOS  falla no--------------------------
		// $this->validateXSD();

		$this->logger->write("Comprobante validar(): Validaciones Correctas");
	}

	/*
	 * General el XML del comprobante CFDI v40
	 * */
	function toXML() {
		$ComplementoConceptoTerceros = false;
		$ComplementoConceptoIedu = false;

		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$comprobante = $this->xml_base->createElement("cfdi:Comprobante");
		$this->xml_base->appendChild($comprobante);
		$schemaLocation = "http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd";

		#InformacionGlobal
		if($this->InformacionGlobal){
			$this->InformacionGlobal->toXML();
			$Informacion_Global=$this->xml_base->importNode($this->InformacionGlobal->importXML(), true);
			$comprobante->appendChild($Informacion_Global);
		}

		# cfdi_relacionados
		if ($this->CfdisRelacionados){
			foreach($this->CfdisRelacionados as $cfdi_relacionado){
				$cfdi_relacionado->toXML();
				$cfdis_relacionados = $this->xml_base->importNode($cfdi_relacionado->importXML(), true);
				$comprobante->appendChild($cfdis_relacionados);
			}
		}
		
		# emisor
		$this->Emisor->toXML();
		$domEmisor = $this->xml_base->importNode($this->Emisor->importXML(), true);
		$comprobante->appendChild($domEmisor);

		# receptor
		$this->Receptor->toXML();
		$domReceptor = $this->xml_base->importNode($this->Receptor->importXML(), true);
		$comprobante->appendChild($domReceptor);

		# conceptos
		$conceptos = $this->xml_base->createElement("cfdi:Conceptos");
		$comprobante->appendChild($conceptos);
		foreach ($this->Conceptos as $key => $concepto) {
			$concepto->toXML();
			$concepto_xml = $this->xml_base->importNode($concepto->importXML(), true);
			$conceptos->appendChild($concepto_xml);
			if($concepto->ComplementoConcepto && $concepto->ComplementoConcepto->nodo_name == 'instEducativas'){
				$ComplementoConceptoIedu = true;
			}
			if($concepto->ComplementoConcepto && $concepto->ComplementoConcepto->nodo_name == 'PorCuentadeTerceros'){
				$ComplementoConceptoTerceros = true;
			}
		}

		// --  complemento de pago
		if($this->TipoDeComprobante == "P"){
		}else
		// --  complemento de nomina
		if($this->TipoDeComprobante == "N"){
		}
		// --  existen impuestos locales
		if ($this->ImpuestosLocales){
		}
		// --  complemento de comercio exterior, detallista o ine
		if ($this->Complemento) {
		}
		
		# impuestos globales -------------------------------------------
		if (!empty($this->Traslados) || !empty($this->Retenciones)) {
			$impuestos = $this->xml_base->createElement("cfdi:Impuestos");
			$comprobante->appendChild($impuestos);

			# retenciones
			if (!empty($this->Retenciones)) {
				$TotalRetenciones = 0;
				$retenciones = $this->xml_base->createElement("cfdi:Retenciones");
				$impuestos->appendChild($retenciones);
				foreach ($this->Retenciones as $key => $retencion) {
					$retencion->toXML();
					$retencion_xml = $this->xml_base->importNode($retencion->importXML(), true);
					$retenciones->appendChild($retencion_xml);
					$this->logger->write("retencion: ".$retencion->Importe);
					$TotalRetenciones += round($retencion->Importe, $this->Decimales);
				}
				$impuestos->setAttribute('TotalImpuestosRetenidos', $this->addZeros($TotalRetenciones));
			}
			
			# traslados
			if (!empty($this->Traslados)) {
				$TotalTraslados = 0;
				$addTotalTraslados = false;
				$traslados = $this->xml_base->createElement("cfdi:Traslados");
				$impuestos->appendChild($traslados);
				foreach ($this->Traslados as $key => $traslado) {
					$traslado->toXML();
					$traslado_xml = $this->xml_base->importNode($traslado->importXML(), true);
					$traslados->appendChild($traslado_xml);
					$this->logger->write("traslado: ".$traslado->Importe);

					if($traslado->TipoFactor == 'Exento') // si es excento no suma ni incluye el total de impuestos trasladados
						continue;

					$TotalTraslados += round($traslado->Importe, $this->Decimales);
					$addTotalTraslados = true;
				}
				if($addTotalTraslados)
					$impuestos->setAttribute('TotalImpuestosTrasladados', $this->addZeros($TotalTraslados));
			}
		}
				
		# nodos que se añadiran despues de timbrar con exito desde el pac.
		if($this->TimbreFiscalDigital || $this->ImpuestosLocales || $this->Nomina || $this->Complemento || $this->Pagos || $this->CartaPorte || $this->CartaPorte20 || $this->CartaPorte30 || $this->CartaPorte31){
			$complemento = $this->xml_base->createElement('cfdi:Complemento');
			$comprobante->appendChild($complemento);
		}
		
		# agrega impuestos locales
		if ($this->ImpuestosLocales){
			$comprobante->setAttribute('xmlns:implocal', 'http://www.sat.gob.mx/implocal');
			$schemaLocation .= " http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd";

			$this->ImpuestosLocales->toXML();
			$impLocales = $this->xml_base->importNode($this->ImpuestosLocales->importXML(), true);
			$complemento->appendChild($impLocales);		 	
		}

		# agrega complemento de pagos
		if ($this->Pagos && $this->TipoDeComprobante == "P") {
			if($this->PagosVersion=='1.0'){
				$comprobante->setAttribute('xmlns:pago10', 'http://www.sat.gob.mx/Pagos');
				$schemaLocation .= " http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd";
			}else{
				$comprobante->setAttribute('xmlns:pago20', "http://www.sat.gob.mx/Pagos20");
				$schemaLocation .= " http://www.sat.gob.mx/Pagos20 http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos20.xsd";
			}
			$this->Pagos->toXML();
			$xmlPagos = $this->xml_base->importNode($this->Pagos->importXML(), true);
			$complemento->appendChild($xmlPagos);
		}

		# agrega complemento nomina
		if ($this->Nomina && $this->TipoDeComprobante == "N") {
			$comprobante->setAttribute('xmlns:nomina12', 'http://www.sat.gob.mx/nomina12');	//nominas
			$schemaLocation .= " http://www.sat.gob.mx/nomina12 http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina12.xsd ";

			$this->Nomina->toXML();
			$xmlNomina = $this->xml_base->importNode($this->Nomina->importXML(), true);
			$complemento->appendChild($xmlNomina);
		}

		# agrega complemento de carta porte
		if ($this->CartaPorte) {
			$comprobante->setAttribute('xmlns:cartaporte', 'http://www.sat.gob.mx/CartaPorte');
			$schemaLocation .= " http://www.sat.gob.mx/CartaPorte http://www.sat.gob.mx/sitio_internet/cfd/CartaPorte/CartaPorte.xsd ";
		
			$this->CartaPorte->toXML();
			$xmlCartaPorte = $this->xml_base->importNode($this->CartaPorte->importXML(), true);
			$complemento->appendChild($xmlCartaPorte);
		}

		# agrega complemento de carta porte 2
		if ($this->CartaPorte20) {
			$comprobante->setAttribute('xmlns:cartaporte20', 'http://www.sat.gob.mx/CartaPorte20');
			$schemaLocation .= " http://www.sat.gob.mx/CartaPorte20 http://www.sat.gob.mx/sitio_internet/cfd/CartaPorte/CartaPorte20.xsd ";
		
			$this->CartaPorte20->toXML();
			$xmlCartaPorte20 = $this->xml_base->importNode($this->CartaPorte20->importXML(), true);
			$complemento->appendChild($xmlCartaPorte20);
		}

		# agrega complemento de carta porte 3
		if ($this->CartaPorte30) {
			$comprobante->setAttribute('xmlns:cartaporte30', 'http://www.sat.gob.mx/CartaPorte30');
			$schemaLocation .= " http://www.sat.gob.mx/CartaPorte30 http://www.sat.gob.mx/sitio_internet/cfd/CartaPorte/CartaPorte30.xsd ";
		
			$this->CartaPorte30->toXML();
			$xmlCartaPorte30 = $this->xml_base->importNode($this->CartaPorte30->importXML(), true);
			$complemento->appendChild($xmlCartaPorte30);
		}

		# agrega complemento de carta porte 3
		if ($this->CartaPorte31) {
			$comprobante->setAttribute('xmlns:cartaporte31', 'http://www.sat.gob.mx/CartaPorte31');
			$schemaLocation .= " http://www.sat.gob.mx/CartaPorte31 http://www.sat.gob.mx/sitio_internet/cfd/CartaPorte/CartaPorte31.xsd ";
		
			$this->CartaPorte31->toXML();
			$xmlCartaPorte31 = $this->xml_base->importNode($this->CartaPorte31->importXML(), true);
			$complemento->appendChild($xmlCartaPorte31);
		}

		# agrega complemento general (comercio exterior, detallista, ine, leyendasFiscales, pfic  )
		if ($this->Complemento) {
			$comprobante->setAttribute('xmlns:cce20', 'http://www.sat.gob.mx/ComercioExterior20');	//complemento comercio_exterior 2.0
			$comprobante->setAttribute('xmlns:detallista', 'http://www.sat.gob.mx/detallista');	//complemento detallista
			$comprobante->setAttribute('xmlns:ine', 'http://www.sat.gob.mx/ine');	//complemento ine
			$comprobante->setAttribute('xmlns:donat', 'http://www.sat.gob.mx/donat');	//complemento donat
			$comprobante->setAttribute('xmlns:leyendasFisc', 'http://www.sat.gob.mx/leyendasFiscales');	//complemento leyendasFisc
			$comprobante->setAttribute('xmlns:pfic', 'http://www.sat.gob.mx/pfic');	//complemento pfic
			$comprobante->setAttribute('xmlns:servicioparcial', 'http://www.sat.gob.mx/servicioparcialconstruccion');	//complemento servicio parcial de construccion
			
			$schemaLocation .= " http://www.sat.gob.mx/ComercioExterior20 http://www.sat.gob.mx/sitio_internet/cfd/ComercioExterior20/ComercioExterior20.xsd http://www.sat.gob.mx/ine http://www.sat.gob.mx/sitio_internet/cfd/ine/ine11.xsd http://www.sat.gob.mx/detallista http://www.sat.gob.mx/sitio_internet/cfd/detallista/detallista.xsd http://www.sat.gob.mx/donat http://www.sat.gob.mx/sitio_internet/cfd/donat/donat11.xsd http://www.sat.gob.mx/leyendasFiscales http://www.sat.gob.mx/sitio_internet/cfd/leyendasFiscales/leyendasFisc.xsd http://www.sat.gob.mx/pfic http://www.sat.gob.mx/sitio_internet/cfd/pfic/pfic.xsd http://www.sat.gob.mx/servicioparcialconstruccion http://www.sat.gob.mx/sitio_internet/cfd/servicioparcialconstruccion/servicioparcialconstruccion.xsd";

			$this->Complemento->toXML();
			$xmlComplemento = $this->xml_base->importNode($this->Complemento->importXML(), true);
			$complemento->appendChild($xmlComplemento);
		}

		# agrega timbrado digital
		if ($this->TimbreFiscalDigital) {
			$this->TimbreFiscalDigital->toXML();
			$domTimbre = $this->xml_base->importNode($this->TimbreFiscalDigital->importXML(), true);
			$complemento->appendChild($domTimbre);
		}

		# agrega addenda
		if ($this->Addenda) {
			$this->Addenda->toXML();
			$isAddenda = $this->Addenda->xml_base->getElementsByTagName("cfdi:Addenda")->item(0);
			if(empty($isAddenda))
			{
				$addenda = $this->xml_base->createElement('cfdi:Addenda');
				$comprobante->appendChild($addenda);
				$xmlAddenda = $this->xml_base->importNode($this->Addenda->importXML(), true);
				$addenda->appendChild($xmlAddenda);
			}else{
				$xmlAddenda = $this->xml_base->importNode($this->Addenda->importXML(), true);
				$comprobante->appendChild($xmlAddenda);
			}
			if(strpos($this->Addenda->StringXML, 'AddendaModelo')){
				$comprobante->setAttribute('xmlns:modelo', 'http://www.gmodelo.com.mx/CFD/Addenda/Receptor');
				$schemaLocation .= " http://www.gmodelo.com.mx/CFD/Addenda/Receptor http://femodelo.gmodelo.com/Addenda/ADDENDAMODELO.xsd";
			}			
		}

		// --  complemento concepto TERCEROS
		if($ComplementoConceptoTerceros){
			$comprobante->setAttribute('xmlns:terceros', 'http://www.sat.gob.mx/terceros');
			$schemaLocation .= " http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd";

		}
		// --  complemento concepto IEDU
		if($ComplementoConceptoIedu){
			$comprobante->setAttribute('xmlns:iedu', 'http://www.sat.gob.mx/iedu');		
			$schemaLocation .= " http://www.sat.gob.mx/iedu http://www.sat.gob.mx/sitio_internet/cfd/iedu/iedu.xsd";
		}
		$comprobante->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
		$comprobante->setAttribute('xmlns:cfdi', 'http://www.sat.gob.mx/cfd/4');
		$comprobante->setAttribute('xsi:schemaLocation', $schemaLocation);
		
		# agregar atributos generales
		$comprobante->SetAttribute('Version', $this->Version);
		if ($this->Serie)
			$comprobante->SetAttribute('Serie', $this->Serie);
		if ($this->Folio)
			$comprobante->SetAttribute('Folio', $this->Folio);
		$comprobante->SetAttribute('Fecha', $this->Fecha);
		$comprobante->setAttribute('Sello', $this->Sello);
		if ($this->FormaPago)
			$comprobante->SetAttribute('FormaPago', $this->FormaPago);
		$comprobante->SetAttribute('NoCertificado', $this->NoCertificado);
		$comprobante->SetAttribute('Certificado', $this->Certificado);
		if ($this->CondicionesDePago)
			$comprobante->SetAttribute('CondicionesDePago', $this->CondicionesDePago);
		$comprobante->SetAttribute('SubTotal', $this->addZeros(round($this->SubTotal, $this->Decimales)));
		if ($this->Descuento)
			$comprobante->SetAttribute('Descuento', $this->addZeros(round($this->Descuento, $this->Decimales)));
		$comprobante->SetAttribute('Moneda', $this->Moneda);
		if ($this->TipoCambio)
			$comprobante->SetAttribute('TipoCambio', $this->TipoCambio);
		$comprobante->SetAttribute('Total', $this->addZeros($this->Total));
		$comprobante->SetAttribute('TipoDeComprobante', $this->TipoDeComprobante);
		$comprobante->SetAttribute('Exportacion', $this->Exportacion);
		if ($this->MetodoPago)
			$comprobante->SetAttribute('MetodoPago', $this->MetodoPago);
		$comprobante->SetAttribute('LugarExpedicion', $this->LugarExpedicion);
		if ($this->Confirmacion)
			$comprobante->SetAttribute('Confirmacion', $this->Confirmacion);
		// throw new Exception(print_r($conceptos));
		$this->logger->write("toXML(): XML Creado Correctamente.");
	}

	/*
	 * Regrea el combrobante formado en XML en formato string
	 * */
	function toStringXML() {
		// throw new Exception(print_r($this->xml_base));
		// return $this->xml_base->saveXML();
		if(method_exists($this->xml_base,'saveXML')){
			return $this->xml_base->saveXML();
		}else{
			return $this->xml_base;
		}
	}

	/*
	 * aqui faltaria ver si la api me retornaria una excepcion en dado caso que llegase a fallar el timbrado
	 * independientemente de las validaciones del pac
	 * */
	function toSaveXML($path = null) {
		if(isset($this->TimbreFiscalDigital->UUID))
		{
			$name = $this->TimbreFiscalDigital->UUID.".xml";
		} else {
			$name = "CfdiV40.xml";
		}
		if (!$path)
			$path = getcwd();

		if ($this->xml_base->save($ruta = $path . '/' . $name)) {
			chmod($ruta, 0777);
			return $ruta;
		} else {
			$this->logger->write("toSaveXml(): Error al guardar el xml , verfique que la ruta sea correcta o que el nombre este correcto. $ruta\n");
			throw new Exception("Error al guardar el xml , verfique que la ruta sea correcta o que el nombre este correcto. $ruta");
		}
	}

	/*
	 * recibe las keys como constructor para poder usarlas en la generacion de la cadena original y el sello encriptado mediante archivo
	 * */
	public function addKeys($cer, $key) {
		//VALIDAR SI LOS SELLOS SON ARCHIVOS
		if (file_exists($cer)) {
			$cer=file_get_contents($cer);
		}else{
			$this->logger->write("addKeys() No se encontro el archivo cer en la ruta :" . $cer);
			throw new Exception("addKeys() No se encontro el archivo cer en la ruta :" . $cer);
		}

		if (file_exists($key)) {
			$key=file_get_contents($key);
		}else{
			$this->logger->write("addKeys() No se encontro el archivo key en la ruta :" . $key);
			throw new Exception("addKeys() No se encontro el archivo key en la ruta :" . $key);
		}
		$this->cer = $cer;
		$this->key = $key;
	}

	/*
	 * recibe las keys como constructor para poder usarlas en la generacion de la cadena original y el sello encriptado mediante string
	 * */
	public function addKeysString($cer, $key) {
		$this->cer = $cer;
		$this->key = $key;
	}

	/**
	 * Funcion que valida el xml vs los xsd del SAT posee 2 parametros para que en dado caso
	 * valide un xml en fisico o un xml DOMDocument string.
	 * @param string $xmlObject el xml
	 * @param string $path la ruta del xml a validar
	 * @return boleean u array de los errores
	 */
	public function validateXSD($path = null) {
		$this->toXML();
		$xmlObject = $this->toStringXML();
		$validator = new DOMValidatorV4;
		// throw new Exception(print_r($validator));
		if ($xmlObject)
			$validated = $validator->validateFeeds($xmlObject);
		//if ($path)
		//    $validated = $validator->validateFeeds($path);
		// throw new Exception(print_r($validator));

		if ($validated) {
			$this->logger->write("Comprobante validateXSD(): Validaciones vs XSD Correctas");
			return true;
		} else {
			$this->logger->write("Comprobante validateXSD(): fallo la validacion : \n" . print_r($validator->displayErrors(), true));
			throw new Exception($validator->displayErrors());
			return false;
		}
	}

	/*
	*Agregar Información Global
	*/

	function addInformacionGlobal($Periodicidad=null, $Meses=null, $Año=null){
		$Informacion_Global=null;
		if($Periodicidad!=null && $Meses!=null && $Año!=null){
			$Informacion_Global= new InformacionGlobal(
				$Periodicidad,
				$Meses,
				trim($Año)
			);
			$this->InformacionGlobal=$Informacion_Global;
		}
		return $Informacion_Global;
	}

	/*
	 * Agregar cfdis relacionados
	 * */
	function addCfdisRelacionados($TipoRelacion) {
		$cfdi_relacionado = new CfdisRelacionados(
			$TipoRelacion
		);
		//$cfdi_relacionado->validar();
		$this->CfdisRelacionados[] = $cfdi_relacionado;
		return $cfdi_relacionado;
	}

	/*
	 * Agregar el emisor al comprobante
	 * */
	function addEmisor($Rfc, $Nombre = null, $RegimenFiscal = null, $FacAtrAdquirente=null) {
		$emisor = new EmisorV4(
				trim($Rfc), trim($Nombre), $RegimenFiscal, trim($FacAtrAdquirente)
		);
		$emisor->validar();
		$this->Emisor = $emisor;
		return $emisor;
	}

	/*
	 * Agrega el receptos al comprobante
	 * */
	function addReceptor($Rfc, $UsoCFDI, $Nombre = null, $DomicilioFiscalReceptor, $ResidenciaFiscal = null, $NumRegIdTrib = null, $RegimenFiscalReceptor) {
		$receptor = new ReceptorV4(
			trim($Rfc), trim($UsoCFDI), trim($Nombre), trim($DomicilioFiscalReceptor), $ResidenciaFiscal, trim($NumRegIdTrib), $RegimenFiscalReceptor
		);
		$receptor->validar();
		$this->Receptor = $receptor;
		return $receptor;
	}
	
	/*
	 * Agrega un concepto general al comprobante
	 * */
	function addConcepto($ClaveProdServ, $Descripcion, $Cantidad, $ValorUnitario, $Unidad = null, $ClaveUnidad, $NoIdentificacion = null, $Descuento = null, $ObjetoImp) {
		$concepto = new ConceptoV4(trim($ClaveProdServ),trim($Descripcion),trim($Cantidad),$ValorUnitario,trim($Unidad),$ClaveUnidad,trim($NoIdentificacion),$Descuento,$this->Decimales,$this->TipoDeComprobante,$ObjetoImp);
		$concepto->validar();
		$this->Conceptos[] = $concepto;

		// este valor es equivalente al subtotal y se utlilizara para validar
		$this->TotalConceptos += round($Cantidad * $ValorUnitario, 6);

		// suma el total de los descuentos
		if (isset($Descuento) && $Descuento > 0) {
			$this->TotalDescuento += $Descuento;
		}
		return $concepto;
	}

	/**
	 * Agregar concepto de Tipo comprobante Pago con valores definidos por el SAT
	 */
	function addConceptoPago($ValorUnitario, $Descuento = null, $ObjetoImp) {
		$concepto = new ConceptoV4(
			84111506,
			"Pago",
			1,
			$ValorUnitario,
			NULL,
			"ACT",
			NULL,
			$Descuento,
			$this->Decimales,
			null,
			$ObjetoImp
		);
		
		$concepto->validar();
		$this->Conceptos[] = $concepto;

		// este valor es equivalente al subtotal y se utlilizara para validar
		$this->TotalConceptos += round($ValorUnitario, $this->Decimales);

		// suma el total de los descuentos
		if (isset($Descuento) && $Descuento > 0) {
			$this->TotalDescuento += $Descuento;
		}
	}

	/**
	 * Agrega Concepto de Tipo comprobante Nomina con valores Definidos por el SAT
	 */
	function addConceptoNomina($ValorUnitario, $Descuento = null, $ObjetoImp) {
		$concepto = new ConceptoV4(
			'84111505', //clave_prod_ser
			'Pago de nómina', //descripcion
			1, //cantidad
			$ValorUnitario, //valor_unitario
			null, //no_identidiacion
			'ACT', //clave_unidad
			null, //unidad
			$Descuento, //descuento
			$this->Decimales,
			null,
			$ObjetoImp
		);
		
		$concepto->validar();
		$this->Conceptos[] = $concepto;

		// este valor es equivalente al subtotal y se utlilizara para validar
		$this->TotalConceptos += round($ValorUnitario, $this->Decimales);

		// suma el total de los descuentos
		if (isset($Descuento) && $Descuento > 0) {
			$this->TotalDescuento += $Descuento;
		}
	}

	/*
	 * Agrega los impuestos globales a nivel comprobante
	 * */
	function addImpuestosGlobales() {
		$traslados = array();
		$retenciones = array();

		//recorre impuestos de conceptos
		foreach ($this->Conceptos as $concepto) {
			# retenciones
			if (!empty($concepto->Retenciones)) {
				foreach ($concepto->Retenciones as $key => $retencion) {
					//agrupado por impuesto
					//$key_ret = $retencion->Impuesto;
					$key_ret = $retencion->Impuesto.'_'.$retencion->TipoFactor.'_'.$retencion->TasaOCuota;
					if(isset($retenciones[$key_ret])){
						$retenciones[$key_ret]['Importe'] += $retencion->Importe;
					}else{
						$retenciones[$key_ret] = array(
							'Impuesto' => $retencion->Impuesto,
							'Importe' => $retencion->Importe
						);
					}
					//$this->TotalRetenciones += $retencion->Importe;
					$this->TotalRetenciones += round($retencion->Importe, $this->Decimales);
				}
			}

			# traslados
			if (!empty($concepto->Traslados)) {
				foreach ($concepto->Traslados as $key => $traslado) {
					//si el impuesto es exento no se agrega en los globales
					// if($traslado->TipoFactor == 'Exento')
					// 	continue;
					
					//agrupado por impuesto, TipoFactor y TasaOCuota
					$key_tras = $traslado->Impuesto.'_'.$traslado->TipoFactor.'_'.$traslado->TasaOCuota;
					if(isset($traslados[$key_tras])){
						$traslados[$key_tras]['Importe'] += $traslado->Importe;
						$traslados[$key_tras]['Base'] += $traslado->Base;
					}else{
						$traslados[$key_tras] = array(
							'Base' => $traslado->Base,
							'Impuesto' => $traslado->Impuesto,
							'TipoFactor' => $traslado->TipoFactor,
							'TasaOCuota' => $traslado->TasaOCuota,
							'Importe' => $traslado->Importe
						);
					}
					//$this->TotalTraslados += $traslado->Importe;
					$this->TotalTraslados += round($traslado->Importe, $this->Decimales);
				}
			}
		}
		
		$retenciones_group = array();
		foreach ($retenciones as $ret) {
			$key_ret = $ret['Impuesto'];
			if(isset($retenciones_group[$key_ret])){
				$retenciones_group[$key_ret]['Importe'] += round($ret['Importe'], $this->Decimales);
			}else{
				$retenciones_group[$key_ret] = array(
					'Impuesto' => $ret['Impuesto'],
					'Importe' => round($ret['Importe'], $this->Decimales)
				);
			}
		}
		foreach ($retenciones_group as $ret) {
			$retencion = new RetencionGlobal($ret['Impuesto'], $ret['Importe'], $this->Decimales);
			$this->Retenciones[] = $retencion;
		}
		
		foreach ($traslados as $tras) {
			$traslado = new TrasladoGlobalV4($tras['Base'],$tras['Impuesto'], $tras['TipoFactor'], trim($tras['TasaOCuota']), $tras['Importe'], $this->Decimales);
			$this->Traslados[] = $traslado;
		}
	}

	/**
	 * Este metodo se encarga de crear un objeto del tipo comprobante
	 * el string del xml lo carga con un simple sxml para convertirlo a objeto, asi
	 * se extraen sus atributos y puede guardardarse dentro del objeto global de comprobante
	 * @param string $xmlString
	 */
	function addTimbreFiscal($xmlString) {
		$xml = new DOMDocument();
		if (!$xml->loadXML($xmlString)) {
			$this->logger->write("addTimbreFiscalDigital(): fallo al cargar el xml del timbre");
			throw new Exception("Error al importar el nodo del xml del timbre fiscal digital " . print_r($errors, true));
		}

		$timbre = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/TimbreFiscalDigital', 'TimbreFiscalDigital')->item(0);

		$attrs = array();
		// este for itera los atributos y los añade a un arreglo para si poder tener acceso a las propiedades del timbre recibido
		for ($i = 0; $i < $timbre->attributes->length; ++$i) {
			$node = $timbre->attributes->item($i);
			$attrs[$node->nodeName] = $node->nodeValue;
		}

		$this->addTimbreFiscalDigital($attrs['Version'], $attrs['UUID'], $attrs['FechaTimbrado'], $attrs['RfcProvCertif'], $attrs['SelloCFD'], $attrs['NoCertificadoSAT'], $attrs['SelloSAT']);
	}

	function addTimbreFiscalDigital($Version, $UUID, $FechaTimbrado, $RfcProvCertif, $SelloCFD, $NoCertificadoSAT, $SelloSAT) {
		$timbreFiscal = new TimbreFiscalDigital(
				$Version, $UUID, $FechaTimbrado, $RfcProvCertif, $SelloCFD, $NoCertificadoSAT, $SelloSAT
		);

		$this->TimbreFiscalDigital = $timbreFiscal;
	}
		
	function addImpuestosLocales($TotaldeRetenciones = 0, $TotaldeTraslados = 0, $version = null){
		$impLocal = new ImpuestosLocales(
			$TotaldeRetenciones,
			$TotaldeTraslados,
			$version
		);
			
		$this->ImpuestosLocales = $impLocal;
		return $impLocal;
	}

	/**
	 * Recibe un string , luego crea un dom document que que importa al nodo principal del xml
	 * este metodo se debe de llamar despues de haber timbrado el XML, no deberia de emitirse una addenda antes de haber timbrado
	 * @param string $addendaxml
	 * @return object Addenda
	 */
	function addAddenda($addendaxml) {

		$variables = array(
			'@@FS@@uuid@@FS@@', 
			'@@CDF@@SerieFolio@@CDF@@', 
			'@@CDF@@RazonSocialReceptor@@CDF@@', 
			'@@CDF@@FechaTimbrado@@CDF@@',
			'@@CDF@@Serie@@CDF@@',
			'@@CDF@@Folio@@CDF@@', 
			'@@CFDI@@CadenaOriginal@@CFDI@@'
		);

		if (!mb_detect_encoding($addendaxml, 'UTF-8')) {
			$addendaxml = utf8_encode($addendaxml);
		}
		$replace = array(
			'UUID' => isset($this->TimbreFiscalDigital->UUID) ? $this->TimbreFiscalDigital->UUID : null,
			'SerieFolio' => $this->Serie . $this->Folio,
			'Nombre' => isset($this->Receptor->Nombre) ? $this->Receptor->Nombre : null,
			'FechaTimbrado' => isset($this->TimbreFiscalDigital->FechaTimbrado) ? $this->TimbreFiscalDigital->FechaTimbrado : null,
			'Serie' => $this->Serie,
			'Folio' => $this->Folio,
			'CadenaOriginal' => $this->getCadenaOriginal()
		);
		$data = str_replace($variables, $replace, $addendaxml);

		$addenda = new Addenda($data);
		$this->Addenda = $addenda;
	}

	/*
	 * Agrega un Pago al complemento de pagos
	 * */
		/*function addPago($FechaPago, $FormaDePagoP, $MonedaP, $Monto
					, $TipoCambioP = null, $NumOperacion = null, $RfcEmisorCtaOrd = null
					, $NomBancoOrdExt = null, $CtaOrdenante = null, $RfcEmisorCtaBen = null
					, $CtaBeneficiario = null, $TipoCadPago = null
					, $CertPago = null, $CadPago = null, $SelloPago = null) {
		$pago = new Pago($FechaPago, $FormaDePagoP, $MonedaP, $Monto, $TipoCambioP, $NumOperacion, $RfcEmisorCtaOrd, $NomBancoOrdExt, $CtaOrdenante, $RfcEmisorCtaBen, $CtaBeneficiario, $TipoCadPago, $CertPago, $CadPago, $SelloPago);

		//$pago->validar();
		$this->Pagos[] = $pago;
		return $pago;
	}*/

	/*
	 * Agrega complemento de pagos
	 * */
	function addPagos($version='1.0') {
		if($version == '2.0'){
			$this->PagosVersion='2.0';
			$pagos = new PagosV2();
		}else{
			$this->PagosVersion='1.0';
			$pagos = new Pagos();
		}
		$this->Pagos = $pagos;
		return $pagos;
	}
					
	/*
	 * Agrega complemento nomina
	 * */
	function addNomina($nomina_xml_string) {

		if (!mb_detect_encoding($nomina_xml_string, 'UTF-8')) {
			$nomina_xml_string = utf8_encode($nomina_xml_string);
		}

		$nomina = new Nomina($nomina_xml_string);
		$this->Nomina = $nomina;
	}

	/*
	 * Agrega complemento nomina con datos
	 * */
	function addNominaData(
		$TipoNomina, 
		$FechaPago, 
		$FechaInicialPago, 
		$FechaFinalPago, 
		$NumDiasPagados, 
		$TotalPercepciones = null, 
		$TotalDeducciones = null, 
		$TotalOtrosPagos = null
	) {

		$newNomina = new NominaData(
			$TipoNomina,
			$FechaPago,
			$FechaInicialPago,
			$FechaFinalPago,
			$NumDiasPagados,
			$TotalPercepciones,
			$TotalDeducciones,
			$TotalOtrosPagos
		);
		$this->Nomina = $newNomina;
		return $newNomina;
	}
					
	/*
	 * Agrega complemento carta porte
	 * */
	function addCartaPorte($TranspInternac, $EntradaSalidaMerc = null, $ViaEntradaSalida = null, $TotalDistRec = null) 
	{
		$carta_porte = new CartaPorte($TranspInternac, $EntradaSalidaMerc, $ViaEntradaSalida, $TotalDistRec);
		$this->CartaPorte = $carta_porte;
		return $carta_porte;
	}
					
	/*
	 * Agrega complemento carta porte 2.0
	 * */
	function addCartaPorte20($TranspInternac, $EntradaSalidaMerc = null, $ViaEntradaSalida = null, $TotalDistRec = null, $PaisOrigenDestino = null) 
	{
		$carta_porte20 = new CartaPorte20($TranspInternac, $EntradaSalidaMerc, $ViaEntradaSalida, $TotalDistRec, $PaisOrigenDestino);
		$this->CartaPorte20 = $carta_porte20;
		return $carta_porte20;
	}
					
	/*
	 * Agrega complemento carta porte 3.0
	 * */
	function addCartaPorte30($IdCCP, $TranspInternac, $EntradaSalidaMerc = null, $ViaEntradaSalida = null, $TotalDistRec = null, $PaisOrigenDestino = null, 
        $RegimenAduanero = null, $RegistroISTMO = null, $UbicacionPoloOrigen = null, $UbicacionPoloDestino = null) 
	{
		$carta_porte30 = new CartaPorte30(
			$IdCCP, 
			$TranspInternac, 
			$EntradaSalidaMerc, 
			$ViaEntradaSalida, 
			$TotalDistRec, 
			$PaisOrigenDestino, 
			$RegimenAduanero, 
			$RegistroISTMO, 
			$UbicacionPoloOrigen, 
			$UbicacionPoloDestino
		);
		$this->CartaPorte30 = $carta_porte30;
		return $carta_porte30;
	}

	function addCartaPorte31($IdCCP, $TranspInternac, $EntradaSalidaMerc = null, $ViaEntradaSalida = null, $TotalDistRec = null, $PaisOrigenDestino = null, 
        $RegistroISTMO = null, $UbicacionPoloOrigen = null, $UbicacionPoloDestino = null) 
	{
		$carta_porte31 = new CartaPorte31(
			$IdCCP, 
			$TranspInternac, 
			$EntradaSalidaMerc, 
			$ViaEntradaSalida, 
			$TotalDistRec, 
			$PaisOrigenDestino, 
			$RegistroISTMO, 
			$UbicacionPoloOrigen, 
			$UbicacionPoloDestino
		);
		$this->CartaPorte31 = $carta_porte31;
		return $carta_porte31;
	}
					
	/*
	 * Agrega complemento general
	 * */
	function addComplemento($complemento_xml_string) {

		if (!mb_detect_encoding($complemento_xml_string, 'UTF-8')) {
			$complemento_xml = utf8_encode($complemento_xml_string);
		}
		
		$complemento = new Complemento($complemento_xml_string);
		$this->Complemento = $complemento;
	}

	// calcula el limite superior e inferior del tipo de cambio si la moneda es distinta al peso mexicano
	// si es peso mexicano no es necesario realizar estos pasos ya que se puede omitir el valor del tipo
	// de cambio del comprobante o dejarlo con el valor de 1.

	public function getMax() {
		$maximo = $this->TipoCambio * 1 + $this->PorcentajeVariacionMoneda;
		return $maximo;
	}

	public function getMin() {
		$minimo = $this->TipoCambio * 1 - $this->PorcentajeVariacionMoneda;
		return $minimo;
	}

	function getCadenaOriginal() {
		$xsl = new DOMDocument;
		$xsl->load(dirname(__FILE__) . "/xslt/cadenaoriginal_4_0.xslt");
		$procesador = new XSLTProcessor;
		$procesador->importStyleSheet($xsl);
		$paso = new DOMDocument;
		$paso->loadXML($this->toStringXML());
		$cadena=$procesador->transformToXML($paso);
		$cadena='|'.trim(substr($cadena,1,strlen($cadena)));
		return $cadena;
	}

	function addSellos() {
		$this->toXML();

		$pkeyid = openssl_get_privatekey($this->key);
		openssl_sign($this->getCadenaOriginal(), $crypttext, $pkeyid, OPENSSL_ALGO_SHA256); // convierte la cadena a sha256
		openssl_free_key($pkeyid); //libera la clave asociada con el indetificador de clave

		$this->Sello = base64_encode($crypttext);
		$this->Certificado = $this->parseCertificado();
	}

	public function validateSellos() {
		# valida los archivos .key y .pem por medio de algunos metodos que se encuentran declarados en este script
		# si llegan estar fuera del rango de fecha o este mal declarado el certificado te regrese una exepcion
		# como tambien que el formato del archivo sea legible
		$noCertificado = $this->getNoCertificado();
		if ($this->NoCertificado != $noCertificado) {
			$this->logger->write("Comprobante validar(): Campo no puede estar vacio :" . print_r($field, true));
			throw new Exception('El numero de certificado declarado :' . $this->NoCertificado . " no coincide con el numero de certificado del archivo .pem : " . $noCertificado);
		}
		$this->verifyValidityPeriod();
		$this->verifyValidCsd();
	}

	public function parseCertificado() {
		$this->Certificado = preg_replace('[\s+]', "", $this->between('-----BEGIN CERTIFICATE-----', '-----END CERTIFICATE-----', $this->cer));
		return $this->Certificado;
	}

	private function between($inicio, $that, $inthat) {
		return $this->before($that, $this->after($inicio, $inthat));
	}

	private function after($inicio, $inthat) {
		if (!is_bool(strpos($inthat, $inicio)))
			return substr($inthat, strpos($inthat, $inicio) + strlen($inicio));
	}

	private function before($inicio, $inthat) {
		return substr($inthat, 0, strpos($inthat, $inicio));
	}

	function validateDecimals() {
		$Total = strlen(substr(strrchr($this->Total, "."), 1));
		$Subtotal = strlen(substr(strrchr($this->SubTotal, "."), 1));


		if (!empty($this->Descuento)) {
			$decimalesDescuento = strlen(substr(strrchr($this->Descuento, "."), 1));
			if ($decimalesDescuento > $this->Decimales) {
				throw new Exception("El descuento de " . $this->Descuento .
				" en el comprobante es mayor que el valor de los decimales especificado por la moneda , valor de decimales: " . $this->Decimales);
			}
		}

		if ($Total > $this->Decimales) {
			throw new Exception("El total de " . $this->Total .
			" no coincide con el valor de los decimales especificado por la moneda " . $this->Moneda . " ,valor de decimales: " . $this->Decimales);
		}
		if ($Subtotal > 6) {
			throw new Exception("El subtotal de " . $this->SubTotal .
			" no coincide con el valor de los decimales especificado por la moneda " . $this->Moneda . "  ,valor de decimales: " . $this->Decimales);
		}
	}

	/*
	 * FUNCIONES PARA VALIDAR LAS KEYS DESDE EL OBJETO DE COMPROBANTE
	 */

	public function getNoCertificado() {
		exec("openssl x509 -in {$this->cer} -noout -serial", $out, $errors);

		if ($errors) {
			$msg = openssl_error_string();
			$this->logger->write("getNoCertificado(): Ha ocurrido un error al intentar validar el csd :" . $msg);
			throw new Exception("Ha ocurrido un error al intentar validar el csd " . $msg);
		}

		$vars = explode("=", $out[0]);
		$certificado = end($vars);
		$no_certificado = '';

		for ($i = 0; $i < strlen($certificado); $i++) {
			if ($i % 2 != 0) {
				$no_certificado .= substr($certificado, $i, 1);
			}
		}

		return $no_certificado;
	}

	public function verifyValidityPeriod() {

		exec("openssl x509 -noout -in {$this->cer} -dates", $out, $errors);

		if ($errors) {
			$msg = openssl_error_string();
			$this->logger->write("verifyValidityPeriod(): Ha ocurrido un error al intentar validar el csd :" . $msg);
			throw new Exception("Ha ocurrido un error al intentar validar el csd " . $msg);
		}
		$fecha_inicial = explode("=", $out[0]);
		$fecha_final = explode("=", $out[1]);

		$now = new DateTime("now", new DateTimeZone('America/Mexico_City'));
		$fecha_inicial = new DateTime(end($fecha_inicial), new DateTimeZone('America/Mexico_City'));
		$fecha_final = new DateTime(end($fecha_final), new DateTimeZone('America/Mexico_City'));

		if ($now < $fecha_inicial || $now > $fecha_final) {
			$this->logger->write("verifyValidityPeriod() el archivo cer.key las fechas del certificado no son validas para emitir, Fecha inicial: "
					. $fecha_inicial . ",Fecha final :" . $fecha_final . " Hoy " . $now);
			throw new Exception("Las fechas del certificado no son validas para emitir, Fecha inicial: "
			. $fecha_inicial . ", Fecha final :" . $fecha_final . " Hoy : " . $now);
		}

		return compact('fecha_inicial', 'fecha_final');
	}

	public function verifyValidCsd() {
		exec("openssl x509 -in {$this->cer} -subject -noout", $out, $errors);
		if ($errors) {
			$msg = openssl_error_string();
			$this->logger->write("verifyValidCsd(): Ha ocurrido un error al intentar validar el csd :" . $msg);
			throw new Exception("Ha ocurrido un error al intentar validar el csd " . $msg);
		}
		$vars = preg_split("/\s?\/\s?/", $out[0]);
		$validCsd = end($vars);
		$valid = explode('=', $validCsd);

		if (!empty($vars)) {
			// $separateDataVars=explode(',',$vars[0]);
			// $getRazonSocial=explode('=',$separateDataVars[1]);
			// $getRFC=explode('=',$separateDataVars[3]);
			$razonSocial = str_replace('name=', '', $vars[2]);
			//$getRazonSocial[1];
			$rfc = str_replace('x500UniqueIdentifier=', '', $vars[4]);
			//$getRFC[1];

			if (trim($this->Emisor->Rfc) != trim($rfc)) {
				$this->logger->write("verifyValidCsd() El RFC del emisor " . $this->Emisor->Rfc . " no coincide con el RFC del archivo .key " . $rfc);
				throw new Exception("el RFC del emisor " . $this->Emisor->Rfc . " no coincide con el RFC del archivo .key " . $rfc);
			}
			if (isset($this->Emisor->Nombre)) {
				if (trim($this->Emisor->Nombre) != trim($razonSocial)) {
					$this->logger->write("verifyValidCsd() El nombre del emisor " . $this->Emisor->Nombre . " no coincide con el nombre/razon social del archivo .key " . $razonSocial);
					throw new Exception("el nombre/razon social del emisor " . $this->Emisor->Nombre . " no coincide con el nombre/razon social del archivo .key " . $razonSocial);
				}
			}
		}
		return $valid[0] === 'OU';
	}

	public function validateKeys() {
		if (!file_exists($this->cer)) {
			$this->logger->write("validateKeys() No se encontro el archivo cer en la ruta :" . $this->cer);
			throw new Exception("validateKeys() No se encontro el archivo cer en la ruta :" . $this->cer);
		}
		if (!file_exists($this->key)) {
			$this->logger->write("validateKeys() No se encontro el archivo cer en la ruta :" . $this->key);
			throw new Exception("validateKeys() No se encontro el archivo cer en la ruta :" . $this->key);
		}
	}

	/*
	 * Funciones para lectura de XML
	 */

	public function fromStringXML($xmlString) {
		//cargar el string a un objeto xml
		$xml = new DOMDocument();
		libxml_use_internal_errors(true);
		if (!$xml->loadXML($xmlString)) {
			$this->logger->write("Error al cargar el XML. No es un XML Valido.");
			$errors = libxml_get_errors();
			throw new Exception("Error al cargar el XML. No es un XML Valido. " . print_r($errors[0]->message, true));
		}

		$this->loadXML($xml);
	}

	public function fromLoadXML($file_xml) {
		//validar si el archivo existe
		if (!file_exists($file_xml)) {
			$this->logger->write("Error al cargar el XML. No existe el archivo: $file_xml");
			throw new Exception("Error al cargar el XML. No existe el archivo: " . $file_xml);
		}

		//leer xml
		$xml = new DOMDocument();
		libxml_use_internal_errors(true);
		if (!$xml->load($file_xml)) {
			$this->logger->write("Error al cargar el XML. No es un XML Valido.");
			$errors = libxml_get_errors();
			throw new Exception("Error al cargar el XML. No es un XML Valido. " . print_r($errors[0]->message, true));
		}

		$this->loadXML($xml);
	}

	public function loadXML($xml, $loadAddenda=true) {
		$this->logger->write("Entra loadXML: ");
		// throw new Exception("xml -> ".print_r($xml->getElementsByTagName('Pagos'),true));
		//Leer Datos
		$comprobante = $this->__getAttrsXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/4', 'Comprobante')->item(0));
		$emisor = $this->__getAttrsXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/4', 'Emisor')->item(0));
		$receptor = $this->__getAttrsXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/4', 'Receptor')->item(0));
		$conceptos = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/4', 'Conceptos')->item(0);
		$informacionGlobal = $this->__getAttrsXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/4', 'InformacionGlobal')->item(0));
		$timbre = $this->__getAttrsXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/TimbreFiscalDigital', 'TimbreFiscalDigital')->item(0));
		
		//InformacionGlobal
		if(!empty($informacionGlobal)){
			$this->addInformacionGlobal($informacionGlobal['Periodicidad'],$informacionGlobal['Meses'],$informacionGlobal['Año']);
		}	

		//cfdis relacionados
		$cfdisRel = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/4', 'CfdiRelacionados'));
		if(isset($cfdisRel['cfdi:CfdiRelacionados'])){
			foreach($cfdisRel['cfdi:CfdiRelacionados'] as $cfdiRelacionado){
				if (!empty($cfdiRelacionado) && count($cfdiRelacionado["valor"]["cfdi:CfdiRelacionado"]) > 0) {
					if(isset($cfdiRelacionado['TipoRelacion'])){
						$cfdi_relacionado = $this->addCfdisRelacionados($cfdiRelacionado['TipoRelacion']);
						// $cfdisRelNode = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/4', 'CfdiRelacionado'));
						foreach ($cfdiRelacionado['valor']['cfdi:CfdiRelacionado'] as $cfdiRel) {
							$cfdi_relacionado->addCfdiUUID($cfdiRel['UUID']);
						}
					}
				}
			}
		}

		//addenda
		$addenda = null;
		$addNodes = $xml->getElementsByTagName('Addenda');
		foreach ($addNodes as $addN) {
			if ($addN->hasChildNodes()) {
				foreach ($addN->childNodes as $a) {
					if ($a->nodeType == 1) {
						//es un nodo correcto
						$add = new DOMDocument();
						$add->appendChild($add->importNode($a, TRUE));
						$addenda = html_entity_decode($add->saveHTML());
						break;
					}
				}
			}
		}

		//complemento de nomina
		$nomina = null;
		$nomNodes = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/nomina12', 'Nomina')->item(0);
		if (!empty($nomNodes) && $nomNodes->hasChildNodes()) {
			$nom = new DOMDocument();
			$nom->appendChild($nom->importNode($nomNodes, TRUE));
			$nomina = $nom->saveXML();
		}

		//complemento de carta porte
		$carta_porte = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/CartaPorte', 'CartaPorte')->item(0);
		if (!empty($carta_porte) && $carta_porte->hasChildNodes()) {
			$cartaPorteNode = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/CartaPorte', 'CartaPorte'));
			// pendiente leer aqui todo el xml Complemento y llenar el objeto de nuevo
            // $cartaPorte = $this->addCartaPorte(
			// 	@$cartaPorteNode['cartaporte:CartaPorte'][0]['TranspInternac'], 
			// 	@$cartaPorteNode['cartaporte:CartaPorte'][0]['EntradaSalidaMerc'], 
			// 	@$cartaPorteNode['cartaporte:CartaPorte'][0]['ViaEntradaSalida'], 
			// 	@$cartaPorteNode['cartaporte:CartaPorte'][0]['TotalDistRec']
			// );
			// if(isset($cartaPorteNode['cartaporte:CartaPorte'][0]['valor']['cartaporte:Ubicaciones']))
			// {

			// }
			$this->Complemento = $cartaPorteNode;
		}

		//complemento de carta porte 2.0
		$carta_porte20 = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/CartaPorte20', 'CartaPorte')->item(0);
		if (!empty($carta_porte20) && $carta_porte20->hasChildNodes()) {
			$cartaPorteNode20 = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/CartaPorte20', 'CartaPorte'));
			$this->Complemento = $cartaPorteNode20;
		}

		//complemento de carta porte 3.0
		$carta_porte30 = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/CartaPorte30', 'CartaPorte')->item(0);
		if (!empty($carta_porte30) && $carta_porte30->hasChildNodes()) {
			$cartaPorteNode30 = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/CartaPorte30', 'CartaPorte'));
			$this->Complemento = $cartaPorteNode30;
			$this->CartaPorte30 = $cartaPorteNode30["cartaporte30:CartaPorte"][0];
		}

		//complemento de carta porte 3.1
		$carta_porte31 = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/CartaPorte31', 'CartaPorte')->item(0);
		if (!empty($carta_porte31) && $carta_porte31->hasChildNodes()) {
			$cartaPorteNode31 = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/CartaPorte31', 'CartaPorte'));
			$this->Complemento = $cartaPorteNode31;
			$this->CartaPorte31 = $cartaPorteNode31["cartaporte31:CartaPorte"][0];
		}

		//complemento de comercio exterior 1.1
		$cee = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/ComercioExterior11', 'ComercioExterior')->item(0);
		if (!empty($cee) && $cee->hasChildNodes()) {
			$ceeNodes = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/ComercioExterior11', 'ComercioExterior'));
			$this->Complemento = $ceeNodes;
		}

		//complemento de comercio exterior 2.0
		$cee = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/ComercioExterior20', 'ComercioExterior')->item(0);
		if (!empty($cee) && $cee->hasChildNodes()) {
			$ceeNodes = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/ComercioExterior20', 'ComercioExterior'));
			$this->Complemento = $ceeNodes;
		}

		//complemento de Donatarias
		$donat = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/donat', 'Donatarias')->item(0);
		if (!empty($donat)) {
			$donatAttrs['Donatarias'] = $this->__getAttrsXML($donat);
			$this->Complemento = $donatAttrs;
		}
		
		//complemento de Leyendas Fiscales
		$leyendasFiscales = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/leyendasFiscales', 'LeyendasFiscales')->item(0);
		if (!empty($leyendasFiscales) && $leyendasFiscales->hasChildNodes()) {
			$leyendaNode = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/leyendasFiscales', 'Leyenda'));
			$this->Complemento = $leyendaNode;
		}

		//complemento pagos
		$pagos = null;
		$pagoNodes = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/Pagos', 'Pagos')->item(0);
		if (!empty($pagoNodes) && $pagoNodes->hasChildNodes()) {
			$this->addPagos();
			$pagosNode = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/Pagos', 'Pago'));
			foreach ($pagosNode['pago10:Pago'] as $item) {
				//agrega pago
				//$this->logger->write("ComplementoDePago item: " . print_r($item, true));
				$pago = $this->Pagos->addPago(
					$item['FechaPago'], $item['FormaDePagoP'], $item['MonedaP'], $item['Monto'], isset($item['TipoCambioP']) ? $item['TipoCambioP'] : null,
					isset($item['NumOperacion']) ? $item['NumOperacion'] : null, $RfcEmisorCtaOrd = null
					, isset($item["NomBancoOrdExt"])? $item["NomBancoOrdExt"] : null, isset($item["CtaOrdenante"])? $item["CtaOrdenante"] : null , $RfcEmisorCtaBen = null
					, isset($item["CtaBeneficiario"]) ? $item["CtaBeneficiario"] : null
				);
				if (isset($item['valor']['pago10:DoctoRelacionado'])) {
					foreach($item['valor']['pago10:DoctoRelacionado'] as $docRel){
						$pago->addDoctoRelacionado(
							$docRel['IdDocumento'], $docRel['MetodoDePagoDR'], $docRel['MonedaDR'], isset($docRel['Serie']) ? $docRel['Serie'] : null,
							isset($docRel['Folio']) ? $docRel['Folio'] : null, isset($docRel['TipoCambioDR']) ? $docRel['TipoCambioDR'] :null, isset($docRel['NumParcialidad']) ? $docRel['NumParcialidad'] :null,
							isset($docRel['ImpSaldoAnt']) ? $docRel['ImpSaldoAnt'] :null, isset($docRel['ImpPagado']) ? $docRel['ImpPagado'] : null, isset($docRel['ImpSaldoInsoluto']) ? $docRel['ImpSaldoInsoluto'] : null, isset($item['MonedaP'])?$item['MonedaP']:null
						);
					}

				}
			}
		}else{
			$pagoNodes = $xml->getElementsByTagNameNS('http://www.sat.gob.mx/Pagos20', 'Pagos')->item(0);
			if (!empty($pagoNodes) && $pagoNodes->hasChildNodes()) {
				$this->addPagos('2.0');
				$pagosNode = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/Pagos20', 'Pago'));
				foreach ($pagosNode['pago20:Pago'] as $item) {
					//agrega pago
					//$this->logger->write("ComplementoDePago item: " . print_r($item, true));
					$pago = $this->Pagos->addPago(
						$item['FechaPago'], $item['FormaDePagoP'], $item['MonedaP'], $item['Monto'], isset($item['TipoCambioP']) ? $item['TipoCambioP'] : null,
						isset($item['NumOperacion']) ? $item['NumOperacion'] : null, $RfcEmisorCtaOrd = null
						, isset($item["NomBancoOrdExt"])? $item["NomBancoOrdExt"] : null, isset($item["CtaOrdenante"])? $item["CtaOrdenante"] : null , $RfcEmisorCtaBen = null
						, isset($item["CtaBeneficiario"]) ? $item["CtaBeneficiario"] : null,
						isset($item["TipoCadPago"]) ? $item["TipoCadPago"] : null,
						isset($item["CertPago"]) ? $item["CertPago"] : null,
						isset($item["CadPago"]) ? $item["CadPago"] : null,
						isset($item["SelloPago"]) ? $item["SelloPago"] : null
					);
					if (isset($item['valor']['pago20:DoctoRelacionado'])) {
						foreach($item['valor']['pago20:DoctoRelacionado'] as $docRel){
							$docRelacionado=$pago->addDoctoRelacionado(
								$docRel['IdDocumento'],
								$docRel['MonedaDR'],
								$docRel['EquivalenciaDR'],
								$item['Monto'],
								isset($docRel['Serie'])?$docRel['Serie']:null,
								isset($docRel['Folio'])?$docRel['Folio']:null,
								$docRel['NumParcialidad'],
								$docRel['ImpSaldoAnt'],
								$docRel['ImpPagado'],
								$docRel['ImpSaldoInsoluto'],
								$docRel['ObjetoImpDR'],
								isset($item['MonedaP'])?$item['MonedaP']:null
							);


							//agrega impuestos DoctoRelacionado
							if (isset($docRel['valor']['pago20:ImpuestosDR'])) {
								foreach ($docRel['valor']['pago20:ImpuestosDR'] as $impuesto) {
									// agrega traslados
									if (isset($impuesto['valor']['pago20:TrasladosDR'])) {
										foreach ($impuesto['valor']['pago20:TrasladosDR'] as $traslados) {
											if (isset($traslados['valor']['pago20:TrasladoDR'])) {
												foreach ($traslados['valor']['pago20:TrasladoDR'] as $traslado) {
													$docRelacionado->addTrasladoPagoDR(
														$traslado['BaseDR'],
                                                    	$traslado['ImpuestoDR'],
                                                    	$traslado['TipoFactorDR'],
                                                    	isset($traslado['TasaOCuotaDR'])?$traslado['TasaOCuotaDR']:null,
                                                    	isset($traslado['ImporteDR'])?$traslado['ImporteDR']:null
													);
												}
											}
										}
									}
									//agrega retenciones
									if (isset($impuesto['valor']['pago20:RetencionesDR'])) {
										foreach ($impuesto['valor']['pago20:RetencionesDR'] as $retenciones) {
											if (isset($retenciones['valor']['pago20:RetencionDR'])) {
												foreach ($retenciones['valor']['pago20:RetencionDR'] as $retencion) {
													$docRelacionado->addRetencionPagoDR(
														$retencion['BaseDR'],
                                                    	$retencion['ImpuestoDR'],
                                                    	$retencion['TipoFactorDR'],
                                                    	$retencion['TasaOCuotaDR'],
                                                    	$retencion['ImporteDR']
													);
												}
											}
										}
									}

								}
							}
						}
	
					}

					//agrega impuestos pago
					if (isset($item['valor']['pago20:ImpuestosP'])) {
						foreach ($item['valor']['pago20:ImpuestosP'] as $impuesto) {
							// agrega traslados
							if (isset($impuesto['valor']['pago20:TrasladosP'])) {
								foreach ($impuesto['valor']['pago20:TrasladosP'] as $traslados) {
									if (isset($traslados['valor']['pago20:TrasladoP'])) {
										foreach ($traslados['valor']['pago20:TrasladoP'] as $traslado) {
											$pago->addTrasladoPago(
												$traslado['BaseP'],
												$traslado['ImpuestoP'],
												$traslado['TipoFactorP'],
												$traslado['TasaOCuotaP'],
												$traslado['ImporteP']
											);
										}
									}
								}
							}
							//agrega retenciones
							if (isset($impuesto['valor']['pago20:RetencionesP'])) {
								foreach ($impuesto['valor']['pago20:RetencionesP'] as $retenciones) {
									if (isset($retenciones['valor']['pago20:RetencionP'])) {
										foreach ($retenciones['valor']['pago20:RetencionP'] as $retencion) {
											$pago->addRetencionPago(
												$retencion['ImpuestoP'],
												$retencion['ImporteP']
											);
										}
									}
								}
							}

						}
					}
					
				}
				$pagosNode = $this->__getNodesXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/Pagos20', 'Totales'));
				foreach ($pagosNode['pago20:Totales'] as $item) {
					//agrega totales
					$pago = $this->Pagos->addTotales(
						isset($item['TotalRetencionesIVA'])?$item['TotalRetencionesIVA']: null,
						isset($item['TotalRetencionesISR'])?$item['TotalRetencionesISR']: null,
						isset($item['TotalRetencionesIEPS'])?$item['TotalRetencionesIEPS']: null,
						isset($item['TotalTrasladosBaseIVA16'])?$item['TotalTrasladosBaseIVA16']: null,
						isset($item['TotalTrasladosImpuestoIVA16'])?$item['TotalTrasladosImpuestoIVA16']: null,
						isset($item['TotalTrasladosBaseIVA8'])?$item['TotalTrasladosBaseIVA8']: null,
						isset($item['TotalTrasladosImpuestoIVA8'])?$item['TotalTrasladosImpuestoIVA8']: null,
						isset($item['TotalTrasladosBaseIVA0'])?$item['TotalTrasladosBaseIVA0']: null,
						isset($item['TotalTrasladosImpuestoIVA0'])?$item['TotalTrasladosImpuestoIVA0']: null,
						isset($item['TotalTrasladosBaseIVAExento'])?$item['TotalTrasladosBaseIVAExento']: null,
						$item['MontoTotalPagos']
					);
				}
			}
		}

	
		//lee impuestos locales
		$impLocales = null;
		$impLocales = $this->__getNodesXML($xml->getElementsByTagName('ImpuestosLocales'));
		
		//valida generales
		// $required = array('NoCertificado', 'SubTotal', 'Moneda', 'Total', 'TipoDeComprobante', 'LugarExpedicion', 'Certificado',
		// 	'Version', 'Sello', 'Fecha', 'Exportacion');
		// $this->__validateFieldsLoadXML($required, $comprobante);

		//agrega generales
		$this->addGenerales(
				$comprobante['NoCertificado'], $comprobante['SubTotal'], $comprobante['Moneda'], $comprobante['Total'], $comprobante['TipoDeComprobante'], isset($comprobante['FormaPago']) ? $comprobante['FormaPago'] : null, isset($comprobante['TipoCambio']) ? $comprobante['TipoCambio'] : null, $comprobante['LugarExpedicion'], isset($comprobante['MetodoPago']) ? $comprobante['MetodoPago'] : null, isset($comprobante['Serie']) ? $comprobante['Serie'] : null, isset($comprobante['Folio']) ? $comprobante['Folio'] : null, $comprobante['Certificado'], isset($comprobante['CondicionesDePago']) ? $comprobante['CondicionesDePago'] : null, isset($comprobante['Descuento']) ? $comprobante['Descuento'] : null, $comprobante['Version'], $comprobante['Sello'], $comprobante['Fecha'], isset($comprobante['Confirmacion']) ? $comprobante['Confirmacion'] : null, isset($comprobante['Exportacion']) ? $comprobante['Exportacion'] : null
		);

		//valida generales
		if($this->TipoDeComprobante == "P"){ //requeridos para comprobante Pagos
			$required = array(
				'Version',
				'Fecha',
				'Sello',
				'NoCertificado',
				'Certificado',
				'Moneda',
				'TipoDeComprobante',
				'LugarExpedicion',
				'Exportacion'
			);
		}else{	//requeridos para comprobantes diferentes de pagos
			$required = array(
				'Version',
				'Fecha',
				'Sello',
				'NoCertificado',
				'Certificado',
				'SubTotal',
				'Moneda',
				'Total',
				'TipoDeComprobante',
				'LugarExpedicion',
				'Exportacion'
			);			
		}	
		foreach ($required as $field) {
			// print_r(' *'.$field.' ->');
			// print_r($this->$field);
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Comprobante validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Comprobante Campo Requerido: ' . $field);
			}
		}

		//agrega emisor
		$this->addEmisor(
				$emisor['Rfc'], isset($emisor['Nombre']) ? $emisor['Nombre'] : null, isset($emisor['RegimenFiscal']) ? $emisor['RegimenFiscal'] : null, isset($emisor['FacAtrAdquirente']) ? $emisor['FacAtrAdquirente'] : null
		);

		//agrega receptor
		$this->addReceptor(
				$receptor['Rfc'], $receptor['UsoCFDI'], isset($receptor['Nombre']) ? $receptor['Nombre'] : null, isset($receptor['DomicilioFiscalReceptor']) ? $receptor['DomicilioFiscalReceptor'] : null, isset($receptor['ResidenciaFiscal']) ? $receptor['ResidenciaFiscal'] : null, isset($receptor['NumRegIdTrib']) ? $receptor['NumRegIdTrib'] : null, isset($receptor['RegimenFiscalReceptor'])?$receptor['RegimenFiscalReceptor']:null
		);

		//agrega conceptos
		foreach ($conceptos->childNodes as $concepto) {
			if($concepto->nodeType != XML_ELEMENT_NODE )
				continue;

			// se obtienen atributos
			$item = $this->__getAttrsXML($concepto);
		
			// Comprobamos si tiene hijos.
			if ($concepto->hasChildNodes()) {
				$childs = $concepto->childNodes;
				$item['valor'] = $this->__getNodesXML($childs);
			} else {
				$item['valor'] = $concepto->nodeValue;
			}

			//agrega concepto
			$concepto = $this->addConcepto(
				$item['ClaveProdServ'], $item['Descripcion'], $item['Cantidad'], $item['ValorUnitario'], isset($item['Unidad']) ? $item['Unidad'] : null, $item['ClaveUnidad'], isset($item['NoIdentificacion']) ? $item['NoIdentificacion'] : null, isset($item['Descuento']) ? $item['Descuento'] : null, $item['ObjetoImp']
			);
			//agrega impuestos
			if (isset($item['valor']['cfdi:Impuestos'])) {
				foreach ($item['valor']['cfdi:Impuestos'] as $impuesto) {
					// agrega traslados
					if (isset($impuesto['valor']['cfdi:Traslados'])) {
						foreach ($impuesto['valor']['cfdi:Traslados'] as $traslados) {
							if (isset($traslados['valor']['cfdi:Traslado'])) {
								foreach ($traslados['valor']['cfdi:Traslado'] as $traslado) {
									$concepto->addTraslado(
										$traslado['Base'],
										$traslado['Impuesto'],
										$traslado['TipoFactor'],
										(isset($traslado['TasaOCuota']) && $traslado['TipoFactor']!='Exento') ? $traslado['TasaOCuota'] : null,
										(isset($traslado['Importe']) && $traslado['TipoFactor']!='Exento') ? $traslado['Importe'] : null
									);
								}
							}
						}
					}
					//agrega retenciones
					if (isset($impuesto['valor']['cfdi:Retenciones'])) {
						foreach ($impuesto['valor']['cfdi:Retenciones'] as $retenciones) {
							if (isset($retenciones['valor']['cfdi:Retencion'])) {
								foreach ($retenciones['valor']['cfdi:Retencion'] as $retencion) {
									$concepto->addRetencion(
										$retencion['Base'],
										$retencion['Impuesto'],
										$retencion['TipoFactor'],
										isset($retencion['TasaOCuota']) ? $retencion['TasaOCuota'] : null,
										isset($retencion['Importe']) ? $retencion['Importe'] : null
									);
								}
							}
						}
					}
				}
			}
			//agrega ACuentaTerceros
			if (isset($item['valor']['cfdi:ACuentaTerceros'])) {
				foreach($item['valor']['cfdi:ACuentaTerceros'] as $aCuentaTercero) {
					$concepto->addACuentaTerceros($aCuentaTercero['RfcACuentaTerceros'], $aCuentaTercero['NombreACuentaTerceros'], $aCuentaTercero['RegimenFiscalACuentaTerceros'], $aCuentaTercero['DomicilioFiscalACuentaTerceros']);	
				}
			}
			//agrega informacion aduanera
			if (isset($item['valor']['cfdi:InformacionAduanera'])) {
				foreach($item['valor']['cfdi:InformacionAduanera'] as $infoAduanera) {
					$concepto->addInformacionAduanera($infoAduanera['NumeroPedimento']);	
				}
			}

			//agrega cuenta predial
			if(isset($item["valor"]["cfdi:CuentaPredial"])){
				foreach($item["valor"]["cfdi:CuentaPredial"] as $predial){
					$concepto->addCuentaPredial($predial["Numero"]);
				}
			}

			//georgeSKS: 23-06-2018
			//$this->logger->write("Concepto: " . print_r($item, true));
			//Complemento Concepto
			if (isset($item["valor"]["cfdi:ComplementoConcepto"])) {
				foreach ($item["valor"]["cfdi:ComplementoConcepto"] as $key => $complementoConcepto) {
					if (isset($complementoConcepto["valor"]["iedu:instEducativas"])) {
						foreach ($complementoConcepto["valor"]["iedu:instEducativas"] as $instEducativas) {
							if(isset($instEducativas['rfcPago'])){
								$instEducativas['rfcPago']=trim($instEducativas['rfcPago']);
								if(strlen($instEducativas['rfcPago'])==0){
									$instEducativas['rfcPago']=null;
								}
							}else{
								$instEducativas['rfcPago']=null;
							}
							
							$concepto->addComplementoConceptoIedu(
								$instEducativas['nombreAlumno'], 
								$instEducativas['CURP'], 
								$instEducativas['nivelEducativo'], 
								$instEducativas['autRVOE'], 
								$instEducativas['rfcPago']
							);
						}
					}

					if (isset($complementoConcepto["valor"]["terceros:PorCuentadeTerceros"])) {
						foreach ($complementoConcepto["valor"]["terceros:PorCuentadeTerceros"] as $key => $porCuentaTercero) {
							$terceros = $concepto->addComplementoConceptoTerceros(
								$porCuentaTercero["rfc"],
								$porCuentaTercero["nombre"]
							);
							if (isset($porCuentaTercero["valor"]["terceros:InformacionAduanera"])) {
								foreach ($porCuentaTercero["valor"]["terceros:InformacionAduanera"] as $key => $infoAduanera) {
									$terceros->addInformacionAduanera(
										$infoAduanera["numero"],
										$infoAduanera["fecha"]
									);
								}
							}
							if(isset($porCuentaTercero["valor"]["terceros:InformacionFiscalTercero"])){
								foreach ($porCuentaTercero["valor"]["terceros:InformacionFiscalTercero"] as $key => $infoFiscal) {
									$terceros->addInformacionFiscalTercero(
										$infoFiscal['calle'],
										$infoFiscal['noInterior'],
										$infoFiscal['noExterior'],
										$infoFiscal['colonia'],
										$infoFiscal['localidad'],
										$infoFiscal['referencia'],
										$infoFiscal['municipio'],
										$infoFiscal['estado'],
										$infoFiscal['pais'],
										$infoFiscal['codigoPostal']
									);
								}
							}
							if (isset($porCuentaTercero["valor"]["terceros:Parte"])) {
								foreach ($porCuentaTercero["valor"]["terceros:Parte"] as $keyP => $parte) {
									$parte_ = $terceros->addParte(
										$parte['cantidad'],
										$parte['descripcion']
									);
									if (isset($parte["valor"]["terceros:InformacionAduanera"])) {
										foreach ($parte["valor"]["terceros:InformacionAduanera"] as $keyiA => $iA) {
											$parte_->addInformacionAduanera(
												$iA["numero"],
												$iA["fecha"]
											);
										}
									}
								}
							}
							$imp_terc = $terceros->addImpuestos();
							if(isset($porCuentaTercero['valor']['terceros:Impuestos'])){
								foreach($porCuentaTercero['valor']['terceros:Impuestos'] as $impuestos_terceros){
									if(isset($impuestos_terceros['valor']['terceros:Retenciones'])){
										foreach($impuestos_terceros['valor']['terceros:Retenciones'] as $retenciones_terceros){
											if(isset($retenciones_terceros['valor']['terceros:Retencion'])){
												foreach($retenciones_terceros['valor']['terceros:Retencion'] as $ret_ter){
													$imp_terc->addRetencion(
														$ret_ter['impuesto'],
														$ret_ter['importe']
													);
												}
											}
										}
									}
									if(isset($impuestos_terceros['valor']['terceros:Traslados'])){
										foreach($impuestos_terceros['valor']['terceros:Traslados'] as $traslados_terceros){
											if(isset($traslados_terceros['valor']['terceros:Traslado'])){
												foreach($traslados_terceros['valor']['terceros:Traslado'] as $tras_ter){
													$imp_terc->addTraslado(
														$tras_ter['impuesto'],
														$tras_ter['tasa'],
														$tras_ter['importe']
													);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			//agrega Parte
			if(isset($item["valor"]["cfdi:Parte"])){
				foreach($item["valor"]["cfdi:Parte"] as $parte){
					$getParte=$concepto->addParte($parte["ClaveProdServ"], $parte["NoIdentificacion"], $parte["Cantidad"], $parte["Unidad"], $parte["Descripcion"], $parte["ValorUnitario"], $parte["Importe"]);
					if(isset($parte["valor"]["cfdi:InformacionAduanera"])){
						foreach($parte["valor"]["cfdi:InformacionAduanera"] as $parteInformacionAduanera){
							$getParte->addInformacionAduanera(
								$parteInformacionAduanera['NumeroPedimento'],
							);
						}
					}
				}
			}
		}

		//agrege impuestos globales
		$this->addImpuestosGlobales();
		
		//agrega impuestoslocales
		if($impLocales && !empty($impLocales)){
			$impLocal = $impLocales['implocal:ImpuestosLocales'][0];
			$impuestosLocales = $this->addImpuestosLocales(
				$impLocal['TotaldeRetenciones'],
				$impLocal['TotaldeTraslados'],
				$impLocal['version']
			);
			//retenciones locales
			if(isset($impLocal['valor']['implocal:RetencionesLocales'])){
				foreach($impLocal['valor']['implocal:RetencionesLocales'] as $retencionLocal){
					$impuestosLocales->addRetencionLocal(
						$retencionLocal['ImpLocRetenido'],
						$retencionLocal['TasadeRetencion'],
						$retencionLocal['Importe']
					);
				}
			}
			//traslados locales
			if(isset($impLocal['valor']['implocal:TrasladosLocales'])){
				foreach($impLocal['valor']['implocal:TrasladosLocales'] as $trasladoLocal){
					$impuestosLocales->addTrasladoLocal(
						$trasladoLocal['ImpLocTrasladado'],
						$trasladoLocal['TasadeTraslado'],
						$trasladoLocal['Importe']
					);
				}
			}			
		}			
		
		//agrega timbre fiscal
		if(!empty($timbre)){
			$this->addTimbreFiscalDigital(
				$timbre['Version'], $timbre['UUID'], $timbre['FechaTimbrado'], $timbre['RfcProvCertif'], $timbre['SelloCFD'], $timbre['NoCertificadoSAT'], $timbre['SelloSAT']
			);
		}

		//agrega addenda
		if($loadAddenda == true){
			if ($addenda) {
				$this->addAddenda($addenda);
			}
		}
		
		//agrega nomina
		if ($nomina) {
			$this->addNomina($nomina);
			$this->Nomina->loadXML();
		}

		$this->xml_base=$xml->saveXML();

	}

	function __getAttrsXML($node) {
		$array = null;
		if(isset($node->attributes)){
			foreach ($node->attributes as $attrName => $attrNode) {
				$array[$attrName] = $node->getAttribute($attrName);
			}
		}
		return $array;
	}

	function __getNodesXML($node) {
		$data = array();
		foreach ($node as $i => $c) {
			if ($c->nodeType == 3 || $c->nodeType == 8)
				continue;
			$data[$c->tagName][$i] = $this->__getAttrsXML($c);
			// Comprobamos si tiene hijos.
			if ($c->hasChildNodes()) {
				$childs = $c->childNodes;
				$data[$c->tagName][$i]['valor'] = $this->__getNodesXML($childs);
			} else {
				$data[$c->tagName][$i]['valor'] = $c->nodeValue;
			}
		}
		return $data;
	}

	function __validateFieldsLoadXML($required, $data) {
		foreach ($required as $field) {
			if (!isset($data[$field]) || $data[$field] == '') {
				$this->logger->write("Comprobante validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Comprobante Campo Requerido: ' . $field);
			}
		}
	}

	/**
	 * Crea el codigo qr con los datos del cfdi emitido.
	 * @return string
	 */
	public function getQrCode($complemento = null) {
		switch (true) {
			case (($complemento == 'CartaPorte30' && @$this->CartaPorte30['Version'] == '3.0') || ($complemento == 'CartaPorte31' && @$this->CartaPorte31['Version'] == '3.1')):

				$url = "https://verificacfdi.facturaelectronica.sat.gob.mx/verificaccp/default.aspx";

				if(!empty($this->CartaPorte30)){
				$idCCP = "?IdCCP=" . $this->CartaPorte30["IdCCP"];
				$ubicaciones = $this->CartaPorte30["valor"]["cartaporte30:Ubicaciones"][0]["valor"]["cartaporte30:Ubicacion"];
				}elseif(!empty($this->CartaPorte31)){
				$idCCP = "?IdCCP=" . $this->CartaPorte31["IdCCP"];
				
					if(!empty($this->CartaPorte31["valor"]["cartaporte31:Ubicaciones"][0]["valor"]["cartaporte31:Ubicacion"]))
					$ubicaciones = $this->CartaPorte31["valor"]["cartaporte31:Ubicaciones"][0]["valor"]["cartaporte31:Ubicacion"];					
					elseif(!empty($this->CartaPorte31["valor"]["cartaporte31:Ubicaciones"][1]["valor"]["cartaporte31:Ubicacion"]))
					$ubicaciones = $this->CartaPorte31["valor"]["cartaporte31:Ubicaciones"][1]["valor"]["cartaporte31:Ubicacion"];	
				}
				$fechaHoraSalidaLlegadaOrigen = "";
				foreach ($ubicaciones as $ubicacion) {
					if ($ubicacion['TipoUbicacion'] === 'Origen') {
						$fechaHoraSalidaLlegadaOrigen = $ubicacion['FechaHoraSalidaLlegada'];
						break;
					}
				}
				$FechaOrig = "&FechaOrig=" . $fechaHoraSalidaLlegadaOrigen;
				$FechaTimb = "&FechaTimb=" . $this->TimbreFiscalDigital->FechaTimbrado;
				$qr = $url . $idCCP . $FechaOrig . $FechaTimb;

				break;
			default:

				$url = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx";
				$id = "?&id=" . $this->TimbreFiscalDigital->UUID;
				$re = "&re=" . $this->Emisor->Rfc;
				$rr = "&rr=" . $this->Receptor->Rfc;
				$total = "&tt=" . $this->addZeros($this->Total, 6);
				$fe = "&fe=" . substr($this->Sello, -8);
				$qr = $url . $id . $re . $rr . $total . $fe;

				break;
		}
		return $qr;
	}

	/**
	 * De existir el nodo de Timbre Fiscal crea la cadena original con los datos del timbre
	 * que nos retorna el pac .
	 * @return string $cadena
	 */
	public function getCadenaOriginalSAT() {
		// Crear un objeto DOMDocument para cargar el TFD
		$xml = new DOMDocument("1.0","UTF-8");
		$this->TimbreFiscalDigital->toXML();
		$xmlFile = $this->TimbreFiscalDigital->toStringXML();
		$xml->loadXML($xmlFile);
	
		// Crear un objeto DOMDocument para cargar el archivo de transformación XSLT
		$xsl = new DOMDocument();
		$xslFile = dirname(__FILE__) . "/xslt/cadenaoriginal_TFD_1_1.xslt";
		$xsl->load($xslFile);
	
		// Crear el procesador XSLT que nos generará la cadena original con base en las reglas descritas en el XSLT
		$proc = new XSLTProcessor;
		
		// Cargar las reglas de transformación desde el archivo XSLT.
		$proc->importStyleSheet($xsl);

		// Generar la cadena original y asignarla a una variable
		$cadenaOriginal = $proc->transformToXML($xml);
		return $cadenaOriginal;
	}

	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return sprintf('%0.' . $dec . 'f', $cantidad);
	}
} 

?>
