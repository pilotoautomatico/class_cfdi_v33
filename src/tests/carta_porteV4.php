<?php
    require '../../vendor/autoload.php';
    Use cfdi\ComprobanteV4;
    use DateTime;
    use DateTimeZone;
    try{
        $objDateTime = new DateTime("now", new DateTimeZone('America/Mexico_City'));
        $key = getcwd() . "/keys/00001000000505422144.key.pem";
	    $cer = getcwd() . "/keys/00001000000505422144.cer.pem";
        $cfdi= new ComprobanteV4();
        $cfdi->addKeys($cer, $key);

        //CARTA PORTE VERSION 2.0 CON VERSION 4 CFDI

        //----COMPROBANTE TIPO INGRESO----//

        //-DATOS COMPROBANTE Y EMISOR Y RECEPTOR PARA TIPO INGRESO 
        /* '00001000000401148681' */
        $cfdi->addGenerales('00001000000505422144', 200, 'AMD', 199.16, 'I', '99', '1', '64000', 'PPD', 'A','11',"","CondicionesDePago",1,'4.0',"",$objDateTime->format('Y-m-d\TH:i:s'),null,"01");
        $cfdi->addEmisor('PAU1207301E5', 'PILOTO AUTOMATICO', '601');
        $cfdi->addReceptor('PAU1207301E5', 'G03', 'PILOTO AUTOMATICO',"64000",null,null,'601');
        
        //-CONCEPTO PARA TIPO INGRESO
        $concepto=$cfdi->addConcepto("78101500","Cigarros",1,200,'Pieza','H87',null,1,'02');
        $concepto->addTraslado(1,'002','Tasa','0.16','0.16');
        $concepto->addRetencion(1,'001','Tasa','0.10','0');
        $concepto->addRetencion(1,'002','Tasa','0.106666','0');
        
        //-IMPUESTOS GLOBALES PARA TIPO INGRESO
        $cfdi->addImpuestosGlobales();

        // 1.AUTOTRANSPORTE
        
        //-CARTA PORTE
        $cartaPorte=$cfdi->addCartaPorte20('No',null,null,2,null);
        
        //--UBICACION CON DOMICILIO 1
        $ubicacion=$cartaPorte->addUbicacion('Origen', "EKU9003173C9",$objDateTime->format('Y-m-d\TH:i:s'),"OR101010");
        $ubicacion->addDomicilio("COA","MEX","25350","calle","211",null,"0347","23","casa blanca 1",'004');
        
        //--UBICACION CON DOMICILIO 2
        $ubicacion=$cartaPorte->addUbicacion('Destino', "AAA010101AAA",$objDateTime->format('Y-m-d\TH:i:s'),"DE202020",null,null,null,null,null,null,null,"1");
        $ubicacion->addDomicilio("COA","MEX","25350","calle","214",null,"0347","23","casa blanca 2",'004');
        
        //--UBICACION CON DOMICILIO 3
        $ubicacion=$cartaPorte->addUbicacion('Destino', "AAA010101AAA",$objDateTime->format('Y-m-d\TH:i:s'),"DE202021",null,null,null,null,null,null,null,"1");
        $ubicacion->addDomicilio("COA","MEX","25350","calle","220",null,"0347","23","casa blanca 3",'004');

        //--MERCANCIAS INFORMACION
        $mercancias=$cartaPorte->addMercancias(2,'2.0','XBX');

        //--MERCANCIAS->MERCANCIA CON CANTIDAD TRANSPORTA 1
        $mercancia=$mercancias->addMercancia('1.0',"11121900","Productos de perfumería",1,"XBX",null,null,null,'Sí','1266','4H2');
        $mercancia->addCantidadTransporta(1,'OR101010',"DE202020");
        
        //--MERCANCIAS->MERCANCIA CON CANTIDAD TRANSPORTA 2
        $mercancia=$mercancias->addMercancia('1.0',"11121900","Productos de perfumería",1,"XBX",null,null,null,'Sí','1266','4H2');
        $mercancia->addCantidadTransporta(1,'OR101010',"DE202021");

        //--MERCANCIAS AUTOTRANSPORTE Y AGREGANDO IDENTIFICACION VEHICULAR, SEGUROS, REMOLQUE
        $autotransporte=$mercancias->addAutotransporte("TPAF01", "NumPermisoSCT");
        $autotransporte->addIdentificacionVehicular("VL", "plac892", "2020");
        $autotransporte->addSeguros("SW Seguros", "123456789", "SW Seguros Ambientales", "123456789");
        $autotransporte->addRemolque("CTR021", "ABC123");
        
        //--FIGURA TRANSPORTE
        $cartaPorte->addFiguraTransporte("01", "VAAM130719H60", "a234567890");


        //2.TRANSPORTE FERROVIARIO

        // //-CARTA PORTE
        // $cartaPorte=$cfdi->addCartaPorte20("Sí",'Entrada',null,"50",'MEX');

        // //--UBICACION CON DOMICILIO 1
        // $ubicacion=$cartaPorte->addUbicacion('Origen', "XAXX010101000", $objDateTime->format('Y-m-d\TH:i:s'), "OR123457", "NombreRemitenteDestinatario", "NumRegIdTrib", "USA", "N618.8","PARACUARO",null,"01",null);
        // $ubicacion->addDomicilio("AL","USA","99056","calle","211",null,"002","0002","casa blanca","000005");
        
        // //--UBICACION CON DOMICILIO 2   
        // $ubicacion=$cartaPorte->addUbicacion('Destino', "URE180429TM6",$objDateTime->format('Y-m-d\TH:i:s'),"DE123457","NombreRemitenteDestinatario","NumRegIdTrib","USA","N618.8","PARACUARO",null,"01","10");
        // $ubicacion->addDomicilio("AL","USA","99056","calle","211",null,"002","0002","casa blanca","000005");
        
        // //--UBICACION CON DOMICILIO 3 
        // $ubicacion=$cartaPorte->addUbicacion('Destino', "URE180429TM6",$objDateTime->format('Y-m-d\TH:i:s'),"DE123457","NombreRemitenteDestinatario","NumRegIdTrib","USA","N618.8","PARACUARO",null,"01","10");
        // $ubicacion->addDomicilio("AL","USA","99056","calle","211",null,"002","0002","casa blanca","000005");

        // //--UBICACION CON DOMICILIO 4
        // $ubicacion=$cartaPorte->addUbicacion('Destino', "URE180429TM6",$objDateTime->format('Y-m-d\TH:i:s'),"DE123457","NombreRemitenteDestinatario","NumRegIdTrib","USA","N618.8","PARACUARO",null,"01","10");
        // $ubicacion->addDomicilio("AL","USA","99056","calle","211",null,"002","0002","casa blanca","000005");
        
        // //--UBICACION CON DOMICILIO 5
        // $ubicacion=$cartaPorte->addUbicacion('Destino', "URE180429TM6",$objDateTime->format('Y-m-d\TH:i:s'),"DE123457","NombreRemitenteDestinatario","NumRegIdTrib","USA","N618.8","PARACUARO",null,"01","10");
        // $ubicacion->addDomicilio("AL","USA","99056","calle","211",null,"002","0002","casa blanca","000005");
        
        // //--UBICACION CON DOMICILIO 6
        // $ubicacion=$cartaPorte->addUbicacion('Destino', "URE180429TM6",$objDateTime->format('Y-m-d\TH:i:s'),"DE123457","NombreRemitenteDestinatario","NumRegIdTrib","USA","N618.8","PARACUARO",null,"01","10");
        // $ubicacion->addDomicilio("AL","USA","99056","calle","211",null,"002","0002","casa blanca","000005");

        // //--MERCANCIAS INFORMACION
        // $mercancias=$cartaPorte->addMercancias(1,1,'A44',1,1);

        // // //--MERCANCIAS->MERCANCIA CON CANTIDAD TRANSPORTA Y PEDIMENTOS 1
        // $mercancia=$mercancias->addMercancia('1',"01010101","Descripcion",1,"A1",'123456','Unidad',"18/62/31cm",'No',null,null,"DescripEmbalaje",null,null,"3923100301","59AB274B-6CBC-4176-9BD7-BB67B28D3EA4");
        // $mercancia->addPedimentos("21  47  3807  8003832");
        // $mercancia->addCantidadTransporta(1,"OR123457","DE123457");

        // // //--MERCANCIAS TRANSPORTE FERROVIARIO Y AGREGANDO DERECHO DE PASO Y CARRO AGREGANDO SU CONTENEDOR
        // $transporteFerroviario=$mercancias->addTransporteFerroviario("TS02","nombreaseg","numpolizaseguro", null,"TT02");
        // $transporteFerroviario->addDerechosDePaso("CDP002",1);
        // $carro=$transporteFerroviario->addCarro("TC03", "matriculacarro", "guiacarro", "1");
        // $carro->addContenedor("TC02", 1,1);

        // //--FIGURA TRANSPORTE AGREGANDO PARTES TRANSPORTE Y DOMICILIO
        // $figuratransporte=$cartaPorte->addFiguraTransporte("03", "URE180429TM6", null,"NombreFigura");
        // $figuratransporte->addPartesTransporte("PT02");
        // $figuratransporte->addDomicilio("AL","USA","99056","calle","211",null,"002","0002","casa blanca","000005");

        //3.TRANSPORTE AEREO

        // //-CARTA PORTE
        // $cartaPorte=$cfdi->addCartaPorte20('Sí',"Entrada",null,null,"MEX");
        
        // //--UBICACION CON DOMICILIO 1
        // $ubicacion=$cartaPorte->addUbicacion("Destino", "URE180429TM6",$objDateTime->format('Y-m-d\TH:i:s'),"DE123457","NombreRemitenteDestinatario",null,null,"EA0417","Loreto",null,"01");
        // $ubicacion->addDomicilio("ZAC","MEX","98057","calle","211",null,"0002","03","casa blanca","056");
        
        // //--UBICACION CON DOMICILIO 2
        // $ubicacion=$cartaPorte->addUbicacion("Origen", "XAXX010101000",$objDateTime->format('Y-m-d\TH:i:s'),"OR123457","NombreRemitenteDestinatario",null,null,"EA0417","Loreto",null,"02");
        // $ubicacion->addDomicilio("ZAC","MEX","98057","calle","211",null,"0002","03","casa blanca","056");

        // //--MERCANCIAS INFORMACION
        // $mercancias=$cartaPorte->addMercancias('1','1',"A44",'1','1');

        // //--MERCANCIAS->MERCANCIA CON CANTIDAD TRANSPORTA y PEDIMENTOS 1 
        // $mercancia=$mercancias->addMercancia('1',"01010101","Descripcion",'1',"A1","123456","Unidad","18/62/31cm",'No',null,null,null,1,"MXN","3923100301","59AB274B-6CBC-4176-9BD7-BB67B28D3EA4");
        // $mercancia->addPedimentos("21  47  3807  8003832");
        // $mercancia->addCantidadTransporta(1,"OR123457","DE123457");

        // //--MERCANCIAS TRANSPORTE AEREO
        // $mercancias->addTransporteAereo("TPAF01","Demo","61E5-WZ","acUbYlBVTmlzx","CA001","NombreAseg","NumPolizaSeguro","LugarContrato",null,null,null,null,null,"NumRegIdTribEmbarc","CAN","Embarcador");
        
        // //--FIGURA TRANSPORTE
        // $figuratransporte=$cartaPorte->addFiguraTransporte("02", "URE180429TM6", null,"NombreFigura");
        // $figuratransporte->addPartesTransporte("PT02");
        // $figuratransporte->addDomicilio("ZAC","MEX","98057","calle","211",null,"0002","03","casa blanca","056");
        
        //4.TRANSPORTE MARITIMO

        // //-CARTA PORTE
        // $cartaPorte=$cfdi->addCartaPorte20('No');

        // //--UBICACION CON DOMICILIO 1
        // $ubicacion=$cartaPorte->addUbicacion("Origen", "URE180429TM6",$objDateTime->format('Y-m-d\TH:i:s'),"OR123457","NombreRemitenteDestinatario",null,null,"PM001","Rosarito","Altura","01");
        // $ubicacion->addDomicilio("ZAC","MEX","99080","calle","211",null,"0814","01","casa blanca","010");
        
        // //--UBICACION CON DOMICILIO 2
        // $ubicacion=$cartaPorte->addUbicacion("Destino", "XAXX010101000",$objDateTime->format('Y-m-d\TH:i:s'),"DE123457","NombreRemitenteDestinatario","NumRegIdTrib",'USA',"PM001","Rosarito",'Altura',"02");
        // $ubicacion->addDomicilio("ZAC","MEX","99080","calle","211",null,"0814","01","casa blanca","010");

        // //--MERCANCIAS INFORMACION
        // $mercancias=$cartaPorte->addMercancias('1','1',"A44",'1','1');

        // //--MERCANCIAS->MERCANCIA CON CANTIDAD TRANSPORTA y DETALLE MERCANCIA 1
        // $mercancia=$mercancias->addMercancia('1',"01010101","Descripcion",'1',"A1","123456","Unidad","18/62/31cm",'No',null,null,null,null,null,null,"59AB274B-6CBC-4176-9BD7-BB67B28D3EA4");
        // $mercancia->addDetalleMercancia("A44",1,1,1,1);
        // $mercancia->addCantidadTransporta(1,"OR123457","DE123457");

        // //--MERCANCIAS TRANSPORTE MARITIMO CON SU CONTENEDOR
        // $transporteMaritimo=$mercancias->addTransporteMaritimo("B02", "1234567", "IMO1234567",'MEX',"10","CGS","numcert","nombreAgente","SCT418/020/2016","TPAF01",'1',"nombreAseg","numpolisa","2021","nombreEnbc","10","10","10","lineanavera","numviaje","numconceembarc");
        // $transporteMaritimo->addContenedor("12345678901","CM001","numprecinto");
        
        // //--FIGURA TRANSPORTE CON PARTES TRANSPORTE Y DOMICILIO
        // $figuratransporte=$cartaPorte->addFiguraTransporte("02", "URE180429TM6", null,"NombreFigura");
        // $figuratransporte->addPartesTransporte("PT02");
        // $figuratransporte->addDomicilio("ZAC","MEX","99080","calle","211",null,"0814","01","casa blanca","010");

        //----COMPROBANTE TIPO TRASLADO----//

        // //-DATOS COMPROBANTE Y EMISOR Y RECEPTOR PARA TIPO TRASLADO
        // $cfdi->addGenerales('00001000000505422144', 0, 'XXX', 0, 'T', null, null, "20000", null, '12',null,"","CondicionesDePago",0,'4.0',"",$objDateTime->format('Y-m-d\TH:i:s'),null,"01");
        // $cfdi->addEmisor('PAU1207301E5', 'PILOTO AUTOMATICO SA DE CV', '601');
        // $cfdi->addReceptor('RPE841207E99', 'G01', 'RESTAURANTE PERISUR SA DE CV',"26015",null,null,'601');
        
        // //-CONCEPTO PARA TIPO TRASLADO
        // $concepto=$cfdi->addConcepto("50211503","Cigarros",1,0,'Pieza','H87',"UT421511",null,'01');
        // $concepto->addInformacionAduanera("21  47  3807  8003832");
        // $concepto->addParte("50211503",'123',1,'Pieza','cosas',200,200);

        //1.AUTOTRANSPORTE

        // //-CARTA PORTE
        // $cartaPorte=$cfdi->addCartaPorte20('No',null,null,1,null);

        // //--UBICACION CON DOMICILIO 1
        // $ubicacion=$cartaPorte->addUbicacion("Origen", "EKU9003173C9",$objDateTime->format('Y-m-d\TH:i:s'),"OR101010");
        // $ubicacion->addDomicilio("COA","MEX","25350","calle","211",null,"0347","23","casa blanca","004");
        
        // //--UBICACION CON DOMICILIO 2
        // $ubicacion=$cartaPorte->addUbicacion("Destino", "AAA010101AAA",$objDateTime->format('Y-m-d\TH:i:s'),"DE202020",null,null,null,null,null,null,null,1);
        // $ubicacion->addDomicilio("COA","MEX","25350","calle","214",null,"0347","23","casa blanca","004");

        // //--MERCANCIAS INFORMACION
        // $mercancias=$cartaPorte->addMercancias(1,'2.0','XBX');

        // //--MERCANCIAS->MERCANCIA CON CANTIDAD TRANSPORTA 1
        // $mercancia=$mercancias->addMercancia('1.0',"11121900","Accesorios de equipo de telefonía",'1.0',"XBX",null,'No');
        // $mercancia->addCantidadTransporta(1,'OR101010',"DE202020");

        // //--MERCANCIAS AUTOTRANSPORTE Y AGREGANDO IDENTIFICACION VEHICULAR, SEGUROS, REMOLQUE
        // $autotransporte=$mercancias->addAutotransporte("TPAF01", "NumPermisoSCT");
        // $autotransporte->addIdentificacionVehicular("VL", "plac892", "2020");
        // $autotransporte->addSeguros("SW Seguros", "123456789", null, null,"SW Seguros");
        // $autotransporte->addRemolque("CTR004", "VL45K98");

        // //--FIGURA TRANSPORTE
        // $cartaPorte->addFiguraTransporte("01", "VAAM130719H60", "a234567890");
        
        //2.TRANSPORTE FERROVIARIO

        // //-CARTA PORTE
        // $cartaPorte=$cfdi->addCartaPorte20('No',null,null,500,null);

        // //--UBICACION 1
        // $cartaPorte->addUbicacion('Origen', "URE180429TM6", $objDateTime->format('Y-m-d\TH:i:s'), "OR000001", "SW SMARTERWEB BODEGAS", null, null, "E366.5","OAXACA",null,"01",null);
        
        // //--UBICACION 2
        // $cartaPorte->addUbicacion('Destino', "EKU9003173C9",$objDateTime->format('Y-m-d\TH:i:s'),"DE000002","SW SMARTERWEB BODEGAS",null,null,"K17","JUCHITAN",null,"02",100);
        
        // //--UBICACION 3 
        // $cartaPorte->addUbicacion('Destino', "EKU9003173C9",$objDateTime->format('Y-m-d\TH:i:s'),"DE000003","SW SMARTERWEB BODEGAS",null,null,"Z203.1","MATIAS ROMERO",null,"02",100);

        // //--UBICACION 4
        // $cartaPorte->addUbicacion('Destino', "EKU9003173C9",$objDateTime->format('Y-m-d\TH:i:s'),"DE000004","SW SMARTERWEB BODEGAS",null,null,"GF016","TUXTEPEC",null,"02",100);
        
        // //--UBICACION 5
        // $cartaPorte->addUbicacion('Destino', "EKU9003173C9",$objDateTime->format('Y-m-d\TH:i:s'),"DE000005","SW SMARTERWEB BODEGAS",null,null,"B0113","SAYULA",null,"02",100);
        
        // //--UBICACION 6
        // $cartaPorte->addUbicacion('Destino', "EKU9003173C9",$objDateTime->format('Y-m-d\TH:i:s'),"DE000006","SW SMARTERWEB BODEGAS",null,null,"ZA012","MINATITLAN",null,"03",100);

        // //--MERCANCIAS INFORMACION
        // $mercancias=$cartaPorte->addMercancias(2,1000,"KGM",10,null);

        // //--MERCANCIAS->MERCANCIA CON CANTIDAD TRANSPORTA 1
        // $mercancia=$mercancias->addMercancia(500,"47121811","Máquinas para limpiar ductos",4,"H87",null,'PZA');
        // $mercancia->addCantidadTransporta(4,"OR000001","DE000006");

        // //--MERCANCIAS->MERCANCIA CON CANTIDAD TRANSPORTA 2
        // $mercancia=$mercancias->addMercancia(500,"23153130","Soportes de máquina o aisladores de vibración",100,"H87",null,'PZA');
        // $mercancia->addCantidadTransporta(10,"OR000001","DE000006");

        // //--MERCANCIAS TRANSPORTE FERROVIARIO Y AGREGANDO DERECHO DE PASO Y CARRO
        // $transporteFerroviario=$mercancias->addTransporteFerroviario("TS01",null,null, null,"TT01");
        // $transporteFerroviario->addDerechosDePaso("CDP114",100);
        // $transporteFerroviario->addCarro("TC08", "A00012", "123ASD", 10);
        
        // //--FIGURA TRANSPORTE
        // $cartaPorte->addFiguraTransporte("01", "CACX7605101P8", "12345678", "SW SW SW");

        //3.TRANSPORTE AEREO

        // //-CARTA PORTE
        // $cartaPorte=$cfdi->addCartaPorte20('No');
        
        // //--UBICACION CON DOMICILIO 1
        // $ubicacion=$cartaPorte->addUbicacion("Destino", "URE180429TM6",$objDateTime->format('Y-m-d\TH:i:s'),null,"NombreRemitenteDestinatario",null,null,"EA0689","Cap.P.A. José Covarrubias Pérez",null,"01");
        // $ubicacion->addDomicilio("ZAC","MEX","99056","calle","211",null,null,null,"casa blanca",null);
        
        // //--UBICACION CON DOMICILIO 2
        // $ubicacion=$cartaPorte->addUbicacion("Origen", "XAXX010101000",$objDateTime->format('Y-m-d\TH:i:s'),null,"NombreRemitenteDestinatario","NumRegIdTrib","USA","EA0748","Campo Puerto Rico",null,"02");
        // $ubicacion->addDomicilio("ZAC","MEX","99056","calle","211",null,null,null,"casa blanca",null);

        // //--MERCANCIAS INFORMACION
        // $mercancias=$cartaPorte->addMercancias(1,1,"A44",1,1);

        // //--MERCANCIAS->MERCANCIA 1  
        // $mercancias->addMercancia(1,"01010101","Descripcion",1,"A1","123456","Unidad","18/62/31cm",'No',null,null,"DescripEmbalaje",1,"MXN",null,"59AB274B-6CBC-4176-9BD7-BB67B28D3EA4");

        // //--MERCANCIAS TRANSPORTE AEREO
        // $mercancias->addTransporteAereo("TPAF01","Demo","61E5-WZ","acUbYlBVTmlzx","CA001","NombreAseg","NumPolizaSeguro","LugarContrato",null,null,null,null,"MELR721216EA1",null,null,"Embarcador");
        
        // //--FIGURA TRANSPORTE
        // $figuratransporte=$cartaPorte->addFiguraTransporte("01", "MELR721216EA1", null,"NombreFigura");
        // $figuratransporte->addDomicilio("JAL","MEX","45601","Calle","NumeroExterior","NumeroInterior",null,null,"Referencia");

        //4.TRANSPORTE MARITIMO

        // //-CARTA PORTE
        // $cartaPorte=$cfdi->addCartaPorte20('No');

        // //--UBICACION CON DOMICILIO 1
        // $ubicacion=$cartaPorte->addUbicacion("Origen", "URE180429TM6",$objDateTime->format('Y-m-d\TH:i:s'),"OR123457","NombreRemitenteDestinatario",null,null,"PM001","Rosarito","Altura","01");
        // $ubicacion->addDomicilio("ZAC","MEX","99080","calle","211",null,"0814","01","casa blanca","010");
        
        // //--UBICACION CON DOMICILIO 2
        // $ubicacion=$cartaPorte->addUbicacion("Destino", "XAXX010101000",$objDateTime->format('Y-m-d\TH:i:s'),"DE123457","NombreRemitenteDestinatario","NumRegIdTrib",'USA',"PM001","Rosarito",'Altura',"02");
        // $ubicacion->addDomicilio("ZAC","MEX","99080","calle","211",null,"0814","01","casa blanca","010");

        // //--MERCANCIAS INFORMACION
        // $mercancias=$cartaPorte->addMercancias(1, 1,"A44", 1,1);

        // //--MERCANCIAS->MERCANCIA CON CANTIDAD TRANSPORTA y DETALLE MERCANCIA 1
        // $mercancia=$mercancias->addMercancia(1,"01010101","Descripcion",1,"A1","123456","Unidad","18/62/31cm",'No',null,null,null,null,null,null,"59AB274B-6CBC-4176-9BD7-BB67B28D3EA4");
        // $mercancia->addDetalleMercancia("A44",1,1,1,1);
        // $mercancia->addCantidadTransporta(1,"OR123457","DE123457");

        // //--MERCANCIAS TRANSPORTE MARITIMO CON SU CONTENEDOR
        // $transporteMaritimo=$mercancias->addTransporteMaritimo("B02", "1234567", "IMO1234567",'MEX', 10,"CGS","numcert","nombreAgente","SCT418/020/2016","TPAF01",'1',"nombreAseg","numpolisa","2021","nombreEnbc","10","10","10","lineanavera","numviaje","numconceembarc");
        // $transporteMaritimo->addContenedor("12345678901","CM001","numprecinto");
        
        // //--FIGURA TRANSPORTE CON PARTES DE TRANSPORTE Y DOMICILIO
        // $figuratransporte=$cartaPorte->addFiguraTransporte("02", "URE180429TM6", null,"NombreFigura");
        // $figuratransporte->addPartesTransporte("PT02");
        // $figuratransporte->addDomicilio("ZAC","MEX","99080","calle","211",null,"0814","01","casa blanca","010");

        $cfdi->addSellos();
        //$cfdi->validateSellos();

        $cfdi->validar();

        $cfdi->validateXSD();
        $cfdi->toXML();
	    $cfdi->toSaveXML();

	    error_log(date("Y-m-d H:i:s") . " : CREATE CFDI GLOBAL: ".print_r($cfdi, true)." \n", 3, "debug.log");
        
        error_log(date("Y-m-d H:i:s") . " : LOAD CFDI GLOBAL: ".print_r($cfdi, true)." \n", 3, "debug.log");

	    print_r($cfdi);

    }catch (Exception $e) {
        echo 'Error al generar el CFDI: ', $e->getMessage(), "\n";
        error_log(date("Y-m-d H:i:s") . " : Error al generar el CFDI INGRESO CON O SIN ADDENDA: " . print_r($e->getMessage(), true) . "\n", 3, "debug.log");
    }
   
?>