<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

use Exception;
use DOMDocument;

Use cfdi\Logger;

class Receptor {
	var $Rfc;
	var $Nombre;
	var $ResidenciaFiscal;
	var $NumRegIdTrib;
	var $UsoCFDI;
	var $xml;
	var $logger;

	public function __construct($Rfc, $UsoCFDI, $Nombre = null, $ResidenciaFiscal = null, $NumRegIdTrib = null) {
		$this->Rfc = $Rfc;
		$this->Nombre = $Nombre;
		$this->ResidenciaFiscal = $ResidenciaFiscal;
		$this->NumRegIdTrib = $NumRegIdTrib;
		$this->UsoCFDI = $UsoCFDI;
		$this->logger = new Logger(); //clase para escribir logs
	}

	public function validar() {
		$required = array(
			'Rfc',
			'UsoCFDI'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Receptor validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Receptor Campo Requerido: ' . $field);
			}
		}
	}

	public function toXML() {
		$this->xml = new DOMdocument("1.0", "UTF-8");
		$domreceptor = $this->xml->createElement('cfdi:Receptor');
		$this->xml->appendChild($domreceptor);

		$domreceptor->setAttribute('Rfc', ($this->Rfc));
		if ($this->Nombre)
			$domreceptor->setAttribute('Nombre', $this->Nombre);
		if ($this->ResidenciaFiscal)
			$domreceptor->setAttribute('ResidenciaFiscal', $this->ResidenciaFiscal);
		if ($this->NumRegIdTrib)
			$domreceptor->setAttribute('NumRegIdTrib', $this->NumRegIdTrib);
		$domreceptor->setAttribute('UsoCFDI', $this->UsoCFDI);

		return $domreceptor;
	}

	function toStringXML() {
		return $this->xml->saveXML();
	}

	function importXML() {
		$xml = $this->xml->getElementsByTagName("cfdi:Receptor")->item(0);
		return $xml;
	}
}
?>