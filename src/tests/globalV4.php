<?php
    require '../../vendor/autoload.php';
    Use cfdi\ComprobanteV4;
    try{
        $key = getcwd() . "/keys/00001000000505422144.key.pem";
	    $cer = getcwd() . "/keys/00001000000505422144.cer.pem";
        $cfdi= new ComprobanteV4();
        $cfdi->addKeys($cer, $key);
        /* '00001000000401148681' */
        $cfdi->addGenerales('00001000000505422144', 3000, 'MXN', 3400, 'I', '99', 1, '35045', 'PUE', '12',null,"","CondicionesDePago",null,'4.0',"",date("Y-m-d\TH:i:s"),null,"01");
        $cfdi->addInformacionGlobal('01','12','2021');
        $cfdi->addEmisor('PAU1207301E5', 'PILOTO AUTOMATICO SA DE CV', '601');
        $cfdi->addReceptor('XAXX010101000', 'P01', 'PUBLICO EN GENERAL',"26015",null,null,'601');

        //se agrega concepto al comprobante
        $concepto = $cfdi->addConcepto('01010101', 'Venta', 1, 1000, 'Actividad', 'ACT', '01',null,'02');
        $concepto->addTraslado(1000, '002', 'Tasa', '0.160000', 160);
        $concepto->addTraslado(1000, '003', 'Tasa', '0.070000', 70);
        $concepto->addRetencion(1000, '002', 'Tasa', '0.150000', 150);

        //se agrega concepto al comprobante
        $concepto = $cfdi->addConcepto('01010101', 'Venta', 1, 1000, 'Actividad', 'ACT', '01',null,'02');
        $concepto->addTraslado(1000, '002', 'Tasa', '0.160000', 160);
        $concepto->addRetencion(1000, '002', 'Tasa', '0.100000', 100);

        //se agrega concepto al comprobante
        $concepto = $cfdi->addConcepto('01010101', 'Venta', 1, 1000, 'Actividad', 'ACT', '01',null,'02');
        $concepto->addTraslado(1000, '003', 'Tasa', '0.300000', 300);
        $concepto->addRetencion(1000, '001', 'Tasa', '0.040000', 40);

        $cfdi->addImpuestosGlobales();

        $cfdi->addSellos();
        //$cfdi->validateSellos();

        $cfdi->validar();

        $cfdi->validateXSD();
        $cfdi->toXML();
	    $cfdi->toSaveXML();

	    error_log(date("Y-m-d H:i:s") . " : CREATE CFDI GLOBAL: ".print_r($cfdi, true)." \n", 3, "debug.log");
        
        error_log(date("Y-m-d H:i:s") . " : LOAD CFDI GLOBAL: ".print_r($cfdi, true)." \n", 3, "debug.log");

	    print_r($cfdi);

    }catch (Exception $e) {
        echo 'Error al generar el CFDI: ', $e->getMessage(), "\n";
        error_log(date("Y-m-d H:i:s") . " : Error al generar el CFDI INGRESO CON O SIN ADDENDA: " . print_r($e->getMessage(), true) . "\n", 3, "debug.log");
    }
   
?>