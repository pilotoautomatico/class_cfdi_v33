<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

Use Exception;
use DOMDocument;

class ConceptoV4 {
	//normales
	var $ClaveProdServ;
	var $Descripcion;
	var $Cantidad;
	var $ValorUnitario;
	var $Importe;
	var $Unidad;
	var $NoIdentificacion;
	var $ClaveUnidad;
	var $Descuento;
	var $ObjetoImp;//
	//objetos
	var $xml_base;
	var $Traslados = array();
	var $Retenciones = array();
	var $InformacionAduanera = [];//
	var $CuentaPredial;
	var $ComplementoConcepto;//
	var $Parte;//
	var $Decimales;
	var $logger;
	var $TipoDeComprobante;
	var $ACuentaTerceros;

	function __construct($ClaveProdServ, $Descripcion, $Cantidad, $ValorUnitario, $Unidad = null, $ClaveUnidad, $NoIdentificacion = null, $Descuento = null, $Decimales = 2, $TipoDeComprobante=null, $ObjetoImp=null) {
		$this->Decimales = $Decimales;
		$this->ClaveProdServ = $ClaveProdServ;
		$this->NoIdentificacion = $NoIdentificacion;
		$this->Descripcion = $Descripcion;
		$this->Cantidad = $Cantidad;
		$this->ValorUnitario = $ValorUnitario;
		$this->Importe = round($Cantidad * $ValorUnitario, 6);
		$this->Unidad = $Unidad;
		$this->NoIdentificacion = $NoIdentificacion;
		$this->Descuento = $Descuento;
		$this->ClaveUnidad = $ClaveUnidad;
		$this->Traslados = array();
		$this->Retenciones = array();
		$this->ComplementoConcepto = null;
		$this->ACuentaTerceros=null;
		$this->logger = new Logger(); //clase para escribir logs
		$this->TipoDeComprobante = $TipoDeComprobante;
		$this->ObjetoImp = $ObjetoImp;//
	}

	function validar() {
		# valida campos requeridos de concepto
		$required = array(
			'ClaveProdServ',
			'Descripcion',
			'Cantidad',
			'ValorUnitario',
			'ClaveUnidad',
			'Importe',
			'ObjetoImp'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Concepto validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Concepto Campo Requerido: ' . $field);
			}
		}

		if(!empty($this->NoIdentificacion)){
			if(strlen($this->NoIdentificacion) < 1 || strlen($this->NoIdentificacion) >100 ){
				$this->logger->write('Concepto validar No Identifación: Debe contener entre 1 a 100 carácter(es) .');
				throw new Exception('El valor de Concepto No Identifación debe ser entre 1 a 100 carácter(es): len='.strlen($this->NoIdentificacion));
			}
		}

		#valida longitud de descripcion
		// if(strlen($this->Descripcion) < 1 || strlen($this->Descripcion) > 1000){
		// se retiró la validación de 1000 carácteres para poder cargar comprobantes que llegan
		// con esta cantidad de carácteres
		if(strlen($this->Descripcion) < 1){
			$this->logger->write('Concepto validar Descripcion: Debe contener al menos 1 carácter.');
			throw new Exception('El valor de Concepto Descripcion es menor a 1 carácter: len='.strlen($this->Descripcion));
		}

		if(!empty($this->Unidad)){
			if(strlen($this->Unidad) < 1 || mb_strlen($this->Unidad) >20 ){
				$this->logger->write('Concepto validar Unidad: Debe contener entre 1 a 20 carácter(es) .');
				throw new Exception('El valor de Concepto Unidad debe ser entre 1 a 20 carácter(es): len='.mb_strlen($this->Unidad));
			}
		}
		
		#valida decimales
		$this->validateDecimals();

		#valida maximos y minimos de concepto
		$limites = $this->validateMaxMin();
			if ($this->Importe < $limites['minimo'] || $this->Importe > $limites['maximo']) {
				$this->logger->write("Concepto validar maximos y minimos(): el importe de " . $this->Importe . "esta fuera del rango permitido minimo :" . $limites["minimo"] . " maximo: " . $limites["maximo"]);
				throw new Exception('El Concepto con el importe' . $this->Importe . 'esta fuera del limite permitido , minimo :' . $limite["minimo"] . " maximo :" . $limite["maximo"]);
			}
		#valida traslados y retenciones
		foreach ($this->Traslados as $traslado) {
			$traslado->validar();
		}
		foreach ($this->Retenciones as $retencion) {
			$retencion->validar();
		}
		
		foreach($this->InformacionAduanera as $InformacionAduanera)
        {
			$InformacionAduanera->validar();
		}

		#valida a cuenta terceros
		if(!empty($this->ACuentaTerceros)){
			$this->ACuentaTerceros->validar();
		}

		#valida datos de catalogos de concepto
		
		#valida complementoConcepto
		if($this->ComplementoConcepto)
			$this->ComplementoConcepto->validar();

		if($this->Parte){
			foreach($this->Parte as $Parte)
			{
				$Parte->validar();
			}
		}
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$concepto = $this->xml_base->createElement("cfdi:Concepto");
		$this->xml_base->appendChild($concepto);

		# datos de concepto
		$concepto->SetAttribute('ClaveProdServ', $this->ClaveProdServ);
		if ($this->NoIdentificacion)
			$concepto->SetAttribute('NoIdentificacion', $this->NoIdentificacion);
		$concepto->SetAttribute('Cantidad', $this->Cantidad);
		$concepto->SetAttribute('ClaveUnidad', $this->ClaveUnidad);
		if ($this->Unidad)
			$concepto->SetAttribute('Unidad', $this->Unidad);
		$concepto->SetAttribute('Descripcion', $this->Descripcion);

		$ValorUnitario = $this->ValorUnitario;
		if($this->TipoDeComprobante != 'P' && $this->TipoDeComprobante != 'N'){
			$ValorUnitario = $this->addZeros($this->ValorUnitario,6);
		}
		$concepto->SetAttribute('ValorUnitario', $ValorUnitario);
		$concepto->SetAttribute('Importe', $this->addZeros($this->Importe, 6));
		if (!is_null($this->Descuento))
			$concepto->SetAttribute('Descuento', $this->addZeros($this->Descuento, 6));
		$concepto->SetAttribute('ObjetoImp', $this->ObjetoImp);
		# impuestos
		if (!empty($this->Traslados) || !empty($this->Retenciones)) {
			$impuestos = $this->xml_base->createElement("cfdi:Impuestos");
			$concepto->appendChild($impuestos);

			# traslados
			if (!empty($this->Traslados)) {
				$traslados = $this->xml_base->createElement("cfdi:Traslados");
				$impuestos->appendChild($traslados);
				foreach ($this->Traslados as $key => $traslado) {
					$traslado->toXML();
					$traslado_xml = $this->xml_base->importNode($traslado->importXML(), true);
					$traslados->appendChild($traslado_xml);
				}
			}

			# retenciones
			if (!empty($this->Retenciones)) {
				$retenciones = $this->xml_base->createElement("cfdi:Retenciones");
				$impuestos->appendChild($retenciones);
				foreach ($this->Retenciones as $key => $retencion) {
					$retencion->toXML();
					$retencion_xml = $this->xml_base->importNode($retencion->importXML(), true);
					$retenciones->appendChild($retencion_xml);
				}
			}
		}

		# ACuentaTerceros
		if(!empty($this->ACuentaTerceros)){
			$this->ACuentaTerceros->toXML();
			$ACuentaTerceros_xml = $this->xml_base->importNode($this->ACuentaTerceros->importXML(), true);
			// $ACuentaTerceros->appendChild($ACuentaTerceros_xml);
			$concepto->appendChild($ACuentaTerceros_xml);
		}

		# informacion aduanera
		if (!empty($this->InformacionAduanera)) 
		{
			foreach($this->InformacionAduanera as $InformacionAduanera)
			{
                $InformacionAduanera->toXML();
                $nodoInformacionAduanera = $this->xml_base->importNode($InformacionAduanera->importXML(), true);
                $concepto->appendChild($nodoInformacionAduanera);
			}
		}
		
		# cuenta predial
		if (!empty($this->CuentaPredial)) {
			$cuentaPredial = $this->xml_base->createElement("cfdi:CuentaPredial");
			$concepto->appendChild($cuentaPredial);
			$cuentaPredial->SetAttribute('Numero', $this->CuentaPredial);
		}

		# complemento concepto
		if(!empty($this->ComplementoConcepto)){
			$complementoConcepto = $this->xml_base->createElement("cfdi:ComplementoConcepto");
			$concepto->appendChild($complementoConcepto);
			$this->ComplementoConcepto->toXML();
			$complementoConcepto_xml = $this->xml_base->importNode($this->ComplementoConcepto->importXML(), true);
			$complementoConcepto->appendChild($complementoConcepto_xml);
		}

		# Parte
		if (!empty($this->Parte)) 
		{
			foreach($this->Parte as $Parte)
			{
                $Parte->toXML();
                $nodoParte = $this->xml_base->importNode($Parte->importXML(), true);
                $concepto->appendChild($nodoParte);
			}
		}
		return $concepto;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("cfdi:Concepto")->item(0);
		return $xml;
	}

	function addCuentaPredial($cuentaPredial){
		$this->CuentaPredial = $cuentaPredial;
	}

	function addInformacionAduanera($NumeroPedimento)
	{
		$InformacionAduanera = new ConceptoInformacionAduaneraV4(
            $NumeroPedimento
        );
		
		$this->InformacionAduanera[] = $InformacionAduanera;
		return $InformacionAduanera;
	}

	function addParte($ClaveProdServP, $NoIdentificacionP, $CantidadP, $UnidadP, $DescripcionP, $ValorUnitarioP, $ImporteP)
	{
		$Parte = new ParteV4(
            $ClaveProdServP, 
			$NoIdentificacionP, 
			$CantidadP, 
			$UnidadP, 
			$DescripcionP, 
			$ValorUnitarioP, 
			$ImporteP
        );
		$this->Parte[] = $Parte;
		return $Parte;
	}

	function addTraslado($Base, $Impuesto, $TipoFactor, $TasaOCuota = null, $Importe = null, $facturaGlobalV4=false) {
		//validar si ya esta agregar un traslado igual ya no agregar otro
		// foreach($this->Traslados as $traslado){
		// 	if($traslado->Base == $Base && $traslado->Impuesto==$Impuesto && $traslado->TipoFactor==$TipoFactor
		// 		&& $traslado->TasaOCuota==$TasaOCuota && $traslado->Importe==$Importe){
		// 		return $traslado;
		// 	}
		// }
		
		$traslado = new TrasladoConcepto(
				$Base, $Impuesto, $TipoFactor, $TasaOCuota, $Importe, $this->Decimales, $facturaGlobalV4
		);
		$this->Traslados[] = $traslado;
		return $traslado;
	}

	function addRetencion($Base, $Impuesto, $TipoFactor, $TasaOCuota = null, $Importe = null) {
		$retencion = new RetencionConcepto(
			$Base, $Impuesto, $TipoFactor, $TasaOCuota, $Importe, $this->Decimales
		);
		$this->Retenciones[] = $retencion;
		return $retencion;
	}

	function addComplementoConceptoIedu($nombreAlumno, $CURP, $nivelEducativo, $autRVOE, $rfcPago=null) {
		$comp = new ComplementoConceptoIedu(
				$nombreAlumno, $CURP, $nivelEducativo, $autRVOE, $rfcPago
		);
		$this->ComplementoConcepto = $comp;
		return $comp;
	}

	function addComplementoConceptoTerceros($rfc, $nombre=null) {
		$comp = new ComplementoConceptoTerceros(
			$rfc, $nombre
		);
		$this->ComplementoConcepto = $comp;
		return $comp;
	}

	function addACuentaTerceros($RfcACuentaTerceros, $NombreACuentaTerceros, $RegimenFiscalACuentaTerceros, $DomicilioFiscalACuentaTerceros) {
		$act = new ACuentaTerceros(
			$RfcACuentaTerceros, $NombreACuentaTerceros, $RegimenFiscalACuentaTerceros,$DomicilioFiscalACuentaTerceros
		);
		$this->ACuentaTerceros = $act;
		return $act;
	}

	function getMin() {
		// $decimalesNum = strlen(substr(strrchr(sprintf('%0.' . $this->Decimales . 'f', $this->ValorUnitario), "."), 1));
		// $minimo = $this->truncateFloat(($this->Cantidad - (pow(10, -$decimalesNum)) / 2) * ($this->ValorUnitario - (pow(10, -$decimalesNum)) / 2), $this->Decimales);
		// return str_replace(',', '', $minimo);
		$decimalesNum = strlen(substr(strrchr(sprintf('%0.' . 6 . 'f', $this->ValorUnitario), "."), 1));
		$minimo = $this->truncateFloat(($this->Cantidad - (pow(10, -$decimalesNum)) / 2) * ($this->ValorUnitario - (pow(10, -$decimalesNum)) / 2), 6);
		return str_replace(',', '', $minimo);
	}

	function getMax() {
		// $decimalesNum = strlen(substr(strrchr(sprintf('%0.' . $this->Decimales . 'f', $this->ValorUnitario), "."), 1));
		// $maximo = round(($this->Cantidad + (pow(10, -$decimalesNum)) / 2 - pow(10, -12)) * ($this->ValorUnitario + (pow(10, -$decimalesNum)) / 2 - pow(10, -12)), $this->Decimales);
		// return $maximo;
		$decimalesNum = strlen(substr(strrchr(sprintf('%0.' . 6 . 'f', $this->ValorUnitario), "."), 1));
		$maximo = round(($this->Cantidad + (pow(10, -$decimalesNum)) / 2 - pow(10, -12)) * ($this->ValorUnitario + (pow(10, -$decimalesNum)) / 2 - pow(10, -12)), 6);
		return $maximo;	
	}

	private function truncateFloat($number, $digitos) {
		$raiz = 10;
		$multiplicador = pow($raiz, $digitos);
		$resultado = ((int) ($number * $multiplicador)) / $multiplicador;
		return number_format($resultado, $digitos);
	}

	private function validateMaxMin() {
		$minimo = $this->getMin();
		$maximo = $this->getMax();
		$array = ['minimo' => $minimo, 'maximo' => $maximo];
		return $array;
	}

	function validateDecimals() {
		$decimalesTotal = strlen(substr(strrchr($this->Importe, "."), 1));
		$decimalesValorUnitario = strlen(substr(strrchr($this->ValorUnitario, "."), 1));
		$decimalesCantidad = strlen(substr(strrchr($this->Cantidad, "."), 6));
		// solamente si es mayor al numero de decimales de los especificados en la moneda te retorna una excepcion
		/*if (!empty($this->Descuento)) {
			$decimalesDescuento = strlen(substr(strrchr($this->Descuento, "."), 1));
			if ($decimalesDescuento > $this->Decimales) {
				throw new Exception("El descuento de " . $this->Descuento .
				" en el concepto es mayor que el valor de los decimales especificado por la moneda , valor de decimales: " . $this->Decimales);
			}
		}*/

		if ($decimalesTotal > 6) {
			throw new Exception("El importe de " . $this->Importe .
			" en el concepto es mayor que el valor de los decimales especificado por la moneda , valor de decimales: " . $this->Decimales);
		}

		// if ($decimalesCantidad > $this->Decimales) {
		// 	throw new Exception("La cantidad de " . $this->Cantidad .
		// 	" en el concepto es mayor que el valor de los decimales especificado, valor de decimales: " . $this->Decimales);
		// }
		/*2017-11-22: se comenta validar decimales de valorUnitario, el PAC si pasa mas decimales de la moneda
		 * if ($decimalesValorUnitario > $this->Decimales) {
			throw new Exception("El valor unitario de " . $this->ValorUnitario .
			" en el concepto no coincide con el valor de los decimales especificado por la moneda ,valor de decimales: " . $this->Decimales);
		}*/
	}
	
	function addZeros($cantidad = null, $dec = null){
		if($cantidad == 0)
			return 0;
		if($dec == null)
			$dec = $this->Decimales;
		return  sprintf('%0.'.$dec.'f',$cantidad);
	}
}



//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de Concepto.InformacionAduanera

class ConceptoInformacionAduaneraV4
{
	// Obligatorios
	var $Cantidad;

	var $xml_base;
	var $logger;

	function __construct($NumeroPedimento)
    {
		$this->xml_base = null;
        $this->NumeroPedimento = $NumeroPedimento;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['NumeroPedimento'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Comprobante.Concepto.InformacionAduanera validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Comprobante.Concepto.InformacionAduanera Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoInformacionAduanera = $this->xml_base->createElement("cfdi:InformacionAduanera");

        $nodoInformacionAduanera->setAttribute('NumeroPedimento',  $this->NumeroPedimento);

		$this->xml_base->appendChild($nodoInformacionAduanera);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cfdi:InformacionAduanera")->item(0);
		return $xml;
	}
}

//CLASE PARTE GLOBAL
class ParteV4 {
	var $ClaveProdServ;
	var $NoIdentificacion;
    var $Cantidad;
	var $Unidad;
	var $Descripcion;
    var $ValorUnitario; 
	var $Importe;

    //OBJETOS
    var $InformacionAduanera = [];//
	
	var $xml;
	var $logger;

	public function __construct($ClaveProdServ, $NoIdentificacion, $Cantidad, $Unidad, $Descripcion, $ValorUnitario, $Importe) {
		$this->ClaveProdServ = $ClaveProdServ;
		$this->NoIdentificacion = $NoIdentificacion;
		$this->Cantidad = $Cantidad;
		$this->Unidad = $Unidad;
        $this->Descripcion = $Descripcion;
        $this->ValorUnitario = $ValorUnitario;
        $this->Importe=$Importe;
		$this->logger = new Logger(); //clase para escribir logs
	}

	public function validar() {
		$required = array(
			'ClaveProdServ',
			'Cantidad',
			'Descripcion',
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Parte validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Parte Campo Requerido: ' . $field);
			}
		}
		if(!empty($this->NoIdentificacion)){
			if(strlen($this->NoIdentificacion) < 1 || strlen($this->NoIdentificacion) >100 ){
				$this->logger->write('Parte validar No Identificacion: Debe contener entre 1 a 100 carácter(es) .');
				throw new Exception('El valor de Parte No Identificacion debe ser entre 1 a 100 carácter(es): len='.strlen($this->NoIdentificacion));
			}	
		}

        if($this->Cantidad < 0.000001){ //valor minimo
			$this->logger->write('Parte Cantidad ' . $this->Cantidad . ' debe tener un valor minimo de 0.000001');
			throw new Exception('Parte Cantidad ' . $this->Cantidad . ' debe tener un valor minimo de 0.000001');
		}

		if(!empty($this->Unidad)){
			if(strlen($this->Unidad) < 1 || strlen($this->Unidad) >20 ){
				$this->logger->write('Parte validar Unidad: Debe contener entre 1 a 20 carácter(es) .');
				throw new Exception('El valor de Parte Unidad debe ser entre 1 a 20 carácter(es): len='.strlen($this->Unidad));
			}
		}

        if(strlen($this->Descripcion) < 1 || strlen($this->Descripcion) >1000 ){
			$this->logger->write('Parte validar Descripcion: Debe contener entre 1 a 1000 carácter(es) .');
			throw new Exception('El valor de Parte Descripcion debe ser entre 1 a 1000 carácter(es): len='.strlen($this->Descripcion));
		}

        foreach($this->InformacionAduanera as $InformacionAduanera)
        {
			$InformacionAduanera->validar();
		}
	}

	public function toXML() {
		$this->xml = new DOMDocument();
		$domParte = $this->xml->createElement('cfdi:Parte');
		$this->xml->appendChild($domParte);

		$domParte->setAttribute('ClaveProdServ', ($this->ClaveProdServ));
        if($this->NoIdentificacion)
		    $domParte->setAttribute('NoIdentificacion', $this->NoIdentificacion);
        $domParte->setAttribute('Cantidad', $this->Cantidad);
        if($this->Unidad)
            $domParte->setAttribute('Unidad', $this->Unidad);
        $domParte->setAttribute('Descripcion', $this->Descripcion);
        if($this->ValorUnitario)
            $domParte->setAttribute('ValorUnitario', $this->ValorUnitario);
        if($this->Importe)
            $domParte->setAttribute('Importe', $this->Importe);
        # informacion aduanera
		if (!empty($this->InformacionAduanera)) 
		{
			foreach($this->InformacionAduanera as $InformacionAduanera)
			{
                $InformacionAduanera->toXML();
                $nodoInformacionAduanera = $this->xml->importNode($InformacionAduanera->importXML(), true);
                $domParte->appendChild($nodoInformacionAduanera);
			}
		}
		return $domParte;
	}

	function toStringXML() {
		return $this->xml->saveXML();
	}

	function importXML() {
		$xml = $this->xml->getElementsByTagName("cfdi:Parte")->item(0);
		return $xml;
	}

    function addInformacionAduanera($NumeroPedimento)
	{
		$InformacionAduanera = new ConceptoInformacionAduaneraV4(
            $NumeroPedimento
        );
		
		$this->InformacionAduanera[] = $InformacionAduanera;
		return $InformacionAduanera;
	}
}
?>
