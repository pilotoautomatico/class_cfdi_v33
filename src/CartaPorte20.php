<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

use Exception;
use DOMDocument;

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20


class CartaPorte20 
{
	// Obligatorios
	public $Version = "2.0";
	var $TranspInternac;
	var $Ubicaciones = [];
	var $Mercancias;

	// opcionales
	var $EntradaSalidaMerc;
    var $PaisOrigenDestino;
	var $ViaEntradaSalida;
	var $TotalDistRec;
	var $FiguraTransporte = [];

	var $xml_base;
	var $logger;

	function __construct($TranspInternac, $EntradaSalidaMerc = null, $ViaEntradaSalida = null, $TotalDistRec = null, $PaisOrigenDestino = null)
    {
		$this->xml_base = null;
        $this->TranspInternac = $TranspInternac;
        $this->EntradaSalidaMerc = $EntradaSalidaMerc;
        $this->PaisOrigenDestino = $PaisOrigenDestino;
        $this->ViaEntradaSalida = $ViaEntradaSalida;
        $this->TotalDistRec = $TotalDistRec;
		$this->Ubicaciones = [];
		$this->Mercancias = null;
		$this->FiguraTransporte = null;

		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['Version', 'TranspInternac'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20 validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20 Campo Requerido: ' . $field);
			}
		}

        if(count($this->Ubicaciones) == 0)
        {
            $this->logger->write("Complemento.CartaPorte20.Ubicaciones validar(): Debe existir un elemento de Ubicaciones.");
            throw new Exception('Complemento.CartaPorte20.Ubicaciones: Debe existir un elemento de Ubicaciones.');
        }

		foreach($this->Ubicaciones as $Ubicacion)
        {
			$Ubicacion->validar();
		}

        if(empty($this->Mercancias))
        {
            $this->logger->write("Complemento.CartaPorte20.Mercancias validar(): Debe existir elemento de Mercancias.");
            throw new Exception('Complemento.CartaPorte20.Mercancias: Debe existir elemento de Mercancias.');
        }

		if($this->Mercancias)
			$this->Mercancias->validar();

        if(!empty($this->FiguraTransporte))
        {
            foreach($this->FiguraTransporte as $TiposFigura)
            {
                $TiposFigura->validar();
            }
        }
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoCartaPorte20 = $this->xml_base->createElement("cartaporte20:CartaPorte");

		$nodoCartaPorte20->setAttribute('Version',  $this->Version);
		$nodoCartaPorte20->setAttribute('TranspInternac',  $this->TranspInternac);
        if($this->EntradaSalidaMerc)
		    $nodoCartaPorte20->setAttribute('EntradaSalidaMerc',  $this->EntradaSalidaMerc);
        if($this->PaisOrigenDestino)
		    $nodoCartaPorte20->setAttribute('PaisOrigenDestino',  $this->PaisOrigenDestino);
        if($this->ViaEntradaSalida)
		    $nodoCartaPorte20->setAttribute('ViaEntradaSalida',  $this->ViaEntradaSalida);
        if($this->TotalDistRec)
		    $nodoCartaPorte20->setAttribute('TotalDistRec',  $this->TotalDistRec);

        if(!empty($this->Ubicaciones))
        {
		    $nodoUbicaciones = $this->xml_base->createElement("cartaporte20:Ubicaciones");
		    $nodoCartaPorte20->appendChild($nodoUbicaciones);

            foreach ($this->Ubicaciones as $key => $Ubicacion) 
            {
                $Ubicacion->toXML();
                $nodoUbicacion = $this->xml_base->importNode($Ubicacion->importXML(), true);
                $nodoUbicaciones->appendChild($nodoUbicacion);
            }
        }

        if(!empty($this->Mercancias))
        {
            $this->Mercancias->toXML();
            $nodoMercancias = $this->xml_base->importNode($this->Mercancias->importXML(), true);
		    $nodoCartaPorte20->appendChild($nodoMercancias);
        }

        if(!empty($this->FiguraTransporte))
        {
		    $nodoFiguraTransporte = $this->xml_base->createElement("cartaporte20:FiguraTransporte");
		    $nodoCartaPorte20->appendChild($nodoFiguraTransporte);

            foreach ($this->FiguraTransporte as $key => $TiposFigura) 
            {
                $TiposFigura->toXML();
                $nodoTiposFigura = $this->xml_base->importNode($TiposFigura->importXML(), true);
                $nodoFiguraTransporte->appendChild($nodoTiposFigura);
            }
        }

		$this->xml_base->appendChild($nodoCartaPorte20);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:CartaPorte")->item(0);
		return $xml;
	}

	function addUbicacion($TipoUbicacion, $RFCRemitenteDestinatario, $FechaHoraSalidaLlegada, $IDUbicacion = null, $NombreRemitenteDestinatario = null, $NumRegIdTrib = null,
        $ResidenciaFiscal = null, $NumEstacion = null, $NombreEstacion = null, $NavegacionTrafico = null, $TipoEstacion = null, $DistanciaRecorrida = null)
    {
		$Ubicacion = new Ubicacion20(
            $TipoUbicacion, 
            $RFCRemitenteDestinatario,
            $FechaHoraSalidaLlegada,
            $IDUbicacion,
            $NombreRemitenteDestinatario,
            $NumRegIdTrib,
            $ResidenciaFiscal,
            $NumEstacion,
            $NombreEstacion,
            $NavegacionTrafico,
            $TipoEstacion,
            $DistanciaRecorrida
        );
		
		// $Ubicacion->validar();
		$this->Ubicaciones[] = $Ubicacion;
		return $Ubicacion;
	}

	function addMercancias($NumTotalMercancias, $PesoBrutoTotal, $UnidadPeso, $PesoNetoTotal = null, $CargoPorTasacion = null) 
    {
		$Mercancias = new Mercancias20(
            $NumTotalMercancias, 
            $PesoBrutoTotal, 
            $UnidadPeso, 
            $PesoNetoTotal, 
            $CargoPorTasacion
        );
		
		// $Mercancias->validar();
		$this->Mercancias = $Mercancias;
		return $Mercancias;
	}

	function addFiguraTransporte($TipoFigura, $RFCFigura = null, $NumLicencia = null, $NombreFigura = null, $NumRegIdTribFigura = null, $ResidenciaFiscalFigura = null) 
    {
		$TiposFigura = new TiposFigura20(
            $TipoFigura, 
            $RFCFigura, 
            $NumLicencia, 
            $NombreFigura, 
            $NumRegIdTribFigura, 
            $ResidenciaFiscalFigura
        );
		
		// $TiposFigura->validar();
		$this->FiguraTransporte[] = $TiposFigura;
		return $TiposFigura;
	}

}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Ubicaciones.Ubicacion

class Ubicacion20 
{
    // obligatorios
    var $TipoUbicacion;
    var $RFCRemitenteDestinatario;
    var $FechaHoraSalidaLlegada;

	// opcionales
    var $IDUbicacion;
    var $NombreRemitenteDestinatario;
    var $NumRegIdTrib;
    var $ResidenciaFiscal;
    var $NumEstacion;
    var $NombreEstacion;
    var $NavegacionTrafico;
    var $TipoEstacion;
    var $DistanciaRecorrida;
    var $Domicilio;

	var $xml_base;
	var $logger;

	function __construct($TipoUbicacion, $RFCRemitenteDestinatario, $FechaHoraSalidaLlegada, $IDUbicacion = null, $NombreRemitenteDestinatario = null, $NumRegIdTrib = null,
        $ResidenciaFiscal = null, $NumEstacion = null, $NombreEstacion = null, $NavegacionTrafico = null, $TipoEstacion = null, $DistanciaRecorrida = null)
    {
		$this->xml_base = null;
        $this->TipoUbicacion = $TipoUbicacion;
        $this->RFCRemitenteDestinatario = $RFCRemitenteDestinatario;
        $this->FechaHoraSalidaLlegada = $FechaHoraSalidaLlegada;
        $this->IDUbicacion = $IDUbicacion;
        $this->NombreRemitenteDestinatario = $NombreRemitenteDestinatario;
        $this->NumRegIdTrib = $NumRegIdTrib;
        $this->ResidenciaFiscal = $ResidenciaFiscal;
        $this->NumEstacion = $NumEstacion;
        $this->NombreEstacion = $NombreEstacion;
        $this->NavegacionTrafico = $NavegacionTrafico;
        $this->TipoEstacion = $TipoEstacion;
        $this->DistanciaRecorrida = $DistanciaRecorrida;

		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoUbicacion', 'RFCRemitenteDestinatario', 'FechaHoraSalidaLlegada'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Ubicaciones.Ubicacion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Ubicaciones.Ubicacion Campo Requerido: ' . $field);
			}
		}

        if($this->Domicilio)
            $this->Domicilio->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoUbicacion = $this->xml_base->createElement("cartaporte20:Ubicacion");

        $nodoUbicacion->setAttribute('TipoUbicacion',  $this->TipoUbicacion);
        $nodoUbicacion->setAttribute('RFCRemitenteDestinatario',  $this->RFCRemitenteDestinatario);
        $nodoUbicacion->setAttribute('FechaHoraSalidaLlegada',  $this->FechaHoraSalidaLlegada);
        if($this->IDUbicacion)
		    $nodoUbicacion->setAttribute('IDUbicacion',  $this->IDUbicacion);
        if($this->NombreRemitenteDestinatario)
		    $nodoUbicacion->setAttribute('NombreRemitenteDestinatario',  $this->NombreRemitenteDestinatario);
        if($this->NumRegIdTrib)
		    $nodoUbicacion->setAttribute('NumRegIdTrib',  $this->NumRegIdTrib);
        if($this->ResidenciaFiscal)
		    $nodoUbicacion->setAttribute('ResidenciaFiscal',  $this->ResidenciaFiscal);
        if($this->NumEstacion)
		    $nodoUbicacion->setAttribute('NumEstacion',  $this->NumEstacion);
        if($this->NombreEstacion)
		    $nodoUbicacion->setAttribute('NombreEstacion',  $this->NombreEstacion);
        if($this->NavegacionTrafico)
		    $nodoUbicacion->setAttribute('NavegacionTrafico',  $this->NavegacionTrafico);
        if($this->TipoEstacion)
		    $nodoUbicacion->setAttribute('TipoEstacion',  $this->TipoEstacion);
        if($this->DistanciaRecorrida)
		    $nodoUbicacion->setAttribute('DistanciaRecorrida',  $this->DistanciaRecorrida);

        // if($this->Origen)
        // {
        //     $this->Origen->toXML();
        //     $nodoOrigen = $this->xml_base->importNode($this->Origen->importXML(), true);
		//     $nodoUbicacion->appendChild($nodoOrigen);
        // }

        // if($this->Destino)
        // {
        //     $this->Destino->toXML();
        //     $nodoDestino = $this->xml_base->importNode($this->Destino->importXML(), true);
		//     $nodoUbicacion->appendChild($nodoDestino);
        // }

        if($this->Domicilio)
        {
            $this->Domicilio->toXML();
            $nodoDomicilio = $this->xml_base->importNode($this->Domicilio->importXML(), true);
		    $nodoUbicacion->appendChild($nodoDomicilio);
        }

		$this->xml_base->appendChild($nodoUbicacion);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:Ubicacion")->item(0);
		return $xml;
	}

    function addDomicilio($Estado, $Pais, $CodigoPostal, $Calle = null, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
        $Domicilio = new Domicilio20(
            $Estado, 
            $Pais, 
            $CodigoPostal, 
            $Calle, 
            $NumeroExterior, 
            $NumeroInterior, 
            $Colonia, 
            $Localidad, 
            $Referencia, 
            $Municipio
        );
        // $Domicilio->validar();
        $this->Domicilio = $Domicilio;
        return $Domicilio;
    }

}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Ubicaciones.Ubicacion.Domicilio
// clase que crea el nodo de CartaPorte20.FiguraTransporte.TiposFigura.PartesTransporte.Domicilio

class Domicilio20 
{
	// Obligatorios
	var $Estado;
	var $Pais;
	var $CodigoPostal;

	// opcionales
	var $Calle;
	var $NumeroExterior;
	var $NumeroInterior;
    var $Colonia;
    var $Localidad;
    var $Referencia;
    var $Municipio;

	var $xml_base;
	var $logger;

	function __construct($Estado, $Pais, $CodigoPostal, $Calle=null, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
		$this->xml_base = null;
        $this->Calle = $Calle;
        $this->Estado = $Estado;
        $this->Pais = $Pais;
        $this->CodigoPostal = $CodigoPostal;
        $this->NumeroExterior = $NumeroExterior;
        $this->NumeroInterior = $NumeroInterior;
        $this->Colonia = $Colonia;
        $this->Localidad = $Localidad;
        $this->Referencia = $Referencia;
        $this->Municipio = $Municipio;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['Estado', 'Pais', 'CodigoPostal'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Domicilio validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Domicilio Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoDomicilio = $this->xml_base->createElement("cartaporte20:Domicilio");
        $nodoDomicilio->setAttribute('Estado',  $this->Estado);
        $nodoDomicilio->setAttribute('Pais',  $this->Pais);
        $nodoDomicilio->setAttribute('CodigoPostal',  $this->CodigoPostal);
        if($this->Calle)
            $nodoDomicilio->setAttribute('Calle',  $this->Calle);
        if($this->NumeroExterior)
		    $nodoDomicilio->setAttribute('NumeroExterior',  $this->NumeroExterior);
        if($this->NumeroInterior)
		    $nodoDomicilio->setAttribute('NumeroInterior',  $this->NumeroInterior);
        if($this->Colonia)
		    $nodoDomicilio->setAttribute('Colonia',  $this->Colonia);
        if($this->Localidad)
		    $nodoDomicilio->setAttribute('Localidad',  $this->Localidad);
        if($this->Referencia)
		    $nodoDomicilio->setAttribute('Referencia',  $this->Referencia);
        if($this->Municipio)
		    $nodoDomicilio->setAttribute('Municipio',  $this->Municipio);

		$this->xml_base->appendChild($nodoDomicilio);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:Domicilio")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias

class Mercancias20
{
	// Obligatorios
	var $PesoBrutoTotal;
	var $UnidadPeso;
	var $NumTotalMercancias;

	// opcionales
    var $PesoNetoTotal;
    var $CargoPorTasacion;

    var $Mercancia = [];
    var $Autotransporte;
    var $TransporteMaritimo;
    var $TransporteAereo;
    var $TransporteFerroviario;

	var $xml_base;
	var $logger;

	function __construct($NumTotalMercancias, $PesoBrutoTotal, $UnidadPeso, $PesoNetoTotal = null, $CargoPorTasacion = null)
    {
		$this->xml_base = null;
        $this->NumTotalMercancias = $NumTotalMercancias;
        $this->PesoBrutoTotal = $PesoBrutoTotal;
        $this->UnidadPeso = $UnidadPeso;
        $this->PesoNetoTotal = $PesoNetoTotal;
        $this->CargoPorTasacion = $CargoPorTasacion;

        $this->Mercancia = [];
        $this->Autotransporte = null;
        $this->TransporteMaritimo = null;
        $this->TransporteAereo = null;
        $this->TransporteFerroviario = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['NumTotalMercancias', 'PesoBrutoTotal', 'UnidadPeso'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias Campo Requerido: ' . $field);
			}
		}

        if(count($this->Mercancia) == 0)
        {
            $this->logger->write("Complemento.CartaPorte20.Mercancias validar(): Debe existir un elemento de Mercancia.");
            throw new Exception('Complemento.CartaPorte20.Mercancias: Debe existir un elemento de Mercancia.');
        }

		foreach($this->Mercancia as $Mercancia)
        {
			$Mercancia->validar();
		}

		if($this->Autotransporte)
			$this->Autotransporte->validar();

		if($this->TransporteMaritimo)
			$this->TransporteMaritimo->validar();

		if($this->TransporteAereo)
			$this->TransporteAereo->validar();

		if($this->TransporteFerroviario)
			$this->TransporteFerroviario->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoMercancias = $this->xml_base->createElement("cartaporte20:Mercancias");
        $nodoMercancias->setAttribute('PesoBrutoTotal',  $this->PesoBrutoTotal);
        $nodoMercancias->setAttribute('UnidadPeso',  $this->UnidadPeso);
        $nodoMercancias->setAttribute('NumTotalMercancias',  $this->NumTotalMercancias);
        if($this->PesoNetoTotal)
		    $nodoMercancias->setAttribute('PesoNetoTotal',  $this->PesoNetoTotal);
        if($this->CargoPorTasacion)
		    $nodoMercancias->setAttribute('CargoPorTasacion',  $this->CargoPorTasacion);

        if(!empty($this->Mercancia))
        {
            foreach ($this->Mercancia as $key => $Mercancia) 
            {
                $Mercancia->toXML();
                $nodoMercancia = $this->xml_base->importNode($Mercancia->importXML(), true);
                $nodoMercancias->appendChild($nodoMercancia);
            }
        }

        if(!empty($this->Autotransporte))
        {
            $this->Autotransporte->toXML();
            $nodoAutotransporte = $this->xml_base->importNode($this->Autotransporte->importXML(), true);
		    $nodoMercancias->appendChild($nodoAutotransporte);
        }

        if(!empty($this->TransporteMaritimo))
        {
            $this->TransporteMaritimo->toXML();
            $nodoTransporteMaritimo = $this->xml_base->importNode($this->TransporteMaritimo->importXML(), true);
		    $nodoMercancias->appendChild($nodoTransporteMaritimo);
        }

        if(!empty($this->TransporteAereo))
        {
            $this->TransporteAereo->toXML();
            $nodoTransporteAereo = $this->xml_base->importNode($this->TransporteAereo->importXML(), true);
		    $nodoMercancias->appendChild($nodoTransporteAereo);
        }

        if(!empty($this->TransporteFerroviario))
        {
            $this->TransporteFerroviario->toXML();
            $nodoTransporteFerroviario = $this->xml_base->importNode($this->TransporteFerroviario->importXML(), true);
		    $nodoMercancias->appendChild($nodoTransporteFerroviario);
        }

		$this->xml_base->appendChild($nodoMercancias);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:Mercancias")->item(0);
		return $xml;
	}

    function addMercancia($PesoEnKg, $BienesTransp, $Descripcion, $Cantidad, $ClaveUnidad, $ClaveSTCC = null, $Unidad = null, $Dimensiones = null, $MaterialPeligroso = null, 
                $CveMaterialPeligroso = null, $Embalaje = null, $DescripEmbalaje = null, $ValorMercancia = null, $Moneda = null, $FraccionArancelaria = null, $UUIDComercioExt = null)
    {
		$Mercancia = new Mercancia20(
            $PesoEnKg, 
            $BienesTransp, 
            $Descripcion, 
            $Cantidad, 
            $ClaveUnidad,
            $ClaveSTCC, 
            $Unidad, 
            $Dimensiones, 
            $MaterialPeligroso, 
            $CveMaterialPeligroso, 
            $Embalaje, 
            $DescripEmbalaje, 
            $ValorMercancia, 
            $Moneda, 
            $FraccionArancelaria, 
            $UUIDComercioExt
        );
		
		// $Mercancia->validar();
		$this->Mercancia[] = $Mercancia;
		return $Mercancia;
    }

    function addAutotransporte($PermSCT, $NumPermisoSCT)
    {
		$Autotransporte = new Autotransporte20(
            $PermSCT, 
            $NumPermisoSCT
        );
		
		// $Autotransporte->validar();
		$this->Autotransporte = $Autotransporte;
		return $Autotransporte;
    }

    function addTransporteMaritimo($TipoEmbarcacion, $Matricula, $NumeroOMI, $NacionalidadEmbarc, $UnidadesDeArqBruto, $TipoCarga, $NumCertITC, $NombreAgenteNaviero, $NumAutorizacionNaviero, 
                $PermSCT = null, $NumPermisoSCT = null, $NombreAseg = null, $NumPolizaSeguro = null, $AnioEmbarcacion = null, $NombreEmbarc = null, $Eslora = null, 
                $Manga = null, $Calado = null, $LineaNaviera = null, $NumViaje = null, $NumConocEmbarc = null)
    {
		$TransporteMaritimo = new TransporteMaritimo20(
            $TipoEmbarcacion, 
            $Matricula, 
            $NumeroOMI, 
            $NacionalidadEmbarc, 
            $UnidadesDeArqBruto, 
            $TipoCarga, 
            $NumCertITC, 
            $NombreAgenteNaviero, 
            $NumAutorizacionNaviero, 
            $PermSCT, 
            $NumPermisoSCT, 
            $NombreAseg, 
            $NumPolizaSeguro, 
            $AnioEmbarcacion, 
            $NombreEmbarc, 
            $Eslora, 
            $Manga, 
            $Calado, 
            $LineaNaviera, 
            $NumViaje, 
            $NumConocEmbarc
        );
		
		// $TransporteMaritimo->validar();
		$this->TransporteMaritimo = $TransporteMaritimo;
		return $TransporteMaritimo;
    }

    function addTransporteAereo($PermSCT, $NumPermisoSCT, $MatriculaAeronave, $NumeroGuia, $CodigoTransportista, $NombreAseg = null, $NumPolizaSeguro = null, $LugarContrato = null,
                        $RFCTransportista = null, $NumRegIdTribTranspor = null, $ResidenciaFiscalTranspor = null, $NombreTransportista = null, $RFCEmbarcador = null, 
                        $NumRegIdTribEmbarc = null, $ResidenciaFiscalEmbarc = null, $NombreEmbarcador = null)
    {
		$TransporteAereo = new TransporteAereo20(
            $PermSCT, 
            $NumPermisoSCT, 
            $MatriculaAeronave, 
            $NumeroGuia, 
            $CodigoTransportista, 
            $NombreAseg,
            $NumPolizaSeguro, 
            $LugarContrato,
            $RFCTransportista, 
            $NumRegIdTribTranspor, 
            $ResidenciaFiscalTranspor, 
            $NombreTransportista, 
            $RFCEmbarcador, 
            $NumRegIdTribEmbarc, 
            $ResidenciaFiscalEmbarc, 
            $NombreEmbarcador
        );
		
		// $TransporteAereo->validar();
		$this->TransporteAereo = $TransporteAereo;
		return $TransporteAereo;
    }

    function addTransporteFerroviario($TipoDeServicio, $NombreAseg = null, $NumPolizaSeguro = null, $Concesionario = null, $TipoDeTrafico= null)
    {
        $TransporteFerroviario = new TransporteFerroviario20(
            $TipoDeServicio, 
            $NombreAseg, 
            $NumPolizaSeguro, 
            $Concesionario,
            $TipoDeTrafico
        );

        // $TransporteFerroviario->validar();
        $this->TransporteFerroviario = $TransporteFerroviario; 
        return $TransporteFerroviario;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.Mercancia

class Mercancia20
{
	// Obligatorios
	var $BienesTransp;
    var $Descripcion;
    var $Cantidad;
    var $ClaveUnidad;
	var $PesoEnKg;

	// opcionales
	var $ClaveSTCC;
    var $Unidad;
    var $Dimensiones;
    var $MaterialPeligroso;
    var $CveMaterialPeligroso;
    var $Embalaje;
    var $DescripEmbalaje;
    var $ValorMercancia;
    var $Moneda;
    var $FraccionArancelaria;
    var $UUIDComercioExt;

    var $Pedimentos = [];
    var $GuiasIdentificacion = [];
    var $CantidadTransporta = [];
    var $DetalleMercancia;

	var $xml_base;
	var $logger;

	function __construct($PesoEnKg, $BienesTransp, $Descripcion, $Cantidad, $ClaveUnidad, $ClaveSTCC = null, $Unidad = null, $Dimensiones = null, $MaterialPeligroso = null, 
                $CveMaterialPeligroso = null, $Embalaje = null, $DescripEmbalaje = null, $ValorMercancia = null, $Moneda = null, $FraccionArancelaria = null, $UUIDComercioExt = null)
    {
		$this->xml_base = null;
        $this->PesoEnKg = $PesoEnKg;
        $this->BienesTransp = $BienesTransp;
        $this->ClaveSTCC = $ClaveSTCC;
        $this->Descripcion = $Descripcion;
        $this->Cantidad = $Cantidad;
        $this->ClaveUnidad = $ClaveUnidad;
        $this->Unidad = $Unidad;
        $this->Dimensiones = $Dimensiones;
        $this->MaterialPeligroso = $MaterialPeligroso;
        $this->CveMaterialPeligroso = $CveMaterialPeligroso;
        $this->Embalaje = $Embalaje;
        $this->DescripEmbalaje = $DescripEmbalaje;
        $this->ValorMercancia = $ValorMercancia;
        $this->Moneda = $Moneda;
        $this->FraccionArancelaria = $FraccionArancelaria;
        $this->UUIDComercioExt = $UUIDComercioExt;

        $this->Pedimentos = [];
        $this->GuiasIdentificacion = [];
        $this->CantidadTransporta = [];
        $this->DetalleMercancia = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['PesoEnKg','BienesTransp', 'Descripcion', 'Cantidad', 'ClaveUnidad'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.Mercancia validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.Mercancia Campo Requerido: ' . $field);
			}
		}

		foreach($this->CantidadTransporta as $CantidadTransporta)
        {
			$CantidadTransporta->validar();
		}

		if($this->DetalleMercancia)
			$this->DetalleMercancia->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoMercancia = $this->xml_base->createElement("cartaporte20:Mercancia");

        $nodoMercancia->setAttribute('BienesTransp',  $this->BienesTransp);
        $nodoMercancia->setAttribute('Descripcion',  $this->Descripcion);
        $nodoMercancia->setAttribute('Cantidad',  $this->Cantidad);
        $nodoMercancia->setAttribute('ClaveUnidad',  $this->ClaveUnidad);
        $nodoMercancia->setAttribute('PesoEnKg',  $this->PesoEnKg);

        if($this->ClaveSTCC)
		    $nodoMercancia->setAttribute('ClaveSTCC',  $this->ClaveSTCC);
        if($this->Unidad)
		    $nodoMercancia->setAttribute('Unidad',  $this->Unidad);
        if($this->Dimensiones)
		    $nodoMercancia->setAttribute('Dimensiones',  $this->Dimensiones);
        if($this->MaterialPeligroso)
		    $nodoMercancia->setAttribute('MaterialPeligroso',  $this->MaterialPeligroso);
        if($this->CveMaterialPeligroso)
		    $nodoMercancia->setAttribute('CveMaterialPeligroso',  $this->CveMaterialPeligroso);
        if($this->Embalaje)
		    $nodoMercancia->setAttribute('Embalaje',  $this->Embalaje);
        if($this->DescripEmbalaje)
		    $nodoMercancia->setAttribute('DescripEmbalaje',  $this->DescripEmbalaje);
        if($this->ValorMercancia)
		    $nodoMercancia->setAttribute('ValorMercancia',  $this->ValorMercancia);
        if($this->Moneda)
		    $nodoMercancia->setAttribute('Moneda',  $this->Moneda);
        if($this->FraccionArancelaria)
		    $nodoMercancia->setAttribute('FraccionArancelaria',  $this->FraccionArancelaria);
        if($this->UUIDComercioExt)
		    $nodoMercancia->setAttribute('UUIDComercioExt',  $this->UUIDComercioExt);

        if(!empty($this->Pedimentos))
        {
            foreach ($this->Pedimentos as $key => $Pedimentos) 
            {
                $Pedimentos->toXML();
                $nodoPedimentos = $this->xml_base->importNode($Pedimentos->importXML(), true);
                $nodoMercancia->appendChild($nodoPedimentos);
            }
        }

        if(!empty($this->GuiasIdentificacion))
        {
            foreach ($this->GuiasIdentificacion as $key => $GuiasIdentificacion) 
            {
                $GuiasIdentificacion->toXML();
                $nodoGuiasIdentificacion = $this->xml_base->importNode($GuiasIdentificacion->importXML(), true);
                $nodoMercancia->appendChild($nodoGuiasIdentificacion);
            }
        }

        if(!empty($this->CantidadTransporta))
        {
            foreach ($this->CantidadTransporta as $key => $CantidadTransporta) 
            {
                $CantidadTransporta->toXML();
                $nodoCantidadTransporta = $this->xml_base->importNode($CantidadTransporta->importXML(), true);
                $nodoMercancia->appendChild($nodoCantidadTransporta);
            }
        }

        if(!empty($this->DetalleMercancia))
        {
            $this->DetalleMercancia->toXML();
            $nodoDetalleMercancia = $this->xml_base->importNode($this->DetalleMercancia->importXML(), true);
		    $nodoMercancia->appendChild($nodoDetalleMercancia);
        }

		$this->xml_base->appendChild($nodoMercancia);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:Mercancia")->item(0);
		return $xml;
	}

    function addPedimentos($Pedimento)
    {
		$Pedimentos = new Pedimentos20(
            $Pedimento 
        );
		
		// $Pedimentos->validar();
		$this->Pedimentos[] = $Pedimentos;
		return $Pedimentos;
    }

    function addGuiasIdentificacion($NumeroGuiaIdentificacion, $DescripGuiaIdentificacion, $PesoGuiaIdentificacion)
    {
		$GuiasIdentificacion = new GuiasIdentificacion20(
            $NumeroGuiaIdentificacion, 
            $DescripGuiaIdentificacion, 
            $PesoGuiaIdentificacion 
        );
		
		// $GuiasIdentificacion->validar();
		$this->GuiasIdentificacion[] = $GuiasIdentificacion;
		return $GuiasIdentificacion;
    }

    function addCantidadTransporta($Cantidad, $IDOrigen, $IDDestino, $CvesTransporte = null)
    {
		$CantidadTransporta = new CantidadTransporta20(
            $Cantidad, 
            $IDOrigen, 
            $IDDestino, 
            $CvesTransporte
        );
		
		// $CantidadTransporta->validar();
		$this->CantidadTransporta[] = $CantidadTransporta;
		return $CantidadTransporta;
    }

    function addDetalleMercancia($UnidadPesoMerc, $PesoBruto, $PesoNeto, $PesoTara, $NumPiezas = null)
    {
		$DetalleMercancia = new DetalleMercancia20(
            $UnidadPesoMerc, 
            $PesoBruto, 
            $PesoNeto, 
            $PesoTara,
            $NumPiezas
        );
		
		// $DetalleMercancia->validar();
		$this->DetalleMercancia = $DetalleMercancia;
		return $DetalleMercancia;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.Mercancia.Pedimentos

class Pedimentos20
{
	// Obligatorios
	var $Pedimento;

	var $xml_base;
	var $logger;

	function __construct($Pedimento)
    {
		$this->xml_base = null;
        $this->Pedimento = $Pedimento;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['Pedimento'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.Mercancia.Pedimentos validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.Mercancia.Pedimentos Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoPedimentos = $this->xml_base->createElement("cartaporte20:Pedimentos");

        $nodoPedimentos->setAttribute('Pedimento',  $this->Pedimento);

		$this->xml_base->appendChild($nodoPedimentos);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:Pedimentos")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.Mercancia.GuiasIdentificacion

class GuiasIdentificacion20
{
	// Obligatorios
	var $NumeroGuiaIdentificacion;
	var $DescripGuiaIdentificacion;
	var $PesoGuiaIdentificacion;

	var $xml_base;
	var $logger;

	function __construct($NumeroGuiaIdentificacion, $DescripGuiaIdentificacion, $PesoGuiaIdentificacion)
    {
		$this->xml_base = null;
        $this->NumeroGuiaIdentificacion = $NumeroGuiaIdentificacion;
        $this->DescripGuiaIdentificacion = $DescripGuiaIdentificacion;
        $this->PesoGuiaIdentificacion = $PesoGuiaIdentificacion;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['NumeroGuiaIdentificacion', 'DescripGuiaIdentificacion', 'PesoGuiaIdentificacion'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.Mercancia.GuiasIdentificacion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.Mercancia.GuiasIdentificacion Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoGuiasIdentificacion = $this->xml_base->createElement("cartaporte20:GuiasIdentificacion");

        $nodoGuiasIdentificacion->setAttribute('NumeroGuiaIdentificacion',  $this->NumeroGuiaIdentificacion);
        $nodoGuiasIdentificacion->setAttribute('DescripGuiaIdentificacion',  $this->DescripGuiaIdentificacion);
        $nodoGuiasIdentificacion->setAttribute('PesoGuiaIdentificacion',  $this->PesoGuiaIdentificacion);

		$this->xml_base->appendChild($nodoGuiasIdentificacion);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:GuiasIdentificacion")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.Mercancia.CantidadTransporta

class CantidadTransporta20
{
	// Obligatorios
	var $Cantidad;
	var $IDOrigen;
	var $IDDestino;

	// opcionales
	var $CvesTransporte;

	var $xml_base;
	var $logger;

	function __construct($Cantidad, $IDOrigen, $IDDestino, $CvesTransporte = null)
    {
		$this->xml_base = null;
        $this->Cantidad = $Cantidad;
        $this->IDOrigen = $IDOrigen;
        $this->IDDestino = $IDDestino;
        $this->CvesTransporte = $CvesTransporte;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['Cantidad', 'IDOrigen', 'IDDestino'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.Mercancia.CantidadTransporta validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.Mercancia.CantidadTransporta Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoCantidadTransporta = $this->xml_base->createElement("cartaporte20:CantidadTransporta");

        $nodoCantidadTransporta->setAttribute('Cantidad',  $this->Cantidad);
        $nodoCantidadTransporta->setAttribute('IDOrigen',  $this->IDOrigen);
        $nodoCantidadTransporta->setAttribute('IDDestino',  $this->IDDestino);
        if($this->CvesTransporte)
		    $nodoCantidadTransporta->setAttribute('CvesTransporte',  $this->CvesTransporte);

		$this->xml_base->appendChild($nodoCantidadTransporta);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:CantidadTransporta")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.Mercancia.DetalleMercancia

class DetalleMercancia20
{
	// Obligatorios
	var $UnidadPesoMerc;
	var $PesoBruto;
	var $PesoNeto;
	var $PesoTara;

	// opcionales
	var $NumPiezas;

	var $xml_base;
	var $logger;

	function __construct($UnidadPesoMerc, $PesoBruto, $PesoNeto, $PesoTara, $NumPiezas = null)
    {
		$this->xml_base = null;
        $this->UnidadPesoMerc = $UnidadPesoMerc;
        $this->PesoBruto = $PesoBruto;
        $this->PesoNeto = $PesoNeto;
        $this->PesoTara = $PesoTara;
        $this->NumPiezas = $NumPiezas;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['UnidadPesoMerc', 'PesoBruto', 'PesoNeto', 'PesoTara'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.Mercancia.DetalleMercancia validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.Mercancia.DetalleMercancia Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoDetalleMercancia = $this->xml_base->createElement("cartaporte20:DetalleMercancia");

        $nodoDetalleMercancia->setAttribute('UnidadPesoMerc',  $this->UnidadPesoMerc);
        $nodoDetalleMercancia->setAttribute('PesoBruto',  $this->PesoBruto);
        $nodoDetalleMercancia->setAttribute('PesoNeto',  $this->PesoNeto);
        $nodoDetalleMercancia->setAttribute('PesoTara',  $this->PesoTara);
        if($this->NumPiezas)
		    $nodoDetalleMercancia->setAttribute('NumPiezas',  $this->NumPiezas);

		$this->xml_base->appendChild($nodoDetalleMercancia);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:DetalleMercancia")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.Autotransporte

class Autotransporte20
{
	// Obligatorios
	var $PermSCT;
	var $NumPermisoSCT;

	var $IdentificacionVehicular;
	var $Seguros;
    var $Remolques = [];

	var $xml_base;
	var $logger;

	function __construct($PermSCT, $NumPermisoSCT)
    {
		$this->xml_base = null;
        $this->PermSCT = $PermSCT;
        $this->NumPermisoSCT = $NumPermisoSCT;

        $this->Remolques = [];
        $this->IdentificacionVehicular = null;
        $this->Seguros = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['PermSCT', 'NumPermisoSCT'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.Autotransporte validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.Autotransporte Campo Requerido: ' . $field);
			}
		}

        if(count($this->Remolques) > 2)
        {
            $this->logger->write("Complemento.CartaPorte20.Mercancias.Autotransporte.Remolques. Debe existir un maximo de 2 elementos Remolque.");
            throw new Exception('Complemento.CartaPorte20.Mercancias.Autotransporte.Remolques Debe existir un maximo de 2 elementos Remolque.');
        }

		foreach($this->Remolques as $Remolques)
        {
			$Remolques->validar();
		}

        if(empty($this->Seguros))
        {
            $this->logger->write("Complemento.CartaPorte20.Mercancias.Autotransporte. Debe existir un elemento Seguros.");
            throw new Exception('Complemento.CartaPorte20.Mercancias.Autotransporte. Debe existir un elemento Seguros.');
        }

        if(empty($this->IdentificacionVehicular))
        {
            $this->logger->write("Complemento.CartaPorte20.Mercancias.Autotransporte. Debe existir un elemento IdentificacionVehicular.");
            throw new Exception('Complemento.CartaPorte20.Mercancias.Autotransporte. Debe existir un elemento IdentificacionVehicular.');
        }

		if($this->Seguros)
			$this->Seguros->validar();

		if($this->IdentificacionVehicular)
			$this->IdentificacionVehicular->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoAutotransporte = $this->xml_base->createElement("cartaporte20:Autotransporte");

        $nodoAutotransporte->setAttribute('PermSCT',  $this->PermSCT);
        $nodoAutotransporte->setAttribute('NumPermisoSCT',  $this->NumPermisoSCT);

        if(!empty($this->IdentificacionVehicular))
        {
            $this->IdentificacionVehicular->toXML();
            $nodoIdentificacionVehicular = $this->xml_base->importNode($this->IdentificacionVehicular->importXML(), true);
		    $nodoAutotransporte->appendChild($nodoIdentificacionVehicular);
        }

        if(!empty($this->Seguros))
        {
            $this->Seguros->toXML();
            $nodoSeguros = $this->xml_base->importNode($this->Seguros->importXML(), true);
		    $nodoAutotransporte->appendChild($nodoSeguros);
        }

        if(!empty($this->Remolques))
        {
		    $nodoRemolques = $this->xml_base->createElement("cartaporte20:Remolques");

            foreach ($this->Remolques as $key => $Remolque) 
            {
                $Remolque->toXML();
                $nodoRemolque = $this->xml_base->importNode($Remolque->importXML(), true);
                $nodoRemolques->appendChild($nodoRemolque);
            }
            
		    $nodoAutotransporte->appendChild($nodoRemolques);
        }

		$this->xml_base->appendChild($nodoAutotransporte);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:Autotransporte")->item(0);
		return $xml;
	}

    function addRemolque($SubTipoRem, $Placa)
    {
		$Remolque = new Remolque20(
            $SubTipoRem, 
            $Placa
        );
		
		// $Remolque->validar();
		$this->Remolques[] = $Remolque;
		return $Remolque;
    }

    function addSeguros($AseguraRespCivil, $PolizaRespCivil, $AseguraMedAmbiente = null, $PolizaMedAmbiente = null, $AseguraCarga = null, $PolizaCarga = null, 
        $PrimaSeguro = null)
    {
		$Seguros = new Seguros20(
            $AseguraRespCivil, 
            $PolizaRespCivil, 
            $AseguraMedAmbiente, 
            $PolizaMedAmbiente, 
            $AseguraCarga, 
            $PolizaCarga, 
            $PrimaSeguro
        );
		
		// $Seguros->validar();
		$this->Seguros = $Seguros;
		return $Seguros;
    }

    function addIdentificacionVehicular($ConfigVehicular, $PlacaVM, $AnioModeloVM)
    {
		$IdentificacionVehicular = new IdentificacionVehicular20(
            $ConfigVehicular, 
            $PlacaVM, 
            $AnioModeloVM
        );
		
		// $IdentificacionVehicular->validar();
		$this->IdentificacionVehicular = $IdentificacionVehicular;
		return $IdentificacionVehicular;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.Autotransporte.IdentificacionVehicular

class IdentificacionVehicular20
{
	// Obligatorios
	var $ConfigVehicular;
	var $PlacaVM;
	var $AnioModeloVM;

	var $xml_base;
	var $logger;

	function __construct($ConfigVehicular, $PlacaVM, $AnioModeloVM)
    {
		$this->xml_base = null;
        $this->ConfigVehicular = $ConfigVehicular;
        $this->PlacaVM = $PlacaVM;
        $this->AnioModeloVM = $AnioModeloVM;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['ConfigVehicular', 'PlacaVM', 'AnioModeloVM'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.Autotransporte.IdentificacionVehicular validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.Autotransporte.IdentificacionVehicular Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoIdentificacionVehicular = $this->xml_base->createElement("cartaporte20:IdentificacionVehicular");

        $nodoIdentificacionVehicular->setAttribute('ConfigVehicular',  $this->ConfigVehicular);
        $nodoIdentificacionVehicular->setAttribute('PlacaVM',  $this->PlacaVM);
        $nodoIdentificacionVehicular->setAttribute('AnioModeloVM',  $this->AnioModeloVM);

		$this->xml_base->appendChild($nodoIdentificacionVehicular);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:IdentificacionVehicular")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.Autotransporte.Seguros

class Seguros20
{
	// Obligatorios
	var $AseguraRespCivil;
	var $PolizaRespCivil;

    // Opcionales
    var $AseguraMedAmbiente;
    var $PolizaMedAmbiente;
    var $AseguraCarga;
    var $PolizaCarga;
    var $PrimaSeguro;

	var $xml_base;
	var $logger;

	function __construct($AseguraRespCivil, $PolizaRespCivil, $AseguraMedAmbiente = null, $PolizaMedAmbiente = null, $AseguraCarga = null, $PolizaCarga = null, $PrimaSeguro = null)
    {
		$this->xml_base = null;
        $this->AseguraRespCivil = $AseguraRespCivil;
        $this->PolizaRespCivil = $PolizaRespCivil;
        $this->AseguraMedAmbiente = $AseguraMedAmbiente;
        $this->PolizaMedAmbiente = $PolizaMedAmbiente;
        $this->AseguraCarga = $AseguraCarga;
        $this->PolizaCarga = $PolizaCarga;
        $this->PrimaSeguro = $PrimaSeguro;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['AseguraRespCivil', 'PolizaRespCivil'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.Autotransporte.Seguros validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.Autotransporte.Seguros Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoSeguros = $this->xml_base->createElement("cartaporte20:Seguros");

        $nodoSeguros->setAttribute('AseguraRespCivil',  $this->AseguraRespCivil);
        $nodoSeguros->setAttribute('PolizaRespCivil',  $this->PolizaRespCivil);
        if($this->AseguraMedAmbiente)
            $nodoSeguros->setAttribute('AseguraMedAmbiente',  $this->AseguraMedAmbiente);
        if($this->PolizaMedAmbiente)
            $nodoSeguros->setAttribute('PolizaMedAmbiente',  $this->PolizaMedAmbiente);
        if($this->AseguraCarga)
            $nodoSeguros->setAttribute('AseguraCarga',  $this->AseguraCarga);
        if($this->PolizaCarga)
            $nodoSeguros->setAttribute('PolizaCarga',  $this->PolizaCarga);
        if($this->PrimaSeguro)
            $nodoSeguros->setAttribute('PrimaSeguro',  $this->PrimaSeguro);

		$this->xml_base->appendChild($nodoSeguros);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:Seguros")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.Autotransporte.Remolques.Remolque

class Remolque20
{
	// Obligatorios
	var $SubTipoRem;
	var $Placa;

	var $xml_base;
	var $logger;

	function __construct($SubTipoRem, $Placa)
    {
		$this->xml_base = null;
        $this->SubTipoRem = $SubTipoRem;
        $this->Placa = $Placa;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['SubTipoRem', 'Placa'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.Autotransporte.Remolques.Remolque validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.Autotransporte.Remolques.Remolque Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoRemolque = $this->xml_base->createElement("cartaporte20:Remolque");

        $nodoRemolque->setAttribute('SubTipoRem',  $this->SubTipoRem);
        $nodoRemolque->setAttribute('Placa',  $this->Placa);

		$this->xml_base->appendChild($nodoRemolque);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:Remolque")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.TransporteMaritimo

class TransporteMaritimo20
{
	// Obligatorios
	var $TipoEmbarcacion;
	var $Matricula;
	var $NumeroOMI;
	var $NacionalidadEmbarc;
	var $UnidadesDeArqBruto;
	var $TipoCarga;
	var $NumCertITC;
	var $NombreAgenteNaviero;
	var $NumAutorizacionNaviero;

	// opcionales
	var $PermSCT;
	var $NumPermisoSCT;
    var $NombreAseg;
    var $NumPolizaSeguro;
    var $AnioEmbarcacion;
    var $NombreEmbarc;
    var $Eslora;
    var $Manga;
    var $Calado;
    var $LineaNaviera;
    var $NumViaje;
    var $NumConocEmbarc;

    var $Contenedor = [];

	var $xml_base;
	var $logger;

	function __construct($TipoEmbarcacion, $Matricula, $NumeroOMI, $NacionalidadEmbarc, $UnidadesDeArqBruto, $TipoCarga, $NumCertITC, $NombreAgenteNaviero, $NumAutorizacionNaviero, 
                $PermSCT = null, $NumPermisoSCT = null, $NombreAseg = null, $NumPolizaSeguro = null, $AnioEmbarcacion = null, $NombreEmbarc = null, $Eslora = null, 
                $Manga = null, $Calado = null, $LineaNaviera = null, $NumViaje = null, $NumConocEmbarc = null)
    {
		$this->xml_base = null;
        $this->TipoEmbarcacion = $TipoEmbarcacion;
        $this->Matricula = $Matricula;
        $this->NumeroOMI = $NumeroOMI;
        $this->NacionalidadEmbarc = $NacionalidadEmbarc;
        $this->UnidadesDeArqBruto = $UnidadesDeArqBruto;
        $this->TipoCarga = $TipoCarga;
        $this->NumCertITC = $NumCertITC;
        $this->NombreAgenteNaviero = $NombreAgenteNaviero;
        $this->NumAutorizacionNaviero = $NumAutorizacionNaviero;
        $this->PermSCT = $PermSCT;
        $this->NumPermisoSCT = $NumPermisoSCT;
        $this->NombreAseg = $NombreAseg;
        $this->NumPolizaSeguro = $NumPolizaSeguro;
        $this->AnioEmbarcacion = $AnioEmbarcacion;
        $this->NombreEmbarc = $NombreEmbarc;
        $this->Eslora = $Eslora;
        $this->Manga = $Manga;
        $this->Calado = $Calado;
        $this->LineaNaviera = $LineaNaviera;        
        $this->NumViaje = $NumViaje;
        $this->NumConocEmbarc = $NumConocEmbarc;

        $this->Contenedor = [];

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoEmbarcacion', 'Matricula', 'NumeroOMI', 'NacionalidadEmbarc', 'UnidadesDeArqBruto', 'TipoCarga', 'NumCertITC', 'NombreAgenteNaviero', 'NumAutorizacionNaviero'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.TransporteMaritimo validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.TransporteMaritimo Campo Requerido: ' . $field);
			}
		}

        if(count($this->Contenedor) == 0)
        {
            $this->logger->write("Complemento.CartaPorte20.Mercancias.TransporteMaritimo. Debe existir un elemento Contenedor.");
            throw new Exception('Complemento.CartaPorte20.Mercancias.TransporteMaritimo. Debe existir un elemento Contenedor.');
        }

		foreach($this->Contenedor as $Contenedor)
        {
			$Contenedor->validar();
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoTransporteMaritimo = $this->xml_base->createElement("cartaporte20:TransporteMaritimo");

        $nodoTransporteMaritimo->setAttribute('TipoEmbarcacion',  $this->TipoEmbarcacion);
        $nodoTransporteMaritimo->setAttribute('Matricula',  $this->Matricula);
        $nodoTransporteMaritimo->setAttribute('NumeroOMI',  $this->NumeroOMI);
        $nodoTransporteMaritimo->setAttribute('NacionalidadEmbarc',  $this->NacionalidadEmbarc);
        $nodoTransporteMaritimo->setAttribute('UnidadesDeArqBruto',  $this->UnidadesDeArqBruto);
        $nodoTransporteMaritimo->setAttribute('TipoCarga',  $this->TipoCarga);
        $nodoTransporteMaritimo->setAttribute('NumCertITC',  $this->NumCertITC);
        $nodoTransporteMaritimo->setAttribute('NombreAgenteNaviero',  $this->NombreAgenteNaviero);
        $nodoTransporteMaritimo->setAttribute('NumAutorizacionNaviero',  $this->NumAutorizacionNaviero);
        if($this->PermSCT)
		    $nodoTransporteMaritimo->setAttribute('PermSCT',  $this->PermSCT);
        if($this->NumPermisoSCT)
		    $nodoTransporteMaritimo->setAttribute('NumPermisoSCT',  $this->NumPermisoSCT);
        if($this->NombreAseg)
		    $nodoTransporteMaritimo->setAttribute('NombreAseg',  $this->NombreAseg);
        if($this->NumPolizaSeguro)
		    $nodoTransporteMaritimo->setAttribute('NumPolizaSeguro',  $this->NumPolizaSeguro);
        if($this->AnioEmbarcacion)
		    $nodoTransporteMaritimo->setAttribute('AnioEmbarcacion',  $this->AnioEmbarcacion);
        if($this->NombreEmbarc)
		    $nodoTransporteMaritimo->setAttribute('NombreEmbarc',  $this->NombreEmbarc);
        if($this->Eslora)
		    $nodoTransporteMaritimo->setAttribute('Eslora',  $this->Eslora);
        if($this->Manga)
		    $nodoTransporteMaritimo->setAttribute('Manga',  $this->Manga);
        if($this->Calado)
		    $nodoTransporteMaritimo->setAttribute('Calado',  $this->Calado);
        if($this->NumViaje)
		    $nodoTransporteMaritimo->setAttribute('NumViaje',  $this->NumViaje);
        // if($this->ValorMercancia)
		//     $nodoTransporteMaritimo->setAttribute('ValorMercancia',  $this->ValorMercancia);
        if($this->NumConocEmbarc)
		    $nodoTransporteMaritimo->setAttribute('NumConocEmbarc',  $this->NumConocEmbarc);

        if(!empty($this->Contenedor))
        {
            foreach ($this->Contenedor as $key => $Contenedor) 
            {
                $Contenedor->toXML();
                $nodoContenedor = $this->xml_base->importNode($Contenedor->importXML(), true);
                $nodoTransporteMaritimo->appendChild($nodoContenedor);
            }
        }

		$this->xml_base->appendChild($nodoTransporteMaritimo);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:TransporteMaritimo")->item(0);
		return $xml;
	}

    function addContenedor($MatriculaContenedor, $TipoContenedor, $NumPrecinto = null)
    {
		$Contenedor = new ContenedorTransporteMaritimo20(
            $MatriculaContenedor, 
            $TipoContenedor, 
            $NumPrecinto
        );
		
		// $Contenedor->validar();
		$this->Contenedor[] = $Contenedor;
		return $Contenedor;

    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.TransporteMaritimo.Contenedor

class ContenedorTransporteMaritimo20
{
	// Obligatorios
	var $MatriculaContenedor;
	var $TipoContenedor;

    // opcionales
    var $NumPrecinto;

	var $xml_base;
	var $logger;

	function __construct($MatriculaContenedor, $TipoContenedor, $NumPrecinto = null)
    {
		$this->xml_base = null;
        $this->MatriculaContenedor = $MatriculaContenedor;
        $this->TipoContenedor = $TipoContenedor;
        $this->NumPrecinto = $NumPrecinto;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['MatriculaContenedor', 'TipoContenedor'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.TransporteMaritimo.Contenedor validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.TransporteMaritimo.Contenedor Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoContenedor = $this->xml_base->createElement("cartaporte20:Contenedor");

        $nodoContenedor->setAttribute('MatriculaContenedor',  $this->MatriculaContenedor);
        $nodoContenedor->setAttribute('TipoContenedor',  $this->TipoContenedor);
        if($this->NumPrecinto)
		    $nodoContenedor->setAttribute('NumPrecinto',  $this->NumPrecinto);

		$this->xml_base->appendChild($nodoContenedor);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:Contenedor")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.TransporteAereo

class TransporteAereo20
{
	// Obligatorios
	var $PermSCT;
	var $NumPermisoSCT;
	var $MatriculaAeronave;
    var $NumeroGuia;
    var $CodigoTransportista;


    // opcionales
    var $NombreAseg;
    var $NumPolizaSeguro;
    var $LugarContrato;
    var $RFCTransportista;
    var $NumRegIdTribTranspor;
    var $ResidenciaFiscalTranspor;
    var $NombreTransportista;
    var $RFCEmbarcador;
    var $NumRegIdTribEmbarc;
    var $ResidenciaFiscalEmbarc;
    var $NombreEmbarcador;

	var $xml_base;
	var $logger;

	function __construct($PermSCT, $NumPermisoSCT, $MatriculaAeronave, $NumeroGuia, $CodigoTransportista, $NombreAseg = null, $NumPolizaSeguro = null, $LugarContrato = null,
                        $RFCTransportista = null, $NumRegIdTribTranspor = null, $ResidenciaFiscalTranspor = null, $NombreTransportista = null, $RFCEmbarcador = null, 
                        $NumRegIdTribEmbarc = null, $ResidenciaFiscalEmbarc = null, $NombreEmbarcador = null)
    {
		$this->xml_base = null;
        $this->PermSCT = $PermSCT;
        $this->NumPermisoSCT = $NumPermisoSCT;
        $this->MatriculaAeronave = $MatriculaAeronave;
        $this->NumeroGuia = $NumeroGuia;
        $this->CodigoTransportista = $CodigoTransportista;
        $this->NombreAseg = $NombreAseg;
        $this->NumPolizaSeguro = $NumPolizaSeguro;
        $this->LugarContrato = $LugarContrato;
        $this->RFCTransportista = $RFCTransportista;
        $this->NumRegIdTribTranspor = $NumRegIdTribTranspor;
        $this->ResidenciaFiscalTranspor = $ResidenciaFiscalTranspor;
        $this->NombreTransportista = $NombreTransportista;
        $this->RFCEmbarcador = $RFCEmbarcador;
        $this->NumRegIdTribEmbarc = $NumRegIdTribEmbarc;
        $this->ResidenciaFiscalEmbarc = $ResidenciaFiscalEmbarc;
        $this->NombreEmbarcador = $NombreEmbarcador;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['PermSCT', 'NumPermisoSCT', 'MatriculaAeronave', 'NumeroGuia', 'CodigoTransportista'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.TransporteAereo validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.TransporteAereo Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoTransporteAereo = $this->xml_base->createElement("cartaporte20:TransporteAereo");

        $nodoTransporteAereo->setAttribute('PermSCT',  $this->PermSCT);
        $nodoTransporteAereo->setAttribute('NumPermisoSCT',  $this->NumPermisoSCT);
        $nodoTransporteAereo->setAttribute('MatriculaAeronave',  $this->MatriculaAeronave);
        $nodoTransporteAereo->setAttribute('NumeroGuia',  $this->NumeroGuia);
        $nodoTransporteAereo->setAttribute('CodigoTransportista',  $this->CodigoTransportista);
        if($this->NombreAseg)
            $nodoTransporteAereo->setAttribute('NombreAseg',  $this->NombreAseg);
        if($this->NumPolizaSeguro)
            $nodoTransporteAereo->setAttribute('NumPolizaSeguro',  $this->NumPolizaSeguro);
        if($this->LugarContrato)
            $nodoTransporteAereo->setAttribute('LugarContrato',  $this->LugarContrato);
        if($this->RFCTransportista)
            $nodoTransporteAereo->setAttribute('RFCTransportista',  $this->RFCTransportista);
        if($this->NumRegIdTribTranspor)
            $nodoTransporteAereo->setAttribute('NumRegIdTribTranspor',  $this->NumRegIdTribTranspor);
        if($this->ResidenciaFiscalTranspor)
            $nodoTransporteAereo->setAttribute('ResidenciaFiscalTranspor',  $this->ResidenciaFiscalTranspor);
        if($this->NombreTransportista)
            $nodoTransporteAereo->setAttribute('NombreTransportista',  $this->NombreTransportista);
        if($this->RFCEmbarcador)
            $nodoTransporteAereo->setAttribute('RFCEmbarcador',  $this->RFCEmbarcador);
        if($this->NumRegIdTribEmbarc)
            $nodoTransporteAereo->setAttribute('NumRegIdTribEmbarc',  $this->NumRegIdTribEmbarc);
        if($this->ResidenciaFiscalEmbarc)
            $nodoTransporteAereo->setAttribute('ResidenciaFiscalEmbarc',  $this->ResidenciaFiscalEmbarc);
        if($this->NombreEmbarcador)
            $nodoTransporteAereo->setAttribute('NombreEmbarcador',  $this->NombreEmbarcador);

		$this->xml_base->appendChild($nodoTransporteAereo);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:TransporteAereo")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.TransporteFerroviario

class TransporteFerroviario20
{
	// Obligatorios
	var $TipoDeServicio;
    var $TipoDeTrafico;

    // opcionales
    var $NombreAseg;
    var $NumPolizaSeguro;
    var $Concesionario;

    var $DerechosDePaso = [];
    var $Carro = [];

	var $xml_base;
	var $logger;

	function __construct($TipoDeServicio, $NombreAseg = null, $NumPolizaSeguro = null, $Concesionario = null, $TipoDeTrafico=null)
    {
		$this->xml_base = null;
        $this->TipoDeServicio = $TipoDeServicio;
        $this->NombreAseg = $NombreAseg;
        $this->NumPolizaSeguro = $NumPolizaSeguro;
        $this->Concesionario = $Concesionario;
        $this->TipoDeTrafico=$TipoDeTrafico;
        $this->DerechosDePaso = [];
        $this->Carro = [];

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoDeServicio','TipoDeTrafico'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.TransporteFerroviario validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.TransporteFerroviario Campo Requerido: ' . $field);
			}
		}

        if(count($this->Carro) == 0)
        {
            $this->logger->write("Complemento.CartaPorte20.Mercancias.TransporteFerroviario validar(): Debe Contener por lo menos un elemento TransporteFerroviario.Carro");
            throw new Exception('Complemento.CartaPorte20.Mercancias.TransporteFerroviario. Debe Contener por lo menos un elemento TransporteFerroviario.Carro: ' . $field);
        }

		foreach($this->DerechosDePaso as $DerechosDePaso)
        {
			$DerechosDePaso->validar();
		}

        foreach($this->Carro as $Carro)
        {
			$Carro->validar();
        }
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoTransporteFerroviario = $this->xml_base->createElement("cartaporte20:TransporteFerroviario");

        $nodoTransporteFerroviario->setAttribute('TipoDeServicio',  $this->TipoDeServicio);
        $nodoTransporteFerroviario->setAttribute('TipoDeTrafico',  $this->TipoDeTrafico);
        if($this->NombreAseg)
            $nodoTransporteFerroviario->setAttribute('NombreAseg',  $this->NombreAseg);
        if($this->NumPolizaSeguro)
            $nodoTransporteFerroviario->setAttribute('NumPolizaSeguro',  $this->NumPolizaSeguro);
        if($this->Concesionario)
            $nodoTransporteFerroviario->setAttribute('Concesionario',  $this->Concesionario);

        if(!empty($this->DerechosDePaso))
        {
            foreach ($this->DerechosDePaso as $key => $DerechosDePaso) 
            {
                $DerechosDePaso->toXML();
                $nodoDerechosDePaso = $this->xml_base->importNode($DerechosDePaso->importXML(), true);
                $nodoTransporteFerroviario->appendChild($nodoDerechosDePaso);
            }
        }

        if(!empty($this->Carro))
        {
            foreach ($this->Carro as $key => $Carro) 
            {
                $Carro->toXML();
                $nodoCarro = $this->xml_base->importNode($Carro->importXML(), true);
                $nodoTransporteFerroviario->appendChild($nodoCarro);
            }
        }

		$this->xml_base->appendChild($nodoTransporteFerroviario);
    }

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:TransporteFerroviario")->item(0);
		return $xml;
	}

    function addDerechosDePaso($TipoDerechoDePaso, $KilometrajePagado)
    {
        $DerechosDePaso = new DerechosDePaso20(
            $TipoDerechoDePaso, 
            $KilometrajePagado
        );

        // $DerechosDePaso->validar();
        $this->DerechosDePaso[] = $DerechosDePaso; 
        return $DerechosDePaso;
    }

    function addCarro($TipoCarro, $MatriculaCarro, $GuiaCarro, $ToneladasNetasCarro)
    {
        $Carro = new Carro20(
            $TipoCarro, 
            $MatriculaCarro, 
            $GuiaCarro, 
            $ToneladasNetasCarro
        );

        // $Carro->validar();
        $this->Carro[] = $Carro; 
        return $Carro;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.TransporteFerroviario.DerechosDePaso

class DerechosDePaso20
{
	// Obligatorios
	var $TipoDerechoDePaso;
	var $KilometrajePagado;

	var $xml_base;
	var $logger;

	function __construct($TipoDerechoDePaso, $KilometrajePagado)
    {
		$this->xml_base = null;
        $this->TipoDerechoDePaso = $TipoDerechoDePaso;
        $this->KilometrajePagado = $KilometrajePagado;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoDerechoDePaso', 'KilometrajePagado'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.TransporteFerroviario.DerechosDePaso validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.TransporteFerroviario.DerechosDePaso Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoDerechosDePaso = $this->xml_base->createElement("cartaporte20:DerechosDePaso");

        $nodoDerechosDePaso->setAttribute('TipoDerechoDePaso',  $this->TipoDerechoDePaso);
        $nodoDerechosDePaso->setAttribute('KilometrajePagado',  $this->KilometrajePagado);

		$this->xml_base->appendChild($nodoDerechosDePaso);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:DerechosDePaso")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.TransporteFerroviario.Carro

class Carro20
{
	// Obligatorios
	var $TipoCarro;
	var $MatriculaCarro;
	var $GuiaCarro;
	var $ToneladasNetasCarro;

    var $Contenedor = [];

	var $xml_base;
	var $logger;

	function __construct($TipoCarro, $MatriculaCarro, $GuiaCarro, $ToneladasNetasCarro)
    {
		$this->xml_base = null;
        $this->TipoCarro = $TipoCarro;
        $this->MatriculaCarro = $MatriculaCarro;
        $this->GuiaCarro = $GuiaCarro;
        $this->ToneladasNetasCarro = $ToneladasNetasCarro;

        $this->Contenedor = [];

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoCarro', 'MatriculaCarro', 'GuiaCarro', 'ToneladasNetasCarro'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.TransporteFerroviario.Carro validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.TransporteFerroviario.Carro Campo Requerido: ' . $field);
			}
		}

		foreach($this->Contenedor as $Contenedor)
        {
			$Contenedor->validar();
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoCarro = $this->xml_base->createElement("cartaporte20:Carro");

        $nodoCarro->setAttribute('TipoCarro',  $this->TipoCarro);
        $nodoCarro->setAttribute('MatriculaCarro',  $this->MatriculaCarro);
        $nodoCarro->setAttribute('GuiaCarro',  $this->GuiaCarro);
        $nodoCarro->setAttribute('ToneladasNetasCarro',  $this->ToneladasNetasCarro);

        if(!empty($this->Contenedor))
        {
            foreach ($this->Contenedor as $key => $Contenedor) 
            {
                $Contenedor->toXML();
                $nodoContenedor = $this->xml_base->importNode($Contenedor->importXML(), true);
                $nodoCarro->appendChild($nodoContenedor);
            }
        }

		$this->xml_base->appendChild($nodoCarro);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:Carro")->item(0);
		return $xml;
	}

    function addContenedor($TipoContenedor, $PesoContenedorVacio, $PesoNetoMercancia)
    {
        $Contenedor = new ContenedorCarro20(
            $TipoContenedor, 
            $PesoContenedorVacio, 
            $PesoNetoMercancia
        );

        // $Contenedor->validar();
        $this->Contenedor[] = $Contenedor; 
        return $Contenedor;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.Mercancias.TransporteFerroviario.Carro.Contenedor

class ContenedorCarro20
{
	// Obligatorios
	var $TipoContenedor;
	var $PesoContenedorVacio;
    var $PesoNetoMercancia;

	var $xml_base;
	var $logger;

	function __construct($TipoContenedor, $PesoContenedorVacio, $PesoNetoMercancia)
    {
		$this->xml_base = null;
        $this->TipoContenedor = $TipoContenedor;
        $this->PesoContenedorVacio = $PesoContenedorVacio;
        $this->PesoNetoMercancia = $PesoNetoMercancia;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoContenedor', 'PesoContenedorVacio', 'PesoNetoMercancia'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.Mercancias.TransporteFerroviario.Carro.Contenedor validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.Mercancias.TransporteFerroviario.Carro.Contenedor Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoContenedor = $this->xml_base->createElement("cartaporte20:Contenedor");

        $nodoContenedor->setAttribute('TipoContenedor',  $this->TipoContenedor);
        $nodoContenedor->setAttribute('PesoContenedorVacio',  $this->PesoContenedorVacio);
        $nodoContenedor->setAttribute('PesoNetoMercancia',  $this->PesoNetoMercancia);

		$this->xml_base->appendChild($nodoContenedor);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:Contenedor")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.FiguraTransporte.TiposFigura

class TiposFigura20
{
	// Obligatorios
	var $TipoFigura;

    // Opcionales
	var $RFCFigura;
    var $NumLicencia;
    var $NombreFigura;
    var $NumRegIdTribFigura;
    var $ResidenciaFiscalFigura;
    var $PartesTransporte = [];
    var $Domicilio = null;

	var $xml_base;
	var $logger;

	function __construct($TipoFigura, $RFCFigura = null, $NumLicencia = null, $NombreFigura = null, $NumRegIdTribFigura = null, $ResidenciaFiscalFigura = null)
    {
		$this->xml_base = null;
        $this->TipoFigura = $TipoFigura;
        $this->RFCFigura = $RFCFigura;
        $this->NumLicencia = $NumLicencia;
        $this->NombreFigura = $NombreFigura;
        $this->NumRegIdTribFigura = $NumRegIdTribFigura;
        $this->ResidenciaFiscalFigura = $ResidenciaFiscalFigura;
        $this->PartesTransporte = [];
        $this->Domicilio = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoFigura'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.FiguraTransporte.TiposFigura validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.FiguraTransporte.TiposFigura Campo Requerido: ' . $field);
			}
		}

        if(!empty($this->PartesTransporte))
        {
            foreach ($this->PartesTransporte as $key => $PartesTransporte) 
			    $PartesTransporte->validar();
        }

        if($this->Domicilio)
            $this->Domicilio->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoTiposFigura = $this->xml_base->createElement("cartaporte20:TiposFigura");

        $nodoTiposFigura->setAttribute('TipoFigura',  $this->TipoFigura);
        if($this->RFCFigura)
            $nodoTiposFigura->setAttribute('RFCFigura',  $this->RFCFigura);
        if($this->NumLicencia)
            $nodoTiposFigura->setAttribute('NumLicencia',  $this->NumLicencia);
        if($this->NombreFigura)
            $nodoTiposFigura->setAttribute('NombreFigura',  $this->NombreFigura);
        if($this->NumRegIdTribFigura)
            $nodoTiposFigura->setAttribute('NumRegIdTribFigura',  $this->NumRegIdTribFigura);
        if($this->ResidenciaFiscalFigura)
            $nodoTiposFigura->setAttribute('ResidenciaFiscalFigura',  $this->ResidenciaFiscalFigura);

        if(!empty($this->PartesTransporte))
        {
            foreach ($this->PartesTransporte as $key => $PartesTransporte) 
            {
                $PartesTransporte->toXML();
                $nodoPartesTransporte = $this->xml_base->importNode($PartesTransporte->importXML(), true);
                $nodoTiposFigura->appendChild($nodoPartesTransporte);
            }
        }

        if(!empty($this->Domicilio))
        {
            $this->Domicilio->toXML();
            $nodoDomicilio = $this->xml_base->importNode($this->Domicilio->importXML(), true);
            $nodoTiposFigura->appendChild($nodoDomicilio);
        }

		$this->xml_base->appendChild($nodoTiposFigura);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:TiposFigura")->item(0);
		return $xml;
	}

    function addPartesTransporte($ParteTransporte)
    {
        $PartesTransporte = new PartesTransporte20(
            $ParteTransporte
        );
        // $PartesTransporte->validar();
        $this->PartesTransporte[] = $PartesTransporte;
        return $PartesTransporte;
    }

    function addDomicilio($Estado, $Pais, $CodigoPostal, $Calle = null, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
        $Domicilio = new Domicilio20(
            $Estado, 
            $Pais, 
            $CodigoPostal, 
            $Calle, 
            $NumeroExterior, 
            $NumeroInterior, 
            $Colonia, 
            $Localidad, 
            $Referencia, 
            $Municipio
        );
        // $Domicilio->validar();
        $this->Domicilio = $Domicilio;
        return $Domicilio;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte20.FiguraTransporte.TiposFigura.PartesTransporte

class PartesTransporte20
{
    // Obligatorios
    var $ParteTransporte;

	var $xml_base;
	var $logger;

	function __construct($ParteTransporte)
    {
		$this->xml_base = null;
        $this->ParteTransporte = $ParteTransporte;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['ParteTransporte'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte20.FiguraTransporte.TiposFigura.PartesTransporte validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte20.FiguraTransporte.TiposFigura.PartesTransporte Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoPartesTransporte = $this->xml_base->createElement("cartaporte20:PartesTransporte");

        $nodoPartesTransporte->setAttribute('ParteTransporte',  $this->ParteTransporte);

		$this->xml_base->appendChild($nodoPartesTransporte);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte20:PartesTransporte")->item(0);
		return $xml;
	}
}

