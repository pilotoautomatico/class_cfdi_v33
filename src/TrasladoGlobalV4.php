<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

use Exception;
use DOMDocument;

class TrasladoGlobalV4{
	var $Base;
	var $Impuesto;
	var $TipoFactor;
	var $TasaOCuota;
	var $Importe;
	var $xml_base;
	var $Decimales;

	function __construct($Base,$Impuesto, $TipoFactor ,$TasaOCuota=null, $Importe=null, $Decimales = 2) {
		$this->Base = $Base;
		$this->Decimales = $Decimales;
		$this->Impuesto = $Impuesto;
		$this->TipoFactor = $TipoFactor;
		$this->TasaOCuota = $TasaOCuota;
		$this->Importe = $Importe;
		if($TipoFactor=='Exento'){
			$this->TasaOCuota = null;
			$this->Importe = null;
		}
	}

	function validar() {
		$required = array(
			'Base',
			'Impuesto',
			'TipoFactor'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Traslado validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Traslado Campo Requerido: ' . $field);
			}
		}

		if($this->Base < 0){ //no debe ser negativo
			$this->logger->write('El atributo Base ' . $this->Base . ' debe tener un valor positivo');
			throw new Exception('El atributo Base ' . $this->Base . ' debe tener un valor positivo');
		}

		if($this->Importe < 0){ //no debe ser negativo
			$this->logger->write('El atributo Importe ' . $this->Importe . ' debe tener un valor positivo');
			throw new Exception('El atributo Importe ' . $this->Importe . ' debe tener un valor positivo');
		}
	}

	// pendiente para ver como va estar el rollo
	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$traslado = $this->xml_base->createElement("cfdi:Traslado");
		$this->xml_base->appendChild($traslado);

		# datos de tralado
		$traslado->SetAttribute('Base', $this->addZeros( round($this->Base, $this->Decimales) ));
		$traslado->SetAttribute('Impuesto', $this->Impuesto);
		$traslado->SetAttribute('TipoFactor', $this->TipoFactor);
		if (!is_null($this->TasaOCuota)){
			$traslado->SetAttribute('TasaOCuota', $this->addZeros($this->TasaOCuota, 6));
		}
		if (!is_null($this->Importe)){
			$traslado->SetAttribute('Importe', $this->addZeros(round($this->Importe,  $this->Decimales)));
		}
		
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("cfdi:Traslado")->item(0);
		return $xml;
	}
	
	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return  sprintf('%0.'.$dec.'f',$cantidad);
	}
}
?>
