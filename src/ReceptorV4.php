<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

use Exception;
use DOMDocument;

Use cfdi\Logger;

class ReceptorV4 {
	var $Rfc;
	var $Nombre;
	var $DomicilioFiscalReceptor;
	var $ResidenciaFiscal;
	var $NumRegIdTrib;
	var $RegimenFiscalReceptor;
	var $UsoCFDI;
	var $xml;
	var $logger;

	public function __construct($Rfc, $UsoCFDI, $Nombre = null, $DomicilioFiscalReceptor = null, $ResidenciaFiscal = null, $NumRegIdTrib = null, $RegimenFiscalReceptor = null) {
		$this->Rfc = $Rfc;
		$this->Nombre = $Nombre;
		$this->DomicilioFiscalReceptor = $DomicilioFiscalReceptor;
		$this->ResidenciaFiscal = $ResidenciaFiscal;
		$this->NumRegIdTrib = $NumRegIdTrib;
		$this->RegimenFiscalReceptor = $RegimenFiscalReceptor;
		$this->UsoCFDI = $UsoCFDI;
		$this->logger = new Logger(); //clase para escribir logs
	}

	public function validar() {
		$required = array(
			'Rfc',
			'Nombre',
			'DomicilioFiscalReceptor',
			'RegimenFiscalReceptor',
			'UsoCFDI'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Receptor validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Receptor Campo Requerido: ' . $field);
			}
		}
		if(strlen($this->Nombre) < 1 || strlen($this->Nombre) >254 ){
			$this->logger->write('Receptor validar Nombre: Debe contener entre 1 a 254 carácter(es) .');
			throw new Exception('El valor de Receptor Nombre debe ser entre 1 a 254 carácter(es): len='.strlen($this->Nombre));
		}
		if(!empty($this->NumRegIdTrib)){
			if(strlen($this->NumRegIdTrib) < 1 || strlen($this->NumRegIdTrib) >40 ){
				$this->logger->write('Receptor validar NumRegIdTrib: Debe contener entre 1 a 40 carácter(es) .');
				throw new Exception('El valor de Receptor NumRegIdTrib debe ser entre 1 a 40 carácter(es): len='.strlen($this->Nombre));
			}
		}
	}

	public function toXML() {
		$this->xml = new DOMdocument("1.0", "UTF-8");
		$domreceptor = $this->xml->createElement('cfdi:Receptor');
		$this->xml->appendChild($domreceptor);

		$domreceptor->setAttribute('Rfc', ($this->Rfc));
		$domreceptor->setAttribute('Nombre', $this->Nombre);
		$domreceptor->setAttribute('DomicilioFiscalReceptor', $this->DomicilioFiscalReceptor);
		if ($this->ResidenciaFiscal)
			$domreceptor->setAttribute('ResidenciaFiscal', $this->ResidenciaFiscal);
		if ($this->NumRegIdTrib)
			$domreceptor->setAttribute('NumRegIdTrib', $this->NumRegIdTrib);
		$domreceptor->setAttribute('RegimenFiscalReceptor', $this->RegimenFiscalReceptor);
		$domreceptor->setAttribute('UsoCFDI', $this->UsoCFDI);

		return $domreceptor;
	}

	function toStringXML() {
		return $this->xml->saveXML();
	}

	function importXML() {
		$xml = $this->xml->getElementsByTagName("cfdi:Receptor")->item(0);
		return $xml;
	}
}
?>