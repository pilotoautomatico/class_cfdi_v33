<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

class Logger {
	var $path_log;
	var $file_log;
	
	public function __construct($sub_dir = 'tmp') {
		$this->path_log = dirname(dirname(dirname(dirname(__DIR__))))."/$sub_dir/";
		$this->file_log = "Cfdi_".date("Y-m-W").".log";	
	}
	
	/*
	 * Escritura de logs
	 * */
	function write($msg = null, $type = 'DEBUG'){
		return false;
		// if($msg){
		// 	//primero valida si existe ubicacion del archivo, sino la crea
		// 	if(!file_exists($this->path_log)){
		// 		mkdir($this->path_log);
		// 	}
		// 	error_log(date("Y-m-d H:i:s") . " $type : " . $msg . "\n", 3, $this->path_log.$this->file_log);
		// }
	}
}
?>