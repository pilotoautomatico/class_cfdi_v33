<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv40 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;
Use cfdi\Data\Arrays;

use Exception;
use DOMDocument;

// esta clase iva a ser ulitlizada para generar los impuestos globales del CFDI , ya que acutalmente los calcula en automatico,
// el nodo de impuestos va cuando se da un anticipo.

class TrasladoDR {
	var $BaseDR;
	var $ImpuestoDR;
	var $TipoFactorDR;
	var $TasaOCuotaDR;
	var $ImporteDR; // el importe se calculara
	var $xml_base;
	var $Decimales;
	var $logger;

	function __construct($BaseDR, $ImpuestoDR, $TipoFactorDR, $TasaOCuotaDR= null, $ImporteDR=null, $Decimales=6) {

		$this->BaseDR = $BaseDR;
        $this->ImpuestoDR = $ImpuestoDR;
		$this->TipoFactorDR = $TipoFactorDR;
		$this->TasaOCuotaDR = $TasaOCuotaDR;
		$this->ImporteDR = $ImporteDR;
		$this->Decimales = $Decimales;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
        if($this->TipoFactorDR=='Tasa' || $this->TipoFactorDR=='Cuota'){
            $required = array(
                'BaseDR',
                'ImpuestoDR',
                'TipoFactorDR',
                "TasaOCuotaDR",
                // "ImporteDR"
            );
        }else{
            $required = array(
                'BaseDR',
                'ImpuestoDR',
                'TipoFactorDR',
            );
        }
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Retencion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Retencion Campo Requerido: ' . $field);
			}
		}

        if($this->BaseDR < 0.000001){ //valor minimo
            $this->logger->write('TrasladoDR BaseDR ' . $this->BaseDR . ' debe tener un valor minimo de 0.000001');
            throw new Exception('TrasladoDR BaseDR ' . $this->BaseDR . ' debe tener un valor minimo de 0.000001');
        }

        if($this->TipoFactorDR=='Tasa' || $this->TipoFactorDR=='Cuota'){
            if($this->TasaOCuotaDR < 0.000000){ //valor minimo
                $this->logger->write('TrasladoDR TasaOCuotaDR ' . $this->TasaOCuotaDR . ' debe tener un valor minimo de 0.000000');
                throw new Exception('TrasladoDR TasaOCuotaDR ' . $this->TasaOCuotaDR . ' debe tener un valor minimo de 0.000000');
            }

            if($this->ImporteDR<0){
                $this->logger->write('TrasladoDR ImporteDR ' . $this->ImporteDR . ' debe tener un valor positivo');
                throw new Exception('TrasladoDR ImporteDR ' . $this->ImporteDR . ' debe tener un valor positivo');
            }
        }else{
			if(!empty($this->TasaOCuotaDR)){
				if($this->TasaOCuotaDR < 0.000000){ //valor minimo
					$this->logger->write('TrasladoDR TasaOCuotaDR ' . $this->TasaOCuotaDR . ' debe tener un valor minimo de 0.000000');
					throw new Exception('TrasladoDR TasaOCuotaDR ' . $this->TasaOCuotaDR . ' debe tener un valor minimo de 0.000000');
				}
			}
			
			if(!empty($this->ImporteDR)){
				if($this->ImporteDR<0){
					$this->logger->write('TrasladoDR ImporteDR ' . $this->ImporteDR . ' debe tener un valor positivo');
					throw new Exception('TrasladoDR ImporteDR ' . $this->ImporteDR . ' debe tener un valor positivo');
				}
			}
		}
        

		// $this->validateDecimals();
		// $this->validateTax();
	}

	// pendiente para ver como va estar el rollo
	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$traslado = $this->xml_base->createElement("pago20:TrasladoDR");
		$this->xml_base->appendChild($traslado);

		# datos de traslado
		$traslado->SetAttribute('BaseDR', $this->addZeros($this->BaseDR));
        $traslado->SetAttribute('ImpuestoDR', $this->ImpuestoDR);
		$traslado->SetAttribute('TipoFactorDR', $this->TipoFactorDR);
        if($this->TipoFactorDR=='Tasa' || $this->TipoFactorDR=='Cuota'){
            $traslado->SetAttribute('TasaOCuotaDR', $this->addZeros($this->TasaOCuotaDR));
			if($this->ImporteDR !== null)
		    	$traslado->SetAttribute('ImporteDR', $this->addZeros($this->ImporteDR));
        }else{
			if($this->TasaOCuotaDR !== null){
				$traslado->SetAttribute('TasaOCuotaDR', $this->addZeros($this->TasaOCuotaDR));
			}
			
			if($this->ImporteDR !== null){
				$traslado->SetAttribute('ImporteDR', $this->addZeros($this->ImporteDR));
			}
		}

        return $traslado;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("pago20:TrasladoDR")->item(0);
		return $xml;
	}

	function addZeros($cantidad = null) {
		return sprintf('%0.' . $this->Decimales . 'f', $cantidad);
	}

	function validateDecimals() {
		$decimalesTotal = strlen(substr(strrchr($this->ImporteDR, "."), 1));
		if ($decimalesTotal > $this->Decimales) {
			throw new Exception("El importe de " . $this->ImporteDR .
			" en el traslado del pago no coincide con el valor de los decimales especificado por la moneda ,valor de decimales: " . $this->Decimales);
		}
	}

	function validateTax() {
		$valorTasa = null;
		$arrayCatalog = new Arrays();

		if ($this->TipoFactorDR == "Cuota" && $this->ImpuestoDR == "003") {
			if ((float) $this->TasaOCuotaDR <= 0.0000 || (float) $this->TasaOCuotaDR > 43.770000) {
				throw new Exception('El valor de la ' . $this->TipoFactorDR . 'que corresponde al impuesto 003 (ISR) : ' . $this->TasaOCuotaDR . ' en la retencion No esta dentro del rango permitido 0.000000 a 43.770000 verfique sus datos');
			}
		}

		if ($this->TipoFactorDR != 'Exento') { //solo valida cuando no sea exento
			$valorTasa = array_search((float) $this->TasaOCuotaDR, $arrayCatalog->arrayTasa[$this->ImpuestoDR][$this->TipoFactorDR]);
			if (!is_int($valorTasa)) {
				throw new Exception('El valor del campo TasaOCuotaDR : ' . $this->TasaOCuotaDR . ' del traslado no contiene un valor del catalogo de c_TasaOCuota especificado por el SAT.<br>'
				. 'Impuestos 001,002,003 valor introducido :' . $this->ImpuestoDR . '<br>'
				. 'Factores Tasa,Cuota,Exento valor introducido :' . $this->TipoFactorDR . '<br>');
			}
		}

		if ($this->ImpuestoDR == '001') {
			throw new Exception('El impuesto 001 que corresponde al ISR no debe declararse en un traslado');
		}
	}
}
?>