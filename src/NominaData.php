<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

use Exception;
use DOMDocument;

class NominaData {
	// Obligatorios
	public $Version = "1.2";

	//VARIABLES
	var $TipoNomina, 
	$FechaPago, 
	$FechaInicialPago, 
	$FechaFinalPago, 
	$NumDiasPagados, 
	$TotalPercepciones = null, 
	$TotalDeducciones = null, 
	$TotalOtrosPagos = null;

	//OBJETOS O ARRAYS
	var $Emisor = null,
	$Receptor = null,
	$Percepciones = null,
	$Deducciones = null,
	$OtrosPagos = null,
	$Incapacidades = null;

	//
	var $xml_base;
	var $logger;

	function __construct(
		$TipoNomina, 
		$FechaPago, 
		$FechaInicialPago, 
		$FechaFinalPago, 
		$NumDiasPagados, 
		$TotalPercepciones = null, 
		$TotalDeducciones = null, 
		$TotalOtrosPagos = null
	){
		$this->xml_base=null;
		$this->TipoNomina=strtoupper((string)$TipoNomina);
		$this->FechaPago= $FechaPago;
		$this->FechaInicialPago = $FechaInicialPago;
		$this->FechaFinalPago = $FechaFinalPago;
		$this->NumDiasPagados = trim((string)$NumDiasPagados);
		$this->NumDiasPagados=$this->addZeros($this->NumDiasPagados,3);
		$this->TotalPercepciones = $TotalPercepciones;
		$this->TotalDeducciones = $TotalDeducciones;
		$this->TotalOtrosPagos = $TotalOtrosPagos;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'TipoNomina',
			'FechaPago',
			'FechaInicialPago',
			'FechaFinalPago',
			'NumDiasPagados'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina Campo Requerido: ' . $field);
			}
		}

		if($this->TipoNomina!='O' && $this->TipoNomina!='E'){
			$this->logger->write('Nomina TipoNomina ' . $this->TipoNomina . ' debe ser O ó E ');
			throw new Exception('Nomina TipoNomina ' . $this->TipoNomina . ' debe ser O ó E ');
		}

		if($this->NumDiasPagados<'0.001' || $this->NumDiasPagados>'36160.000'){
			$this->logger->write('Nomina validar NumDiasPagados: Debe ser entre 0.001 y 36160.000 .');
			throw new Exception('Nomina validar NumDiasPagados: Debe ser entre 0.001 y 36160.000:'.$this->NumDiasPagados);
		}
		//NODOS
		
		//EMISOR
		if($this->Emisor!=null){
			//VALIDAR NODO
			$this->Emisor->validar();
		}

		//RECEPTOR
		if($this->Receptor==null){
			$this->logger->write("Nomina validar(): Nodo Receptor requerido :" . print_r($this->Receptor, true));
			throw new Exception('Nomina Nodo Receptor Requerido: ' . $this->Receptor);
		}else{
			//VALIDAR NODO
			$this->Receptor->validar();
		}

		//PERCEPCIONES
		if($this->Percepciones!=null){
			//VALIDAR NODO
			$this->Percepciones->validar();
		}

		//DEDUCCIONES
		if($this->Deducciones!=null){
			//VALIDAR NODO
			$this->Deducciones->validar();
		}

		//OTROSPAGOS
		if($this->OtrosPagos!=null){
			//VALIDAR NODO
			$this->OtrosPagos->validar();
		}

		//INCAPACIDADES
		if($this->Incapacidades!=null){
			//VALIDAR NODO
			$this->Incapacidades->validar();
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoNomina = $this->xml_base->createElement("nomina12:Nomina");
		$nodoNomina->setAttribute('Version', $this->Version);
		$nodoNomina->setAttribute('TipoNomina', $this->TipoNomina);
		$nodoNomina->setAttribute('FechaPago', $this->FechaPago);
		$nodoNomina->setAttribute('FechaInicialPago', $this->FechaInicialPago);
		$nodoNomina->setAttribute('FechaFinalPago', $this->FechaFinalPago);
		$nodoNomina->setAttribute('NumDiasPagados', $this->NumDiasPagados);
		if($this->TotalPercepciones!=null){
			$nodoNomina->setAttribute('TotalPercepciones', $this->TotalPercepciones);
		}
		if($this->TotalDeducciones!=null){
			$nodoNomina->setAttribute('TotalDeducciones', $this->TotalDeducciones);
		}
		if($this->TotalOtrosPagos!=null){
			$nodoNomina->setAttribute('TotalOtrosPagos', $this->TotalOtrosPagos);
		}

		//NODOS

		//EMISOR
		if($this->Emisor!=null){
			$this->Emisor->toXML();
			$nodoEmisor = $this->xml_base->importNode($this->Emisor->importXML(), true);
			$nodoNomina->appendChild($nodoEmisor);
		}

		//RECEPTOR
		$this->Receptor->toXML();
		$nodoReceptor = $this->xml_base->importNode($this->Receptor->importXML(), true);
		$nodoNomina->appendChild($nodoReceptor);

		//PERCEPCIONES
		if($this->Percepciones!=null){
			$this->Percepciones->toXML();
			$nodoPercepciones = $this->xml_base->importNode($this->Percepciones->importXML(), true);
			$nodoNomina->appendChild($nodoPercepciones);
		}

		//DEDUCCIONES
		if($this->Deducciones!=null){
			$this->Deducciones->toXML();
			$nodoDeducciones = $this->xml_base->importNode($this->Deducciones->importXML(), true);
			$nodoNomina->appendChild($nodoDeducciones);
		}

		//OTROSPAGOS
		if($this->OtrosPagos!=null){
			$this->OtrosPagos->toXML();
			$nodoOtrosPagos = $this->xml_base->importNode($this->OtrosPagos->importXML(), true);
			$nodoNomina->appendChild($nodoOtrosPagos);
		}

		//INCAPACIDADES
		if($this->Incapacidades!=null){
			//VALIDAR NODO
			$this->Incapacidades->toXML();
			$nodoIncapacidades = $this->xml_base->importNode($this->Incapacidades->importXML(), true);
			$nodoNomina->appendChild($nodoIncapacidades);
		}

		$this->xml_base->appendChild($nodoNomina);

		return $nodoNomina;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:Nomina")->item(0);
		return $xml;
	}
	
	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return sprintf('%0.' . $dec . 'f', $cantidad);
	}


	function addEmisor(
		$Curp = null, 
		$RegistroPatronal = null, 
		$RfcPatronOrigen = null
	){
		$newEmisor= new NominaEmisorData(
			$Curp, 
			$RegistroPatronal, 
			$RfcPatronOrigen
		);
		$this->Emisor = $newEmisor;
		return $newEmisor;
	}

	function addReceptor(
		$Curp,
		$NumSeguridadSocial = null,
		$FechaInicioRelLaboral = null,
		$Antigüedad = null,
		$TipoContrato,
		$Sindicalizado = null,
		$TipoJornada = null,
		$TipoRegimen,
		$NumEmpleado,
		$Departamento = null,
		$Puesto = null,
		$RiesgoPuesto = null,
		$PeriodicidadPago,
		$Banco = null,
		$CuentaBancaria = null,
		$SalarioBaseCotApor = null,
		$SalarioDiarioIntegrado = null,
		$ClaveEntFed
	){
		$newReceptor= new NominaReceptorData(
			$Curp,
			$NumSeguridadSocial,
			$FechaInicioRelLaboral,
			$Antigüedad,
			$TipoContrato,
			$Sindicalizado,
			$TipoJornada,
			$TipoRegimen,
			$NumEmpleado,
			$Departamento,
			$Puesto,
			$RiesgoPuesto,
			$PeriodicidadPago,
			$Banco,
			$CuentaBancaria,
			$SalarioBaseCotApor,
			$SalarioDiarioIntegrado,
			$ClaveEntFed
		);
		$this->Receptor = $newReceptor;
		return $newReceptor;
	}

	function addPercepciones(
		$TotalSueldos = null, 
		$TotalSeparacionIndemnizacion = null,
		$TotalJubilacionPensionRetiro = null,
		$TotalGravado,
		$TotalExento
	){
		$newPercepciones= new PercepcionesData(
			$TotalSueldos, 
			$TotalSeparacionIndemnizacion,
			$TotalJubilacionPensionRetiro,
			$TotalGravado,
			$TotalExento
		);
		$this->Percepciones = $newPercepciones;
		return $newPercepciones;
	}

	function addDeducciones(
		$TotalOtrasDeducciones = null,
		$TotalImpuestosRetenidos = null
	){
		$newDeducciones= new DeduccionesData(
			$TotalOtrasDeducciones,
			$TotalImpuestosRetenidos
		);
		$this->Deducciones = $newDeducciones;
		return $newDeducciones;
	}

	function addOtrosPagos(){
		$newOtrosPagos= new OtrosPagosData();
		$this->OtrosPagos = $newOtrosPagos;
		return $newOtrosPagos;
	}

	function addIncapacidades(){
		$newIncapacidades= new IncapacidadesData();
		$this->Incapacidades = $newIncapacidades;
		return $newIncapacidades;
	}
}

class NominaEmisorData{
	//VARIABLES
	var $Curp = null, 
	$RegistroPatronal = null, 
	$RfcPatronOrigen = null;

	//OBJETOS O ARRAYS
	var $EntidadSNCF = null;

	//
	var $xml_base;
	var $logger;

	function __construct($Curp = null, $RegistroPatronal = null, $RfcPatronOrigen = null){
		$this->xml_base=null;
		$this->Curp= $Curp;
		$this->RegistroPatronal = trim($RegistroPatronal);
		$this->RfcPatronOrigen= $RfcPatronOrigen;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		if($this->RegistroPatronal!=null){
			if(strlen($this->RegistroPatronal) < 1 || strlen($this->RegistroPatronal)>20 ){
				$this->logger->write('Nomina Emisor validar RegistroPatronal: Debe contener entre 1 a 20 carácter(es) .');
				throw new Exception('El valor de Nomina Emisor RegistroPatronal debe ser entre 1 a 20 carácter(es): len='.strlen($this->RegistroPatronal));
			}
		}

		if($this->EntidadSNCF!=null){
			$this->EntidadSNCF->validar();
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoEmisor = $this->xml_base->createElement("nomina12:Emisor");
		if($this->Curp!=null){
			$nodoEmisor->setAttribute('Curp', $this->Curp);
		}
		if($this->RegistroPatronal!=null){
			$nodoEmisor->setAttribute('RegistroPatronal', $this->RegistroPatronal);
		}
		if($this->RfcPatronOrigen!=null){
			$nodoEmisor->setAttribute('RfcPatronOrigen', $this->RfcPatronOrigen);
		}
		
		//NODOS
		if($this->EntidadSNCF!=null){
			$this->EntidadSNCF->toXML();
			$nodoEntidadSNCF = $this->xml_base->importNode($this->EntidadSNCF->importXML(), true);
			$nodoEmisor->appendChild($nodoEntidadSNCF);
		}

		$this->xml_base->appendChild($nodoEmisor);

		return $nodoEmisor;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:Emisor")->item(0);
		return $xml;
	}

	function addEntidadSNCF($OrigenRecurso, $MontoRecursoPropio = null){
		$newEntidadSNCF= new EntidadSNCFData($OrigenRecurso, $MontoRecursoPropio);
		$this->EntidadSNCF = $newEntidadSNCF;
		return $newEntidadSNCF;
	}
	
}

class EntidadSNCFData {
	//VARIABLES
	var $OrigenRecurso = null, 
	$MontoRecursoPropio = null;

	//
	var $xml_base;
	var $logger;

	function __construct($OrigenRecurso, $MontoRecursoPropio = null){
		$this->xml_base=null;
		$this->OrigenRecurso = $OrigenRecurso;
		$this->MontoRecursoPropio = $MontoRecursoPropio;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'OrigenRecurso'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina EntidadSNCF validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina EntidadSNCF Campo Requerido: ' . $field);
			}
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoEntidadSNCF = $this->xml_base->createElement("nomina12:EntidadSNCF");
		$nodoEntidadSNCF->setAttribute('OrigenRecurso', $this->OrigenRecurso);
		if($this->MontoRecursoPropio!=null){
			$nodoEntidadSNCF->setAttribute('MontoRecursoPropio', $this->MontoRecursoPropio);
		}

		$this->xml_base->appendChild($nodoEntidadSNCF);

		return $nodoEntidadSNCF;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:EntidadSNCF")->item(0);
		return $xml;
	}
}

class NominaReceptorData {
	//VARIABLES
	var $Curp = null,
	$NumSeguridadSocial = null,
	$FechaInicioRelLaboral = null,
	$Antigüedad = null,
	$TipoContrato = null,
	$Sindicalizado = null,
	$TipoJornada = null,
	$TipoRegimen = null,
	$NumEmpleado = null,
	$Departamento = null,
	$Puesto = null,
	$RiesgoPuesto = null,
	$PeriodicidadPago = null,
	$Banco = null,
	$CuentaBancaria = null,
	$SalarioBaseCotApor = null,
	$SalarioDiarioIntegrado = null,
	$ClaveEntFed = null;

	//OBJETOS O ARRAYS
	var $SubContratacion = array();

	//
	var $xml_base;
	var $logger;

	function __construct(
		$Curp,
		$NumSeguridadSocial = null,
		$FechaInicioRelLaboral = null,
		$Antigüedad = null,
		$TipoContrato,
		$Sindicalizado = null,
		$TipoJornada = null,
		$TipoRegimen,
		$NumEmpleado,
		$Departamento = null,
		$Puesto = null,
		$RiesgoPuesto = null,
		$PeriodicidadPago,
		$Banco = null,
		$CuentaBancaria = null,
		$SalarioBaseCotApor = null,
		$SalarioDiarioIntegrado = null,
		$ClaveEntFed
	){
		$this->xml_base=null;
		$this->Curp = $Curp;
		$this->NumSeguridadSocial = $NumSeguridadSocial!= null?trim((string)$NumSeguridadSocial):$NumSeguridadSocial;
		$this->FechaInicioRelLaboral = $FechaInicioRelLaboral;
		$this->Antigüedad = $Antigüedad!= null?trim((string)$Antigüedad):$Antigüedad;
		$this->TipoContrato = $TipoContrato;
		$this->Sindicalizado = $Sindicalizado!= null?trim((string)$Sindicalizado):$Sindicalizado;
		$this->TipoJornada = $TipoJornada;
		$this->TipoRegimen = $TipoRegimen;
		$this->NumEmpleado = trim((string)$NumEmpleado);
		$this->Departamento = $Departamento!= null?trim((string)$Departamento):$Departamento;
		$this->Puesto = $Puesto!= null?trim((string)$Puesto):$Puesto;
		$this->RiesgoPuesto = $RiesgoPuesto;
		$this->PeriodicidadPago = $PeriodicidadPago;
		$this->Banco = $Banco;
		$this->CuentaBancaria = $CuentaBancaria;
		$this->SalarioBaseCotApor = $SalarioBaseCotApor;
		$this->SalarioDiarioIntegrado = $SalarioDiarioIntegrado;
		$this->ClaveEntFed = $ClaveEntFed;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		
		$required = array(
			'Curp',
			'TipoContrato',
			'TipoRegimen',
			'NumEmpleado',
			'PeriodicidadPago',
			'ClaveEntFed'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina Receptor validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina Receptor Campo Requerido: ' . $field);
			}
		}

		if($this->NumSeguridadSocial!=null){
			if(strlen($this->NumSeguridadSocial) < 1 || strlen($this->NumSeguridadSocial)>15 ){
				$this->logger->write('Nomina Receptor validar NumSeguridadSocial: Debe contener entre 1 a 15 carácter(es) .');
				throw new Exception('El valor de Nomina Receptor NumSeguridadSocial debe ser entre 1 a 15 carácter(es): len='.strlen($this->NumSeguridadSocial));
			}
		}

		if($this->Sindicalizado!=null){
			if($this->Sindicalizado!='Sí' && $this->Sindicalizado!='No'){
				$this->logger->write('Nomina Receptor Sindicalizado ' . $this->Sindicalizado . ' debe ser Sí ó No');
				throw new Exception('Nomina Receptor Sindicalizado ' . $this->Sindicalizado . ' debe ser Sí ó No ');	
			}
		}

		if(strlen($this->NumEmpleado) < 1 || strlen($this->NumEmpleado)>15 ){
			$this->logger->write('Nomina Receptor validar NumEmpleado: Debe contener entre 1 a 15 carácter(es) .');
			throw new Exception('El valor de Nomina Receptor NumEmpleado debe ser entre 1 a 15 carácter(es): len='.strlen($this->NumEmpleado));
		}

		if($this->Departamento!=null){
			if(strlen($this->Departamento) < 1 || strlen($this->Departamento)>100 ){
				$this->logger->write('Nomina Receptor validar Departamento: Debe contener entre 1 a 100 carácter(es) .');
				throw new Exception('El valor de Nomina Receptor Departamento debe ser entre 1 a 100 carácter(es): len='.strlen($this->Departamento));
			}
		}

		if($this->Puesto!=null){
			if(strlen($this->Puesto) < 1 || strlen($this->Puesto)>100 ){
				$this->logger->write('Nomina Receptor validar Puesto: Debe contener entre 1 a 100 carácter(es) .');
				throw new Exception('El valor de Nomina Receptor Puesto debe ser entre 1 a 100 carácter(es): len='.strlen($this->Puesto));
			}
		}

		if(!empty($this->SubContratacion)){
			foreach($this->SubContratacion as $SubContratacionItem)
        	{
				$SubContratacionItem->validar();
			}
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoReceptor = $this->xml_base->createElement("nomina12:Receptor");

		$nodoReceptor->setAttribute('Curp', $this->Curp);
		if($this->NumSeguridadSocial!= null){
			$nodoReceptor->setAttribute('NumSeguridadSocial', $this->NumSeguridadSocial);
		}
		if($this->FechaInicioRelLaboral!= null){
			$nodoReceptor->setAttribute('FechaInicioRelLaboral', $this->FechaInicioRelLaboral);
		}
		if($this->Antigüedad!= null){
			$nodoReceptor->setAttribute('Antigüedad', $this->Antigüedad);
		}
		$nodoReceptor->setAttribute('TipoContrato', $this->TipoContrato);
		if($this->Sindicalizado!= null){
			$nodoReceptor->setAttribute('Sindicalizado', $this->Sindicalizado);
		}
		if($this->TipoJornada!= null){
			$nodoReceptor->setAttribute('TipoJornada', $this->TipoJornada);
		}
		$nodoReceptor->setAttribute('TipoRegimen', $this->TipoRegimen);
		$nodoReceptor->setAttribute('NumEmpleado', $this->NumEmpleado);
		if($this->Departamento!= null){
			$nodoReceptor->setAttribute('Departamento', $this->Departamento);
		}
		if($this->Puesto!= null){
			$nodoReceptor->setAttribute('Puesto', $this->Puesto);
		}
		if($this->RiesgoPuesto!= null){
			$nodoReceptor->setAttribute('RiesgoPuesto', $this->RiesgoPuesto);
		}
		$nodoReceptor->setAttribute('PeriodicidadPago', $this->PeriodicidadPago);
		if($this->Banco!= null){
			$nodoReceptor->setAttribute('Banco', $this->Banco);
		}
		if($this->CuentaBancaria!= null){
			$nodoReceptor->setAttribute('CuentaBancaria', $this->CuentaBancaria);
		}
		if($this->SalarioBaseCotApor!= null){
			$nodoReceptor->setAttribute('SalarioBaseCotApor', $this->SalarioBaseCotApor);
		}
		if($this->SalarioDiarioIntegrado!= null){
			$nodoReceptor->setAttribute('SalarioDiarioIntegrado', $this->SalarioDiarioIntegrado);
		}
		$nodoReceptor->setAttribute('ClaveEntFed', $this->ClaveEntFed);

		//NODOS

		if(!empty($this->SubContratacion)){
			foreach($this->SubContratacion as $SubContratacionItem)
			{
				$SubContratacionItem->toXML();
				$nodoSubContratacion = $this->xml_base->importNode($SubContratacionItem->importXML(), true);
				$nodoReceptor->appendChild($nodoSubContratacion);

			}
		}

		$this->xml_base->appendChild($nodoReceptor);

		return $nodoReceptor;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:Receptor")->item(0);
		return $xml;
	}

	function addSubContratacion($RfcLabora, $PorcentajeTiempo){
		$newSubContratacion= new SubContratacionData($RfcLabora, $PorcentajeTiempo);
		$this->SubContratacion[] = $newSubContratacion;
		return $newSubContratacion;
	}
}

class SubContratacionData {
	//VARIABLES
	var $RfcLabora = null, 
	$PorcentajeTiempo = null;

	//
	var $xml_base;
	var $logger;

	function __construct($RfcLabora, $PorcentajeTiempo){
		$this->xml_base=null;
		$this->RfcLabora = $RfcLabora;
		$this->PorcentajeTiempo = trim((string)$PorcentajeTiempo);
		$this->PorcentajeTiempo=$this->addZeros($this->PorcentajeTiempo,3);
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'RfcLabora',
			'PorcentajeTiempo'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina SubContratacion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina SubContratacion Campo Requerido: ' . $field);
			}
		}

		if($this->PorcentajeTiempo<'0.001' || $this->PorcentajeTiempo>'100.000'){
			$this->logger->write('Nomina SubContratacion validar PorcentajeTiempo: Debe ser entre 0.001 y 100.000 .');
			throw new Exception('Nomina SubContratacion validar PorcentajeTiempo: Debe ser entre 0.001 y 100.000:'.$this->PorcentajeTiempo);
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoSubContratacion = $this->xml_base->createElement("nomina12:SubContratacion");
		$nodoSubContratacion->setAttribute('RfcLabora', $this->RfcLabora);
		$nodoSubContratacion->setAttribute('PorcentajeTiempo', $this->PorcentajeTiempo);

		$this->xml_base->appendChild($nodoSubContratacion);

		return $nodoSubContratacion;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:SubContratacion")->item(0);
		return $xml;
	}

	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return sprintf('%0.' . $dec . 'f', $cantidad);
	}
}

class PercepcionesData {
	//VARIABLES
	var $TotalSueldos = null, 
	$TotalSeparacionIndemnizacion = null,
	$TotalJubilacionPensionRetiro = null,
	$TotalGravado = null,
	$TotalExento = null;

	//OBJETOS O ARRAYS
	var $Percepcion = array(),
	$JubilacionPensionRetiro = null,
	$SeparacionIndemnizacion = null;

	//
	var $xml_base;
	var $logger;

	function __construct(
		$TotalSueldos = null, 
		$TotalSeparacionIndemnizacion = null,
		$TotalJubilacionPensionRetiro = null,
		$TotalGravado,
		$TotalExento
	){
		$this->xml_base=null;
		$this->TotalSueldos = $TotalSueldos; 
		$this->TotalSeparacionIndemnizacion = $TotalSeparacionIndemnizacion;
		$this->TotalJubilacionPensionRetiro = $TotalJubilacionPensionRetiro;
		$this->TotalGravado = $TotalGravado;
		$this->TotalExento = $TotalExento;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'TotalGravado',
			'TotalExento'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina Percepciones validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina Percepciones Campo Requerido: ' . $field);
			}
		}

		//NODOS
		
		//PERCEPCION
		foreach($this->Percepcion as $PercepcionItem)
		{
			$PercepcionItem->validar();

		}

		//JUBILACIONPENSIONRETIRO
		if($this->JubilacionPensionRetiro!=null){
			$this->JubilacionPensionRetiro->validar();
		}

		//SEPARACIONINDEMNIZACION
		if($this->SeparacionIndemnizacion!=null){
			$this->SeparacionIndemnizacion->validar();
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoPercepciones = $this->xml_base->createElement("nomina12:Percepciones");
		if($this->TotalSueldos!=null){
			$nodoPercepciones->setAttribute('TotalSueldos', $this->TotalSueldos);	
		}
		if($this->TotalSeparacionIndemnizacion!=null){
			$nodoPercepciones->setAttribute('TotalSeparacionIndemnizacion', $this->TotalSeparacionIndemnizacion);	
		}
		if($this->TotalJubilacionPensionRetiro!=null){
			$nodoPercepciones->setAttribute('TotalJubilacionPensionRetiro', $this->TotalJubilacionPensionRetiro);	
		}
		$nodoPercepciones->setAttribute('TotalGravado', $this->TotalGravado);
		$nodoPercepciones->setAttribute('TotalExento', $this->TotalExento);

		//NODOS

		//PERCEPCION
		foreach($this->Percepcion as $PercepcionItem)
		{
			$PercepcionItem->toXML();
			$nodoPercepcion = $this->xml_base->importNode($PercepcionItem->importXML(), true);
			$nodoPercepciones->appendChild($nodoPercepcion);

		}

		//JUBILACIONPENSIONRETIRO
		if($this->JubilacionPensionRetiro!=null){
			$this->JubilacionPensionRetiro->toXML();
			$nodoJubilacionPensionRetiro = $this->xml_base->importNode($this->JubilacionPensionRetiro->importXML(), true);
			$nodoPercepciones->appendChild($nodoJubilacionPensionRetiro);
		}

		//SEPARACIONINDEMNIZACION
		if($this->SeparacionIndemnizacion!=null){
			$this->SeparacionIndemnizacion->toXML();
			$nodoSeparacionIndemnizacion = $this->xml_base->importNode($this->SeparacionIndemnizacion->importXML(), true);
			$nodoPercepciones->appendChild($nodoSeparacionIndemnizacion);
		}

		$this->xml_base->appendChild($nodoPercepciones);

		return $nodoPercepciones;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:Percepciones")->item(0);
		return $xml;
	}

	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return sprintf('%0.' . $dec . 'f', $cantidad);
	}

	function addPercepcion(
		$TipoPercepcion,
		$Clave,
		$Concepto,
		$ImporteGravado,
		$ImporteExento
	){
		$newPercepcion=new PercepcionData(
			$TipoPercepcion,
			$Clave,
			$Concepto,
			$ImporteGravado,
			$ImporteExento
		);
		$this->Percepcion[]=$newPercepcion;
		return $newPercepcion;
	}

	function addJubilacionPensionRetiro(
		$TotalUnaExhibicion = null,
		$TotalParcialidad = null,
		$MontoDiario = null,
		$IngresoAcumulable,
		$IngresoNoAcumulable
	){
		$newJubilacionPensionRetiro=new JubilacionPensionRetiroData(
			$TotalUnaExhibicion,
			$TotalParcialidad,
			$MontoDiario,
			$IngresoAcumulable,
			$IngresoNoAcumulable
		);
		$this->JubilacionPensionRetiro=$newJubilacionPensionRetiro;
		return $newJubilacionPensionRetiro;
	}

	function addSeparacionIndemnizacion(
		$TotalPagado,
		$NumAñosServicio,
		$UltimoSueldoMensOrd,
		$IngresoAcumulable,
		$IngresoNoAcumulable
	){
		$newSeparacionIndemnizacion=new SeparacionIndemnizacionData(
			$TotalPagado,
			$NumAñosServicio,
			$UltimoSueldoMensOrd,
			$IngresoAcumulable,
			$IngresoNoAcumulable
		);
		$this->SeparacionIndemnizacion=$newSeparacionIndemnizacion;
		return $newSeparacionIndemnizacion;
	}
}

class PercepcionData {
	//VARIABLES
	var $TipoPercepcion = null,
	$Clave = null,
	$Concepto = null,
	$ImporteGravado = null,
	$ImporteExento = null;

	//OBJETOS O ARRAYS
	var $AccionesOTitulos = null,
	$HorasExtra = array();

	//
	var $xml_base;
	var $logger;

	function __construct(
		$TipoPercepcion,
		$Clave,
		$Concepto,
		$ImporteGravado,
		$ImporteExento
	){
		$this->xml_base=null;
		$this->TipoPercepcion=$TipoPercepcion;
		$this->Clave=trim($Clave);
		$this->Concepto=trim($Concepto);
		$this->ImporteGravado=$ImporteGravado;
		$this->ImporteExento=$ImporteExento;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'TipoPercepcion',
			'Clave',
			'Concepto',
			'ImporteGravado',
			'ImporteExento'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina Percepcion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina Percepcion Campo Requerido: ' . $field);
			}
		}

		if(strlen($this->Clave) < 3 || strlen($this->Clave)>15 ){
			$this->logger->write('Nomina Percepcion validar Clave: Debe contener entre 3 a 15 carácter(es) .');
			throw new Exception('El valor de Nomina Percepcion Clave debe ser entre 3 a 15 carácter(es): len='.strlen($this->Clave));
		}

		if(strlen($this->Concepto) < 1 || strlen($this->Concepto)>100 ){
			$this->logger->write('Nomina Percepcion validar Concepto: Debe contener entre 1 a 100 carácter(es) .');
			throw new Exception('El valor de Nomina Percepcion Concepto debe ser entre 1 a 100 carácter(es): len='.strlen($this->Concepto));
		}
		//NODOS

		//ACCIONESOTITULOS
		if($this->AccionesOTitulos!=null){
			$this->AccionesOTitulos->validar();
		}

		//HORASEXTRA
		if(!empty($this->HorasExtra)){
			foreach ($this->HorasExtra as $HorasExtraItem){
				$HorasExtraItem->validar();
			}
		}

	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoPercepcion = $this->xml_base->createElement("nomina12:Percepcion");
		$nodoPercepcion->setAttribute('TipoPercepcion', $this->TipoPercepcion);
		$nodoPercepcion->setAttribute('Clave', $this->Clave);
		$nodoPercepcion->setAttribute('Concepto', $this->Concepto);
		$nodoPercepcion->setAttribute('ImporteGravado', $this->ImporteGravado);
		$nodoPercepcion->setAttribute('ImporteExento', $this->ImporteExento);

		//NODOS

		//ACCIONESOTITULOS
		if($this->AccionesOTitulos!=null){
			$this->AccionesOTitulos->toXML();
			$nodoAccionesOTitulos = $this->xml_base->importNode($this->AccionesOTitulos->importXML(), true);
			$nodoPercepcion->appendChild($nodoAccionesOTitulos);
		}

		//HORASEXTRA
		if(!empty($this->HorasExtra)){
			foreach ($this->HorasExtra as $HorasExtraItem){
				$HorasExtraItem->toXML();
				$nodoHorasExtra = $this->xml_base->importNode($HorasExtraItem->importXML(), true);
				$nodoPercepcion->appendChild($nodoHorasExtra);
			}
		}
		

		$this->xml_base->appendChild($nodoPercepcion);

		return $nodoPercepcion;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:Percepcion")->item(0);
		return $xml;
	}

	function addAccionesOTitulos(
		$ValorMercado,
		$PrecioAlOtorgarse
	){
		$newAccionesOTitulos = new AccionesOTitulosData(
			$ValorMercado,
			$PrecioAlOtorgarse
		);
		$this->AccionesOTitulos=$newAccionesOTitulos;
		return $newAccionesOTitulos;
	}

	function addHorasExtra(
		$Dias,
		$TipoHoras,
		$HorasExtra,
		$ImportePagado
	){
		$newHorasExtra = new HorasExtraData(
			$Dias,
			$TipoHoras,
			$HorasExtra,
			$ImportePagado
		);
		$this->HorasExtra[]=$newHorasExtra;
		return $newHorasExtra;
	}
}

class AccionesOTitulosData {
	//VARIABLES
	var $ValorMercado = null,
	$PrecioAlOtorgarse = null;

	//
	var $xml_base;
	var $logger;

	function __construct(
		$ValorMercado,
		$PrecioAlOtorgarse
	){
		$this->xml_base=null;
		$this->ValorMercado=trim((string)$ValorMercado);
		$this->ValorMercado=$this->addZeros($this->ValorMercado,6);

		$this->PrecioAlOtorgarse=trim((string)$PrecioAlOtorgarse);
		$this->PrecioAlOtorgarse=$this->addZeros($this->PrecioAlOtorgarse,6);
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'ValorMercado',
			'PrecioAlOtorgarse'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina AccionesOTitulos validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina AccionesOTitulos Campo Requerido: ' . $field);
			}
		}

		if($this->ValorMercado<'0.000001'){
			$this->logger->write('Nomina AccionesOTitulos validar ValorMercado: Debe ser mayor igual 0.000001 .');
			throw new Exception('Nomina AccionesOTitulos validar ValorMercado: Debe ser mayor igual 0.000001:'.$this->ValorMercado);
		}

		if($this->PrecioAlOtorgarse<'0.000001'){
			$this->logger->write('Nomina AccionesOTitulos validar PrecioAlOtorgarse: Debe ser mayor igual 0.000001 .');
			throw new Exception('Nomina AccionesOTitulos validar PrecioAlOtorgarse: Debe ser mayor igual 0.000001:'.$this->PrecioAlOtorgarse);
		}

	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoAccionesOTitulos = $this->xml_base->createElement("nomina12:AccionesOTitulos");
		$nodoAccionesOTitulos->setAttribute('ValorMercado', $this->ValorMercado);
		$nodoAccionesOTitulos->setAttribute('PrecioAlOtorgarse', $this->PrecioAlOtorgarse);
		
		$this->xml_base->appendChild($nodoAccionesOTitulos);

		return $nodoAccionesOTitulos;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:AccionesOTitulos")->item(0);
		return $xml;
	}

	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return sprintf('%0.' . $dec . 'f', $cantidad);
	}
}

class HorasExtraData {
	//VARIABLES
	var $Dias = null,
	$TipoHoras = null,
	$HorasExtra = null,
	$ImportePagado = null;

	//
	var $xml_base;
	var $logger;

	function __construct(
		$Dias,
		$TipoHoras,
		$HorasExtra,
		$ImportePagado
	){
		$this->xml_base=null;
		$this->Dias=trim((string)$Dias);
		$this->TipoHoras=$TipoHoras;
		$this->HorasExtra=$HorasExtra;
		$this->ImportePagado=$ImportePagado;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'Dias',
			'TipoHoras',
			'HorasExtra',
			'ImportePagado'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina HorasExtra validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina HorasExtra Campo Requerido: ' . $field);
			}
		}

		if($this->Dias<'1'){
			$this->logger->write('Nomina HorasExtra validar Dias: Debe ser mayor igual 1 .');
			throw new Exception('Nomina HorasExtra validar Dias: Debe ser mayor igual 1:'.$this->Dias);
		}

		if($this->HorasExtra<'1'){
			$this->logger->write('Nomina HorasExtra validar HorasExtra: Debe ser mayor igual 1 .');
			throw new Exception('Nomina HorasExtra validar HorasExtra: Debe ser mayor igual 1:'.$this->HorasExtra);
		}

	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoAccionesOTitulos = $this->xml_base->createElement("nomina12:HorasExtra");
		$nodoAccionesOTitulos->setAttribute('Dias', $this->Dias);
		$nodoAccionesOTitulos->setAttribute('TipoHoras', $this->TipoHoras);
		$nodoAccionesOTitulos->setAttribute('HorasExtra', $this->HorasExtra);
		$nodoAccionesOTitulos->setAttribute('ImportePagado', $this->ImportePagado);
		
		$this->xml_base->appendChild($nodoAccionesOTitulos);

		return $nodoAccionesOTitulos;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:HorasExtra")->item(0);
		return $xml;
	}
	
}

class JubilacionPensionRetiroData {
	//VARIABLES
	var $TotalUnaExhibicion = null,
	$TotalParcialidad = null,
	$MontoDiario = null,
	$IngresoAcumulable = null,
	$IngresoNoAcumulable = null;

	//
	var $xml_base;
	var $logger;

	function __construct(
		$TotalUnaExhibicion = null,
		$TotalParcialidad = null,
		$MontoDiario = null,
		$IngresoAcumulable,
		$IngresoNoAcumulable
	){
		$this->xml_base=null;
		$this->TotalUnaExhibicion = $TotalUnaExhibicion;
		$this->TotalParcialidad = $TotalParcialidad;
		$this->MontoDiario = $MontoDiario;
		$this->IngresoAcumulable = $IngresoAcumulable;
		$this->IngresoNoAcumulable = $IngresoNoAcumulable;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'IngresoAcumulable',
			'IngresoNoAcumulable'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina JubilacionPensionRetiro validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina JubilacionPensionRetiro Campo Requerido: ' . $field);
			}
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoJubilacionPensionRetiro = $this->xml_base->createElement("nomina12:JubilacionPensionRetiro");
		if($this->TotalUnaExhibicion){
			$nodoJubilacionPensionRetiro->setAttribute('TotalUnaExhibicion', $this->TotalUnaExhibicion);
		}
		if($this->TotalParcialidad){
			$nodoJubilacionPensionRetiro->setAttribute('TotalParcialidad', $this->TotalParcialidad);
		}
		if($this->MontoDiario){
			$nodoJubilacionPensionRetiro->setAttribute('MontoDiario', $this->MontoDiario);
		}
		$nodoJubilacionPensionRetiro->setAttribute('IngresoAcumulable', $this->IngresoAcumulable);
		$nodoJubilacionPensionRetiro->setAttribute('IngresoNoAcumulable', $this->IngresoNoAcumulable);
		
		$this->xml_base->appendChild($nodoJubilacionPensionRetiro);

		return $nodoJubilacionPensionRetiro;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:JubilacionPensionRetiro")->item(0);
		return $xml;
	}
}

class SeparacionIndemnizacionData {
	//VARIABLES
	var $TotalPagado = null,
	$NumAñosServicio = null,
	$UltimoSueldoMensOrd = null,
	$IngresoAcumulable = null,
	$IngresoNoAcumulable = null;

	//
	var $xml_base;
	var $logger;

	function __construct(
		$TotalPagado,
		$NumAñosServicio,
		$UltimoSueldoMensOrd,
		$IngresoAcumulable,
		$IngresoNoAcumulable
	){
		$this->xml_base=null;
		$this->TotalPagado=$TotalPagado;
		$this->NumAñosServicio=trim((string)$NumAñosServicio);
		$this->UltimoSueldoMensOrd=$UltimoSueldoMensOrd;
		$this->IngresoAcumulable=$IngresoAcumulable;
		$this->IngresoNoAcumulable=$IngresoNoAcumulable;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'TotalPagado',
			'NumAñosServicio',
			'UltimoSueldoMensOrd',
			'IngresoAcumulable',
			'IngresoNoAcumulable'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina SeparacionIndemnizacion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina SeparacionIndemnizacion Campo Requerido: ' . $field);
			}
		}

		if($this->NumAñosServicio<'1' || $this->NumAñosServicio>'99'){
			$this->logger->write('Nomina SeparacionIndemnizacion validar NumAñosServicio: Debe ser entre 1 y 99 .');
			throw new Exception('Nomina SeparacionIndemnizacion validar NumAñosServicio: Debe ser entre 1 y 99:'.$this->NumAñosServicio);
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoSeparacionIndemnizacion = $this->xml_base->createElement("nomina12:SeparacionIndemnizacion");
		$nodoSeparacionIndemnizacion->setAttribute('TotalPagado', $this->TotalPagado);
		$nodoSeparacionIndemnizacion->setAttribute('NumAñosServicio', $this->NumAñosServicio);
		$nodoSeparacionIndemnizacion->setAttribute('UltimoSueldoMensOrd', $this->UltimoSueldoMensOrd);
		$nodoSeparacionIndemnizacion->setAttribute('IngresoAcumulable', $this->IngresoAcumulable);
		$nodoSeparacionIndemnizacion->setAttribute('IngresoNoAcumulable', $this->IngresoNoAcumulable);
		
		$this->xml_base->appendChild($nodoSeparacionIndemnizacion);

		return $nodoSeparacionIndemnizacion;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:SeparacionIndemnizacion")->item(0);
		return $xml;
	}
}

class DeduccionesData {
	//VARIABLES
	var $TotalOtrasDeducciones = null,
	$TotalImpuestosRetenidos = null;

	//OBJETOS O ARRAYS
	var $Deduccion = array();

	//
	var $xml_base;
	var $logger;

	function __construct(
		$TotalOtrasDeducciones = null,
		$TotalImpuestosRetenidos = null
	){
		$this->xml_base=null;
		$this->TotalOtrasDeducciones = $TotalOtrasDeducciones;
		$this->TotalImpuestosRetenidos = $TotalImpuestosRetenidos;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		//NODOS
		foreach ($this->Deduccion as $DeduccionItem) {
			$DeduccionItem->validar();
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoDeducciones = $this->xml_base->createElement("nomina12:Deducciones");
		if($this->TotalOtrasDeducciones!=null){
			$nodoDeducciones->setAttribute('TotalOtrasDeducciones', $this->TotalOtrasDeducciones);
		}
		if($this->TotalImpuestosRetenidos){
			$nodoDeducciones->setAttribute('TotalImpuestosRetenidos', $this->TotalImpuestosRetenidos);	
		}

		//NODOS
		foreach ($this->Deduccion as $DeduccionItem) {
			$DeduccionItem->toXML();
			$nodoDeduccion = $this->xml_base->importNode($DeduccionItem->importXML(), true);
			$nodoDeducciones->appendChild($nodoDeduccion);
		}
		
		$this->xml_base->appendChild($nodoDeducciones);

		return $nodoDeducciones;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:Deducciones")->item(0);
		return $xml;
	}

	function addDeduccion(
		$TipoDeduccion,
		$Clave,
		$Concepto,
		$Importe
	){
		$newDeduccion= new DeduccionData(
			$TipoDeduccion,
			$Clave,
			$Concepto,
			$Importe
		);
		$this->Deduccion[]=$newDeduccion;
		return $newDeduccion;
	}
}

class DeduccionData {
	//VARIABLES
	var $TipoDeduccion = null,
	$Clave = null,
	$Concepto = null,
	$Importe = null;

	//
	var $xml_base;
	var $logger;

	function __construct(
		$TipoDeduccion,
		$Clave,
		$Concepto,
		$Importe
	){
		$this->xml_base=null;
		$this->TipoDeduccion=$TipoDeduccion;
		$this->Clave=trim($Clave);
		$this->Concepto=trim($Concepto);
		$this->Importe=$Importe;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'TipoDeduccion',
			'Clave',
			'Concepto',
			'Importe'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina Deduccion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina Deduccion Campo Requerido: ' . $field);
			}
		}

		if(strlen($this->Clave) < 3 || strlen($this->Clave)>15 ){
			$this->logger->write('Nomina Deduccion validar Clave: Debe contener entre 3 a 15 carácter(es) .');
			throw new Exception('El valor de Nomina Deduccion Clave debe ser entre 3 a 15 carácter(es): len='.strlen($this->Clave));
		}

		if(strlen($this->Concepto) < 1 || strlen($this->Concepto)>100 ){
			$this->logger->write('Nomina Deduccion validar Concepto: Debe contener entre 1 a 100 carácter(es) .');
			throw new Exception('El valor de Nomina Deduccion Concepto debe ser entre 1 a 100 carácter(es): len='.strlen($this->Concepto));
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoDeduccion = $this->xml_base->createElement("nomina12:Deduccion");
		$nodoDeduccion->setAttribute('TipoDeduccion', $this->TipoDeduccion);
		$nodoDeduccion->setAttribute('Clave', $this->Clave);
		$nodoDeduccion->setAttribute('Concepto', $this->Concepto);
		$nodoDeduccion->setAttribute('Importe', $this->Importe);

		$this->xml_base->appendChild($nodoDeduccion);

		return $nodoDeduccion;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:Deduccion")->item(0);
		return $xml;
	}
}

class OtrosPagosData {
	//OBJETOS O ARRAYS
	var $OtroPago=array();

	//
	var $xml_base;
	var $logger;

	function __construct(){
		$this->xml_base=null;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		//OTROPAGO
		if(empty($this->OtroPago)){
			$this->logger->write("Nomina OtrosPagos validar(): Nodo OtroPago requerido :" . print_r($this->OtroPago, true));
			throw new Exception('Nomina OtrosPagos Nodo OtroPago Requerido: ' . $this->OtroPago);
		}else{
			foreach ($this->OtroPago as $OtroPagoItem){
				$OtroPagoItem->validar();
			}
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoOtrosPagos = $this->xml_base->createElement("nomina12:OtrosPagos");
		//NODOS
		foreach ($this->OtroPago as $OtroPagoItem){
			$OtroPagoItem->toXML();
			$nodoOtroPago = $this->xml_base->importNode($OtroPagoItem->importXML(), true);
			$nodoOtrosPagos->appendChild($nodoOtroPago);
		}

		$this->xml_base->appendChild($nodoOtrosPagos);

		return $nodoOtrosPagos;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:OtrosPagos")->item(0);
		return $xml;
	}

	function addOtroPago(
		$TipoOtroPago,
		$Clave,
		$Concepto,
		$Importe
	){
		$newOtroPago= new OtroPagoData(
			$TipoOtroPago,
			$Clave,
			$Concepto,
			$Importe
		);
		$this->OtroPago[] = $newOtroPago;
		return $newOtroPago;
	}
}

class OtroPagoData {
	//VARIABLES
	var $TipoOtroPago = null,
	$Clave = null,
	$Concepto = null,
	$Importe = null;

	//OBJETOS O ARRAYS
	var $SubsidioAlEmpleo = null,
	$CompensacionSaldosAFavor = null;

	//
	var $xml_base;
	var $logger;

	function __construct(
		$TipoOtroPago,
		$Clave,
		$Concepto,
		$Importe
	){
		$this->xml_base=null;
		$this->TipoOtroPago = $TipoOtroPago;
		$this->Clave = trim($Clave);
		$this->Concepto = trim($Concepto);
		$this->Importe = $Importe;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'TipoOtroPago',
			'Clave',
			'Concepto',
			'Importe'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina OtroPago validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina OtroPago Campo Requerido: ' . $field);
			}
		}

		if(strlen($this->Clave) < 3 || strlen($this->Clave)>15 ){
			$this->logger->write('Nomina OtroPago validar Clave: Debe contener entre 3 a 15 carácter(es) .');
			throw new Exception('El valor de Nomina OtroPago Clave debe ser entre 3 a 15 carácter(es): len='.strlen($this->Clave));
		}

		if(strlen($this->Concepto) < 1 || strlen($this->Concepto)>100 ){
			$this->logger->write('Nomina OtroPago validar Concepto: Debe contener entre 1 a 100 carácter(es) .');
			throw new Exception('El valor de Nomina OtroPago Concepto debe ser entre 1 a 100 carácter(es): len='.strlen($this->Concepto));
		}
		
		//NODOS

		//SUBSIDIOALEMPLEO
		if($this->SubsidioAlEmpleo==null){
			$this->logger->write("Nomina OtroPago validar(): Nodo SubsidioAlEmpleo requerido :" . print_r($this->SubsidioAlEmpleo, true));
			throw new Exception('Nomina OtroPago Nodo SubsidioAlEmpleo Requerido: ' . $this->SubsidioAlEmpleo);
		}else{
			//VALIDAR NODO
			$this->SubsidioAlEmpleo->validar();
		}

		//COMPENSACIONSALDOSAFAVOR
		if($this->CompensacionSaldosAFavor!=null){
			$this->CompensacionSaldosAFavor->validar();
		}


	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoOtroPago = $this->xml_base->createElement("nomina12:OtroPago");
		$nodoOtroPago->setAttribute('TipoOtroPago', $this->TipoOtroPago);
		$nodoOtroPago->setAttribute('Clave', $this->Clave);
		$nodoOtroPago->setAttribute('Concepto', $this->Concepto);
		$nodoOtroPago->setAttribute('Importe', $this->Importe);

		//SUBSIDIOALEMPLEO
		$this->SubsidioAlEmpleo->toXML();
		$nodoSubsidioAlEmpleo = $this->xml_base->importNode($this->SubsidioAlEmpleo->importXML(), true);
		$nodoOtroPago->appendChild($nodoSubsidioAlEmpleo);

		//COMPENSACIONSALDOSAFAVOR
		if($this->CompensacionSaldosAFavor!=null){
			$this->CompensacionSaldosAFavor->toXML();
			$nodoCompensacionSaldosAFavor = $this->xml_base->importNode($this->CompensacionSaldosAFavor->importXML(), true);
			$nodoOtroPago->appendChild($nodoCompensacionSaldosAFavor);
		}

		$this->xml_base->appendChild($nodoOtroPago);

		return $nodoOtroPago;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:OtroPago")->item(0);
		return $xml;
	}

	function addSubsidioAlEmpleo($SubsidioCausado){
		$newSubsidioAlEmpleo = new SubsidioAlEmpleoData($SubsidioCausado);
		$this->SubsidioAlEmpleo=$newSubsidioAlEmpleo;
		return $newSubsidioAlEmpleo;
	}

	function addCompensacionSaldosAFavor(
		$SaldoAFavor,
		$Año,
		$RemanenteSalFav
	){
		$newCompensacionSaldosAFavor = new CompensacionSaldosAFavorData(
			$SaldoAFavor,
			$Año, 
			$RemanenteSalFav
		);
		$this->CompensacionSaldosAFavor= $newCompensacionSaldosAFavor;
		return $newCompensacionSaldosAFavor;
	}
}

class SubsidioAlEmpleoData {
	//VARIABLES
	var $SubsidioCausado = null;

	//
	var $xml_base;
	var $logger;

	function __construct(
		$SubsidioCausado
	){
		$this->xml_base=null;
		$this->SubsidioCausado = $SubsidioCausado;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'SubsidioCausado',
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina SubsidioAlEmpleo validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina SubsidioAlEmpleo Campo Requerido: ' . $field);
			}
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoSubsidioAlEmpleo = $this->xml_base->createElement("nomina12:SubsidioAlEmpleo");
		$nodoSubsidioAlEmpleo->setAttribute('SubsidioCausado', $this->SubsidioCausado);

		$this->xml_base->appendChild($nodoSubsidioAlEmpleo);

		return $nodoSubsidioAlEmpleo;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:SubsidioAlEmpleo")->item(0);
		return $xml;
	}
}

class CompensacionSaldosAFavorData {
	//VARIABLES
	var $SaldoAFavor = null,
	$Año = null,
	$RemanenteSalFav = null;

	//
	var $xml_base;
	var $logger;

	function __construct(
		$SaldoAFavor,
		$Año,
		$RemanenteSalFav
	){
		$this->xml_base=null;
		$this->SaldoAFavor = $SaldoAFavor;
		$this->Año = trim((string)$Año);
		$this->RemanenteSalFav = $RemanenteSalFav;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'SaldoAFavor',
			'Año',
			'RemanenteSalFav'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina CompensacionSaldosAFavor validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina CompensacionSaldosAFavor Campo Requerido: ' . $field);
			}
		}

		if($this->Año<'2016'){
			$this->logger->write('Nomina CompensacionSaldosAFavor validar Año: Debe ser mayor igual 2016 .');
			throw new Exception('Nomina CompensacionSaldosAFavor validar Año: Debe ser mayor igual 2016:'.$this->Año);
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoCompensacionSaldosAFavor = $this->xml_base->createElement("nomina12:CompensacionSaldosAFavor");
		$nodoCompensacionSaldosAFavor->setAttribute('SaldoAFavor', $this->SaldoAFavor);
		$nodoCompensacionSaldosAFavor->setAttribute('Año', $this->Año);
		$nodoCompensacionSaldosAFavor->setAttribute('RemanenteSalFav', $this->RemanenteSalFav);

		$this->xml_base->appendChild($nodoCompensacionSaldosAFavor);

		return $nodoCompensacionSaldosAFavor;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:CompensacionSaldosAFavor")->item(0);
		return $xml;
	}
}

class IncapacidadesData {
	//OBJETOS O ARRAYS
	var $Incapacidad=array();

	//
	var $xml_base;
	var $logger;

	function __construct(){
		$this->xml_base=null;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		//Incapacidad
		if(empty($this->Incapacidad)){
			$this->logger->write("Nomina Incapacidades validar(): Nodo Incapacidad requerido :" . print_r($this->Incapacidad, true));
			throw new Exception('Nomina Incapacidades Nodo Incapacidad Requerido: ' . $this->Incapacidad);
		}else{
			foreach ($this->Incapacidad as $IncapacidadItem){
				$IncapacidadItem->validar();
			}
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoIncapacidades = $this->xml_base->createElement("nomina12:Incapacidades");
		//NODOS
		foreach ($this->Incapacidad as $IncapacidadItem){
			$IncapacidadItem->toXML();
			$nodoIncapacidad = $this->xml_base->importNode($IncapacidadItem->importXML(), true);
			$nodoIncapacidades->appendChild($nodoIncapacidad);
		}

		$this->xml_base->appendChild($nodoIncapacidades);

		return $nodoIncapacidades;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:Incapacidades")->item(0);
		return $xml;
	}

	function addIncapacidad(
		$DiasIncapacidad,
		$TipoIncapacidad,
		$ImporteMonetario = null
	){
		$newIncapacidad= new IncapacidadData(
			$DiasIncapacidad,
			$TipoIncapacidad,
			$ImporteMonetario
		);
		$this->Incapacidad[] = $newIncapacidad;
		return $newIncapacidad;
	}
}

class IncapacidadData {
	//VARIABLES
	var $DiasIncapacidad = null,
	$TipoIncapacidad = null,
	$ImporteMonetario = null;

	//
	var $xml_base;
	var $logger;

	function __construct(
		$DiasIncapacidad,
		$TipoIncapacidad,
		$ImporteMonetario = null
	){
		$this->xml_base=null;
		$this->DiasIncapacidad = trim((string)$DiasIncapacidad);
		$this->TipoIncapacidad = $TipoIncapacidad;
		$this->ImporteMonetario = $ImporteMonetario;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'DiasIncapacidad',
			'TipoIncapacidad'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Nomina Incapacidad validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Nomina Incapacidad Campo Requerido: ' . $field);
			}
		}

		if($this->DiasIncapacidad<'1'){
			$this->logger->write('Nomina Incapacidad validar DiasIncapacidad: Debe ser mayor igual 1 .');
			throw new Exception('Nomina Incapacidad validar DiasIncapacidad: Debe ser mayor igual 1:'.$this->DiasIncapacidad);
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodoIncapacidad = $this->xml_base->createElement("nomina12:Incapacidad");
		$nodoIncapacidad->setAttribute('DiasIncapacidad', $this->DiasIncapacidad);
		$nodoIncapacidad->setAttribute('TipoIncapacidad', $this->TipoIncapacidad);
		if($this->ImporteMonetario!=null){
			$nodoIncapacidad->setAttribute('ImporteMonetario', $this->ImporteMonetario);
		}

		$this->xml_base->appendChild($nodoIncapacidad);

		return $nodoIncapacidad;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("nomina12:Incapacidad")->item(0);
		return $xml;
	}
}
?>