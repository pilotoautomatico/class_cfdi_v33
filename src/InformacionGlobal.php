<?php
/* +-------------------------------------------------------------------------------------+
 * |                 © 2015-2022 PILOTO AUTOMATICO                                       |
 * | Clase cfdiv40 para precisar la información relacionada con el comprobante global    |
 * |                                                                                     |
 * +----------------------------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

use Exception;
use DOMDocument;

class InformacionGlobal {
	var $Periodicidad;
	var $Meses;
	var $Año;
	var $xml;
	var $logger;

	public function __construct($Periodicidad, $Meses, $Año) {
		$this->Periodicidad = $Periodicidad;
		$this->Meses = $Meses;
		$this->Año = $Año;
		$this->logger = new Logger(); //clase para escribir logs
	}

	public function validar() {
		// valida el Periodicidad
		$required = array(
			'Periodicidad',
            'Meses',
            'Año'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Información Global validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Información Global Campo Requerido: ' . $field);
			}
		}
        #validar el año que sea mayor o igual a 2021
		if ($this->Año<'2021'){
			$this->logger->write("Información Global validar(): el año minimo debe ser el 2021");
			throw new Exception('Información Global validar(): el año minimo debe ser el 2021');
		}
	}

	// Crea los atrubutos tomando como base el array de las reglas de validacion, asi evito que agrege campos de mas en el
	public function toXML() {
		$this->xml = new DOMdocument("1.0", "UTF-8");
		$domInformacionGlobal = $this->xml->createElement('cfdi:InformacionGlobal');
		$this->xml->appendChild($domInformacionGlobal);

		$domInformacionGlobal->setAttribute('Periodicidad', $this->Periodicidad);
		$domInformacionGlobal->setAttribute('Meses', $this->Meses);
		$domInformacionGlobal->setAttribute('Año', $this->Año);

		return $domInformacionGlobal;
	}

	function toStringXML() {
		return $this->xml->saveXML();
	}

	function importXML() {
		$xml = $this->xml->getElementsByTagName("cfdi:InformacionGlobal")->item(0);
		return $xml;
	}
}
?>