<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

use Exception;
use DOMDocument;

class EmisorV4 {
	var $Rfc;
	var $Nombre;
	var $RegimenFiscal;
	var $FacAtrAdquirente;
	var $xml;
	var $logger;

	public function __construct($Rfc, $Nombre = null, $RegimenFiscal = null, $FacAtrAdquirente=null) {
		$this->Rfc = $Rfc;
		$this->Nombre = $Nombre;
		$this->RegimenFiscal = $RegimenFiscal;
		$this->FacAtrAdquirente = $FacAtrAdquirente;
		$this->logger = new Logger(); //clase para escribir logs
	}

	public function validar() {
		// valida el RFC
		$required = array(
			'Rfc',
			'Nombre',
			'RegimenFiscal',
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Emisor validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Emisor Campo Requerido: ' . $field);
			}
		}
		if(strlen($this->Nombre) < 1 || strlen($this->Nombre) >254 ){
			$this->logger->write('Emisor validar Nombre: Debe contener entre 1 a 254 carácter(es) .');
			throw new Exception('El valor de Emisor Nombre debe ser entre 1 a 254 carácter(es): len='.strlen($this->Descripcion));
		}
	}

	// Crea los atrubutos tomando como base el array de las reglas de validacion, asi evito que agrege campos de mas en el
	public function toXML() {
		$this->xml = new DOMdocument("1.0", "UTF-8");
		$domemisor = $this->xml->createElement('cfdi:Emisor');
		$this->xml->appendChild($domemisor);

		$domemisor->setAttribute('Rfc', $this->Rfc);
		$domemisor->setAttribute('Nombre', $this->Nombre);
		$domemisor->setAttribute('RegimenFiscal', $this->RegimenFiscal);
		if ($this->FacAtrAdquirente)
			$domemisor->setAttribute('FacAtrAdquirente', $this->FacAtrAdquirente);
		return $domemisor;
	}

	function toStringXML() {
		return $this->xml->saveXML();
	}

	function importXML() {
		$xml = $this->xml->getElementsByTagName("cfdi:Emisor")->item(0);
		return $xml;
	}
}
?>