<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

use Exception;
use DOMDocument;

Use cfdi\Logger;

class ACuentaTerceros {
	var $RfcACuentaTerceros;
    var $NombreACuentaTerceros; 
    var $RegimenFiscalACuentaTerceros;
    var $DomicilioFiscalACuentaTerceros;
	var $xml;
	var $logger;

	public function __construct($RfcACuentaTerceros, $NombreACuentaTerceros, $RegimenFiscalACuentaTerceros, $DomicilioFiscalACuentaTerceros) {
		$this->RfcACuentaTerceros = $RfcACuentaTerceros;
		$this->NombreACuentaTerceros = $NombreACuentaTerceros;
		$this->RegimenFiscalACuentaTerceros = $RegimenFiscalACuentaTerceros;
		$this->DomicilioFiscalACuentaTerceros = $DomicilioFiscalACuentaTerceros;
		$this->logger = new Logger(); //clase para escribir logs
	}

	public function validar() {
		$required = array(
			'RfcACuentaTerceros',
			'NombreACuentaTerceros',
			'RegimenFiscalACuentaTerceros',
			'DomicilioFiscalACuentaTerceros'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("ACuentaTerceros validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('ACuentaTerceros Campo Requerido: ' . $field);
			}
		}

        if(strlen($this->NombreACuentaTerceros) < 1 || strlen($this->NombreACuentaTerceros) >254 ){
			$this->logger->write('ACuentaTerceros validar Nombre A Cuenta Terceros: Debe contener entre 1 a 254 carácter(es) .');
			throw new Exception('El valor de ACuentaTerceros Nombre A Cuenta Terceros debe ser entre 1 a 254 carácter(es): len='.strlen($this->NombreACuentaTerceros));
		}

        if(strlen($this->DomicilioFiscalACuentaTerceros) > 5){
			$this->logger->write('ACuentaTerceros validar DomicilioFiscalACuentaTerceros A Cuenta Terceros: Debe contener entre 5 carácteres .');
			throw new Exception('El valor de ACuentaTerceros DomicilioFiscalACuentaTerceros A Cuenta Terceros debe ser 5 carácteres: len='.strlen($this->DomicilioFiscalACuentaTerceros));
		}
	}

	public function toXML() {
		$this->xml = new DOMdocument("1.0", "UTF-8");
		$domacuentadeterceros = $this->xml->createElement('cfdi:ACuentaTerceros');
		$this->xml->appendChild($domacuentadeterceros);

		$domacuentadeterceros->setAttribute('RfcACuentaTerceros', ($this->RfcACuentaTerceros));
		$domacuentadeterceros->setAttribute('NombreACuentaTerceros', $this->NombreACuentaTerceros);
		$domacuentadeterceros->setAttribute('RegimenFiscalACuentaTerceros', $this->RegimenFiscalACuentaTerceros);
        $domacuentadeterceros->setAttribute('DomicilioFiscalACuentaTerceros', $this->DomicilioFiscalACuentaTerceros);

		return $domacuentadeterceros;
	}

	function toStringXML() {
		return $this->xml->saveXML();
	}

	function importXML() {
		$xml = $this->xml->getElementsByTagName("cfdi:ACuentaTerceros")->item(0);
		return $xml;
	}
}
?>