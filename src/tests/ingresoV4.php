<?php
    require '../../vendor/autoload.php';
    Use cfdi\ComprobanteV4;
    use DateTime;
    use DateTimeZone;
    try{
        $objDateTime = new DateTime("now", new DateTimeZone('America/Mexico_City'));
        //--SELLOS EN ARCHIVOS
        $key = getcwd() . "/keys/00001000000505422144.key.pem";
	    $cer = getcwd() . "/keys/00001000000505422144.cer.pem";

        //--SELLOS EN STRING
        // $key =<<<PEM
        // -----BEGIN PRIVATE KEY-----
        // MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCk7V9zWK0nBvZu
        // 50als34CQxXa/kUMO6BzrYPIY2s5XmmthoNze7zx3pNQURYivHH72TJtKQV1EA4p
        // hchk4yf8iLcTHFlAghXsS1e/icEaUZvQPS3j8f3YBCq8q3/3IbPkjIs7/CbphYfp
        // fOpHtcRmJGdPu5Ocs4GJ4p2f2telalZwUI0PYCQipYN6reJFB+LRrCWkFWU5MLYk
        // Czf9u/NtO6wjd87+QMupgA6ZWV5uFjgug8z6DIi6Jqsj1vkWOKoXBoZG1NtSJi6K
        // E1Lc+W3AIWCxZLfcaBhoXb4p60KFnMEIAYJ+zk1AuEtMUee7q9Vnq5qMlAY45Bos
        // K6W50MnRAgMBAAECggEAVwrgQWQfnmVSUh3Fb1ZXvqHhOv7POjGN8P82ed/eixyE
        // 8Yv4fE5l3dD2NoFVCWgjUCPFNU9dDfc3QjrVFEm+pM0SsgdYI2+EWn05BmGRhG5l
        // qXVgbtkce5FXbNOmaatj4c86/KGLd9t9a4D9tHL+AFpeG/qHYUBrsXXkcqVxAl9N
        // The+x3y5o+2OF3HMk/kIoUEzpaWJhZM4KK31FIJTLczJQH/1iq/0uGp8M2846ebO
        // 2thcg0vWJ/bRsr73uBhFVu/AwBUT4hcFw5ZE2yn87F2WbsO/2S+UJdqEI/hjCwXZ
        // MpZhHK+2g+HHZ1Rj0ZZnRKqYdQtl6sBlW9jXBdQAQQKBgQDwLHAvCAK0zn56aIDH
        // rPQRS6DInhiqRewB2BAiKyvtwO95BdEIkBWPrvHQjrGPdX1iALxrPtVHZ4A1JTTW
        // uaJfEtVa74AMtXQ4K5T330uiKmN8iCWUvdWobkzc3+1phRiOjUQoOJvP55rW0Nx/
        // qXYxDoO2xt8PuOKRzSVoFMwGRQKBgQCvy5S36syYgV3C5fI31ND25gq8J6PcO8v1
        // qrQEx/BFvR0pz2UQqWYQqbVgXrXPMxmObp75jTTB7mX3r3aBwguyyj7onHn99So4
        // ucR86IyMJvftXal9rRkLXcIOF/RDQtORexGGEV/pHZsEkJGoF+/ELmQ3vI9P7Doj
        // S7SJn18EHQKBgFET71sxLRi0jOXt2WuO62XbTg/rrw/sQtvsgKXIRTAh7bXBTOvD
        // GERFHsT1JuPzGby32uOpdn6AZjuKr+e9Cx9lUDk+mM+xgeZkcAmti+7WuHcVzvoD
        // i9ksQy5dOdxhXgThk5ywX4kz+oWCRK2kG448BCLzx+Ga3IGz62jTfHyxAoGANcYR
        // XNzu7quL0cVi/V8U+KFPoVSC1ChacHBiFqBOSBbcJ+Dv6lPXC7vN5oeyF4prA92k
        // tXNTMwtIl6ZNms2PpIYnDiJnUp6+5pE0oHzGGU0GQ86FvRLI+WgZpxP+vOBhNTwv
        // FRKVgwpyU8ghNgj0Z89FCqOcNvdspUVQGVTtzKUCgYEA5HmEZm1kNwjf0jrT1QdT
        // ObjsHHu9a6PD/TkyJ/HNZNtsQf5BzXI8au41NNaoADAXTtI3L9h9huR/WyMig46N
        // pSeZ7hlYhEm1bwVZtirn/rdjotKSOsdtIui7WixageQPTs7q+5kWd2lcHs1Jja7S
        // iik3yE+7hnyaVOXZqJtuv9c=
        // -----END PRIVATE KEY-----        
        // PEM;
	    // $cer =<<<PEM
        // -----BEGIN CERTIFICATE-----
        // MIIGCDCCA/CgAwIBAgIUMDAwMDEwMDAwMDA1MTExMTQ0NTUwDQYJKoZIhvcNAQEL
        // BQAwggGEMSAwHgYDVQQDDBdBVVRPUklEQUQgQ0VSVElGSUNBRE9SQTEuMCwGA1UE
        // CgwlU0VSVklDSU8gREUgQURNSU5JU1RSQUNJT04gVFJJQlVUQVJJQTEaMBgGA1UE
        // CwwRU0FULUlFUyBBdXRob3JpdHkxKjAoBgkqhkiG9w0BCQEWG2NvbnRhY3RvLnRl
        // Y25pY29Ac2F0LmdvYi5teDEmMCQGA1UECQwdQVYuIEhJREFMR08gNzcsIENPTC4g
        // R1VFUlJFUk8xDjAMBgNVBBEMBTA2MzAwMQswCQYDVQQGEwJNWDEZMBcGA1UECAwQ
        // Q0lVREFEIERFIE1FWElDTzETMBEGA1UEBwwKQ1VBVUhURU1PQzEVMBMGA1UELRMM
        // U0FUOTcwNzAxTk4zMVwwWgYJKoZIhvcNAQkCE01yZXNwb25zYWJsZTogQURNSU5J
        // U1RSQUNJT04gQ0VOVFJBTCBERSBTRVJWSUNJT1MgVFJJQlVUQVJJT1MgQUwgQ09O
        // VFJJQlVZRU5URTAeFw0yMjAxMjkxNDM3NDRaFw0yNjAxMjkxNDM3NDRaMIHWMSgw
        // JgYDVQQDEx9JTlRFR1JPUEFZIE1FWElDTyBTIEEgUCBJIERFIENWMSgwJgYDVQQp
        // Ex9JTlRFR1JPUEFZIE1FWElDTyBTIEEgUCBJIERFIENWMSgwJgYDVQQKEx9JTlRF
        // R1JPUEFZIE1FWElDTyBTIEEgUCBJIERFIENWMSUwIwYDVQQtExxJTUUyMDAyMTdN
        // QjIgLyBHQUNBODYwODE4OUU1MR4wHAYDVQQFExUgLyBHQUNBODYwODE4SE5MUkhO
        // MDAxDzANBgNVBAsTBk1BVFJJWjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
        // ggEBAKTtX3NYrScG9m7nRqWzfgJDFdr+RQw7oHOtg8hjazleaa2Gg3N7vPHek1BR
        // FiK8cfvZMm0pBXUQDimFyGTjJ/yItxMcWUCCFexLV7+JwRpRm9A9LePx/dgEKryr
        // f/chs+SMizv8JumFh+l86ke1xGYkZ0+7k5yzgYninZ/a16VqVnBQjQ9gJCKlg3qt
        // 4kUH4tGsJaQVZTkwtiQLN/278207rCN3zv5Ay6mADplZXm4WOC6DzPoMiLomqyPW
        // +RY4qhcGhkbU21ImLooTUtz5bcAhYLFkt9xoGGhdvinrQoWcwQgBgn7OTUC4S0xR
        // 57ur1WermoyUBjjkGiwrpbnQydECAwEAAaMdMBswDAYDVR0TAQH/BAIwADALBgNV
        // HQ8EBAMCBsAwDQYJKoZIhvcNAQELBQADggIBAIqI+6WU2/pwWa0tJOBSCtyB4lDY
        // doYU/7OKUgEZCNTtDcOx4JszRAa1Ia2LKZeBWnZHorQcZtcEpVrB7nVm6YtaD4iJ
        // NguTinjbo3xgorrDY8MSc2oYgXWh/fTKwfZ2kI5sz6emmJJiLzsxCO7dXckutUP8
        // yYkgcx/hp7rQCukKNhB+TckgbUO1JU4ILJruSkaOVWcSdVHnuYFOAOGyliGPdp9P
        // iDyJe2/KupiFq9pcd1Qz3aUow2SJy7mnIBCs32WIs1PC2y7CAA7QhJmLCoBIeYFy
        // EDyI93+Oqx+VV9kncq4cvnC7bSW9/eAVTecF8cJgpwiUVDjfdC9EMBbhjxo6Vycr
        // CEYEE4ZoT5nl0t336B4q2kQqynbrrBdiE+sN4XFVdlSzv8HoDrEGjGWEm63kh4o6
        // qpQP+GpLZTqMmH5jznLahanT/fwBOrLP0kpCZGeazshUvq09mo1I2lyHKCCY/Fal
        // bntdIlD7nP4jFtw8mf4FqLerZG7xgxrhkKH4QmPBVVoDkWOqFB8fxplM3lxGuX2W
        // vErrnbIG5gaKGzW4sQQukDqv5JQu+J5mk1H+nVwn58jomas074yfTJOuX/5n3uWV
        // 7EfNPGKvhI5FVFTy3z+1/fk8Z+F0ayB0xZy+12qZ/MzGtyHJFd1mmfMo7MLpLvwW
        // i1GNmVFX0k/tFXnY
        // -----END CERTIFICATE-----     
        // PEM;

        $cfdi= new ComprobanteV4();

        //MANDAR SELLOS MEDIANTE ARCHIVO
        $cfdi->addKeys($cer, $key);

        //MANDAR SELLOS MEDIANTE STRING
        //$cfdi->addKeysString($cer, $key);
        /* '00001000000505422144' */
        $cfdi->addGenerales('00001000000505422144', 100, 'MXN', 116, 'I', '01', 1, '64000', 'PUE', 'A','12',"","CONDICIONES","0.00",'4.0',"",$objDateTime->format('Y-m-d\TH:i:s'),null,"01");

        $cfdi->addEmisor('PAU1207301E5', 'PILOTO AUTOMATICO', '601');
        $cfdi->addReceptor('PAU1207301E5', 'G03', 'PILOTO AUTOMATICO',"64000",null,null,'601');

        //se agrega concepto al comprobante
        $concepto = $cfdi->addConcepto('52121500', 'CONSUMO DE ALIMENTOS', 1, 100, 'PZ', 'E48', 'VTA12345',0,'02');
        $concepto->addTraslado(100, '002', 'Tasa', '0.160000', 16);
        //$concepto->addTraslado(1000, '003', 'Tasa', '0.070000', 70);
        //$concepto->addRetencion(1000, '002', 'Tasa', '0.150000', 150);

        //se agrega concepto al comprobante
        //$concepto = $cfdi->addConcepto('01010101', 'descripcion 2', 1, 1000, 'TONELADA', 'F52', '0001',null,'02');
        //$concepto->addTraslado(1000, '002', 'Tasa', '0.160000', 160);
        //$concepto->addRetencion(1000, '002', 'Tasa', '0.100000', 100);

        //se agrega concepto al comprobante
        //$concepto = $cfdi->addConcepto('01010101', 'descripcion 3', 1, 1000, 'TONELADA', 'F52', '0001',null,'02');
        //$concepto->addTraslado(1000, '003', 'Tasa', '0.300000', 300);
        //$concepto->addRetencion(1000, '001', 'Tasa', '0.040000', 40);

        $cfdi->addImpuestosGlobales();

        $cfdi->addSellos();
        //$cfdi->validateSellos();

        $cfdi->validar();

        $cfdi->validateXSD();
        //AGREGAR XML ADDENDA (DESCOMENTAR DESDE LA LINEA 40 A HASTA LA LINEA 64 PARA AGREGAR LA ADDENDA)
        // $xmlAddenda=<<<XML
        // <Edit_Mensaje>
        //     <Mensaje>
        //         <Remitente>
        //             <Nombre>Nombre del remitente</Nombre>
        //             <Mail> Correo del remitente </Mail>
        //         </Remitente>
        //         <Destinatario>
        //             <Nombre>Nombre del destinatario</Nombre>
        //             <Mail>Correo del destinatario</Mail>
        //         </Destinatario>
        //         <Texto>
        //             <Asunto>
        //                     Este es mi documento con una estructura muy sencilla 
        //                     no contiene atributos ni entidades...
        //             </Asunto>
        //             <Parrafo>
        //                     Este es mi documento con una estructura muy sencilla 
        //                     no contiene atributos ni entidades...
        //             </Parrafo>
        //         </Texto>
        //     </Mensaje>
        // </Edit_Mensaje>
        // XML;
        // $cfdi->addAddenda($xmlAddenda);
        $cfdi->toXML();
	    $cfdi->toSaveXML();
        print_r($cfdi->getCadenaOriginal());

	    error_log(date("Y-m-d H:i:s") . " : CREATE CFDI INGRESO CON O SIN ADDENDA: ".print_r($cfdi, true)." \n", 3, "debug.log");
        
        error_log(date("Y-m-d H:i:s") . " : LOAD CFDI INGRESO CON O SIN ADDENDA: ".print_r($cfdi, true)." \n", 3, "debug.log");

	    print_r($cfdi);

    }catch (Exception $e) {
        echo 'Error al generar el CFDI: ', $e->getMessage(), "\n";
        error_log(date("Y-m-d H:i:s") . " : Error al generar el CFDI INGRESO CON O SIN ADDENDA: " . print_r($e->getMessage(), true) . "\n", 3, "debug.log");
    }
   
?>