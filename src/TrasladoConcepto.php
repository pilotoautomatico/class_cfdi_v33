<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;
Use cfdi\Data\Arrays;

use Exception;
use DOMDocument;

class TrasladoConcepto {
	//normales
	var $Base;
	var $Impuesto;
	var $TipoFactor;
	var $TasaOCuota;
	var $Importe;
	var $xml_base;
	var $Decimales;
	var $facturaGlobalV4;
	var $logger;

	function __construct($Base, $Impuesto, $TipoFactor, $TasaOCuota = null, $Importe = null, $Decimales = 2, $facturaGlobalV4=false) {
		$this->logger = new Logger(); //clase para escribir logs
		$this->Base = $Base;
		$this->Impuesto = $Impuesto;
		$this->TipoFactor = $TipoFactor;
		$this->TasaOCuota = $TasaOCuota!=null ? $this->addZeros($TasaOCuota, 6) : $TasaOCuota;
		$this->Importe = $Importe;
		if($TipoFactor=='Exento'){
			$this->TasaOCuota = null;
			$this->Importe = null;
		}
		$this->facturaGlobalV4 = $facturaGlobalV4;
		$this->Decimales = $Decimales;
	}

	function validar() {
		# valida campos requeridos de comprobantes
		$required = array(
			'Base',
			'Impuesto',
			'TipoFactor'
		);
		//$this->validateDecimals();
		$this->validateTax();

		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Traslado validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Traslado Campo Requerido: ' . $field);
			}
		}

		if($this->Base < 0.000001){ //valor minimo
			$this->logger->write('El Traslado Base ' . $this->Base . ' debe tener un valor minimo de 0.000001');
			throw new Exception('El Traslado Base ' . $this->Base . ' debe tener un valor minimo de 0.000001');
		}

		
		// if($this->TipoFactor!='Exento'){ //solo valida cuando no sea exento
		// 	$limite = $this->validateMaxMin();	
		// 	if ($this->Importe < $limite['minimo'] || $this->Importe > $limite['maximo']) {
		// 		$bccomp = bccomp($limite['minimo'], $this->Importe); //0 si los dos operandos son iguales, 1 si el left_operand es mayor que el right_operand, de lo contrario -1.
		// 		$bccomp_2 = bccomp($this->Importe, $limite['maximo']); //0 si los dos operandos son iguales, 1 si el left_operand es mayor que el right_operand, de lo contrario -1.
		// 		if($bccomp == 1 || $bccomp_2 == 1){
		// 			$this->logger->write("Traslado validar maximos y minimos(): el importe de " . $this->Importe . "esta fuera del rango permitido minimo :" . $limite["minimo"] . " maximo :  " . $limite["maximo"]);
		// 			throw new Exception('El Traslado con el importe ' . $this->Importe . ' esta fuera del limite permitido , minimo : ' . $limite["minimo"] . " maximo :" . $limite["maximo"]);
		// 		}
		// 	}
		// }
		
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$traslado = $this->xml_base->createElement("cfdi:Traslado");
		$this->xml_base->appendChild($traslado);

		# datos de tralado
		$traslado->SetAttribute('Base', $this->addZeros($this->Base, 6));
		$traslado->SetAttribute('Impuesto', $this->Impuesto);
		$traslado->SetAttribute('TipoFactor', $this->TipoFactor);
		if (!is_null($this->TasaOCuota)){
			$traslado->SetAttribute('TasaOCuota', $this->addZeros($this->TasaOCuota, 6));
		}
		if (!is_null($this->Importe)){
			if($this->facturaGlobalV4==false){
				$traslado->SetAttribute('Importe', $this->addZeros($this->Importe, 6));
			}else{
				$dec_qty = $this->Decimales;
				$dec_qty2 = strlen(substr(strrchr($this->Importe, "."), 1));
				$dec_qty = ($dec_qty2>$dec_qty ? $dec_qty2 : $dec_qty);
				$traslado->SetAttribute('Importe', $this->addZeros($this->Importe,$dec_qty));
			}
		}
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("cfdi:Traslado")->item(0);
		return $xml;
	}

	function getMin() {
		$decimalesNum = strlen(substr(strrchr($this->Base, "."), 1));
		$minimo = $this->truncateFloat(($this->Base - (pow(10, -$decimalesNum)) / 2) * ($this->TasaOCuota), $this->Decimales);
		return str_replace(',', '', $minimo);
	}

	function getMax() {
		$decimalesNum = strlen(substr(strrchr($this->Base, "."), 1));
		$value='1';
		for ($i=1;$i<=$this->Decimales;$i++){
			$value.='0';
		}
		$maximo = ($this->addZeros(($this->Base + (pow(10, -$decimalesNum)) / 2 - pow(10, -12)) * ($this->TasaOCuota), $this->Decimales))+(1/(float)$value);
		return $maximo;
	}

	private function truncateFloat($number, $digitos) {
		$raiz = 10;
		$multiplicador = pow($raiz, $digitos);
		$resultado = ((int) ($number * $multiplicador)) / $multiplicador;
		return number_format($resultado, $digitos);
	}

	private function validateMaxMin() {
		$minimo = $this->getMin();
		$maximo = $this->getMax();
		$array = ['minimo' => $minimo, 'maximo' => $maximo];
		return $array;
	}

	function validateTax() {
		$valorTasa = null;
		$arrayCatalog = new Arrays();

		if ($this->TipoFactor == "Cuota" && $this->Impuesto == "003") {
			if ((float) $this->TasaOCuota <= 0.0000 || (float) $this->TasaOCuota > 43.770000) {
				throw new Exception('El valor de la ' . $this->TipoFactor . 'que corresponde al impuesto 003 (ISR) : ' . $this->TasaOCuota . ' en la retencion No esta dentro del rango permitido 0.000000 a 43.770000 verfique sus datos');
			}
		}

		/*
		if($this->TipoFactor!='Exento'){ //solo valida cuando no sea exento
			$valorTasa = array_search((float) $this->TasaOCuota, $arrayCatalog->arrayTasa[$this->Impuesto][$this->TipoFactor]);
			if (!is_int($valorTasa)) {
				throw new Exception('El valor del campo TasaOCuota : ' . $this->TasaOCuota . ' del traslado no contiene un valor del catalogo de c_TasaOCuota especificado por el SAT.<br>'
				. 'Impuestos 001,002,003 valor introducido :' . $this->Impuesto . '<br>'
				. 'Factores Tasa,Cuota,Exento valor introducido :' . $this->TipoFactor . '<br>');
			}
		}
		*/
		
		if ($this->Impuesto == '001') {
			throw new Exception('El impuesto 001 que corresponde al ISR no debe declararse en un traslado');
		}
	}

	function validateDecimals() {
		$decimalesTotal = strlen(substr(strrchr($this->Importe, "."), 1));
		if ($decimalesTotal > $this->Decimales) {
			throw new Exception("El importe de " . $this->Importe .
			" en el traslado no coincide con el valor de los decimales especificado por la moneda ,valor de decimales: " . $this->Decimales);
		}
	}
	
	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return  sprintf('%0.'.$dec.'f',$cantidad);
	}
}
?>
