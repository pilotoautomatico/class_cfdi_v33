<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;
Use cfdi\Data\Arrays;

use Exception;
use DOMDocument;

// esta clase iva a ser ulitlizada para generar los impuestos globales del CFDI , ya que acutalmente los calcula en automatico,
// el nodo de impuestos va cuando se da un anticipo.

class TrasladoPago {
	//var $Base;
	var $Impuesto;
	var $TipoFactor;
	var $TasaOCuota;
	var $Importe; // el importe se calculara
	var $xml_base;
	var $Decimales;
	var $logger;

	function __construct($Impuesto, $TipoFactor, $TasaOCuota, $Importe, $Decimales) {
		$this->Impuesto = $Impuesto;
		$this->TipoFactor = $TipoFactor;
		$this->TasaOCuota = $TasaOCuota;
		$this->Importe = $Importe;
		$this->Decimales = $Decimales;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		$required = array(
			'Impuesto',
			'TipoFactor',
			"TasaOCuota",
			"Importe"
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Retencion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Retencion Campo Requerido: ' . $field);
			}
		}
		$this->validateDecimals();
		$this->validateTax();
	}

	// pendiente para ver como va estar el rollo
	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$traslado = $this->xml_base->createElement("pago10:Traslado");
		$this->xml_base->appendChild($traslado);

		# datos de tralado
		$traslado->SetAttribute('Impuesto', $this->Impuesto);
		$traslado->SetAttribute('TipoFactor', $this->TipoFactor);
		$traslado->SetAttribute('TasaOCuota', $this->TasaOCuota);
		$traslado->SetAttribute('Importe', $this->addZeros($this->Importe));
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("pago10:Traslado")->item(0);
		return $xml;
	}

	function addZeros($cantidad = null) {
		return sprintf('%0.' . $this->Decimales . 'f', $cantidad);
	}

	function validateDecimals() {
		$decimalesTotal = strlen(substr(strrchr($this->Importe, "."), 1));
		if ($decimalesTotal > $this->Decimales) {
			throw new Exception("El importe de " . $this->Importe .
			" en el traslado del pago no coincide con el valor de los decimales especificado por la moneda ,valor de decimales: " . $this->Decimales);
		}
	}

	function validateTax() {
		$valorTasa = null;
		$arrayCatalog = new Arrays();

		if ($this->TipoFactor == "Cuota" && $this->Impuesto == "003") {
			if ((float) $this->TasaOCuota <= 0.0000 || (float) $this->TasaOCuota > 43.770000) {
				throw new Exception('El valor de la ' . $this->TipoFactor . 'que corresponde al impuesto 003 (ISR) : ' . $this->TasaOCuota . ' en la retencion No esta dentro del rango permitido 0.000000 a 43.770000 verfique sus datos');
			}
		}

		if ($this->TipoFactor != 'Exento') { //solo valida cuando no sea exento
			$valorTasa = array_search((float) $this->TasaOCuota, $arrayCatalog->arrayTasa[$this->Impuesto][$this->TipoFactor]);
			if (!is_int($valorTasa)) {
				throw new Exception('El valor del campo TasaOCuota : ' . $this->TasaOCuota . ' del traslado no contiene un valor del catalogo de c_TasaOCuota especificado por el SAT.<br>'
				. 'Impuestos 001,002,003 valor introducido :' . $this->Impuesto . '<br>'
				. 'Factores Tasa,Cuota,Exento valor introducido :' . $this->TipoFactor . '<br>');
			}
		}

		if ($this->Impuesto == '001') {
			throw new Exception('El impuesto 001 que corresponde al ISR no debe declararse en un traslado');
		}
	}
}
?>