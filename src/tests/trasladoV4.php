<?php
    require '../../vendor/autoload.php';
    Use cfdi\ComprobanteV4;
    try{
        $key = getcwd() . "/keys/00001000000505422144.key.pem";
	    $cer = getcwd() . "/keys/00001000000505422144.cer.pem";
        $cfdi= new ComprobanteV4();
        $cfdi->addKeys($cer, $key);
        /* '00001000000401148681' */
        $cfdi->addGenerales('00001000000505422144', 0, 'AMD', 0, 'T', null, 1, '35045', null, '12',null,"",null,null,'4.0',"",date("Y-m-d\TH:i:s"),null,"01");
        $cfdi->addEmisor('PAU1207301E5', 'PILOTO AUTOMATICO SA DE CV', '601');
        $cfdi->addReceptor('RPE841207E99', 'G01', 'RESTAURANTE PERISUR SA DE CV',"26015",null,null,'601');

        //se agrega concepto al comprobante
        $concepto = $cfdi->addConcepto('50211503', 'Cigarros', 1, 0, "Pieza", 'H87', null,0,'01');
        // $concepto->addTraslado(1, '002', 'Tasa', '0.160000', 1);

        // $cfdi->addImpuestosGlobales();

        $cfdi->addSellos();
        //$cfdi->validateSellos();

        $cfdi->validar();

        $cfdi->validateXSD();
        $cfdi->toXML();
	    $cfdi->toSaveXML();

	    error_log(date("Y-m-d H:i:s") . " : CREATE CFDI TRASLADO: ".print_r($cfdi, true)." \n", 3, "debug.log");
        
        error_log(date("Y-m-d H:i:s") . " : LOAD CFDI TRASLADO: ".print_r($cfdi, true)." \n", 3, "debug.log");

	    print_r($cfdi);

    }catch (Exception $e) {
        echo 'Error al generar el CFDI: ', $e->getMessage(), "\n";
        error_log(date("Y-m-d H:i:s") . " : Error al generar el CFDI TRASLADO: " . print_r($e->getMessage(), true) . "\n", 3, "debug.log");
    }
   
?>