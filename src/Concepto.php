<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

Use Exception;
use DOMDocument;

class Concepto {
	//normales
	var $ClaveProdServ;
	var $Descripcion;
	var $Cantidad;
	var $ValorUnitario;
	var $Importe;
	var $Unidad;
	var $NoIdentificacion;
	var $ClaveUnidad;
	var $Descuento;
	//objetos
	var $xml_base;
	var $Traslados = array();
	var $Retenciones = array();
	var $InformacionAduanera = [];
	var $CuentaPredial;
	var $ComplementoConcepto;
	var $Parte;
	var $Decimales;
	var $logger;
	var $TipoDeComprobante;

	function __construct($ClaveProdServ, $Descripcion, $Cantidad, $ValorUnitario, $Unidad = null, $ClaveUnidad, $NoIdentificacion = null, $Descuento = null, $Decimales = 2, $TipoDeComprobante=null) {
		$this->Decimales = $Decimales;
		$this->ClaveProdServ = $ClaveProdServ;
		$this->Descripcion = $Descripcion;
		$this->Cantidad = $Cantidad;
		$this->ValorUnitario = $ValorUnitario;
		$this->Importe = round(($Cantidad * $ValorUnitario), $this->Decimales);
		$this->Unidad = $Unidad;
		$this->NoIdentificacion = $NoIdentificacion;
		$this->Descuento = $Descuento;
		$this->ClaveUnidad = $ClaveUnidad;
		$this->Traslados = array();
		$this->Retenciones = array();
		$this->ComplementoConcepto = null;
		$this->logger = new Logger(); //clase para escribir logs
		$this->TipoDeComprobante = $TipoDeComprobante;
	}

	function validar() {
		# valida campos requeridos de concepto
		$required = array(
			'ClaveProdServ',
			'Descripcion',
			'Cantidad',
			'ValorUnitario',
			'ClaveUnidad'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Concepto validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Concepto Campo Requerido: ' . $field);
			}
		}

		#valida longitud de descripcion
		// if(strlen($this->Descripcion) < 1 || strlen($this->Descripcion) > 1000){
		// se retiró la validación de 1000 carácteres para poder cargar comprobantes que llegan
		// con esta cantidad de carácteres
		if(strlen($this->Descripcion) < 1){
			$this->logger->write('Concepto validar Descripcion: Debe contener al menos 1 carácter.');
			throw new Exception('El valor de Concepto Descripcion es menor a 1 carácter: len='.strlen($this->Descripcion));
		}
		
		#valida decimales
		$this->validateDecimals();

		#valida maximos y minimos de concepto
		$limites = $this->validateMaxMin();
		if ($this->Importe < $limites['minimo'] || $this->Importe > $limites['maximo']) {
			$this->logger->write("Concepto validar maximos y minimos(): el importe de " . $this->Importe . "esta fuera del rango permitido minimo :" . $limites["minimo"] . " maximo: " . $limites["maximo"]);
			throw new Exception('El Concepto con el importe' . $this->Importe . 'esta fuera del limite permitido , minimo :' . $limite["minimo"] . " maximo :" . $limite["maximo"]);
		}
		#valida traslados y retenciones
		foreach ($this->Traslados as $traslado) {
			$traslado->validar();
		}
		foreach ($this->Retenciones as $retencion) {
			$retencion->validar();
		}
		
		foreach($this->InformacionAduanera as $InformacionAduanera)
        {
			$InformacionAduanera->validar();
		}

		#valida datos de catalogos de concepto
		
		#valida complementoConcepto
		if($this->ComplementoConcepto)
			$this->ComplementoConcepto->validar();
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$concepto = $this->xml_base->createElement("cfdi:Concepto");
		$this->xml_base->appendChild($concepto);

		# datos de concepto
		$concepto->SetAttribute('ClaveProdServ', $this->ClaveProdServ);
		if ($this->NoIdentificacion)
			$concepto->SetAttribute('NoIdentificacion', $this->NoIdentificacion);
		$concepto->SetAttribute('Cantidad', $this->Cantidad);
		$concepto->SetAttribute('ClaveUnidad', $this->ClaveUnidad);
		if ($this->Unidad)
			$concepto->SetAttribute('Unidad', $this->Unidad);
		$concepto->SetAttribute('Descripcion', $this->Descripcion);

		$ValorUnitario = $this->ValorUnitario;
		if($this->TipoDeComprobante != 'P' && $this->TipoDeComprobante != 'N'){
			$ValorUnitario = $this->addZeros($this->ValorUnitario, 6);
		}
		$concepto->SetAttribute('ValorUnitario', $ValorUnitario);
		$concepto->SetAttribute('Importe', $this->addZeros($this->Importe));
		if ($this->Descuento)
			$concepto->SetAttribute('Descuento', $this->addZeros($this->Descuento));

		# impuestos
		if (!empty($this->Traslados) || !empty($this->Retenciones)) {
			$impuestos = $this->xml_base->createElement("cfdi:Impuestos");
			$concepto->appendChild($impuestos);

			# traslados
			if (!empty($this->Traslados)) {
				$traslados = $this->xml_base->createElement("cfdi:Traslados");
				$impuestos->appendChild($traslados);
				foreach ($this->Traslados as $key => $traslado) {
					$traslado->toXML();
					$traslado_xml = $this->xml_base->importNode($traslado->importXML(), true);
					$traslados->appendChild($traslado_xml);
				}
			}

			# retenciones
			if (!empty($this->Retenciones)) {
				$retenciones = $this->xml_base->createElement("cfdi:Retenciones");
				$impuestos->appendChild($retenciones);
				foreach ($this->Retenciones as $key => $retencion) {
					$retencion->toXML();
					$retencion_xml = $this->xml_base->importNode($retencion->importXML(), true);
					$retenciones->appendChild($retencion_xml);
				}
			}
		}
		
		# informacion aduanera
		if (!empty($this->InformacionAduanera)) 
		{
			foreach($this->InformacionAduanera as $InformacionAduanera)
			{
                $InformacionAduanera->toXML();
                $nodoInformacionAduanera = $this->xml_base->importNode($InformacionAduanera->importXML(), true);
                $concepto->appendChild($nodoInformacionAduanera);
			}
		}
		
		# cuenta predial
		if (!empty($this->CuentaPredial)) {
			$cuentaPredial = $this->xml_base->createElement("cfdi:CuentaPredial");
			$concepto->appendChild($cuentaPredial);
			$cuentaPredial->SetAttribute('Numero', $this->CuentaPredial);
		}

		# Parte
		# complemento concepto
		if(!empty($this->ComplementoConcepto)){
			$complementoConcepto = $this->xml_base->createElement("cfdi:ComplementoConcepto");
			$concepto->appendChild($complementoConcepto);
			$this->ComplementoConcepto->toXML();
			$complementoConcepto_xml = $this->xml_base->importNode($this->ComplementoConcepto->importXML(), true);
			$complementoConcepto->appendChild($complementoConcepto_xml);
		}
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("cfdi:Concepto")->item(0);
		return $xml;
	}

	function addCuentaPredial($cuentaPredial){
		$this->CuentaPredial = $cuentaPredial;
	}

	function addInformacionAduanera($NumeroPedimento)
	{
		$InformacionAduanera = new ConceptoInformacionAduanera(
            $NumeroPedimento
        );
		
		$this->InformacionAduanera[] = $InformacionAduanera;
		return $InformacionAduanera;
	}

	function addTraslado($Base, $Impuesto, $TipoFactor, $TasaOCuota = null, $Importe = null) {
		//validar si ya esta agregar un traslado igual ya no agregar otro
		// foreach($this->Traslados as $traslado){
		// 	if($traslado->Base == $Base && $traslado->Impuesto==$Impuesto && $traslado->TipoFactor==$TipoFactor
		// 		&& $traslado->TasaOCuota==$TasaOCuota && $traslado->Importe==$Importe){
		// 		return $traslado;
		// 	}
		// }
		
		$traslado = new TrasladoConcepto(
				$Base, $Impuesto, $TipoFactor, $TasaOCuota, $Importe, $this->Decimales
		);
		$this->Traslados[] = $traslado;
		return $traslado;
	}

	function addRetencion($Base, $Impuesto, $TipoFactor, $TasaOCuota = null, $Importe = null) {
		$retencion = new RetencionConcepto(
				$Base, $Impuesto, $TipoFactor, $TasaOCuota, $Importe, $this->Decimales
		);
		$this->Retenciones[] = $retencion;
		return $retencion;
	}

	function addComplementoConceptoIedu($nombreAlumno, $CURP, $nivelEducativo, $autRVOE, $rfcPago=null) {
		$comp = new ComplementoConceptoIedu(
				$nombreAlumno, $CURP, $nivelEducativo, $autRVOE, $rfcPago
		);
		$this->ComplementoConcepto = $comp;
		return $comp;
	}

	function addComplementoConceptoTerceros($rfc, $nombre=null) {
		$comp = new ComplementoConceptoTerceros(
			$rfc, $nombre
		);
		$this->ComplementoConcepto = $comp;
		return $comp;
	}

	function getMin() {
		$decimalesNum = strlen(substr(strrchr(sprintf('%0.' . $this->Decimales . 'f', $this->ValorUnitario), "."), 1));
		$minimo = $this->truncateFloat(($this->Cantidad - (pow(10, -$decimalesNum)) / 2) * ($this->ValorUnitario - (pow(10, -$decimalesNum)) / 2), $this->Decimales);
		return str_replace(',', '', $minimo);
	}

	function getMax() {
		$decimalesNum = strlen(substr(strrchr(sprintf('%0.' . $this->Decimales . 'f', $this->ValorUnitario), "."), 1));
		$maximo = round(($this->Cantidad + (pow(10, -$decimalesNum)) / 2 - pow(10, -12)) * ($this->ValorUnitario + (pow(10, -$decimalesNum)) / 2 - pow(10, -12)), $this->Decimales);
		return $maximo;
	}

	private function truncateFloat($number, $digitos) {
		$raiz = 10;
		$multiplicador = pow($raiz, $digitos);
		$resultado = ((int) ($number * $multiplicador)) / $multiplicador;
		return number_format($resultado, $digitos);
	}

	private function validateMaxMin() {
		$minimo = $this->getMin();
		$maximo = $this->getMax();
		$array = ['minimo' => $minimo, 'maximo' => $maximo];
		return $array;
	}

	function validateDecimals() {
		$decimalesTotal = strlen(substr(strrchr($this->Importe, "."), 1));
		$decimalesValorUnitario = strlen(substr(strrchr($this->ValorUnitario, "."), 1));

		// solamente si es mayor al numero de decimales de los especificados en la moneda te retorna una excepcion
		/*if (!empty($this->Descuento)) {
			$decimalesDescuento = strlen(substr(strrchr($this->Descuento, "."), 1));
			if ($decimalesDescuento > $this->Decimales) {
				throw new Exception("El descuento de " . $this->Descuento .
				" en el concepto es mayor que el valor de los decimales especificado por la moneda , valor de decimales: " . $this->Decimales);
			}
		}*/

		if ($decimalesTotal > $this->Decimales) {
			throw new Exception("El importe de " . $this->Importe .
			" en el concepto es mayor que el valor de los decimales especificado por la moneda , valor de decimales: " . $this->Decimales);
		}
		/*2017-11-22: se comenta validar decimales de valorUnitario, el PAC si pasa mas decimales de la moneda
		 * if ($decimalesValorUnitario > $this->Decimales) {
			throw new Exception("El valor unitario de " . $this->ValorUnitario .
			" en el concepto no coincide con el valor de los decimales especificado por la moneda ,valor de decimales: " . $this->Decimales);
		}*/
	}
	
	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return  sprintf('%0.'.$dec.'f',$cantidad);
	}
}



//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de Concepto.InformacionAduanera

class ConceptoInformacionAduanera
{
	// Obligatorios
	var $Cantidad;

	var $xml_base;
	var $logger;

	function __construct($NumeroPedimento)
    {
		$this->xml_base = null;
        $this->NumeroPedimento = $NumeroPedimento;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['NumeroPedimento'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Comprobante.Concepto.InformacionAduanera validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Comprobante.Concepto.InformacionAduanera Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoInformacionAduanera = $this->xml_base->createElement("cfdi:InformacionAduanera");

        $nodoInformacionAduanera->setAttribute('NumeroPedimento',  $this->NumeroPedimento);

		$this->xml_base->appendChild($nodoInformacionAduanera);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cfdi:InformacionAduanera")->item(0);
		return $xml;
	}
}

?>
