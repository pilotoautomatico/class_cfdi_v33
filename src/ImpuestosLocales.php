<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;

Use Exception;
use DOMDocument;

class ImpuestosLocales {
	//normales
	var $version;
	var $TotaldeRetenciones;
	var $TotaldeTraslados;
	//objetos
	var $Traslados = array();
	var $Retenciones = array();
	
	var $isTotRet = false;
	var $isTotTras = false;
	var $Decimales;
	var $logger;
	
	function __construct($TotaldeRetenciones = 0, $TotaldeTraslados = 0, $version = null, $Decimales = 2) {
		$this->version = $version ? $version : '1.0';
		$this->TotaldeRetenciones = $TotaldeRetenciones;
		$this->TotaldeTraslados = $TotaldeTraslados;
		$this->Decimales = $Decimales;
		$this->Traslados = array();
		$this->Retenciones = array();
		
		if($TotaldeRetenciones != 0)
			$this->isTotRet = true;
		if($TotaldeTraslados != 0)
			$this->isTotTras = true;
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		
		#valida traslados
		$totalTraslados = 0;
		foreach ($this->Traslados as $traslado) {
			$totalTraslados += $traslado->Importe;
			$traslado->validar();
		}			
		// if(!$this->TotaldeTraslados){ #sino se asignan valores en el constructor se asignan los calculados
		// 	$this->TotaldeTraslados = $totalTraslados;
		// }
		// if($totalTraslados != $this->TotaldeTraslados){
		// 	$this->logger->write("ImpuestosLocales validar(): El Total de TrasladosLocales [".$totalTraslados."] no es correcto con lo reportado [".$this->TotaldeTraslados."]");
		// 	throw new Exception("ImpuestosLocales validar(): El Total de TrasladosLocales [".$totalTraslados."] no es correcto con lo reportado [".$this->TotaldeTraslados."]");			
		// }
		
		#valida retenciones
		$totalRetenciones = 0;
		foreach ($this->Retenciones as $retencion) {
			$totalRetenciones += $retencion->Importe;
			$retencion->validar();
		}			
		// if(!$this->TotaldeRetenciones){ #sino se asignan valores en el constructor se asignan los calculados
		// 	$this->TotaldeRetenciones = $totalRetenciones;
		// }
		// if($totalRetenciones != $this->TotaldeRetenciones){
		// 	$this->logger->write("ImpuestosLocales validar(): El Total de RetencionesLocales [".$totalRetenciones."] no es correcto con lo reportado [".$this->TotaldeRetenciones."]");
		// 	throw new Exception("ImpuestosLocales validar(): El Total de RetencionesLocales [".$totalRetenciones."] no es correcto con lo reportado [".$this->TotaldeRetenciones."]");			
		// }
		
		# valida decimales permitidos		
		$decimales = array(
			'TotaldeRetenciones',
			'TotaldeTraslados'
		);
		/*foreach ($decimales as $field) {
			$this->validateDecimals($field);	
		}*/
		
		# valida campos requeridos de concepto
		$required = array(
			'version',
			'TotaldeRetenciones',
			'TotaldeTraslados'
		);
		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("ImpuestosLocales validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('ImpuestosLocales Campo Requerido: ' . $field);
			}
		}
	}

	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$impLocales = $this->xml_base->createElement("implocal:ImpuestosLocales");
		$this->xml_base->appendChild($impLocales);

		# retenciones
		if (!empty($this->Retenciones)) {
			foreach ($this->Retenciones as $key => $retencion) {
				$retencion->toXML();
				$retencion_xml = $this->xml_base->importNode($retencion->importXML(), true);
				$impLocales->appendChild($retencion_xml);
			}
		}

		# traslados
		if (!empty($this->Traslados)) {
			foreach ($this->Traslados as $key => $traslado) {
				$traslado->toXML();
				$traslado_xml = $this->xml_base->importNode($traslado->importXML(), true);
				$impLocales->appendChild($traslado_xml);
			}
		}
		
		#atributos de impuestos locales
		$impLocales->SetAttribute('version', $this->version);
		$impLocales->SetAttribute('TotaldeRetenciones', $this->addZeros($this->TotaldeRetenciones));
		$impLocales->SetAttribute('TotaldeTraslados', $this->addZeros($this->TotaldeTraslados));		

	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("implocal:ImpuestosLocales")->item(0);
		return $xml;
	}

	function addTrasladoLocal($ImpLocTrasladado, $TasadeTraslado, $Importe) {
		$traslado = new TrasladoLocal(
				$ImpLocTrasladado, $TasadeTraslado, $Importe
		);
		$this->Traslados[] = $traslado;
		if(!$this->isTotTras)
			$this->TotaldeTraslados += $Importe;
		return $traslado;
	}

	function addRetencionLocal($ImpLocRetenido, $TasadeRetencion, $Importe) {
		$retencion = new RetencionLocal(
				$ImpLocRetenido, $TasadeRetencion, $Importe
		);
		$this->Retenciones[] = $retencion;
		if(!$this->isTotRet)
			$this->TotaldeRetenciones += $Importe;
		return $retencion;
	}

 	function validateDecimals($field) {
		$decimales = strlen(substr(strrchr($this->$field, "."), 1));
		if ($decimales > $this->Decimales) {
			throw new Exception("El valor de $field " . $this->$field . " en Impuestos Locales excede los decimales permitidos en impuestos locales: " . $this->Decimales);
		}
	}

	function addZeros($cantidad = null){
		return  sprintf('%0.'.$this->Decimales.'f',$cantidad);
	}
}
?>