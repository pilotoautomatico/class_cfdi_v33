<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

use Exception;
use DOMDocument;

class TrasladoGlobal{
	var $Impuesto;
	var $TipoFactor;
	var $TasaOCuota;
	var $Importe;
	var $xml_base;
	var $Decimales;

	function __construct($Impuesto, $TipoFactor ,$TasaOCuota, $Importe, $Decimales = 2) {
		$this->Decimales = $Decimales;
		$this->Impuesto = $Impuesto;
		$this->TipoFactor = $TipoFactor;
		$this->TasaOCuota = $TasaOCuota;
		$this->Importe = $Importe;
	}

	function validar() {
	}

	// pendiente para ver como va estar el rollo
	function toXML() {
		$this->xml_base = new DOMdocument("1.0", "UTF-8");
		$traslado = $this->xml_base->createElement("cfdi:Traslado");
		$this->xml_base->appendChild($traslado);

		# datos de tralado
		$traslado->SetAttribute('Impuesto', $this->Impuesto);
		$traslado->SetAttribute('TipoFactor', $this->TipoFactor);
		$traslado->SetAttribute('TasaOCuota', $this->addZeros($this->TasaOCuota, 6));
		$traslado->SetAttribute('Importe', $this->addZeros( round($this->Importe, $this->Decimales) ));
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("cfdi:Traslado")->item(0);
		return $xml;
	}
	
	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return  sprintf('%0.'.$dec.'f',$cantidad);
	}
}
?>