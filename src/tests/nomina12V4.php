<?php
    require '../../vendor/autoload.php';
    Use cfdi\ComprobanteV4;
    use DateTime;
    use DateTimeZone;
    try{
        $objDateTime = new DateTime("now", new DateTimeZone('America/Mexico_City'));
        $key = getcwd() . "/keys/00001000000505422144.key.pem";
	    $cer = getcwd() . "/keys/00001000000505422144.cer.pem";
        $cfdi= new ComprobanteV4();
        $cfdi->addKeys($cer, $key);


        //----se agrega datos generales, datos del emisor y receptor ejemplo de nomina normal, nomina con incapacidades, nomina sin deducciones
        $cfdi->addGenerales('00001000000505422144', 5000, 'MXN', 4700, 'N', '99', 1, '64000', 'PPD', 'A','12',"","CONDICIONES",300,'4.0',"",$objDateTime->format('Y-m-d\TH:i:s'),null,"01");

        $cfdi->addEmisor('PAU1207301E5', 'PILOTO AUTOMATICO', '601');
        $cfdi->addReceptor('PAU1207301E5', 'CN01', 'PILOTO AUTOMATICO',"64000",null,null,'601');

        //se agrega concepto al comprobante
        $concepto = $cfdi->addConcepto("84111505", "Pago de nómina",1,5000,null,"ACT",null,300,'01');

        //se agrega complemento nomina con la nueva clase
        $nomina=$cfdi->addNominaData("O","2022-02-28","2022-02-15","2022-02-28",15,5000,300,null);
        $nomina->addEmisor(null,"Y3753002108",null);
        $nomina->addReceptor("ROHR970702HTSJRD01","33169781326","2019-10-01","P126W","01","No","01","02","06","MONTERREY","PROGRAMADOR","4","04",null,"000000000000000000","490.22","146.47","NLE");
        
        //PERCEPCIONES
        $percepciones=$nomina->addPercepciones(5000,null,null,2808.8,2191.2);
        $percepciones->addPercepcion("001","00500","Sueldos, Salarios Rayas y Jornales",2808.8,2191.2);

        //DEDUCCIONES -> COMENTAR CUANDO SEA UN EJEMPLO SIN DEDUCCIONES
        $deducciones=$nomina->addDeducciones(200,100);
        $deducciones->addDeduccion("001","00301","Seguro Social",200);
        $deducciones->addDeduccion("002","00302","ISR",100);

        //INCAPACIDADES -> DESCOMENTAR CUANDO SEA UN EJEMPLO CON INCAPACIDADES
        // $incapacidades=$nomina->addIncapacidades();
        // $incapacidades->addIncapacidad(1,"01",100);

        // //----se agrega datos generales, datos del emisor y receptor ejemplo de nomina con horas extras
        // $cfdi->addGenerales('00001000000505422144', 5100, 'MXN', 4800, 'N', '99', 1, '64000', 'PPD', 'A','12',"","CONDICIONES",300,'4.0',"",$objDateTime->format('Y-m-d\TH:i:s'),null,"01");

        // $cfdi->addEmisor('PAU1207301E5', 'PILOTO AUTOMATICO', '601');
        // $cfdi->addReceptor('PAU1207301E5', 'CN01', 'PILOTO AUTOMATICO',"64000",null,null,'601');

        // //se agrega concepto al comprobante
        // $concepto = $cfdi->addConcepto("84111505", "Pago de nómina",1,5100,null,"ACT",null,300,'01');

        // //se agrega complemento nomina con la nueva clase
        // $nomina=$cfdi->addNominaData("O","2022-02-28","2022-02-15","2022-02-28",15,5100,300,null);
        // $nomina->addEmisor(null,"Y3753002108",null);
        // $nomina->addReceptor("ROHR970702HTSJRD01","33169781326","2019-10-01","P126W","01","No","01","02","06","MONTERREY","PROGRAMADOR","4","04",null,"000000000000000000","490.22","146.47","NLE");
        
        // //PERCEPCIONES
        // $percepciones=$nomina->addPercepciones(5100,null,null,2858.8,2141.2);
        // $percepciones->addPercepcion("001","00500","Sueldos, Salarios Rayas y Jornales",2808.8,2191.2);
        // $percepcion=$percepciones->addPercepcion("019","00100","Horas Extra",50,50);
        // $percepcion->addHorasExtra(1,"01",2,100);

        // //DEDUCCIONES
        // $deducciones=$nomina->addDeducciones(200,100);
        // $deducciones->addDeduccion("001","00301","Seguro Social",200);
        // $deducciones->addDeduccion("002","00302","ISR",100);

        // //INCAPACIDADES
        // $incapacidades=$nomina->addIncapacidades();
        // $incapacidades->addIncapacidad(1,"01",100);

        // //----se agrega datos generales, datos del emisor y receptor ejemplo de nomina extraordinaria
        // $cfdi->addGenerales('00001000000505422144', 10000, 'MXN', 10000, 'N', '99', 1, '64000', 'PPD', 'A','12',"","CONDICIONES",0,'4.0',"",$objDateTime->format('Y-m-d\TH:i:s'),null,"01");

        // $cfdi->addEmisor('PAU1207301E5', 'PILOTO AUTOMATICO', '601');
        // $cfdi->addReceptor('PAU1207301E5', 'CN01', 'PILOTO AUTOMATICO',"64000",null,null,'601');

        // //se agrega concepto al comprobante
        // $concepto = $cfdi->addConcepto("84111505", "Pago de nómina",1,10000,null,"ACT",null,0,'01');

        // //se agrega complemento nomina con la nueva clase
        // $nomina=$cfdi->addNominaData("E","2022-02-28","2022-02-28","2022-02-28",30,10000);
        // $nomina->addEmisor(null,"Y3753002108",null);
        // $nomina->addReceptor("ROHR970702HTSJRD01","33169781326","2019-10-01","P126W","01","No","01","02","06","MONTERREY","PROGRAMADOR","4","04",null,"000000000000000000",null,"146.47","NLE");
        
        // //PERCEPCIONES
        // //COMENTAR CUANDO SEA EJEMPLO DE SEPARACION INDEMNIZACION Y JUBILACION PENSION RETIRO
        // $percepciones=$nomina->addPercepciones(10000,null,null,0,10000); 
        
        // //DESCOMENTAR CUANDO SEA EJEMPLO DE SEPARACION INDEMNIZACION
        // // $percepciones=$nomina->addPercepciones(null,10500,null,0,10500);

        // //DESCOMENTAR CUANDO SEA EJEMPLO DE JUBILACION PENSION RETIRO
        // // $percepciones=$nomina->addPercepciones(null,null,10000,0,10000); 
        
        // //COMENTAR CUANDO SEA EJEMPLO DE SEPARACION INDEMNIZACION Y JUBILACION PENSION RETIRO
        // $percepciones->addPercepcion("002","00500","Gratificación Anual (Aguinaldo)",0,10000);

        // //DESCOMENTAR CUANDO SEA EJEMPLO DE SEPARACION INDEMNIZACION
        // // $percepcion=$percepciones->addPercepcion("023","00500","Pagos por separación",0,10000);
        // // $percepcion=$percepciones->addPercepcion("025","00100","Indemnizaciones",0,500);
        // // $percepciones->addSeparacionIndemnizacion(10500,1,10000,10000,0);

        // //DESCOMENTAR CUANDO SEA EJEMPLO DE JUBILACION PENSION RETIRO CON TOTALUNAEXHIBICION Y SIN MONTO DIARIO Y TOTAL PARCIALIDAD
        // // $percepciones->addPercepcion("039","00500","Jubilaciones, pensiones o haberes de retiro",0,10000);
        // // $percepciones->addJubilacionPensionRetiro(10000,null,null,10000,0);

        // //DESCOMENTAR CUANDO SEA EJEMPLO DE JUBILACION PENSION RETIRO SIN TOTALUNAEXHIBICION Y CON MONTO DIARIO Y TOTAL PARCIALIDAD
        // // $percepciones->addPercepcion("044","00500","Jubilaciones, pensiones o haberes de retiro",0,10000);
        // // $percepciones->addJubilacionPensionRetiro(null,3000,100,10000,0);

        $cfdi->addSellos();

        $cfdi->validar();

        $cfdi->validateXSD();
        $cfdi->toXML();
	    $cfdi->toSaveXML();
        print_r($cfdi->getCadenaOriginal());

	    error_log(date("Y-m-d H:i:s") . " : CREATE CFDI INGRESO CON O SIN ADDENDA: ".print_r($cfdi, true)." \n", 3, "debug.log");
        
        error_log(date("Y-m-d H:i:s") . " : LOAD CFDI INGRESO CON O SIN ADDENDA: ".print_r($cfdi, true)." \n", 3, "debug.log");

	    print_r($cfdi);

    }catch (Exception $e) {
        echo 'Error al generar el CFDI: ', $e->getMessage(), "\n";
        error_log(date("Y-m-d H:i:s") . " : Error al generar el CFDI INGRESO CON O SIN ADDENDA: " . print_r($e->getMessage(), true) . "\n", 3, "debug.log");
    }
   
?>