<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv40 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ERROR);

Use cfdi\Logger;
Use cfdi\Data\Arrays;
Use cfdi\RetencionPago;
Use cfdi\TrasladoPago;

use Exception;
use DOMDocument;

// clase que crea el nodo de pagosv2
class PagosV2 {
	//public $xmlns = "http://www.sat.gob.mx/Pagos";
	/*
	 * Descargar el xsd
	 * añadir la validacion del xsd al metodo actual ?
	 */
	//public $xsi = "http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos20.xsd";

	// Obligatorios
	public $Version = "2.0";
	var $pagos = array();
	var $totales = [];
	var $xml_base;
	var $logger;

	function __construct(){
		$this->xml_base=null;
		$this->pagos = array();
		$this->totales= array();
		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() {
		foreach($this->pagos as $pago){
			$pago->validar();
		}

		foreach($this->totales as $totales)
        {
			$totales->validar();
		}
	}

	function toXML(){
		$this->xml_base = new DOMDocument();
		$nodopagos = $this->xml_base->createElement("pago20:Pagos");
		$nodopagos->setAttribute('Version', "2.0");
		$this->xml_base->appendChild($nodopagos);

		foreach($this->totales as $totales)
		{
			$totales->toXML();
			$nodoTotales = $this->xml_base->importNode($totales->importXML(), true);
			$nodopagos->appendChild($nodoTotales);
		}

		foreach ($this->pagos as $key => $pago) {
			$pago->toXML();
			$domPago = $this->xml_base->importNode($pago->importXML(), true);
			$nodopagos->appendChild($domPago);
		}

		return $nodopagos;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("pago20:Pagos")->item(0);
		return $xml;
	}

	function addPago($FechaPago, $FormaDePagoP, $MonedaP, $Monto
	, $TipoCambioP = null, $NumOperacion = null, $RfcEmisorCtaOrd = null
	, $NomBancoOrdExt = null, $CtaOrdenante = null, $RfcEmisorCtaBen = null
	, $CtaBeneficiario = null, $TipoCadPago = null, $CertPago = null, $CadPago = null, $SelloPago = null) {
		$pago = new PagoV2($FechaPago, $FormaDePagoP, $MonedaP, $Monto, $TipoCambioP, $NumOperacion, $RfcEmisorCtaOrd, $NomBancoOrdExt, $CtaOrdenante, $RfcEmisorCtaBen, $CtaBeneficiario, $TipoCadPago, $CertPago, $CadPago, $SelloPago);
		$this->pagos[] = $pago;
		$pago->validar();
		return $pago;
	}

	function addTotales($TotalRetencionesIVA=null, $TotalRetencionesISR=null, $TotalRetencionesIEPS=null, $TotalTrasladosBaseIVA16=null, 
    $TotalTrasladosImpuestoIVA16=null, $TotalTrasladosBaseIVA8=null, $TotalTrasladosImpuestoIVA8=null, $TotalTrasladosBaseIVA0=null,
	$TotalTrasladosImpuestoIVA0=null,$TotalTrasladosBaseIVAExento=null,$MontoTotalPagos) {
		$totales = new Totales(
			$TotalRetencionesIVA, $TotalRetencionesISR, $TotalRetencionesIEPS, $TotalTrasladosBaseIVA16, 
    		$TotalTrasladosImpuestoIVA16, $TotalTrasladosBaseIVA8, $TotalTrasladosImpuestoIVA8, $TotalTrasladosBaseIVA0, 
			$TotalTrasladosImpuestoIVA0, $TotalTrasladosBaseIVAExento,$MontoTotalPagos
		);
		$this->totales[] = $totales;
		$totales->validar();
		return $totales;
	}

}


//Clase Nodo Pago
class PagoV2 {
	//Requeridos
	public $FechaPago;
	public $FormaDePagoP;
	public $MonedaP;
	public $Monto;

	//Opcionales
	public $TipoCambioP;
	public $NumOperacion;
	public $RfcEmisorCtaOrd;
	public $NomBancoOrdExt;
	public $CtaOrdenante;
	public $RfcEmisorCtaBen;
	public $CtaBeneficiario;
	public $TipoCadPago;
	public $CertPago;
	public $CadPago;
	public $SelloPago;
	public $xml_base;
	//public $PorcentajeVariacionMoneda;
	// estas variables servira para validar los montos cuando sea distinto de pesos
	public $montoMXN = 0;
	public $totalDocumentos = 0;

	//nodo DoctoRelacionado
	public $DoctoRelacionado = array();
	public $Decimales;

	//impuestos
	public $TotalTraslados = 0;
	public $TotalRetenciones = 0;
	public $Traslados = array();
	public $Retenciones = array();

	var $logger;

	function __construct($FechaPago, $FormaDePagoP, $MonedaP, $Monto
	, $TipoCambioP = null, $NumOperacion = null, $RfcEmisorCtaOrd = null
	, $NomBancoOrdExt = null, $CtaOrdenante = null, $RfcEmisorCtaBen = null
	, $CtaBeneficiario = null, $TipoCadPago = null
	, $CertPago = null, $CadPago = null, $SelloPago = null) {
		$arrayCatalog = new Arrays();
		$this->FechaPago = $FechaPago;
		$this->FormaDePagoP = $FormaDePagoP;
		$this->MonedaP = $MonedaP;
		$this->TipoCambioP = $TipoCambioP;
		$this->Monto = $Monto;
		$this->RfcEmisorCtaOrd = $RfcEmisorCtaOrd;
		$this->TipoCadPago = $TipoCadPago;
		$this->CertPago = $CertPago;
		$this->CadPago = $CadPago;
		$this->SelloPago = $SelloPago; // el sello generacion pendiente
		$this->NomBancoOrdExt = $NomBancoOrdExt;
		$this->NumOperacion = $NumOperacion;
		$this->CtaOrdenante = $CtaOrdenante;
		$this->RfcEmisorCtaBen = $RfcEmisorCtaBen;
		$this->CtaBeneficiario = $CtaBeneficiario;
		$this->PorcentajeVariacionMoneda = $arrayCatalog->arrayMoneda[$this->MonedaP]['porcentaje_variacion'];
		$this->DoctoRelacionado = array();
		$this->Traslados = array();
		$this->Retenciones = array();
		$this->Decimales = $arrayCatalog->arrayMoneda[$this->MonedaP]['decimales'];
		$this->logger = new Logger(); //clase para escribir logs

		if (!empty($this->TipoCambioP) and $this->MonedaP != "MXN") {
			$this->montoMXN = round($this->Monto * $this->TipoCambioP, $this->Decimales);
		}

		if (!array_key_exists($this->MonedaP, $arrayCatalog->arrayMoneda)) {
			$this->logger->write("Construct Pagos() La moneda declarada " . $this->MonedaP . " no se encuentra dentro del catalogo del SAT");
			throw new Exception('La moneda declarada  no se encuentra en el catalogo de monedas. Valor reportado : '.$this->MonedaP);
		}
	}

	function validar() {
		$required;
		// primero se valida que existan los parametros requeridos y que no sean nulos
		// verficar si el pago no es bancarizado
		
		if ($this->TipoCadPago <> null) {
			$required = array('FechaPago', 'FormaDePagoP', 'MonedaP', 'Monto', "CertPago", "CadPago", "SelloPago");
		} else {
			$required = array('FechaPago', 'FormaDePagoP', 'MonedaP', 'Monto');
		}
		if(!empty($this->MonedaP)) {
			if($this->MonedaP!='MXN'){
				array_push($required, "TipoCambioP");
			}
		}

		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("ComplementoDePagoV2 validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('ComplementoDePagoV2 Campo Requerido: ' . $field);
			}
		}

		if(!empty($this->MonedaP)) {
			if($this->MonedaP!='MXN'){
				if($this->TipoCambioP < 0.000001){ //valor minimo
					$this->logger->write('ComplementoDePagoV2 TipoCambioP ' . $this->TipoCambioP . ' debe tener un valor minimo de 0.000001');
					throw new Exception('ComplementoDePagoV2 TipoCambioP ' . $this->TipoCambioP . ' debe tener un valor minimo de 0.000001');
				}
			}else{
				if(!empty($this->TipoCambioP)){
					if($this->TipoCambioP < 0.000001){ //valor minimo
						$this->logger->write('ComplementoDePagoV2 TipoCambioP ' . $this->TipoCambioP . ' debe tener un valor minimo de 0.000001');
						throw new Exception('ComplementoDePagoV2 TipoCambioP ' . $this->TipoCambioP . ' debe tener un valor minimo de 0.000001');
					}
				}
			}
		}else{
			if(!empty($this->TipoCambioP)){
				if($this->TipoCambioP < 0.000001){ //valor minimo
					$this->logger->write('ComplementoDePagoV2 TipoCambioP ' . $this->TipoCambioP . ' debe tener un valor minimo de 0.000001');
					throw new Exception('ComplementoDePagoV2 TipoCambioP ' . $this->TipoCambioP . ' debe tener un valor minimo de 0.000001');
				}
			}
		}

		
		//if (!empty($this->TipoCambioP)) {
		//	$limite = $this->validateMaxMin();
		//	if ($this->TipoCambioP < $limite['minimo'] || $this->TipoCambioP > $limite['maximo']) {
		//		$this->logger->write("Retencion validar maximos y minimos(): el importe de " . $this->TipoCambioP . "esta fuera del rango permitido minimo :" . $limite["minimo"] . " maximo: " . $limite["maximo"]);
		//		throw new Exception('El tipo de cambio declarado de  ' . $this->TipoCambioP . ' esta fuera del limite permitido , minimo : ' . $limite["minimo"] . " maximo :" . $limite["maximo"]);
		//	}
		//}
		// validacion del del nodo de complemento de pago
		if ($this->FormaDePagoP == 99) {
			$this->logger->write("ComplementoDePagoV2 validar(): FormaDePagoP no valido :" . $this->FormaDePagoP);
			throw new Exception('ComplementoDePagoV2 Campo FormaDePagoP debe ser distinto de  ' . $this->FormaDePagoP);
		}

		if ($this->MonedaP == "XXX") {
			$this->logger->write("ComplementoDePagoV2 validar(): MonedaP no valido :" . $this->MonedaP);
			throw new Exception('ComplementoDePagoV2 Campo MonedaP debe ser distinto de : ' . $this->MonedaP);
		}

		// if ($this->MonedaP != "MXN" AND empty($this->TipoCambioP)) {
		// 	$this->logger->write("ComplementoDePagoV2 validar(): TipoDeCambioP intoduzca el tipo de cambio de la moneda extranjera :" . $this->TipoCambioP);
		// 	throw new Exception('Si el tipo de cambio es distinto de MXN debe introducir el tipo de cambio de la moneda extranjera.: ' . $this->TipoCambioP);
		// }
		
		// if ($this->MonedaP == "MXN" AND !empty($this->TipoCambioP)) {
		// 	$this->logger->write("ComplementoDePagoV2 validar(): Cuando el campo monedaP sea MXN, el campo TipoCambioP no se debe registrar, valor registrado :" . $this->TipoCambioP);
		// 	throw new Exception('Cuando el campo monedaP sea MXN, el campo TipoCambioP no se debe registrar, valor registrado : ' . $this->TipoCambioP);
		// }

		if ($this->Monto < 0) {
			$this->logger->write("ComplementoDePagoV2 validar(): Monto debe ser un valor numerico mayor :" . $this->TipoCambioP);
			throw new Exception('ComplementoDePagoV2 validar(): Monto debe ser un valor numerico mayor a : ' . $this->TipoCambioP);
		}

		if(!empty($this->NumOperacion)){
			if(strlen($this->NumOperacion) < 0 || strlen($this->NumOperacion)>100 ){
				$this->logger->write('ComplementoDePagoV2 validar NumOperacion: Debe contener entre 1 a 100 carácter(es) .');
				throw new Exception('El valor de ComplementoDePagoV2 NumOperacion debe ser entre 1 a 100 carácter(es): len='.strlen($this->NumOperacion));
			}
		}

		if(!empty($this->RfcEmisorCtaOrd)){
			if(strlen($this->RfcEmisorCtaOrd) < 12 || strlen($this->RfcEmisorCtaOrd)>13 ){
				$this->logger->write('ComplementoDePagoV2 validar RfcEmisorCtaOrd: Debe contener entre 12 a 13 carácter(es) .');
				throw new Exception('El valor de ComplementoDePagoV2 RfcEmisorCtaOrd debe ser entre 12 a 13 carácter(es): len='.strlen($this->RfcEmisorCtaOrd));
			}
		}

		if(!empty($this->NomBancoOrdExt)){
			if(strlen($this->NomBancoOrdExt) < 1 || strlen($this->NomBancoOrdExt)>300 ){
				$this->logger->write('ComplementoDePagoV2 validar NomBancoOrdExt: Debe contener entre 1 a 300 carácter(es) .');
				throw new Exception('El valor de ComplementoDePagoV2 NomBancoOrdExt debe ser entre 1 a 300 carácter(es): len='.strlen($this->NomBancoOrdExt));
			}
		}

		if(!empty($this->CtaOrdenante)){
			if(strlen($this->CtaOrdenante) < 10 || strlen($this->CtaOrdenante)>50 ){
				$this->logger->write('ComplementoDePagoV2 validar CtaOrdenante: Debe contener entre 10 a 50 carácter(es) .');
				throw new Exception('El valor de ComplementoDePagoV2 CtaOrdenante debe ser entre 10 a 50 carácter(es): len='.strlen($this->CtaOrdenante));
			}
		}

		if(!empty($this->CtaBeneficiario)){
			if(strlen($this->CtaBeneficiario) < 10 || strlen($this->CtaBeneficiario)>50 ){
				$this->logger->write('ComplementoDePagoV2 validar CtaBeneficiario: Debe contener entre 10 a 50 carácter(es) .');
				throw new Exception('El valor de ComplementoDePagoV2 CtaBeneficiario debe ser entre 10 a 50 carácter(es): len='.strlen($this->CtaBeneficiario));
			}
		}

		if(!empty($this->CadPago)){
			if(strlen($this->CadPago) < 1 || strlen($this->CadPago)>8192 ){
				$this->logger->write('ComplementoDePagoV2 validar CadPago: Debe contener entre 1 a 8192 carácter(es) .');
				throw new Exception('El valor de ComplementoDePagoV2 CadPago debe ser entre 1 a 8192 carácter(es): len='.strlen($this->CadPago));
			}
		}
	
		
		
		//$this->logger->write("ComplementoDePagoV2 validar(): Monto debe ser un valor numerico mayor :" . $this->TipoCambioP);
		//throw new Exception('No tiene datos' . $this->DoctoRelacionado);
		foreach ($this->DoctoRelacionado as $doc) {
			$doc->validar();
			if ($doc->MonedaDR != "MXN" and ! empty($doc->TipoCambioDR)) {
					$this->totalDocumentos += round($doc->ImpPagado * $doc->TipoCambioDR, $this->Decimales);
				} else {
					$this->totalDocumentos += round($doc->ImpPagado,$this->Decimales);
				}
		}
		
		

		// valida contra el monto con moneda extranjera convertido a pesos con el total de documentos en pesos
		if ($this->montoMXN > 0 && $this->montoMXN <  $this->totalDocumentos) {
			$this->logger->write("ComplementoDePagoV2 validar():La suma de los valores registrados en el nodo DoctoRelacionado, atributo ImpPagado, sea menor o igual  al monto en pesos : " . $this->montoMXN . " suma de los ImpPagado en pesos : " . $this->totalDocumentos);
			throw new Exception("ComplementoDePagoV2 validar():La suma de los valores registrados en el nodo DoctoRelacionado, atributo ImpPagado, sea menor o igual  al monto en pesos : " . $this->montoMXN . " suma de los ImpPagado en pesos : " . $this->totalDocumentos);
		}

		// valida contra el monto en pesos con el total de documentos en pesos
		if ($this->montoMXN == 0 and $this->Monto < $this->totalDocumentos) {
			$this->logger->write("ComplementoDePagoV2 validar():La suma de los valores registrados en el nodo DoctoRelacionado, atributo ImpPagado, sea menor o igual  al monto en pesos:" . $this->Monto . " suma de los ImpPagado en pesos : " . $this->totalDocumentos);
			throw new Exception("ComplementoDePagoV2 validar():La suma de los valores registrados en el nodo DoctoRelacionado, atributo ImpPagado, sea menor o igual  al monto en pesos:" . $this->Monto . " suma de los ImpPagado en pesos : " . $this->totalDocumentos);
		}
	}

	function toXML() {
		$this->xml_base = new DOMDocument();

		#Nodo pago detalle del pago
		$nodopago = $this->xml_base->createElement("pago20:Pago");
		$nodopago->setAttribute('FechaPago', $this->FechaPago);
		$nodopago->setAttribute('FormaDePagoP', $this->FormaDePagoP);
		$nodopago->setAttribute('MonedaP', $this->MonedaP);
		$nodopago->setAttribute('Monto', $this->addZeros($this->Monto));
		// atributos condicionales del nodo de pago
		if ($this->TipoCambioP)
			$nodopago->setAttribute('TipoCambioP', $this->TipoCambioP);
		if ($this->NumOperacion)
			$nodopago->setAttribute('NumOperacion', $this->NumOperacion);
		if ($this->RfcEmisorCtaOrd)
			$nodopago->setAttribute('RfcEmisorCtaOrd', $this->RfcEmisorCtaOrd);
		if ($this->NomBancoOrdExt)
			$nodopago->setAttribute('NomBancoOrdExt', $this->NomBancoOrdExt);
		if ($this->CtaOrdenante)
			$nodopago->setAttribute('CtaOrdenante', $this->CtaOrdenante);
		if ($this->RfcEmisorCtaBen)
			$nodopago->setAttribute('RfcEmisorCtaBen', $this->RfcEmisorCtaBen);
		if ($this->CtaBeneficiario)
			$nodopago->setAttribute('CtaBeneficiario', $this->CtaBeneficiario);
		if ($this->TipoCadPago)
			$nodopago->setAttribute('TipoCadPago', $this->TipoCadPago);
		if ($this->TipoCadPago <> null) {
			$nodopago->setAttribute('CertPago', $this->CertPago);
			$nodopago->setAttribute('CadPago', $this->CadPago);
			$nodopago->setAttribute('SelloPago', $this->SelloPago);
		}else{
			if ($this->CertPago)
				$nodopago->setAttribute('CertPago', $this->CertPago);
			if ($this->CadPago)
				$nodopago->setAttribute('CadPago', $this->CadPago);
			if ($this->CadPago)
				$nodopago->setAttribute('SelloPago', $this->SelloPago);
		}
		$this->xml_base->appendChild($nodopago);

		// para añadir los documentosrelacionados
		foreach ($this->DoctoRelacionado as $documento) {
			$documento->toXML();
			$documento_xml = $this->xml_base->importNode($documento->importXML(), true);
			$nodopago->appendChild($documento_xml);
		}
		

		// de existir traslados u retenciones añadir el nodo de impuestos
		// aqui se calcularia el total de los impuestos retenidos y el total de los impuestos trasladados
		// para añadirlos al nodo de impuestos

		if (!empty($this->Traslados) || !empty($this->Retenciones)) {
			$impuestos = $this->xml_base->createElement("pago20:ImpuestosP");
			$nodopago->appendChild($impuestos);
			if (!empty($this->Retenciones)) {
				$retenciones = $this->xml_base->CreateElement("pago20:RetencionesP");
				$impuestos->appendChild($retenciones);
				foreach ($this->Retenciones as $retencion) {
					$retencion->toXML();
					$retencion_xml = $this->xml_base->importNode($retencion->importXML(), true);
					$retenciones->appendChild($retencion_xml);
				}
			}
			if (!empty($this->Traslados)) {
				$traslados = $this->xml_base->CreateElement("pago20:TrasladosP");
				$impuestos->appendChild($traslados);
				foreach ($this->Traslados as $traslado) {
					$traslado->toXML();
					$traslado_xml = $this->xml_base->importNode($traslado->importXML(), true);
					$traslados->appendChild($traslado_xml);
				}
			}
		}
		return $nodopago;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("pago20:Pago")->item(0);
		return $xml;
	}

	// calcula el limite superior e inferior del tipo de cambio si la moneda es distinta al peso mexicano
	// si es peso mexicano no es necesario realizar estos pasos ya que se puede omitir el valor del tipo
	// de cambio del comprobante o dejarlo con el valor de 1.
	// no se aqui que onda por que si se llegara a dar el caso de que el tipo de cambio de cambio llegase a exceder
	// los limites se debe de pedir una confirmacion ante el SAT para ratificar que es el valor correcto y añadirla en el atributo
	// dicha clave en la confirmacion

	public function getMax() {
		$maximo = $this->TipoCambio * 1 + $this->PorcentajeVariacionMoneda;
		return $maximo;
	}

	public function getMin() {
		$minimo = $this->TipoCambio * 1 - $this->PorcentajeVariacionMoneda;
		return $minimo;
	}

	private function validateMaxMin() {
		$minimo = $this->getMin();
		$maximo = $this->getMax();
		$array = ['minimo' => $minimo, 'maximo' => $maximo];
		return $array;
	}

	/**
	 * Funcion que se encarga de añadir los documentos relacionados dentro del nodo de pago
	 */
	function addDoctoRelacionado($IdDocumento, $MonedaDR, $EquivalenciaDR, $Monto, $Serie = null, $Folio = null, $NumParcialidad, $ImpSaldoAnt, $ImpPagado, $ImpSaldoInsoluto, $ObjetoImpDR, $MonedaP= null ) {
		$doctoRelacionado = new DoctoRelacionadoV2($IdDocumento, $MonedaDR, $EquivalenciaDR, $Monto, $Serie, $Folio, $NumParcialidad, $ImpSaldoAnt, $ImpPagado, $ImpSaldoInsoluto, $ObjetoImpDR, $MonedaP==null?$this->MonedaP:$MonedaP);
		$this->DoctoRelacionado[] = $doctoRelacionado;
		$doctoRelacionado->validar();
		return $doctoRelacionado;
	}

	/**
	 * Funcion que se encargara de añadir los impuestos retenidos globales del pago
	 */
	function addRetencionPago($Impuesto, $Importe) {
		$retencionPago = new RetencionPagoV2($Impuesto, $Importe);
		$this->Retenciones[] = $retencionPago;
		// $this->validateRetencion();
		$retencionPago->validar();
		return $retencionPago;
	}

	/**
	 * Funcion que se encargara de añadir los impuestos retenidos globales del pago
	 */
	function addTrasladoPago($Base, $Impuesto, $TipoFactor, $TasaOCuota, $Importe) {
		$trasladoPago = new TrasladoPagoV2($Base, $Impuesto, $TipoFactor, $TasaOCuota, $Importe);
		$this->Traslados[] = $trasladoPago;
		// $this->validateTraslado();
		$trasladoPago->validar();
		return $trasladoPago;
	}

	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return sprintf('%0.' . $dec . 'f', $cantidad);
	}

	/**
	 * Basicamente es el mismo procedimiento de agrupar los impuestos
	 * pero es implementado de forma de que encuentre impuestos iguales no para sumarlos
	 * si no evitar que siga el procedimiento ya que el pago no debe tener dos impuestos iguales
	 * ya los impuestos van en base al monto del pago cuando se efectua un anticipo.
	 *  @throws Exception
	 */
	function validateRetencion() {
		$retenciones = array();

		foreach ($this->Retenciones as $key => $retencion) {
			//agrupado por impuesto
			if (empty($retenciones)) {
				$retenciones[] = array(
					'Impuesto' => $retencion->Impuesto,
					'Importe' => $retencion->Importe
				);
			} else {
				$is_impto = array_search($retencion->Impuesto, array_column($retenciones, 'Impuesto'));
				if (is_int($is_impto)) {
					$this->logger->write("Pagos validar(): Una retencion con la clave :" . $retencion->Impuesto . " a sido declarado anteriormente .");
					throw new Exception("Pagos validar(): La retención con la clave :" . $retencion->Impuesto . " a sido declarado anteriormente, pruebe declarando un impuesto distinto.");
				} else {
					$retenciones[] = array('Impuesto' => $retencion->Impuesto, 'Importe' => $retencion->Importe);
				}
			}
		}
	}

	/**
	 * Metodo para evitar que introduzca traslados iguales
	 *
	 * @throws Exception
	 */
	function validateTraslado() {
		$traslados = array();
		foreach ($this->Traslados as $key => $traslado) {
			//agrupado por impuesto, TipoFactor y TasaOCuota
			if (empty($traslados)) {
				$traslados[] = array(
					'Impuesto' => $traslado->Impuesto,
					'TipoFactor' => $traslado->TipoFactor,
					'TasaOCuota' => $traslado->TasaOCuota,
					'Importe' => $traslado->Importe
				);
			} else {
				$is_impto = array_search($traslado->Impuesto, array_column($traslados, 'Impuesto'));
				$is_fact = array_search($traslado->TipoFactor, array_column($traslados, 'TipoFactor'));
				$is_toc = array_search($traslado->TasaOCuota, array_column($traslados, 'TasaOCuota'));
				if (is_int($is_impto) && is_int($is_fact) && is_int($is_toc) && $is_fact == $is_impto && $is_fact == $is_toc) {
					$this->logger->write("Pagos validar(): El traslado con la clave del impuesto :" . $traslado->Impuesto . " ,TipoFactor : " . $traslado->TipoFactor . ", y TazaOCuota de " . $traslado->TasaOCuota . " ya ha sido declarado anteriormente.");
					throw new Exception(" : Pagos validar(): El traslado con la clave :" . $traslado->Impuesto . " ,TipoFactor : " . $traslado->TipoFactor . ", y TazaOCuota de " . $traslado->TasaOCuota . " ya ha sido declarado anteriormente, declare un traslado con distintos parametros.");
				} else {
					$traslados[] = array('Impuesto' => $traslado->Impuesto,
						'TipoFactor' => $traslado->TipoFactor,
						'TasaOCuota' => $traslado->TasaOCuota,
						'Importe' => $traslado->Importe);
				}
			}
		}
	}
}

?>