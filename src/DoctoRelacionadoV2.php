<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Logger;
Use cfdi\Data\Arrays;

use Exception;
use DOMDocument;

class DoctoRelacionadoV2 {
	public $IdDocumento;//
	public $Serie;//
	public $Folio;//
	public $MonedaDR;//
	public $EquivalenciaDR;//
	// public $TipoCambioDR;//
	// public $MetodoDePagoDR;
	public $NumParcialidad;//
	public $ImpSaldoAnt;//
	public $ImpPagado;//
	public $ImpSaldoInsoluto;//
	public $Monto; // valor del nodo de pago, sirve para calcular y validar el attributo ImpSaldoInsoluto
	public $ObjetoImpDR;//
	public $Decimales;
	public $moneda;
	public $xml_base;
	public $MonedaP;
	public $TrasladosDR = array();
	public $RetencionesDR = array();
	var $logger;

	function __construct($IdDocumento, $MonedaDR, $EquivalenciaDR=null, $Monto, $Serie = null, $Folio = null, $NumParcialidad, $ImpSaldoAnt, $ImpPagado, $ImpSaldoInsoluto, $ObjetoImpDR, $MonedaP = null) {
		$this->logger = new Logger(); //clase para escribir logs
		$arrayCatalog = new Arrays();
		$this->IdDocumento = $IdDocumento;
		$this->Serie = $Serie;
		$this->Folio = $Folio;
		$this->MonedaDR = $MonedaDR;
		$this->EquivalenciaDR = round($EquivalenciaDR,6);
		$this->MonedaP = $MonedaP;
		// $this->TipoCambioDR = $TipoCambioDR;
		// $this->MetodoDePagoDR = $MetodoDePagoDR;
		$this->NumParcialidad = $NumParcialidad;
		$this->Monto = $Monto;
		$this->moneda = $arrayCatalog->arrayMoneda[$this->MonedaDR];
		$this->Decimales = $this->moneda['decimales'];
		$this->ImpSaldoAnt = round($ImpSaldoAnt,$this->Decimales);
		$this->ImpPagado = round($ImpPagado,$this->Decimales);
		$this->ImpSaldoInsoluto = round($ImpSaldoInsoluto,$this->Decimales);
		$this->ObjetoImpDR=$ObjetoImpDR;
		$this->TrasladosDR = array();
		$this->RetencionesDR = array();
		if($this->ImpSaldoInsoluto == -0){
			$this->ImpSaldoInsoluto = abs($this->ImpSaldoInsoluto);
		}

		if (!array_key_exists($this->MonedaDR, $arrayCatalog->arrayMoneda)) {
			$this->logger->write("Construct Pago() La moneda declarada " . $this->MonedaDR . " no se encuentra dentro del catalogo del SAT");
			throw new Exception('La moneda declarada ' . $this->MonedaDR . '  no se encuentra declarada en el catalogo de monedas.');
		}
	}

	function validar() {
	 //  print_r($this);
		// primero validar los elementos requeridos
		$required = array(
			"IdDocumento", 
			"MonedaDR", 
			'NumParcialidad', 
			'ImpSaldoAnt',
			'ImpPagado',
			'ImpSaldoInsoluto',
			'ObjetoImpDR'
		);
	
		// if ($this->MetodoDePagoDR == "PPD") {
		// 	$required = array("IdDocumento", "MetodoDePagoDR", "MonedaDR", "NumParcialidad", "ImpSaldoAnt");
		// }

		// ver aqui que onda con este campo ya que puede ser opcinal cuando solo existe un documento
		// relacionado pero cuando hay 2 o mas debe se obligatorio
		// if (!empty($this->TipoCambioDR)) {
		// 	array_push($required, "ImpPagado");
		// }

		if(!empty($this->MonedaP) && !empty($this->MonedaDR)) {
			if($this->MonedaP!=$this->MonedaDR){
				array_push($required, "EquivalenciaDR");
			}
		}

		foreach ($required as $field) {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("DoctoRelacionadoV2 validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('DoctoRelacionadoV2 Campo Requerido: ' . $field);
			}
		}

		if(!empty($this->MonedaP) && !empty($this->MonedaDR)) {
			if($this->MonedaP!=$this->MonedaDR){
				if($this->EquivalenciaDR < 0.000001){ //valor minimo
					$this->logger->write('DoctoRelacionadoV2 EquivalenciaDR ' . $this->EquivalenciaDR . ' debe tener un valor minimo de 0.000001');
					throw new Exception('DoctoRelacionadoV2 EquivalenciaDR ' . $this->EquivalenciaDR . ' debe tener un valor minimo de 0.000001');
				}
			}else{
				if(!empty($this->EquivalenciaDR)){
					if($this->EquivalenciaDR < 0.000001){ //valor minimo
						$this->logger->write('DoctoRelacionadoV2 EquivalenciaDR ' . $this->EquivalenciaDR . ' debe tener un valor minimo de 0.000001');
						throw new Exception('DoctoRelacionadoV2 EquivalenciaDR ' . $this->EquivalenciaDR . ' debe tener un valor minimo de 0.000001');
					}
				}
			}
		}

		if(strlen($this->IdDocumento) < 16 || strlen($this->IdDocumento)>36 ){
			$this->logger->write('DoctoRelacionadoV2 validar IdDocumento: Debe contener entre 16 a 36 carácter(es) .');
			throw new Exception('El valor de DoctoRelacionadoV2 IdDocumento debe ser entre 16 a 36 carácter(es): len='.strlen($this->IdDocumento));
		}

		if(!empty($this->Serie)){
			if(strlen($this->Serie) < 1 || strlen($this->Serie)>25 ){
				$this->logger->write('DoctoRelacionadoV2 validar Serie: Debe contener entre 1 a 25 carácter(es) .');
				throw new Exception('El valor de DoctoRelacionadoV2 Serie debe ser entre 1 a 25 carácter(es): len='.strlen($this->Serie));
			}
		}

		if(!empty($this->Folio)){
			if(strlen($this->Folio) < 1 || strlen($this->Folio)>40 ){
				$this->logger->write('DoctoRelacionadoV2 validar Folio: Debe contener entre 1 a 40 carácter(es) .');
				throw new Exception('El valor de DoctoRelacionadoV2 Folio debe ser entre 1 a 40 carácter(es): len='.strlen($this->Folio));
			}
		}

		// validar los decimales
		if (!empty($this->ImpSaldoAnt)) {
			$this->validateDecimals($this->ImpSaldoAnt, "ImpSaldoAnt");
		}
		if (!empty($this->ImpPagado)) {
			$this->validateDecimals($this->ImpPagado, "ImpPagado");
		}
		if (!empty($this->ImpSaldoInsoluto)) {
			$this->validateDecimals($this->ImpSaldoInsoluto, "ImpSaldoInsoluto");
		}

		// if (empty($this->ImpSaldoAnt) && $this->ImpSaldoAnt <= 0) {
		// 	$this->logger->write("validar DoctoRelacionado() el valor de ImpSaldoAnt " . $this->ImpSaldoAnt . " debe ser mayor a cero.");
		// 	throw new Exception("validar DoctoRelacionado() el valor de ImpSaldoAnt " . $this->ImpSaldoAnt . " debe ser mayor a cero.");
		// }

		if ($this->MonedaDR == "XXX") {
			$this->logger->write(" : DoctoRelacionado validar(): MonedaDR no valido :" . $this->MonedaDR);	
			throw new Exception('DoctoRelacionado Campo MonedaDR debe ser distinto de : ' . $this->MonedaDR);
		}
		if($this->ImpSaldoAnt>0){
			if ($this->ImpPagado>0) {
				$impsaldoinsoluto = $this->ImpSaldoAnt - $this->ImpPagado;
			} else {
				$impsaldoinsoluto = $this->ImpSaldoAnt - $this->Monto;
			}
			$impsaldoinsoluto = round($impsaldoinsoluto,6);
			
			$bccomp = bccomp((float)$impsaldoinsoluto, (float)$this->ImpSaldoInsoluto); //0 si los dos operandos son iguales, 1 si el left_operand es mayor que el right_operand, de lo contrario -1.
			if($bccomp != 0){
				$this->logger->write("DoctoRelacionado validar(): El valor del ImpSaldoInsoluto declarado  debe ser mayor o igual a cero y calcularse con la suma de los campos ImpSaldoAnt menos el ImpPagado o el Monto. Valor Recibido: ".$this->ImpSaldoInsoluto." Valor esperado: " . $impsaldoinsoluto);
				throw new Exception("  : DoctoRelacionado validar(): El valor del ImpSaldoInsoluto declarado  debe ser mayor o igual a cero y calcularse con la suma de los campos ImpSaldoAnt menos el ImpPagado o el Monto. Valor Recibido: ".$this->ImpSaldoInsoluto." Valor esperado: " . $impsaldoinsoluto);
			}
		}else{
			$this->logger->write("DoctoRelacionado validar(): El campo ImpSaldoAnt debe ser mayor a 0 el valor ingresado :" . $this->ImpSaldoAnt);
			throw new Exception('DoctoRelacionado El campo ImpSaldoAnt debe ser mayor a 0 el valor ingresado : ' . $this->ImpSaldoAnt);
		}

		// if ($this->MonedaDR == "MXN" AND (!empty($this->MonedaP == "MXN"))) {
		// 	$this->logger->write("ComplementoDePago validar(): Cuando el campo monedaDR sea MXN, el campo TipoCambioDR no se debe registrar, valor registrado :" . $this->TipoCambioDR);
		// 	throw new Exception('Cuando el campo monedaDR sea MXN, el campo TipoCambioDR no se debe registrar, valor registrado : ' . $this->TipoCambioDR);
		// }
//        if ($this->MonedaDR == "MXN" and $this->TipoCambioDR != null) {
//            $this->logger->write("DoctoRelacionado validar(): El campo TipoCambioDR no se debe registrar cuando la moneda es MXN");
//            throw new Exception('" : DoctoRelacionado validar(): El campo TipoCambioDR no se debe registrar cuando la moneda es MXN ');
//        }
	}

	function toXML() {
		$this->xml_base = new DOMDocument('1.0', 'UTF-8');
		$documento = $this->xml_base->createElement("pago20:DoctoRelacionado");
		$this->xml_base->appendChild($documento);
		$documento->setAttribute("IdDocumento", $this->IdDocumento);
		if ($this->Serie)
			$documento->setAttribute("Serie", $this->Serie);
		if ($this->Folio)
			$documento->setAttribute("Folio", $this->Folio);
		$documento->setAttribute("MonedaDR", $this->MonedaDR);
		if(!empty($this->MonedaP) && !empty($this->MonedaDR)) {
			if($this->MonedaP!=$this->MonedaDR){
				$documento->setAttribute("EquivalenciaDR", $this->EquivalenciaDR);
			}else{
				if(!empty($this->EquivalenciaDR)){
					$documento->setAttribute("EquivalenciaDR", $this->EquivalenciaDR);
				}
			}
		}
		$documento->setAttribute("NumParcialidad", $this->NumParcialidad);
		$documento->setAttribute("ImpSaldoAnt", $this->addZeros($this->ImpSaldoAnt));
		$documento->setAttribute("ImpPagado", $this->addZeros($this->ImpPagado));
		$documento->setAttribute("ImpSaldoInsoluto", $this->addZeros($this->ImpSaldoInsoluto));
		$documento->setAttribute("ObjetoImpDR", $this->ObjetoImpDR);
		if (!empty($this->TrasladosDR) || !empty($this->RetencionesDR)) {
			$impuestosDR = $this->xml_base->createElement("pago20:ImpuestosDR");
			$documento->appendChild($impuestosDR);
			if (!empty($this->RetencionesDR)) {
				$retencionesDR = $this->xml_base->CreateElement("pago20:RetencionesDR");
				$impuestosDR->appendChild($retencionesDR);
				foreach ($this->RetencionesDR as $retencionDR) {
					$retencionDR->toXML();
					$retencionDR_xml = $this->xml_base->importNode($retencionDR->importXML(), true);
					$retencionesDR->appendChild($retencionDR_xml);
				}
			}
			if (!empty($this->TrasladosDR)) {
				$trasladosDR = $this->xml_base->CreateElement("pago20:TrasladosDR");
				$impuestosDR->appendChild($trasladosDR);
				foreach ($this->TrasladosDR as $trasladoDR) {
					$trasladoDR->toXML();
					$trasladoDR_xml = $this->xml_base->importNode($trasladoDR->importXML(), true);
					$trasladosDR->appendChild($trasladoDR_xml);
				}
			}
		}
		return $documento;
	}

	function toStringXML() {
		return $this->xml_base->saveXML();
	}

	function importXML() {
		$xml = $this->xml_base->getElementsByTagName("pago20:DoctoRelacionado")->item(0);
		return $xml;
	}

	function addRetencionPagoDR($BaseDR, $ImpuestoDR, $TipoFactorDR, $TasaOCuotaDR, $ImporteDR) {
		$retencionDR = new RetencionDR($BaseDR, $ImpuestoDR, $TipoFactorDR, $TasaOCuotaDR, $ImporteDR);
		$this->RetencionesDR[] = $retencionDR;
		$retencionDR->validar();
		return $retencionDR;
	}

	function addTrasladoPagoDR($BaseDR, $ImpuestoDR, $TipoFactorDR, $TasaOCuotaDR= null, $ImporteDR=null) {
		$trasladoDR = new TrasladoDR($BaseDR, $ImpuestoDR, $TipoFactorDR, $TasaOCuotaDR, $ImporteDR);
		$this->TrasladosDR[] = $trasladoDR;
		$trasladoDR->validar();
		return $trasladoDR;
	}

	function validateDecimals($valor, $nombre = null) {
		$decimalesTotal = strlen(substr(strrchr($valor, "."), 1));
		if ($decimalesTotal > $this->Decimales) {
			throw new Exception("El valor de " . $valor .
			" en el attributo " . $nombre . " no coincide con el valor de los decimales especificado por la moneda ,valor de decimales: " . $this->Decimales);
		}
	}

	function addZeros($cantidad = null, $dec = null){
		if($dec == null)
			$dec = $this->Decimales;
		return sprintf('%0.' . $dec . 'f', $cantidad);
	}
}
?>