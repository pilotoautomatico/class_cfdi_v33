<?php
/* +----------------------------------------------------------------+
 * |                 © 2015-2020 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

Use cfdi\Comprobante;
Use cfdi\ComprobanteV4;
Use cfdi\Logger;

Use Exception;
Use DOMDocument;

class ReadComprobante {
	var $logger;

	public function __construct(){
		$this->logger = new Logger(); //clase para escribir logs
		$this->logger->write('Entra ReadComprobante [__construct()]');
	}

	public function loadXML($xmlString, $loadAddenda=true) {
		$this->logger->write("Entra loadXML");
		//INICIALIZAR XML
		$xml = new DOMDocument();
		
		//VERIFICAR SI EL ARCHIVO EXISTE
		if (!file_exists($xmlString)) {
			//CARGAR DATOS DEL COMPROBANTE DESDE UN STRING
			libxml_use_internal_errors(true);
			if (!$xml->loadXML($xmlString)) {
				$this->logger->write("Error al cargar el XML. No es un XML Valido.");
				$errors = libxml_get_errors();
				return $errors[0]->message;
			}
		}else{
			//CARGAR DATOS DEL COMPROBANTE DESDE UN ARCHIVO
			libxml_use_internal_errors(true);
			if (!$xml->load($xmlString)) {
				$this->logger->write("Error al cargar el XML. No es un XML Valido.");
				$errors = libxml_get_errors();
				return $errors[0]->message;
			}
		}

		//VALIDAR EL TAG DEL COMPROBANTE SI ES VERSION 4 O SINO ES LA VERSION 3 O NO ES UN XML VALIDO
		$comprobanteXML = $this->__getAttrsXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/4', 'Comprobante')->item(0));
		$comprobante= null;
		if(!empty($comprobanteXML)){
			$versionComprobante=$comprobanteXML['Version'];
			$comprobante= new ComprobanteV4();
			$comprobante->loadXML($xml,$loadAddenda);
		}else{
			$comprobanteXML = $this->__getAttrsXML($xml->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/3', 'Comprobante')->item(0));
			if(!empty($comprobanteXML)){
				$versionComprobante=$comprobanteXML['Version'];
				$comprobante= new Comprobante();
				$comprobante->loadXML($xml,$loadAddenda);
			}else{
				$this->logger->write("Error al cargar el XML. No es un XML Valido.");
				$errors = libxml_get_errors();
				return $errors[0]->message;
			}
		}
		$dataComprobante=array();
		$dataComprobante['comprobante']=$comprobante;
		$dataComprobante['version']=$versionComprobante;
		$this->logger->write("ARRAY COMPROBANTE ".print_r($dataComprobante, true));
		return $dataComprobante;
	}

	function __getAttrsXML($node) {
		$array = null;
		if(isset($node->attributes)){
			foreach ($node->attributes as $attrName => $attrNode) {
				$array[$attrName] = $node->getAttribute($attrName);
			}
		}
		return $array;
	}
} 

?>