<?php
/* +----------------------------------------------------------------+
 * |                 © 3115-3131 PILOTO AUTOMATICO                  |
 * | Clase cfdiv33 para verificar y sellar comprobantes fiscales    |
 * | digitales                                                      |
 * +----------------------------------------------------------------+ */

namespace cfdi;

use Exception;
use DOMDocument;

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31


class CartaPorte31
{
	// Obligatorios
	public $Version = "3.1";
    var $IdCCP;
	var $TranspInternac;
	var $Ubicaciones = [];
	var $Mercancias;

	// opcionales
	var $EntradaSalidaMerc;
    var $PaisOrigenDestino;
	var $ViaEntradaSalida;
	var $TotalDistRec;
    var $RegistroISTMO;
    var $UbicacionPoloOrigen;
    var $UbicacionPoloDestino;
	var $FiguraTransporte = [];
	var $RegimenesAduaneros = [];

	var $xml_base;
	var $logger;

	function __construct($IdCCP, $TranspInternac, $EntradaSalidaMerc = null, $ViaEntradaSalida = null, $TotalDistRec = null, $PaisOrigenDestino = null, 
        $RegistroISTMO = null, $UbicacionPoloOrigen = null, $UbicacionPoloDestino = null)
    {
		$this->xml_base = null;
        $this->IdCCP = $IdCCP;
        $this->TranspInternac = $TranspInternac;
        $this->EntradaSalidaMerc = $EntradaSalidaMerc;
        $this->PaisOrigenDestino = $PaisOrigenDestino;
        $this->ViaEntradaSalida = $ViaEntradaSalida;
        $this->TotalDistRec = $TotalDistRec;
        $this->RegistroISTMO = $RegistroISTMO;
        $this->UbicacionPoloOrigen = $UbicacionPoloOrigen;
        $this->UbicacionPoloDestino = $UbicacionPoloDestino;
		$this->Ubicaciones = [];
		$this->Mercancias = null;
		$this->FiguraTransporte = null;
        $this->RegimenesAduaneros = [];

		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['Version', 'TranspInternac', 'IdCCP'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31 validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31 Campo Requerido: ' . $field);
			}
		}

        if(strlen($this->IdCCP) != 36)
        {
            $this->logger->write("Complemento.CartaPorte31.IdCCP Atributo requerido para expresar los 36 caracteres del folio del complemento Carta Porte (IdCCP) de la transacción de timbrado conforme al estándar RFC 4122, para la identificación del CFDI con complemento Carta Porte.");
            throw new Exception('Complemento.CartaPorte31.IdCCP Atributo requerido para expresar los 36 caracteres del folio del complemento Carta Porte (IdCCP) de la transacción de timbrado conforme al estándar RFC 4122, para la identificación del CFDI con complemento Carta Porte. ');
        }

        if(count($this->Ubicaciones) == 0)
        {
            $this->logger->write("Complemento.CartaPorte31.Ubicaciones validar(): Debe existir un elemento de Ubicaciones.");
            throw new Exception('Complemento.CartaPorte31.Ubicaciones: Debe existir un elemento de Ubicaciones.');
        }

		foreach($this->Ubicaciones as $Ubicacion)
        {
			$Ubicacion->validar();
		}

        if(empty($this->Mercancias))
        {
            $this->logger->write("Complemento.CartaPorte31.Mercancias validar(): Debe existir elemento de Mercancias.");
            throw new Exception('Complemento.CartaPorte31.Mercancias: Debe existir elemento de Mercancias.');
        }

		if($this->Mercancias)
			$this->Mercancias->validar();

        if(!empty($this->FiguraTransporte))
        {
            foreach($this->FiguraTransporte as $TiposFigura)
            {
                $TiposFigura->validar();
            }
        }
        if(!empty($this->RegimenesAduaneros))
        {
            if($this->TranspInternac !== "Sí") {
                $this->logger->write("Complemento.CartaPorte31.RegimenesAduaneros validar(): Nodo puede existir solo si el atributo 'TranspInternac' contenga el valor 'Sí'.");
                throw new Exception("Complemento.CartaPorte31.RegimenesAduaneros: Nodo puede existir solo si el atributo 'TranspInternac' contenga el valor 'Sí'.");
            }

            if(count($this->RegimenesAduaneros) > 10) {
                $this->logger->write("Complemento.CartaPorte31.RegimenesAduaneros validar(): Máximo 10 nodos RegimenAduaneroCCP.");
                throw new Exception('Complemento.CartaPorte31.RegimenesAduaneros: Máximo 10 nodos RegimenAduaneroCCP.');
            }

            foreach($this->RegimenesAduaneros as $RegimenAduaneroCCP)
            {
                $RegimenAduaneroCCP->validar();
            }
        }
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoCartaPorte31 = $this->xml_base->createElement("cartaporte31:CartaPorte");

		$nodoCartaPorte31->setAttribute('Version',  $this->Version);
		$nodoCartaPorte31->setAttribute('IdCCP',  $this->IdCCP);
		$nodoCartaPorte31->setAttribute('TranspInternac',  $this->TranspInternac);
        // if($this->RegimenAduanero)
		//     $nodoCartaPorte31->setAttribute('RegimenAduanero',  $this->RegimenAduanero);
        if($this->EntradaSalidaMerc)
		    $nodoCartaPorte31->setAttribute('EntradaSalidaMerc',  $this->EntradaSalidaMerc);
        if($this->PaisOrigenDestino)
		    $nodoCartaPorte31->setAttribute('PaisOrigenDestino',  $this->PaisOrigenDestino);
        if($this->ViaEntradaSalida)
		    $nodoCartaPorte31->setAttribute('ViaEntradaSalida',  $this->ViaEntradaSalida);
        if($this->TotalDistRec)
		    $nodoCartaPorte31->setAttribute('TotalDistRec',  $this->TotalDistRec);
        if($this->RegistroISTMO)
		    $nodoCartaPorte31->setAttribute('RegistroISTMO',  $this->RegistroISTMO);
        if($this->UbicacionPoloOrigen)
		    $nodoCartaPorte31->setAttribute('UbicacionPoloOrigen',  $this->UbicacionPoloOrigen);
        if($this->UbicacionPoloDestino)
		    $nodoCartaPorte31->setAttribute('UbicacionPoloDestino',  $this->UbicacionPoloDestino);

        if (!empty($this->RegimenesAduaneros)) {
            $nodoRegimenesAduaneros = $this->xml_base->createElement("cartaporte31:RegimenesAduaneros");
            $nodoCartaPorte31->appendChild($nodoRegimenesAduaneros);

            foreach ($this->RegimenesAduaneros as $key => $RegimenAduaneroCCP) {
                $RegimenAduaneroCCP->toXML();
                $nodoRegimenAduaneroCCP = $this->xml_base->importNode($RegimenAduaneroCCP->importXML(), true);
                $nodoRegimenesAduaneros->appendChild($nodoRegimenAduaneroCCP);
            }
        }
        
        if(!empty($this->Ubicaciones))
        {
		    $nodoUbicaciones = $this->xml_base->createElement("cartaporte31:Ubicaciones");
		    $nodoCartaPorte31->appendChild($nodoUbicaciones);

            foreach ($this->Ubicaciones as $key => $Ubicacion) 
            {
                $Ubicacion->toXML();
                $nodoUbicacion = $this->xml_base->importNode($Ubicacion->importXML(), true);
                $nodoUbicaciones->appendChild($nodoUbicacion);
            }
        }

        if(!empty($this->Mercancias))
        {
            $this->Mercancias->toXML();
            $nodoMercancias = $this->xml_base->importNode($this->Mercancias->importXML(), true);
		    $nodoCartaPorte31->appendChild($nodoMercancias);
        }

        if(!empty($this->FiguraTransporte))
        {
		    $nodoFiguraTransporte = $this->xml_base->createElement("cartaporte31:FiguraTransporte");
		    $nodoCartaPorte31->appendChild($nodoFiguraTransporte);

            foreach ($this->FiguraTransporte as $key => $TiposFigura) 
            {
                $TiposFigura->toXML();
                $nodoTiposFigura = $this->xml_base->importNode($TiposFigura->importXML(), true);
                $nodoFiguraTransporte->appendChild($nodoTiposFigura);
            }
        }

		$this->xml_base->appendChild($nodoCartaPorte31);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:CartaPorte")->item(0);
		return $xml;
	}

	function addUbicacion($TipoUbicacion, $RFCRemitenteDestinatario, $FechaHoraSalidaLlegada, $IDUbicacion = null, $NombreRemitenteDestinatario = null, $NumRegIdTrib = null,
        $ResidenciaFiscal = null, $NumEstacion = null, $NombreEstacion = null, $NavegacionTrafico = null, $TipoEstacion = null, $DistanciaRecorrida = null)
    {
		$Ubicacion = new Ubicacion31(
            $TipoUbicacion, 
            $RFCRemitenteDestinatario,
            $FechaHoraSalidaLlegada,
            $IDUbicacion,
            $NombreRemitenteDestinatario,
            $NumRegIdTrib,
            $ResidenciaFiscal,
            $NumEstacion,
            $NombreEstacion,
            $NavegacionTrafico,
            $TipoEstacion,
            $DistanciaRecorrida
        );
		
		// $Ubicacion->validar();
		$this->Ubicaciones[] = $Ubicacion;
		return $Ubicacion;
	}

	function addMercancias($NumTotalMercancias, $PesoBrutoTotal, $UnidadPeso, $PesoNetoTotal = null, $CargoPorTasacion = null, $LogisticaInversaRecoleccionDevolucion=null) 
    {
		$Mercancias = new Mercancias31(
            $NumTotalMercancias, 
            $PesoBrutoTotal, 
            $UnidadPeso, 
            $PesoNetoTotal, 
            $CargoPorTasacion,
            $LogisticaInversaRecoleccionDevolucion
        );
		
		// $Mercancias->validar();
		$this->Mercancias = $Mercancias;
		return $Mercancias;
	}

	function addFiguraTransporte($TipoFigura, $RFCFigura = null, $NumLicencia = null, $NombreFigura = null, $NumRegIdTribFigura = null, $ResidenciaFiscalFigura = null) 
    {
		$TiposFigura = new TiposFigura31(
            $TipoFigura, 
            $RFCFigura, 
            $NumLicencia, 
            $NombreFigura, 
            $NumRegIdTribFigura, 
            $ResidenciaFiscalFigura
        );
		
		// $TiposFigura->validar();
		$this->FiguraTransporte[] = $TiposFigura;
		return $TiposFigura;
	}

	function addRegimenAduanero($RegimenAduaneroCCP) 
    {
		$RegimenAduaneroCCP = new RegimenAduaneroCCP31($RegimenAduaneroCCP);
		
		// $RegimenAduaneroCCP->validar();
		$this->RegimenesAduaneros[] = $RegimenAduaneroCCP;
		return $RegimenAduaneroCCP;
	}

}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Ubicaciones.Ubicacion

class Ubicacion31 
{
    // obligatorios
    var $TipoUbicacion;
    var $RFCRemitenteDestinatario;
    var $FechaHoraSalidaLlegada;

	// opcionales
    var $IDUbicacion;
    var $NombreRemitenteDestinatario;
    var $NumRegIdTrib;
    var $ResidenciaFiscal;
    var $NumEstacion;
    var $NombreEstacion;
    var $NavegacionTrafico;
    var $TipoEstacion;
    var $DistanciaRecorrida;
    var $Domicilio;

	var $xml_base;
	var $logger;

	function __construct($TipoUbicacion, $RFCRemitenteDestinatario, $FechaHoraSalidaLlegada, $IDUbicacion = null, $NombreRemitenteDestinatario = null, $NumRegIdTrib = null,
        $ResidenciaFiscal = null, $NumEstacion = null, $NombreEstacion = null, $NavegacionTrafico = null, $TipoEstacion = null, $DistanciaRecorrida = null)
    {
		$this->xml_base = null;
        $this->TipoUbicacion = $TipoUbicacion;
        $this->RFCRemitenteDestinatario = $RFCRemitenteDestinatario;
        $this->FechaHoraSalidaLlegada = $FechaHoraSalidaLlegada;
        $this->IDUbicacion = $IDUbicacion;
        $this->NombreRemitenteDestinatario = $NombreRemitenteDestinatario;
        $this->NumRegIdTrib = $NumRegIdTrib;
        $this->ResidenciaFiscal = $ResidenciaFiscal;
        $this->NumEstacion = $NumEstacion;
        $this->NombreEstacion = $NombreEstacion;
        $this->NavegacionTrafico = $NavegacionTrafico;
        $this->TipoEstacion = $TipoEstacion;
        $this->DistanciaRecorrida = $DistanciaRecorrida;

		$this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoUbicacion', 'RFCRemitenteDestinatario', 'FechaHoraSalidaLlegada'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Ubicaciones.Ubicacion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Ubicaciones.Ubicacion Campo Requerido: ' . $field);
			}
		}

        if($this->Domicilio)
            $this->Domicilio->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoUbicacion = $this->xml_base->createElement("cartaporte31:Ubicacion");

        $nodoUbicacion->setAttribute('TipoUbicacion',  $this->TipoUbicacion);
        $nodoUbicacion->setAttribute('RFCRemitenteDestinatario',  $this->RFCRemitenteDestinatario);
        $nodoUbicacion->setAttribute('FechaHoraSalidaLlegada',  $this->FechaHoraSalidaLlegada);
        if($this->IDUbicacion)
		    $nodoUbicacion->setAttribute('IDUbicacion',  $this->IDUbicacion);
        if($this->NombreRemitenteDestinatario)
		    $nodoUbicacion->setAttribute('NombreRemitenteDestinatario',  $this->NombreRemitenteDestinatario);
        if($this->NumRegIdTrib)
		    $nodoUbicacion->setAttribute('NumRegIdTrib',  $this->NumRegIdTrib);
        if($this->ResidenciaFiscal)
		    $nodoUbicacion->setAttribute('ResidenciaFiscal',  $this->ResidenciaFiscal);
        if($this->NumEstacion)
		    $nodoUbicacion->setAttribute('NumEstacion',  $this->NumEstacion);
        if($this->NombreEstacion)
		    $nodoUbicacion->setAttribute('NombreEstacion',  $this->NombreEstacion);
        if($this->NavegacionTrafico)
		    $nodoUbicacion->setAttribute('NavegacionTrafico',  $this->NavegacionTrafico);
        if($this->TipoEstacion)
		    $nodoUbicacion->setAttribute('TipoEstacion',  $this->TipoEstacion);
        if($this->DistanciaRecorrida)
		    $nodoUbicacion->setAttribute('DistanciaRecorrida',  $this->DistanciaRecorrida);

        // if($this->Origen)
        // {
        //     $this->Origen->toXML();
        //     $nodoOrigen = $this->xml_base->importNode($this->Origen->importXML(), true);
		//     $nodoUbicacion->appendChild($nodoOrigen);
        // }

        // if($this->Destino)
        // {
        //     $this->Destino->toXML();
        //     $nodoDestino = $this->xml_base->importNode($this->Destino->importXML(), true);
		//     $nodoUbicacion->appendChild($nodoDestino);
        // }

        if($this->Domicilio)
        {
            $this->Domicilio->toXML();
            $nodoDomicilio = $this->xml_base->importNode($this->Domicilio->importXML(), true);
		    $nodoUbicacion->appendChild($nodoDomicilio);
        }

		$this->xml_base->appendChild($nodoUbicacion);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:Ubicacion")->item(0);
		return $xml;
	}

    function addDomicilio($Estado, $Pais, $CodigoPostal, $Calle = null, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
        $Domicilio = new Domicilio31(
            $Estado, 
            $Pais, 
            $CodigoPostal, 
            $Calle, 
            $NumeroExterior, 
            $NumeroInterior, 
            $Colonia, 
            $Localidad, 
            $Referencia, 
            $Municipio
        );
        // $Domicilio->validar();
        $this->Domicilio = $Domicilio;
        return $Domicilio;
    }

}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Ubicaciones.Ubicacion.Domicilio
// clase que crea el nodo de CartaPorte31.FiguraTransporte.TiposFigura.PartesTransporte.Domicilio

class Domicilio31 
{
	// Obligatorios
	var $Estado;
	var $Pais;
	var $CodigoPostal;

	// opcionales
	var $Calle;
	var $NumeroExterior;
	var $NumeroInterior;
    var $Colonia;
    var $Localidad;
    var $Referencia;
    var $Municipio;

	var $xml_base;
	var $logger;

	function __construct($Estado, $Pais, $CodigoPostal, $Calle=null, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
		$this->xml_base = null;
        $this->Calle = $Calle;
        $this->Estado = $Estado;
        $this->Pais = $Pais;
        $this->CodigoPostal = $CodigoPostal;
        $this->NumeroExterior = $NumeroExterior;
        $this->NumeroInterior = $NumeroInterior;
        $this->Colonia = $Colonia;
        $this->Localidad = $Localidad;
        $this->Referencia = $Referencia;
        $this->Municipio = $Municipio;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['Estado', 'Pais', 'CodigoPostal'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Domicilio validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Domicilio Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoDomicilio = $this->xml_base->createElement("cartaporte31:Domicilio");
        $nodoDomicilio->setAttribute('Estado',  $this->Estado);
        $nodoDomicilio->setAttribute('Pais',  $this->Pais);
        $nodoDomicilio->setAttribute('CodigoPostal',  $this->CodigoPostal);
        if($this->Calle)
            $nodoDomicilio->setAttribute('Calle',  $this->Calle);
        if($this->NumeroExterior)
		    $nodoDomicilio->setAttribute('NumeroExterior',  $this->NumeroExterior);
        if($this->NumeroInterior)
		    $nodoDomicilio->setAttribute('NumeroInterior',  $this->NumeroInterior);
        if($this->Colonia)
		    $nodoDomicilio->setAttribute('Colonia',  $this->Colonia);
        if($this->Localidad)
		    $nodoDomicilio->setAttribute('Localidad',  $this->Localidad);
        if($this->Referencia)
		    $nodoDomicilio->setAttribute('Referencia',  $this->Referencia);
        if($this->Municipio)
		    $nodoDomicilio->setAttribute('Municipio',  $this->Municipio);

		$this->xml_base->appendChild($nodoDomicilio);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:Domicilio")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias

class Mercancias31
{
	// Obligatorios
	var $PesoBrutoTotal;
	var $UnidadPeso;
	var $NumTotalMercancias;

	// opcionales
    var $PesoNetoTotal;
    var $CargoPorTasacion;
    var $LogisticaInversaRecoleccionDevolucion;

    var $Mercancia = [];
    var $Autotransporte;
    var $TransporteMaritimo;
    var $TransporteAereo;
    var $TransporteFerroviario;

	var $xml_base;
	var $logger;

	function __construct($NumTotalMercancias, $PesoBrutoTotal, $UnidadPeso, $PesoNetoTotal = null, $CargoPorTasacion = null, $LogisticaInversaRecoleccionDevolucion=null)
    {
		$this->xml_base = null;
        $this->NumTotalMercancias = $NumTotalMercancias;
        $this->PesoBrutoTotal = $PesoBrutoTotal;
        $this->UnidadPeso = $UnidadPeso;
        $this->PesoNetoTotal = $PesoNetoTotal;
        $this->CargoPorTasacion = $CargoPorTasacion;
        $this->LogisticaInversaRecoleccionDevolucion = $LogisticaInversaRecoleccionDevolucion;

        $this->Mercancia = [];
        $this->Autotransporte = null;
        $this->TransporteMaritimo = null;
        $this->TransporteAereo = null;
        $this->TransporteFerroviario = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['NumTotalMercancias', 'PesoBrutoTotal', 'UnidadPeso'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias Campo Requerido: ' . $field);
			}
		}

        if(count($this->Mercancia) == 0)
        {
            $this->logger->write("Complemento.CartaPorte31.Mercancias validar(): Debe existir un elemento de Mercancia.");
            throw new Exception('Complemento.CartaPorte31.Mercancias: Debe existir un elemento de Mercancia.');
        }

		foreach($this->Mercancia as $Mercancia)
        {
			$Mercancia->validar();
		}

		if($this->Autotransporte)
			$this->Autotransporte->validar();

		if($this->TransporteMaritimo)
			$this->TransporteMaritimo->validar();

		if($this->TransporteAereo)
			$this->TransporteAereo->validar();

		if($this->TransporteFerroviario)
			$this->TransporteFerroviario->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoMercancias = $this->xml_base->createElement("cartaporte31:Mercancias");
        $nodoMercancias->setAttribute('PesoBrutoTotal',  $this->PesoBrutoTotal);
        $nodoMercancias->setAttribute('UnidadPeso',  $this->UnidadPeso);
        $nodoMercancias->setAttribute('NumTotalMercancias',  $this->NumTotalMercancias);
        if($this->PesoNetoTotal)
		    $nodoMercancias->setAttribute('PesoNetoTotal',  $this->PesoNetoTotal);
        if($this->CargoPorTasacion)
		    $nodoMercancias->setAttribute('CargoPorTasacion',  $this->CargoPorTasacion);
        if($this->LogisticaInversaRecoleccionDevolucion)
		    $nodoMercancias->setAttribute('LogisticaInversaRecoleccionDevolucion',  $this->LogisticaInversaRecoleccionDevolucion);

        if(!empty($this->Mercancia))
        {
            foreach ($this->Mercancia as $key => $Mercancia) 
            {
                $Mercancia->toXML();
                $nodoMercancia = $this->xml_base->importNode($Mercancia->importXML(), true);
                $nodoMercancias->appendChild($nodoMercancia);
            }
        }

        if(!empty($this->Autotransporte))
        {
            $this->Autotransporte->toXML();
            $nodoAutotransporte = $this->xml_base->importNode($this->Autotransporte->importXML(), true);
		    $nodoMercancias->appendChild($nodoAutotransporte);
        }

        if(!empty($this->TransporteMaritimo))
        {
            $this->TransporteMaritimo->toXML();
            $nodoTransporteMaritimo = $this->xml_base->importNode($this->TransporteMaritimo->importXML(), true);
		    $nodoMercancias->appendChild($nodoTransporteMaritimo);
        }

        if(!empty($this->TransporteAereo))
        {
            $this->TransporteAereo->toXML();
            $nodoTransporteAereo = $this->xml_base->importNode($this->TransporteAereo->importXML(), true);
		    $nodoMercancias->appendChild($nodoTransporteAereo);
        }

        if(!empty($this->TransporteFerroviario))
        {
            $this->TransporteFerroviario->toXML();
            $nodoTransporteFerroviario = $this->xml_base->importNode($this->TransporteFerroviario->importXML(), true);
		    $nodoMercancias->appendChild($nodoTransporteFerroviario);
        }

		$this->xml_base->appendChild($nodoMercancias);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:Mercancias")->item(0);
		return $xml;
	}

    function addMercancia($PesoEnKg, $BienesTransp, $Descripcion, $Cantidad, $ClaveUnidad, $ClaveSTCC = null, $Unidad = null, $Dimensiones = null, $MaterialPeligroso = null, 
                $CveMaterialPeligroso = null, $Embalaje = null, $DescripEmbalaje = null, $ValorMercancia = null, $Moneda = null, $FraccionArancelaria = null, $UUIDComercioExt = null, 
                $SectorCOFEPRIS = null,
                $NombreIngredienteActivo = null,
                $NomQuimico = null,
                $DenominacionGenericaProd = null,
                $DenominacionDistintivaProd = null,
                $Fabricante = null,
                $FechaCaducidad = null,
                $LoteMedicamento = null,
                $FormaFarmaceutica = null,
                $CondicionesEspTransp = null,
                $RegistroSanitarioFolioAutorizacion = null,
                $PermisoImportacion = null,
                $FolioImpoVUCEM = null,
                $NumCAS = null,
                $RazonSocialEmpImp = null,
                $NumRegSanPlagCOFEPRIS = null,
                $DatosFabricante = null,
                $DatosFormulador = null,
                $DatosMaquilador = null,
                $UsoAutorizado = null, 
                $TipoMateria = null,
                $DescripcionMateria = null)
    {
		$Mercancia = new Mercancia31(
            $PesoEnKg, 
            $BienesTransp, 
            $Descripcion, 
            $Cantidad, 
            $ClaveUnidad,
            $ClaveSTCC, 
            $Unidad, 
            $Dimensiones, 
            $MaterialPeligroso, 
            $CveMaterialPeligroso, 
            $Embalaje, 
            $DescripEmbalaje, 
            $ValorMercancia, 
            $Moneda, 
            $FraccionArancelaria, 
            $UUIDComercioExt,     
            $SectorCOFEPRIS,
            $NombreIngredienteActivo,
            $NomQuimico,
            $DenominacionGenericaProd,
            $DenominacionDistintivaProd,
            $Fabricante,
            $FechaCaducidad,
            $LoteMedicamento,
            $FormaFarmaceutica,
            $CondicionesEspTransp,
            $RegistroSanitarioFolioAutorizacion,
            $PermisoImportacion,
            $FolioImpoVUCEM,
            $NumCAS,
            $RazonSocialEmpImp,
            $NumRegSanPlagCOFEPRIS,
            $DatosFabricante,
            $DatosFormulador,
            $DatosMaquilador,
            $UsoAutorizado, 
            $TipoMateria,
            $DescripcionMateria
        );
		
		// $Mercancia->validar();
		$this->Mercancia[] = $Mercancia;
		return $Mercancia;
    }

    function addAutotransporte($PermSCT, $NumPermisoSCT)
    {
		$Autotransporte = new Autotransporte31(
            $PermSCT, 
            $NumPermisoSCT
        );
		
		// $Autotransporte->validar();
		$this->Autotransporte = $Autotransporte;
		return $Autotransporte;
    }

    function addTransporteMaritimo($TipoEmbarcacion, $Matricula, $NumeroOMI, $NacionalidadEmbarc, $UnidadesDeArqBruto, $TipoCarga, $NumCertITC, $NombreAgenteNaviero, $NumAutorizacionNaviero, 
                $PermSCT = null, $NumPermisoSCT = null, $NombreAseg = null, $NumPolizaSeguro = null, $AnioEmbarcacion = null, $NombreEmbarc = null, $Eslora = null, 
                $Manga = null, $Calado = null, $LineaNaviera = null, $NumViaje = null, $NumConocEmbarc = null)
    {
		$TransporteMaritimo = new TransporteMaritimo31(
            $TipoEmbarcacion, 
            $Matricula, 
            $NumeroOMI, 
            $NacionalidadEmbarc, 
            $UnidadesDeArqBruto, 
            $TipoCarga, 
            $NumCertITC, 
            $NombreAgenteNaviero, 
            $NumAutorizacionNaviero, 
            $PermSCT, 
            $NumPermisoSCT, 
            $NombreAseg, 
            $NumPolizaSeguro, 
            $AnioEmbarcacion, 
            $NombreEmbarc, 
            $Eslora, 
            $Manga, 
            $Calado, 
            $LineaNaviera, 
            $NumViaje, 
            $NumConocEmbarc
        );
		
		// $TransporteMaritimo->validar();
		$this->TransporteMaritimo = $TransporteMaritimo;
		return $TransporteMaritimo;
    }

    function addTransporteAereo($PermSCT, $NumPermisoSCT, $MatriculaAeronave, $NumeroGuia, $CodigoTransportista, $NombreAseg = null, $NumPolizaSeguro = null, $LugarContrato = null,
                        $RFCTransportista = null, $NumRegIdTribTranspor = null, $ResidenciaFiscalTranspor = null, $NombreTransportista = null, $RFCEmbarcador = null, 
                        $NumRegIdTribEmbarc = null, $ResidenciaFiscalEmbarc = null, $NombreEmbarcador = null)
    {
		$TransporteAereo = new TransporteAereo31(
            $PermSCT, 
            $NumPermisoSCT, 
            $MatriculaAeronave, 
            $NumeroGuia, 
            $CodigoTransportista, 
            $NombreAseg,
            $NumPolizaSeguro, 
            $LugarContrato,
            $RFCTransportista, 
            $NumRegIdTribTranspor, 
            $ResidenciaFiscalTranspor, 
            $NombreTransportista, 
            $RFCEmbarcador, 
            $NumRegIdTribEmbarc, 
            $ResidenciaFiscalEmbarc, 
            $NombreEmbarcador
        );
		
		// $TransporteAereo->validar();
		$this->TransporteAereo = $TransporteAereo;
		return $TransporteAereo;
    }

    function addTransporteFerroviario($TipoDeServicio, $NombreAseg = null, $NumPolizaSeguro = null, $Concesionario = null, $TipoDeTrafico= null)
    {
        $TransporteFerroviario = new TransporteFerroviario31(
            $TipoDeServicio, 
            $NombreAseg, 
            $NumPolizaSeguro, 
            $Concesionario,
            $TipoDeTrafico
        );

        // $TransporteFerroviario->validar();
        $this->TransporteFerroviario = $TransporteFerroviario; 
        return $TransporteFerroviario;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.Mercancia

class Mercancia31
{
	// Obligatorios
	var $BienesTransp;
    var $Descripcion;
    var $Cantidad;
    var $ClaveUnidad;
	var $PesoEnKg;

	// opcionales
	var $ClaveSTCC;
    var $Unidad;
    var $Dimensiones;
    var $MaterialPeligroso;
    var $CveMaterialPeligroso;
    var $Embalaje;
    var $DescripEmbalaje;
    var $ValorMercancia;
    var $Moneda;
    var $FraccionArancelaria;
    var $UUIDComercioExt; 
    var $SectorCOFEPRIS;
    var $NombreIngredienteActivo;
    var $NomQuimico;
    var $DenominacionGenericaProd;
    var $DenominacionDistintivaProd;
    var $Fabricante;
    var $FechaCaducidad;
    var $LoteMedicamento;
    var $FormaFarmaceutica;
    var $CondicionesEspTransp;
    var $RegistroSanitarioFolioAutorizacion;
    var $PermisoImportacion;
    var $FolioImpoVUCEM;
    var $NumCAS;
    var $RazonSocialEmpImp;
    var $NumRegSanPlagCOFEPRIS;
    var $DatosFabricante;
    var $DatosFormulador;
    var $DatosMaquilador;
    var $UsoAutorizado;
    var $TipoMateria;
    var $DescripcionMateria;

    var $DocumentacionAduanera = [];
    var $GuiasIdentificacion = [];
    var $CantidadTransporta = [];
    var $DetalleMercancia;

	var $xml_base;
	var $logger;

	function __construct($PesoEnKg, $BienesTransp, $Descripcion, $Cantidad, $ClaveUnidad, $ClaveSTCC = null, $Unidad = null, $Dimensiones = null, $MaterialPeligroso = null, 
                $CveMaterialPeligroso = null, $Embalaje = null, $DescripEmbalaje = null, $ValorMercancia = null, $Moneda = null, $FraccionArancelaria = null, $UUIDComercioExt = null,
                $SectorCOFEPRIS = null,
                $NombreIngredienteActivo = null,
                $NomQuimico = null,
                $DenominacionGenericaProd = null,
                $DenominacionDistintivaProd = null,
                $Fabricante = null,
                $FechaCaducidad = null,
                $LoteMedicamento = null,
                $FormaFarmaceutica = null,
                $CondicionesEspTransp = null,
                $RegistroSanitarioFolioAutorizacion = null,
                $PermisoImportacion = null,
                $FolioImpoVUCEM = null,
                $NumCAS = null,
                $RazonSocialEmpImp = null,
                $NumRegSanPlagCOFEPRIS = null,
                $DatosFabricante = null,
                $DatosFormulador = null,
                $DatosMaquilador = null,
                $UsoAutorizado = null, 
                $TipoMateria = null,
                $DescripcionMateria = null)
    {
		$this->xml_base = null;
        $this->PesoEnKg = $PesoEnKg;
        $this->BienesTransp = $BienesTransp;
        $this->ClaveSTCC = $ClaveSTCC;
        $this->Descripcion = $Descripcion;
        $this->Cantidad = $Cantidad;
        $this->ClaveUnidad = $ClaveUnidad;
        $this->Unidad = $Unidad;
        $this->Dimensiones = $Dimensiones;
        $this->MaterialPeligroso = $MaterialPeligroso;
        $this->CveMaterialPeligroso = $CveMaterialPeligroso;
        $this->Embalaje = $Embalaje;
        $this->DescripEmbalaje = $DescripEmbalaje;
        $this->ValorMercancia = $ValorMercancia;
        $this->Moneda = $Moneda;
        $this->FraccionArancelaria = $FraccionArancelaria;
        $this->UUIDComercioExt = $UUIDComercioExt;

        $this->DocumentacionAduanera = [];
        $this->GuiasIdentificacion = [];
        $this->CantidadTransporta = [];
        $this->DetalleMercancia = null;

        $this->SectorCOFEPRIS = $SectorCOFEPRIS;
        $this->NombreIngredienteActivo = $NombreIngredienteActivo;
        $this->NomQuimico = $NomQuimico;
        $this->DenominacionGenericaProd = $DenominacionGenericaProd;
        $this->DenominacionDistintivaProd = $DenominacionDistintivaProd;
        $this->Fabricante = $Fabricante;
        $this->FechaCaducidad = $FechaCaducidad;
        $this->LoteMedicamento = $LoteMedicamento;
        $this->FormaFarmaceutica = $FormaFarmaceutica;
        $this->CondicionesEspTransp = $CondicionesEspTransp;
        $this->RegistroSanitarioFolioAutorizacion = $RegistroSanitarioFolioAutorizacion;
        $this->PermisoImportacion = $PermisoImportacion;
        $this->FolioImpoVUCEM = $FolioImpoVUCEM;
        $this->NumCAS = $NumCAS;
        $this->RazonSocialEmpImp = $RazonSocialEmpImp;
        $this->NumRegSanPlagCOFEPRIS = $NumRegSanPlagCOFEPRIS;
        $this->DatosFabricante = $DatosFabricante;
        $this->DatosFormulador = $DatosFormulador;
        $this->DatosMaquilador = $DatosMaquilador;
        $this->UsoAutorizado = $UsoAutorizado;
        $this->TipoMateria = $TipoMateria;
        $this->DescripcionMateria = $DescripcionMateria;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['PesoEnKg','BienesTransp', 'Descripcion', 'Cantidad', 'ClaveUnidad'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.Mercancia validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.Mercancia Campo Requerido: ' . $field);
			}
		}

		foreach($this->CantidadTransporta as $CantidadTransporta)
        {
			$CantidadTransporta->validar();
		}

		if($this->DetalleMercancia)
			$this->DetalleMercancia->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoMercancia = $this->xml_base->createElement("cartaporte31:Mercancia");

        $nodoMercancia->setAttribute('BienesTransp',  $this->BienesTransp);
        $nodoMercancia->setAttribute('Descripcion',  $this->Descripcion);
        $nodoMercancia->setAttribute('Cantidad',  $this->Cantidad);
        $nodoMercancia->setAttribute('ClaveUnidad',  $this->ClaveUnidad);
        $nodoMercancia->setAttribute('PesoEnKg',  $this->PesoEnKg);

        if($this->ClaveSTCC)
		    $nodoMercancia->setAttribute('ClaveSTCC',  $this->ClaveSTCC);
        if($this->Unidad)
		    $nodoMercancia->setAttribute('Unidad',  $this->Unidad);
        if($this->Dimensiones)
		    $nodoMercancia->setAttribute('Dimensiones',  $this->Dimensiones);
        if($this->MaterialPeligroso)
		    $nodoMercancia->setAttribute('MaterialPeligroso',  $this->MaterialPeligroso);
        if($this->CveMaterialPeligroso)
		    $nodoMercancia->setAttribute('CveMaterialPeligroso',  $this->CveMaterialPeligroso);
        if($this->Embalaje)
		    $nodoMercancia->setAttribute('Embalaje',  $this->Embalaje);
        if($this->DescripEmbalaje)
		    $nodoMercancia->setAttribute('DescripEmbalaje',  $this->DescripEmbalaje);
        if($this->ValorMercancia)
		    $nodoMercancia->setAttribute('ValorMercancia',  $this->ValorMercancia);
        if($this->Moneda)
		    $nodoMercancia->setAttribute('Moneda',  $this->Moneda);
        if($this->FraccionArancelaria)
		    $nodoMercancia->setAttribute('FraccionArancelaria',  $this->FraccionArancelaria);
        if($this->UUIDComercioExt)
		    $nodoMercancia->setAttribute('UUIDComercioExt',  $this->UUIDComercioExt);
        if($this->SectorCOFEPRIS)
		    $nodoMercancia->setAttribute('SectorCOFEPRIS',  $this->SectorCOFEPRIS);
        if($this->NombreIngredienteActivo)
		    $nodoMercancia->setAttribute('NombreIngredienteActivo',  $this->NombreIngredienteActivo);
        if($this->NomQuimico)
		    $nodoMercancia->setAttribute('NomQuimico',  $this->NomQuimico);
        if($this->DenominacionGenericaProd)
		    $nodoMercancia->setAttribute('DenominacionGenericaProd',  $this->DenominacionGenericaProd);
        if($this->DenominacionDistintivaProd)
		    $nodoMercancia->setAttribute('DenominacionDistintivaProd',  $this->DenominacionDistintivaProd);
        if($this->Fabricante)
		    $nodoMercancia->setAttribute('Fabricante',  $this->Fabricante);
        if($this->FechaCaducidad)
		    $nodoMercancia->setAttribute('FechaCaducidad',  $this->FechaCaducidad);
        if($this->LoteMedicamento)
		    $nodoMercancia->setAttribute('LoteMedicamento',  $this->LoteMedicamento);
        if($this->FormaFarmaceutica)
		    $nodoMercancia->setAttribute('FormaFarmaceutica',  $this->FormaFarmaceutica);
        if($this->CondicionesEspTransp)
		    $nodoMercancia->setAttribute('CondicionesEspTransp',  $this->CondicionesEspTransp);
        if($this->RegistroSanitarioFolioAutorizacion)
		    $nodoMercancia->setAttribute('RegistroSanitarioFolioAutorizacion',  $this->RegistroSanitarioFolioAutorizacion);
        if($this->PermisoImportacion)
		    $nodoMercancia->setAttribute('PermisoImportacion',  $this->PermisoImportacion);
        if($this->FolioImpoVUCEM)
		    $nodoMercancia->setAttribute('FolioImpoVUCEM',  $this->FolioImpoVUCEM);
        if($this->UUIDComercioExt)
		    $nodoMercancia->setAttribute('UUIDComercioExt',  $this->UUIDComercioExt);
        if($this->NumCAS)
		    $nodoMercancia->setAttribute('NumCAS',  $this->NumCAS);
        if($this->RazonSocialEmpImp)
		    $nodoMercancia->setAttribute('RazonSocialEmpImp',  $this->RazonSocialEmpImp);
        if($this->NumRegSanPlagCOFEPRIS)
		    $nodoMercancia->setAttribute('NumRegSanPlagCOFEPRIS',  $this->NumRegSanPlagCOFEPRIS);
        if($this->DatosFabricante)
		    $nodoMercancia->setAttribute('DatosFabricante',  $this->DatosFabricante);
        if($this->DatosFormulador)
		    $nodoMercancia->setAttribute('DatosFormulador',  $this->DatosFormulador);
        if($this->DatosMaquilador)
		    $nodoMercancia->setAttribute('DatosMaquilador',  $this->DatosMaquilador);
        if($this->UsoAutorizado)
		    $nodoMercancia->setAttribute('UsoAutorizado',  $this->UsoAutorizado);
        if($this->TipoMateria)
		    $nodoMercancia->setAttribute('TipoMateria',  $this->TipoMateria);
        if($this->DescripcionMateria)
		    $nodoMercancia->setAttribute('DescripcionMateria',  $this->DescripcionMateria);

        if(!empty($this->DocumentacionAduanera))
        {
            foreach ($this->DocumentacionAduanera as $key => $DocumentacionAduanera) 
            {
                $DocumentacionAduanera->toXML();
                $nodoDocumentacionAduanera = $this->xml_base->importNode($DocumentacionAduanera->importXML(), true);
                $nodoMercancia->appendChild($nodoDocumentacionAduanera);
            }
        }

        if(!empty($this->GuiasIdentificacion))
        {
            foreach ($this->GuiasIdentificacion as $key => $GuiasIdentificacion) 
            {
                $GuiasIdentificacion->toXML();
                $nodoGuiasIdentificacion = $this->xml_base->importNode($GuiasIdentificacion->importXML(), true);
                $nodoMercancia->appendChild($nodoGuiasIdentificacion);
            }
        }

        if(!empty($this->CantidadTransporta))
        {
            foreach ($this->CantidadTransporta as $key => $CantidadTransporta) 
            {
                $CantidadTransporta->toXML();
                $nodoCantidadTransporta = $this->xml_base->importNode($CantidadTransporta->importXML(), true);
                $nodoMercancia->appendChild($nodoCantidadTransporta);
            }
        }

        if(!empty($this->DetalleMercancia))
        {
            $this->DetalleMercancia->toXML();
            $nodoDetalleMercancia = $this->xml_base->importNode($this->DetalleMercancia->importXML(), true);
		    $nodoMercancia->appendChild($nodoDetalleMercancia);
        }

		$this->xml_base->appendChild($nodoMercancia);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:Mercancia")->item(0);
		return $xml;
	}

    function addDocumentacionAduanera($TipoDocumento, $NumPedimento = null, $IdentDocAduanero = null, $RFCImpo = null)
    {
		$DocumentacionAduanera = new DocumentacionAduanera31(
            $TipoDocumento, 
            $NumPedimento, 
            $IdentDocAduanero, 
            $RFCImpo 
        );
		
		// $DocumentacionAduanera->validar();
		$this->DocumentacionAduanera[] = $DocumentacionAduanera;
		return $DocumentacionAduanera;
    }

    function addGuiasIdentificacion($NumeroGuiaIdentificacion, $DescripGuiaIdentificacion, $PesoGuiaIdentificacion)
    {
		$GuiasIdentificacion = new GuiasIdentificacion31(
            $NumeroGuiaIdentificacion, 
            $DescripGuiaIdentificacion, 
            $PesoGuiaIdentificacion 
        );
		
		// $GuiasIdentificacion->validar();
		$this->GuiasIdentificacion[] = $GuiasIdentificacion;
		return $GuiasIdentificacion;
    }

    function addCantidadTransporta($Cantidad, $IDOrigen, $IDDestino, $CvesTransporte = null)
    {
		$CantidadTransporta = new CantidadTransporta31(
            $Cantidad, 
            $IDOrigen, 
            $IDDestino, 
            $CvesTransporte
        );
		
		// $CantidadTransporta->validar();
		$this->CantidadTransporta[] = $CantidadTransporta;
		return $CantidadTransporta;
    }

    function addDetalleMercancia($UnidadPesoMerc, $PesoBruto, $PesoNeto, $PesoTara, $NumPiezas = null)
    {
		$DetalleMercancia = new DetalleMercancia31(
            $UnidadPesoMerc, 
            $PesoBruto, 
            $PesoNeto, 
            $PesoTara,
            $NumPiezas
        );
		
		// $DetalleMercancia->validar();
		$this->DetalleMercancia = $DetalleMercancia;
		return $DetalleMercancia;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.Mercancia.DocumentacionAduanera

class DocumentacionAduanera31
{
	// Obligatorios
	var $TipoDocumento;

    // Opcionales
    var $NumPedimento;
    var $IdentDocAduanero;
    var $RFCImpo;

	var $xml_base;
	var $logger;

	function __construct($TipoDocumento, $NumPedimento = null, $IdentDocAduanero = null, $RFCImpo = null)
    {
		$this->xml_base = null;
        $this->TipoDocumento = $TipoDocumento;
        $this->NumPedimento = $NumPedimento;
        $this->IdentDocAduanero = $IdentDocAduanero;
        $this->RFCImpo = $RFCImpo;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoDocumento'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.Mercancia.DocumentacionAduanera validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.Mercancia.DocumentacionAduanera Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoDocumentacionAduanera = $this->xml_base->createElement("cartaporte31:DocumentacionAduanera");

        $nodoDocumentacionAduanera->setAttribute('TipoDocumento',  $this->TipoDocumento);
        if($this->NumPedimento)
		    $nodoDocumentacionAduanera->setAttribute('NumPedimento',  $this->NumPedimento);
        if($this->IdentDocAduanero)
		    $nodoDocumentacionAduanera->setAttribute('IdentDocAduanero',  $this->IdentDocAduanero);
        if($this->RFCImpo)
		    $nodoDocumentacionAduanera->setAttribute('RFCImpo',  $this->RFCImpo);

		$this->xml_base->appendChild($nodoDocumentacionAduanera);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:DocumentacionAduanera")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.Mercancia.GuiasIdentificacion

class GuiasIdentificacion31
{
	// Obligatorios
	var $NumeroGuiaIdentificacion;
	var $DescripGuiaIdentificacion;
	var $PesoGuiaIdentificacion;

	var $xml_base;
	var $logger;

	function __construct($NumeroGuiaIdentificacion, $DescripGuiaIdentificacion, $PesoGuiaIdentificacion)
    {
		$this->xml_base = null;
        $this->NumeroGuiaIdentificacion = $NumeroGuiaIdentificacion;
        $this->DescripGuiaIdentificacion = $DescripGuiaIdentificacion;
        $this->PesoGuiaIdentificacion = $PesoGuiaIdentificacion;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['NumeroGuiaIdentificacion', 'DescripGuiaIdentificacion', 'PesoGuiaIdentificacion'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.Mercancia.GuiasIdentificacion validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.Mercancia.GuiasIdentificacion Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoGuiasIdentificacion = $this->xml_base->createElement("cartaporte31:GuiasIdentificacion");

        $nodoGuiasIdentificacion->setAttribute('NumeroGuiaIdentificacion',  $this->NumeroGuiaIdentificacion);
        $nodoGuiasIdentificacion->setAttribute('DescripGuiaIdentificacion',  $this->DescripGuiaIdentificacion);
        $nodoGuiasIdentificacion->setAttribute('PesoGuiaIdentificacion',  $this->PesoGuiaIdentificacion);

		$this->xml_base->appendChild($nodoGuiasIdentificacion);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:GuiasIdentificacion")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.Mercancia.CantidadTransporta

class CantidadTransporta31
{
	// Obligatorios
	var $Cantidad;
	var $IDOrigen;
	var $IDDestino;

	// opcionales
	var $CvesTransporte;

	var $xml_base;
	var $logger;

	function __construct($Cantidad, $IDOrigen, $IDDestino, $CvesTransporte = null)
    {
		$this->xml_base = null;
        $this->Cantidad = $Cantidad;
        $this->IDOrigen = $IDOrigen;
        $this->IDDestino = $IDDestino;
        $this->CvesTransporte = $CvesTransporte;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['Cantidad', 'IDOrigen', 'IDDestino'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.Mercancia.CantidadTransporta validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.Mercancia.CantidadTransporta Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoCantidadTransporta = $this->xml_base->createElement("cartaporte31:CantidadTransporta");

        $nodoCantidadTransporta->setAttribute('Cantidad',  $this->Cantidad);
        $nodoCantidadTransporta->setAttribute('IDOrigen',  $this->IDOrigen);
        $nodoCantidadTransporta->setAttribute('IDDestino',  $this->IDDestino);
        if($this->CvesTransporte)
		    $nodoCantidadTransporta->setAttribute('CvesTransporte',  $this->CvesTransporte);

		$this->xml_base->appendChild($nodoCantidadTransporta);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:CantidadTransporta")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.Mercancia.DetalleMercancia

class DetalleMercancia31
{
	// Obligatorios
	var $UnidadPesoMerc;
	var $PesoBruto;
	var $PesoNeto;
	var $PesoTara;

	// opcionales
	var $NumPiezas;

	var $xml_base;
	var $logger;

	function __construct($UnidadPesoMerc, $PesoBruto, $PesoNeto, $PesoTara, $NumPiezas = null)
    {
		$this->xml_base = null;
        $this->UnidadPesoMerc = $UnidadPesoMerc;
        $this->PesoBruto = $PesoBruto;
        $this->PesoNeto = $PesoNeto;
        $this->PesoTara = $PesoTara;
        $this->NumPiezas = $NumPiezas;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['UnidadPesoMerc', 'PesoBruto', 'PesoNeto', 'PesoTara'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.Mercancia.DetalleMercancia validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.Mercancia.DetalleMercancia Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoDetalleMercancia = $this->xml_base->createElement("cartaporte31:DetalleMercancia");

        $nodoDetalleMercancia->setAttribute('UnidadPesoMerc',  $this->UnidadPesoMerc);
        $nodoDetalleMercancia->setAttribute('PesoBruto',  $this->PesoBruto);
        $nodoDetalleMercancia->setAttribute('PesoNeto',  $this->PesoNeto);
        $nodoDetalleMercancia->setAttribute('PesoTara',  $this->PesoTara);
        if($this->NumPiezas)
		    $nodoDetalleMercancia->setAttribute('NumPiezas',  $this->NumPiezas);

		$this->xml_base->appendChild($nodoDetalleMercancia);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:DetalleMercancia")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.Autotransporte

class Autotransporte31
{
	// Obligatorios
	var $PermSCT;
	var $NumPermisoSCT;

	var $IdentificacionVehicular;
	var $Seguros;
    var $Remolques = [];

	var $xml_base;
	var $logger;

	function __construct($PermSCT, $NumPermisoSCT)
    {
		$this->xml_base = null;
        $this->PermSCT = $PermSCT;
        $this->NumPermisoSCT = $NumPermisoSCT;

        $this->Remolques = [];
        $this->IdentificacionVehicular = null;
        $this->Seguros = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['PermSCT', 'NumPermisoSCT'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.Autotransporte validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.Autotransporte Campo Requerido: ' . $field);
			}
		}

        if(count($this->Remolques) > 2)
        {
            $this->logger->write("Complemento.CartaPorte31.Mercancias.Autotransporte.Remolques. Debe existir un maximo de 2 elementos Remolque.");
            throw new Exception('Complemento.CartaPorte31.Mercancias.Autotransporte.Remolques Debe existir un maximo de 2 elementos Remolque.');
        }

		foreach($this->Remolques as $Remolques)
        {
			$Remolques->validar();
		}

        if(empty($this->Seguros))
        {
            $this->logger->write("Complemento.CartaPorte31.Mercancias.Autotransporte. Debe existir un elemento Seguros.");
            throw new Exception('Complemento.CartaPorte31.Mercancias.Autotransporte. Debe existir un elemento Seguros.');
        }

        if(empty($this->IdentificacionVehicular))
        {
            $this->logger->write("Complemento.CartaPorte31.Mercancias.Autotransporte. Debe existir un elemento IdentificacionVehicular.");
            throw new Exception('Complemento.CartaPorte31.Mercancias.Autotransporte. Debe existir un elemento IdentificacionVehicular.');
        }

		if($this->Seguros)
			$this->Seguros->validar();

		if($this->IdentificacionVehicular)
			$this->IdentificacionVehicular->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoAutotransporte = $this->xml_base->createElement("cartaporte31:Autotransporte");

        $nodoAutotransporte->setAttribute('PermSCT',  $this->PermSCT);
        $nodoAutotransporte->setAttribute('NumPermisoSCT',  $this->NumPermisoSCT);

        if(!empty($this->IdentificacionVehicular))
        {
            $this->IdentificacionVehicular->toXML();
            $nodoIdentificacionVehicular = $this->xml_base->importNode($this->IdentificacionVehicular->importXML(), true);
		    $nodoAutotransporte->appendChild($nodoIdentificacionVehicular);
        }

        if(!empty($this->Seguros))
        {
            $this->Seguros->toXML();
            $nodoSeguros = $this->xml_base->importNode($this->Seguros->importXML(), true);
		    $nodoAutotransporte->appendChild($nodoSeguros);
        }

        if(!empty($this->Remolques))
        {
		    $nodoRemolques = $this->xml_base->createElement("cartaporte31:Remolques");

            foreach ($this->Remolques as $key => $Remolque) 
            {
                $Remolque->toXML();
                $nodoRemolque = $this->xml_base->importNode($Remolque->importXML(), true);
                $nodoRemolques->appendChild($nodoRemolque);
            }
            
		    $nodoAutotransporte->appendChild($nodoRemolques);
        }

		$this->xml_base->appendChild($nodoAutotransporte);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:Autotransporte")->item(0);
		return $xml;
	}

    function addRemolque($SubTipoRem, $Placa)
    {
		$Remolque = new Remolque31(
            $SubTipoRem, 
            $Placa
        );
		
		// $Remolque->validar();
		$this->Remolques[] = $Remolque;
		return $Remolque;
    }

    function addSeguros($AseguraRespCivil, $PolizaRespCivil, $AseguraMedAmbiente = null, $PolizaMedAmbiente = null, $AseguraCarga = null, $PolizaCarga = null, 
        $PrimaSeguro = null)
    {
		$Seguros = new Seguros31(
            $AseguraRespCivil, 
            $PolizaRespCivil, 
            $AseguraMedAmbiente, 
            $PolizaMedAmbiente, 
            $AseguraCarga, 
            $PolizaCarga, 
            $PrimaSeguro
        );
		
		// $Seguros->validar();
		$this->Seguros = $Seguros;
		return $Seguros;
    }

    function addIdentificacionVehicular($ConfigVehicular, $PlacaVM, $AnioModeloVM, $PesoBrutoVehicular)
    {
		$IdentificacionVehicular = new IdentificacionVehicular31(
            $ConfigVehicular, 
            $PlacaVM, 
            $AnioModeloVM,
            $PesoBrutoVehicular
        );
		
		// $IdentificacionVehicular->validar();
		$this->IdentificacionVehicular = $IdentificacionVehicular;
		return $IdentificacionVehicular;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.Autotransporte.IdentificacionVehicular

class IdentificacionVehicular31
{
	// Obligatorios
	var $ConfigVehicular;
	var $PlacaVM;
	var $AnioModeloVM;
    var $PesoBrutoVehicular;

	var $xml_base;
	var $logger;

	function __construct($ConfigVehicular, $PlacaVM, $AnioModeloVM, $PesoBrutoVehicular)
    {
		$this->xml_base = null;
        $this->ConfigVehicular = $ConfigVehicular;
        $this->PlacaVM = $PlacaVM;
        $this->AnioModeloVM = $AnioModeloVM;
        $this->PesoBrutoVehicular = $PesoBrutoVehicular;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['ConfigVehicular', 'PlacaVM', 'AnioModeloVM', 'PesoBrutoVehicular'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.Autotransporte.IdentificacionVehicular validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.Autotransporte.IdentificacionVehicular Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoIdentificacionVehicular = $this->xml_base->createElement("cartaporte31:IdentificacionVehicular");

        $nodoIdentificacionVehicular->setAttribute('ConfigVehicular',  $this->ConfigVehicular);
        $nodoIdentificacionVehicular->setAttribute('PesoBrutoVehicular',  $this->PesoBrutoVehicular);
        $nodoIdentificacionVehicular->setAttribute('PlacaVM',  $this->PlacaVM);
        $nodoIdentificacionVehicular->setAttribute('AnioModeloVM',  $this->AnioModeloVM);

		$this->xml_base->appendChild($nodoIdentificacionVehicular);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:IdentificacionVehicular")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.Autotransporte.Seguros

class Seguros31
{
	// Obligatorios
	var $AseguraRespCivil;
	var $PolizaRespCivil;

    // Opcionales
    var $AseguraMedAmbiente;
    var $PolizaMedAmbiente;
    var $AseguraCarga;
    var $PolizaCarga;
    var $PrimaSeguro;

	var $xml_base;
	var $logger;

	function __construct($AseguraRespCivil, $PolizaRespCivil, $AseguraMedAmbiente = null, $PolizaMedAmbiente = null, $AseguraCarga = null, $PolizaCarga = null, $PrimaSeguro = null)
    {
		$this->xml_base = null;
        $this->AseguraRespCivil = $AseguraRespCivil;
        $this->PolizaRespCivil = $PolizaRespCivil;
        $this->AseguraMedAmbiente = $AseguraMedAmbiente;
        $this->PolizaMedAmbiente = $PolizaMedAmbiente;
        $this->AseguraCarga = $AseguraCarga;
        $this->PolizaCarga = $PolizaCarga;
        $this->PrimaSeguro = $PrimaSeguro;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['AseguraRespCivil', 'PolizaRespCivil'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.Autotransporte.Seguros validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.Autotransporte.Seguros Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoSeguros = $this->xml_base->createElement("cartaporte31:Seguros");

        $nodoSeguros->setAttribute('AseguraRespCivil',  $this->AseguraRespCivil);
        $nodoSeguros->setAttribute('PolizaRespCivil',  $this->PolizaRespCivil);
        if($this->AseguraMedAmbiente)
            $nodoSeguros->setAttribute('AseguraMedAmbiente',  $this->AseguraMedAmbiente);
        if($this->PolizaMedAmbiente)
            $nodoSeguros->setAttribute('PolizaMedAmbiente',  $this->PolizaMedAmbiente);
        if($this->AseguraCarga)
            $nodoSeguros->setAttribute('AseguraCarga',  $this->AseguraCarga);
        if($this->PolizaCarga)
            $nodoSeguros->setAttribute('PolizaCarga',  $this->PolizaCarga);
        if($this->PrimaSeguro)
            $nodoSeguros->setAttribute('PrimaSeguro',  $this->PrimaSeguro);

		$this->xml_base->appendChild($nodoSeguros);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:Seguros")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.Autotransporte.Remolques.Remolque

class Remolque31
{
	// Obligatorios
	var $SubTipoRem;
	var $Placa;

	var $xml_base;
	var $logger;

	function __construct($SubTipoRem, $Placa)
    {
		$this->xml_base = null;
        $this->SubTipoRem = $SubTipoRem;
        $this->Placa = $Placa;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['SubTipoRem', 'Placa'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.Autotransporte.Remolques.Remolque validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.Autotransporte.Remolques.Remolque Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoRemolque = $this->xml_base->createElement("cartaporte31:Remolque");

        $nodoRemolque->setAttribute('SubTipoRem',  $this->SubTipoRem);
        $nodoRemolque->setAttribute('Placa',  $this->Placa);

		$this->xml_base->appendChild($nodoRemolque);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:Remolque")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.TransporteMaritimo

class TransporteMaritimo31
{
	// Obligatorios
	var $TipoEmbarcacion;
	var $Matricula;
	var $NumeroOMI;
	var $NacionalidadEmbarc;
	var $UnidadesDeArqBruto;
	var $TipoCarga;
	var $NumCertITC;
	var $NombreAgenteNaviero;
	var $NumAutorizacionNaviero;

	// opcionales
	var $PermSCT;
	var $NumPermisoSCT;
    var $NombreAseg;
    var $NumPolizaSeguro;
    var $AnioEmbarcacion;
    var $NombreEmbarc;
    var $Eslora;
    var $Manga;
    var $Calado;
    var $LineaNaviera;
    var $NumViaje;
    var $NumConocEmbarc;

    var $Contenedor = [];

	var $xml_base;
	var $logger;

	function __construct($TipoEmbarcacion, $Matricula, $NumeroOMI, $NacionalidadEmbarc, $UnidadesDeArqBruto, $TipoCarga, $NumCertITC, $NombreAgenteNaviero, $NumAutorizacionNaviero, 
                $PermSCT = null, $NumPermisoSCT = null, $NombreAseg = null, $NumPolizaSeguro = null, $AnioEmbarcacion = null, $NombreEmbarc = null, $Eslora = null, 
                $Manga = null, $Calado = null, $LineaNaviera = null, $NumViaje = null, $NumConocEmbarc = null)
    {
		$this->xml_base = null;
        $this->TipoEmbarcacion = $TipoEmbarcacion;
        $this->Matricula = $Matricula;
        $this->NumeroOMI = $NumeroOMI;
        $this->NacionalidadEmbarc = $NacionalidadEmbarc;
        $this->UnidadesDeArqBruto = $UnidadesDeArqBruto;
        $this->TipoCarga = $TipoCarga;
        $this->NumCertITC = $NumCertITC;
        $this->NombreAgenteNaviero = $NombreAgenteNaviero;
        $this->NumAutorizacionNaviero = $NumAutorizacionNaviero;
        $this->PermSCT = $PermSCT;
        $this->NumPermisoSCT = $NumPermisoSCT;
        $this->NombreAseg = $NombreAseg;
        $this->NumPolizaSeguro = $NumPolizaSeguro;
        $this->AnioEmbarcacion = $AnioEmbarcacion;
        $this->NombreEmbarc = $NombreEmbarc;
        $this->Eslora = $Eslora;
        $this->Manga = $Manga;
        $this->Calado = $Calado;
        $this->LineaNaviera = $LineaNaviera;        
        $this->NumViaje = $NumViaje;
        $this->NumConocEmbarc = $NumConocEmbarc;

        $this->Contenedor = [];

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoEmbarcacion', 'Matricula', 'NumeroOMI', 'NacionalidadEmbarc', 'UnidadesDeArqBruto', 'TipoCarga', 'NumCertITC', 'NombreAgenteNaviero', 'NumAutorizacionNaviero'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteMaritimo validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.TransporteMaritimo Campo Requerido: ' . $field);
			}
		}

        if(count($this->Contenedor) == 0)
        {
            $this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteMaritimo. Debe existir un elemento Contenedor.");
            throw new Exception('Complemento.CartaPorte31.Mercancias.TransporteMaritimo. Debe existir un elemento Contenedor.');
        }

		foreach($this->Contenedor as $Contenedor)
        {
			$Contenedor->validar();
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoTransporteMaritimo = $this->xml_base->createElement("cartaporte31:TransporteMaritimo");

        $nodoTransporteMaritimo->setAttribute('TipoEmbarcacion',  $this->TipoEmbarcacion);
        $nodoTransporteMaritimo->setAttribute('Matricula',  $this->Matricula);
        $nodoTransporteMaritimo->setAttribute('NumeroOMI',  $this->NumeroOMI);
        $nodoTransporteMaritimo->setAttribute('NacionalidadEmbarc',  $this->NacionalidadEmbarc);
        $nodoTransporteMaritimo->setAttribute('UnidadesDeArqBruto',  $this->UnidadesDeArqBruto);
        $nodoTransporteMaritimo->setAttribute('TipoCarga',  $this->TipoCarga);
        $nodoTransporteMaritimo->setAttribute('NumCertITC',  $this->NumCertITC);
        $nodoTransporteMaritimo->setAttribute('NombreAgenteNaviero',  $this->NombreAgenteNaviero);
        $nodoTransporteMaritimo->setAttribute('NumAutorizacionNaviero',  $this->NumAutorizacionNaviero);
        if($this->PermSCT)
		    $nodoTransporteMaritimo->setAttribute('PermSCT',  $this->PermSCT);
        if($this->NumPermisoSCT)
		    $nodoTransporteMaritimo->setAttribute('NumPermisoSCT',  $this->NumPermisoSCT);
        if($this->NombreAseg)
		    $nodoTransporteMaritimo->setAttribute('NombreAseg',  $this->NombreAseg);
        if($this->NumPolizaSeguro)
		    $nodoTransporteMaritimo->setAttribute('NumPolizaSeguro',  $this->NumPolizaSeguro);
        if($this->AnioEmbarcacion)
		    $nodoTransporteMaritimo->setAttribute('AnioEmbarcacion',  $this->AnioEmbarcacion);
        if($this->NombreEmbarc)
		    $nodoTransporteMaritimo->setAttribute('NombreEmbarc',  $this->NombreEmbarc);
        if($this->Eslora)
		    $nodoTransporteMaritimo->setAttribute('Eslora',  $this->Eslora);
        if($this->Manga)
		    $nodoTransporteMaritimo->setAttribute('Manga',  $this->Manga);
        if($this->Calado)
		    $nodoTransporteMaritimo->setAttribute('Calado',  $this->Calado);
        if($this->NumViaje)
		    $nodoTransporteMaritimo->setAttribute('NumViaje',  $this->NumViaje);
        // if($this->ValorMercancia)
		//     $nodoTransporteMaritimo->setAttribute('ValorMercancia',  $this->ValorMercancia);
        if($this->NumConocEmbarc)
		    $nodoTransporteMaritimo->setAttribute('NumConocEmbarc',  $this->NumConocEmbarc);

        if(!empty($this->Contenedor))
        {
            foreach ($this->Contenedor as $key => $Contenedor) 
            {
                $Contenedor->toXML();
                $nodoContenedor = $this->xml_base->importNode($Contenedor->importXML(), true);
                $nodoTransporteMaritimo->appendChild($nodoContenedor);
            }
        }

		$this->xml_base->appendChild($nodoTransporteMaritimo);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:TransporteMaritimo")->item(0);
		return $xml;
	}

    function addContenedor($MatriculaContenedor, $TipoContenedor, $NumPrecinto = null)
    {
		$Contenedor = new ContenedorTransporteMaritimo31(
            $MatriculaContenedor, 
            $TipoContenedor, 
            $NumPrecinto
        );
		
		// $Contenedor->validar();
		$this->Contenedor[] = $Contenedor;
		return $Contenedor;

    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.TransporteMaritimo.Contenedor

class ContenedorTransporteMaritimo31
{
	// Obligatorios
	var $MatriculaContenedor;
	var $TipoContenedor;

    // opcionales
    var $NumPrecinto;
    var $RemolquesCCP = [];

	var $xml_base;
	var $logger;

	function __construct($MatriculaContenedor, $TipoContenedor, $NumPrecinto = null)
    {
		$this->xml_base = null;
        $this->MatriculaContenedor = $MatriculaContenedor;
        $this->TipoContenedor = $TipoContenedor;
        $this->NumPrecinto = $NumPrecinto;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['MatriculaContenedor', 'TipoContenedor'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteMaritimo.Contenedor validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.TransporteMaritimo.Contenedor Campo Requerido: ' . $field);
			}
		}

        if(!empty($this->RemolquesCCP)) {
            if($this->TipoContenedor === "CM011") {
                $this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteMaritimo.Contenedor.RemolquesCCP validar(): La clave del atributo 'TipoContenedor' debe ser 'CM011' para incluir este nodo");
				throw new Exception("Complemento.CartaPorte31.Mercancias.TransporteMaritimo.Contenedor.RemolquesCCP: La clave del atributo 'TipoContenedor' debe ser 'CM011' para incluir este nodo");
            }
            if(count($this->RemolquesCCP) > 2) {
                $this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteMaritimo.Contenedor.RemolquesCCP validar(): Solo hasta 2 nodos RemolqueCCP");
				throw new Exception('Complemento.CartaPorte31.Mercancias.TransporteMaritimo.Contenedor.RemolquesCCP: Solo hasta 2 nodos RemolqueCCP');
            }
        }
	}

    function addRemolqueCCP($SubTipoRemCCP, $PlacaCCP){
        $RemolqueCCP = new RemolqueCCP31(
            $SubTipoRemCCP,
            $PlacaCCP
        );

        $this->RemolquesCCP[] = $RemolqueCCP;
        return $RemolqueCCP;
    }

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoContenedor = $this->xml_base->createElement("cartaporte31:Contenedor");
        $nodoContenedor->setAttribute('MatriculaContenedor',  $this->MatriculaContenedor);
        $nodoContenedor->setAttribute('TipoContenedor',  $this->TipoContenedor);
        if($this->NumPrecinto)
		    $nodoContenedor->setAttribute('NumPrecinto',  $this->NumPrecinto);

        if(!empty($this->RemolquesCCP))
        {
            $nodoRemolquesCCP = $this->xml_base->createElement("cartaporte31:RemolquesCCP");

            foreach ($this->RemolquesCCP as $RemolqueCCP) 
            {
                $RemolqueCCP->toXML();
                $nodoRemolqueCCP = $this->xml_base->importNode($RemolqueCCP->importXML(), true);
                $nodoRemolquesCCP->appendChild($nodoRemolqueCCP);
            }
            
            $nodoContenedor->appendChild($nodoRemolquesCCP);
        }
		$this->xml_base->appendChild($nodoContenedor);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:Contenedor")->item(0);
		return $xml;
	}
}

// clase que crea el nodo de CartaPorte31.Mercancias.TransporteMaritimo.Contenedor.RemolquesCCP.RemolqueCCP
class RemolqueCCP31
{
    var $SubTipoRemCCP;
	var $PlacaCCP;

	var $xml_base;
	var $logger;

	function __construct($SubTipoRemCCP, $PlacaCCP)
    {
		$this->xml_base = null;
        $this->SubTipoRemCCP = $SubTipoRemCCP;
        $this->PlacaCCP = $PlacaCCP;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['SubTipoRemCCP', 'PlacaCCP'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteMaritimo.Contenedor.RemolquesCCP.RemolqueCCP validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.TransporteMaritimo.Contenedor.RemolquesCCP.RemolqueCCP Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoRemolqueCCP = $this->xml_base->createElement("cartaporte31:RemolqueCCP");

        $nodoRemolqueCCP->setAttribute('SubTipoRemCCP',  $this->SubTipoRemCCP);
        $nodoRemolqueCCP->setAttribute('PlacaCCP',  $this->PlacaCCP);

		$this->xml_base->appendChild($nodoRemolqueCCP);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:RemolqueCCP")->item(0);
		return $xml;
	}
}
//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.TransporteAereo

class TransporteAereo31
{
	// Obligatorios
	var $PermSCT;
	var $NumPermisoSCT;
	var $MatriculaAeronave;
    var $NumeroGuia;
    var $CodigoTransportista;


    // opcionales
    var $NombreAseg;
    var $NumPolizaSeguro;
    var $LugarContrato;
    var $RFCTransportista;
    var $NumRegIdTribTranspor;
    var $ResidenciaFiscalTranspor;
    var $NombreTransportista;
    var $RFCEmbarcador;
    var $NumRegIdTribEmbarc;
    var $ResidenciaFiscalEmbarc;
    var $NombreEmbarcador;

	var $xml_base;
	var $logger;

	function __construct($PermSCT, $NumPermisoSCT, $MatriculaAeronave, $NumeroGuia, $CodigoTransportista, $NombreAseg = null, $NumPolizaSeguro = null, $LugarContrato = null,
                        $RFCTransportista = null, $NumRegIdTribTranspor = null, $ResidenciaFiscalTranspor = null, $NombreTransportista = null, $RFCEmbarcador = null, 
                        $NumRegIdTribEmbarc = null, $ResidenciaFiscalEmbarc = null, $NombreEmbarcador = null)
    {
		$this->xml_base = null;
        $this->PermSCT = $PermSCT;
        $this->NumPermisoSCT = $NumPermisoSCT;
        $this->MatriculaAeronave = $MatriculaAeronave;
        $this->NumeroGuia = $NumeroGuia;
        $this->CodigoTransportista = $CodigoTransportista;
        $this->NombreAseg = $NombreAseg;
        $this->NumPolizaSeguro = $NumPolizaSeguro;
        $this->LugarContrato = $LugarContrato;
        $this->RFCTransportista = $RFCTransportista;
        $this->NumRegIdTribTranspor = $NumRegIdTribTranspor;
        $this->ResidenciaFiscalTranspor = $ResidenciaFiscalTranspor;
        $this->NombreTransportista = $NombreTransportista;
        $this->RFCEmbarcador = $RFCEmbarcador;
        $this->NumRegIdTribEmbarc = $NumRegIdTribEmbarc;
        $this->ResidenciaFiscalEmbarc = $ResidenciaFiscalEmbarc;
        $this->NombreEmbarcador = $NombreEmbarcador;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['PermSCT', 'NumPermisoSCT', 'MatriculaAeronave', 'NumeroGuia', 'CodigoTransportista'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteAereo validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.TransporteAereo Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoTransporteAereo = $this->xml_base->createElement("cartaporte31:TransporteAereo");

        $nodoTransporteAereo->setAttribute('PermSCT',  $this->PermSCT);
        $nodoTransporteAereo->setAttribute('NumPermisoSCT',  $this->NumPermisoSCT);
        $nodoTransporteAereo->setAttribute('MatriculaAeronave',  $this->MatriculaAeronave);
        $nodoTransporteAereo->setAttribute('NumeroGuia',  $this->NumeroGuia);
        $nodoTransporteAereo->setAttribute('CodigoTransportista',  $this->CodigoTransportista);
        if($this->NombreAseg)
            $nodoTransporteAereo->setAttribute('NombreAseg',  $this->NombreAseg);
        if($this->NumPolizaSeguro)
            $nodoTransporteAereo->setAttribute('NumPolizaSeguro',  $this->NumPolizaSeguro);
        if($this->LugarContrato)
            $nodoTransporteAereo->setAttribute('LugarContrato',  $this->LugarContrato);
        if($this->RFCTransportista)
            $nodoTransporteAereo->setAttribute('RFCTransportista',  $this->RFCTransportista);
        if($this->NumRegIdTribTranspor)
            $nodoTransporteAereo->setAttribute('NumRegIdTribTranspor',  $this->NumRegIdTribTranspor);
        if($this->ResidenciaFiscalTranspor)
            $nodoTransporteAereo->setAttribute('ResidenciaFiscalTranspor',  $this->ResidenciaFiscalTranspor);
        if($this->NombreTransportista)
            $nodoTransporteAereo->setAttribute('NombreTransportista',  $this->NombreTransportista);
        if($this->RFCEmbarcador)
            $nodoTransporteAereo->setAttribute('RFCEmbarcador',  $this->RFCEmbarcador);
        if($this->NumRegIdTribEmbarc)
            $nodoTransporteAereo->setAttribute('NumRegIdTribEmbarc',  $this->NumRegIdTribEmbarc);
        if($this->ResidenciaFiscalEmbarc)
            $nodoTransporteAereo->setAttribute('ResidenciaFiscalEmbarc',  $this->ResidenciaFiscalEmbarc);
        if($this->NombreEmbarcador)
            $nodoTransporteAereo->setAttribute('NombreEmbarcador',  $this->NombreEmbarcador);

		$this->xml_base->appendChild($nodoTransporteAereo);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:TransporteAereo")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.TransporteFerroviario

class TransporteFerroviario31
{
	// Obligatorios
	var $TipoDeServicio;
    var $TipoDeTrafico;

    // opcionales
    var $NombreAseg;
    var $NumPolizaSeguro;
    var $Concesionario;

    var $DerechosDePaso = [];
    var $Carro = [];

	var $xml_base;
	var $logger;

	function __construct($TipoDeServicio, $NombreAseg = null, $NumPolizaSeguro = null, $Concesionario = null, $TipoDeTrafico=null)
    {
		$this->xml_base = null;
        $this->TipoDeServicio = $TipoDeServicio;
        $this->NombreAseg = $NombreAseg;
        $this->NumPolizaSeguro = $NumPolizaSeguro;
        $this->Concesionario = $Concesionario;
        $this->TipoDeTrafico=$TipoDeTrafico;
        $this->DerechosDePaso = [];
        $this->Carro = [];

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoDeServicio','TipoDeTrafico'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteFerroviario validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.TransporteFerroviario Campo Requerido: ' . $field);
			}
		}

        if(count($this->Carro) == 0)
        {
            $this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteFerroviario validar(): Debe Contener por lo menos un elemento TransporteFerroviario.Carro");
            throw new Exception('Complemento.CartaPorte31.Mercancias.TransporteFerroviario. Debe Contener por lo menos un elemento TransporteFerroviario.Carro: ' . $field);
        }

		foreach($this->DerechosDePaso as $DerechosDePaso)
        {
			$DerechosDePaso->validar();
		}

        foreach($this->Carro as $Carro)
        {
			$Carro->validar();
        }
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoTransporteFerroviario = $this->xml_base->createElement("cartaporte31:TransporteFerroviario");

        $nodoTransporteFerroviario->setAttribute('TipoDeServicio',  $this->TipoDeServicio);
        $nodoTransporteFerroviario->setAttribute('TipoDeTrafico',  $this->TipoDeTrafico);
        if($this->NombreAseg)
            $nodoTransporteFerroviario->setAttribute('NombreAseg',  $this->NombreAseg);
        if($this->NumPolizaSeguro)
            $nodoTransporteFerroviario->setAttribute('NumPolizaSeguro',  $this->NumPolizaSeguro);
        if($this->Concesionario)
            $nodoTransporteFerroviario->setAttribute('Concesionario',  $this->Concesionario);

        if(!empty($this->DerechosDePaso))
        {
            foreach ($this->DerechosDePaso as $key => $DerechosDePaso) 
            {
                $DerechosDePaso->toXML();
                $nodoDerechosDePaso = $this->xml_base->importNode($DerechosDePaso->importXML(), true);
                $nodoTransporteFerroviario->appendChild($nodoDerechosDePaso);
            }
        }

        if(!empty($this->Carro))
        {
            foreach ($this->Carro as $key => $Carro) 
            {
                $Carro->toXML();
                $nodoCarro = $this->xml_base->importNode($Carro->importXML(), true);
                $nodoTransporteFerroviario->appendChild($nodoCarro);
            }
        }

		$this->xml_base->appendChild($nodoTransporteFerroviario);
    }

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:TransporteFerroviario")->item(0);
		return $xml;
	}

    function addDerechosDePaso($TipoDerechoDePaso, $KilometrajePagado)
    {
        $DerechosDePaso = new DerechosDePaso31(
            $TipoDerechoDePaso, 
            $KilometrajePagado
        );

        // $DerechosDePaso->validar();
        $this->DerechosDePaso[] = $DerechosDePaso; 
        return $DerechosDePaso;
    }

    function addCarro($TipoCarro, $MatriculaCarro, $GuiaCarro, $ToneladasNetasCarro)
    {
        $Carro = new Carro31(
            $TipoCarro, 
            $MatriculaCarro, 
            $GuiaCarro, 
            $ToneladasNetasCarro
        );

        // $Carro->validar();
        $this->Carro[] = $Carro; 
        return $Carro;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.TransporteFerroviario.DerechosDePaso

class DerechosDePaso31
{
	// Obligatorios
	var $TipoDerechoDePaso;
	var $KilometrajePagado;

	var $xml_base;
	var $logger;

	function __construct($TipoDerechoDePaso, $KilometrajePagado)
    {
		$this->xml_base = null;
        $this->TipoDerechoDePaso = $TipoDerechoDePaso;
        $this->KilometrajePagado = $KilometrajePagado;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoDerechoDePaso', 'KilometrajePagado'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteFerroviario.DerechosDePaso validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.TransporteFerroviario.DerechosDePaso Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoDerechosDePaso = $this->xml_base->createElement("cartaporte31:DerechosDePaso");

        $nodoDerechosDePaso->setAttribute('TipoDerechoDePaso',  $this->TipoDerechoDePaso);
        $nodoDerechosDePaso->setAttribute('KilometrajePagado',  $this->KilometrajePagado);

		$this->xml_base->appendChild($nodoDerechosDePaso);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:DerechosDePaso")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.TransporteFerroviario.Carro

class Carro31
{
	// Obligatorios
	var $TipoCarro;
	var $MatriculaCarro;
	var $GuiaCarro;
	var $ToneladasNetasCarro;

    var $Contenedor = [];

	var $xml_base;
	var $logger;

	function __construct($TipoCarro, $MatriculaCarro, $GuiaCarro, $ToneladasNetasCarro)
    {
		$this->xml_base = null;
        $this->TipoCarro = $TipoCarro;
        $this->MatriculaCarro = $MatriculaCarro;
        $this->GuiaCarro = $GuiaCarro;
        $this->ToneladasNetasCarro = $ToneladasNetasCarro;

        $this->Contenedor = [];

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoCarro', 'MatriculaCarro', 'GuiaCarro', 'ToneladasNetasCarro'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteFerroviario.Carro validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.TransporteFerroviario.Carro Campo Requerido: ' . $field);
			}
		}

		foreach($this->Contenedor as $Contenedor)
        {
			$Contenedor->validar();
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoCarro = $this->xml_base->createElement("cartaporte31:Carro");

        $nodoCarro->setAttribute('TipoCarro',  $this->TipoCarro);
        $nodoCarro->setAttribute('MatriculaCarro',  $this->MatriculaCarro);
        $nodoCarro->setAttribute('GuiaCarro',  $this->GuiaCarro);
        $nodoCarro->setAttribute('ToneladasNetasCarro',  $this->ToneladasNetasCarro);

        if(!empty($this->Contenedor))
        {
            foreach ($this->Contenedor as $key => $Contenedor) 
            {
                $Contenedor->toXML();
                $nodoContenedor = $this->xml_base->importNode($Contenedor->importXML(), true);
                $nodoCarro->appendChild($nodoContenedor);
            }
        }

		$this->xml_base->appendChild($nodoCarro);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:Carro")->item(0);
		return $xml;
	}

    function addContenedor($TipoContenedor, $PesoContenedorVacio, $PesoNetoMercancia)
    {
        $Contenedor = new ContenedorCarro31(
            $TipoContenedor, 
            $PesoContenedorVacio, 
            $PesoNetoMercancia
        );

        // $Contenedor->validar();
        $this->Contenedor[] = $Contenedor; 
        return $Contenedor;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.Mercancias.TransporteFerroviario.Carro.Contenedor

class ContenedorCarro31
{
	// Obligatorios
	var $TipoContenedor;
	var $PesoContenedorVacio;
    var $PesoNetoMercancia;

	var $xml_base;
	var $logger;

	function __construct($TipoContenedor, $PesoContenedorVacio, $PesoNetoMercancia)
    {
		$this->xml_base = null;
        $this->TipoContenedor = $TipoContenedor;
        $this->PesoContenedorVacio = $PesoContenedorVacio;
        $this->PesoNetoMercancia = $PesoNetoMercancia;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoContenedor', 'PesoContenedorVacio', 'PesoNetoMercancia'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.Mercancias.TransporteFerroviario.Carro.Contenedor validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.Mercancias.TransporteFerroviario.Carro.Contenedor Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoContenedor = $this->xml_base->createElement("cartaporte31:Contenedor");

        $nodoContenedor->setAttribute('TipoContenedor',  $this->TipoContenedor);
        $nodoContenedor->setAttribute('PesoContenedorVacio',  $this->PesoContenedorVacio);
        $nodoContenedor->setAttribute('PesoNetoMercancia',  $this->PesoNetoMercancia);

		$this->xml_base->appendChild($nodoContenedor);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:Contenedor")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.FiguraTransporte.TiposFigura

class TiposFigura31
{
	// Obligatorios
	var $TipoFigura;

    // Opcionales
	var $RFCFigura;
    var $NumLicencia;
    var $NombreFigura;
    var $NumRegIdTribFigura;
    var $ResidenciaFiscalFigura;
    var $PartesTransporte = [];
    var $Domicilio = null;

	var $xml_base;
	var $logger;

	function __construct($TipoFigura, $RFCFigura = null, $NumLicencia = null, $NombreFigura = null, $NumRegIdTribFigura = null, $ResidenciaFiscalFigura = null)
    {
		$this->xml_base = null;
        $this->TipoFigura = $TipoFigura;
        $this->RFCFigura = $RFCFigura;
        $this->NumLicencia = $NumLicencia;
        $this->NombreFigura = $NombreFigura;
        $this->NumRegIdTribFigura = $NumRegIdTribFigura;
        $this->ResidenciaFiscalFigura = $ResidenciaFiscalFigura;
        $this->PartesTransporte = [];
        $this->Domicilio = null;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['TipoFigura'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.FiguraTransporte.TiposFigura validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.FiguraTransporte.TiposFigura Campo Requerido: ' . $field);
			}
		}

        if(!empty($this->PartesTransporte))
        {
            foreach ($this->PartesTransporte as $key => $PartesTransporte) 
			    $PartesTransporte->validar();
        }

        if($this->Domicilio)
            $this->Domicilio->validar();
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoTiposFigura = $this->xml_base->createElement("cartaporte31:TiposFigura");

        $nodoTiposFigura->setAttribute('TipoFigura',  $this->TipoFigura);
        if($this->RFCFigura)
            $nodoTiposFigura->setAttribute('RFCFigura',  $this->RFCFigura);
        if($this->NumLicencia)
            $nodoTiposFigura->setAttribute('NumLicencia',  $this->NumLicencia);
        if($this->NombreFigura)
            $nodoTiposFigura->setAttribute('NombreFigura',  $this->NombreFigura);
        if($this->NumRegIdTribFigura)
            $nodoTiposFigura->setAttribute('NumRegIdTribFigura',  $this->NumRegIdTribFigura);
        if($this->ResidenciaFiscalFigura)
            $nodoTiposFigura->setAttribute('ResidenciaFiscalFigura',  $this->ResidenciaFiscalFigura);

        if(!empty($this->PartesTransporte))
        {
            foreach ($this->PartesTransporte as $key => $PartesTransporte) 
            {
                $PartesTransporte->toXML();
                $nodoPartesTransporte = $this->xml_base->importNode($PartesTransporte->importXML(), true);
                $nodoTiposFigura->appendChild($nodoPartesTransporte);
            }
        }

        if(!empty($this->Domicilio))
        {
            $this->Domicilio->toXML();
            $nodoDomicilio = $this->xml_base->importNode($this->Domicilio->importXML(), true);
            $nodoTiposFigura->appendChild($nodoDomicilio);
        }

		$this->xml_base->appendChild($nodoTiposFigura);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:TiposFigura")->item(0);
		return $xml;
	}

    function addPartesTransporte($ParteTransporte)
    {
        $PartesTransporte = new PartesTransporte31(
            $ParteTransporte
        );
        // $PartesTransporte->validar();
        $this->PartesTransporte[] = $PartesTransporte;
        return $PartesTransporte;
    }

    function addDomicilio($Estado, $Pais, $CodigoPostal, $Calle = null, $NumeroExterior = null, $NumeroInterior = null, 
                        $Colonia = null, $Localidad = null, $Referencia = null, $Municipio = null)
    {
        $Domicilio = new Domicilio31(
            $Estado, 
            $Pais, 
            $CodigoPostal, 
            $Calle, 
            $NumeroExterior, 
            $NumeroInterior, 
            $Colonia, 
            $Localidad, 
            $Referencia, 
            $Municipio
        );
        // $Domicilio->validar();
        $this->Domicilio = $Domicilio;
        return $Domicilio;
    }
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.FiguraTransporte.TiposFigura.PartesTransporte

class PartesTransporte31
{
    // Obligatorios
    var $ParteTransporte;

	var $xml_base;
	var $logger;

	function __construct($ParteTransporte)
    {
		$this->xml_base = null;
        $this->ParteTransporte = $ParteTransporte;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['ParteTransporte'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.FiguraTransporte.TiposFigura.PartesTransporte validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.FiguraTransporte.TiposFigura.PartesTransporte Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoPartesTransporte = $this->xml_base->createElement("cartaporte31:PartesTransporte");

        $nodoPartesTransporte->setAttribute('ParteTransporte',  $this->ParteTransporte);

		$this->xml_base->appendChild($nodoPartesTransporte);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:PartesTransporte")->item(0);
		return $xml;
	}
}

//--------------------------------------------------------------------------------------------------
// clase que crea el nodo de CartaPorte31.RegimenesAduaneros.RegimenAduaneroCCP
class RegimenAduaneroCCP31
{
	// Obligatorios
	var $RegimenAduanero;

	var $xml_base;
	var $logger;

	function __construct($RegimenAduanero)
    {
		$this->xml_base = null;
        $this->RegimenAduanero = $RegimenAduanero;

	    $this->logger = new Logger(); //clase para escribir logs
	}

	function validar() 
    {
        $required = ['RegimenAduanero'];
		foreach ($required as $field) 
        {
			if (!isset($this->$field) || $this->$field === '') {
				$this->logger->write("Complemento.CartaPorte31.RegimenesAduaneros.RegimenAduaneroCCP validar(): Campo no puede estar vacio :" . print_r($field, true));
				throw new Exception('Complemento.CartaPorte31.RegimenesAduaneros.RegimenAduaneroCCP Campo Requerido: ' . $field);
			}
		}
	}

	function toXML()
    {
		$this->xml_base = new DOMDocument();
		$nodoRegimenAduaneroCCP = $this->xml_base->createElement("cartaporte31:RegimenAduaneroCCP");
        $nodoRegimenAduaneroCCP->setAttribute('RegimenAduanero',  $this->RegimenAduanero);
		$this->xml_base->appendChild($nodoRegimenAduaneroCCP);
	}

	function toStringXML() 
    {
		return $this->xml_base->saveXML();
	}

	function importXML() 
    {
		$xml = $this->xml_base->getElementsByTagName("cartaporte31:RegimenAduaneroCCP")->item(0);
		return $xml;
	}
}